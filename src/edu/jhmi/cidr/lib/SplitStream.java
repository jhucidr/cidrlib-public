package edu.jhmi.cidr.lib;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: </p>
 *
 * @author Michael Barnhart
 * @version 1.0
 */
public class SplitStream extends PrintStream {
    private final List<PrintStream> outs;
    private final String newLine;

    public SplitStream(PrintStream source, List<PrintStream> outs) {
        super(source);
        this.outs = outs;
        this.newLine = System.getProperty("line.separator");
    }

    @Override
    public void print(String string) {
        super.print(string);

        super.flush();
        for (PrintStream ps : outs) {
            ps.print(string);
            ps.print(newLine);
            ps.flush();
        }
    }

    public static void main(String[] args) throws Exception {
        List<PrintStream> outputStreams = new ArrayList<PrintStream>();
        outputStreams.add(new PrintStream(new FileOutputStream("C:\\LogFile.log")));
        SplitStream ss = new SplitStream(System.out, outputStreams);

        System.setOut(ss);
        System.setErr(ss);

        System.out.println("Printing to the screen and a file simultaneously 1.");
        System.out.println("Printing to the screen and a file simultaneously 2.");
        System.out.println("Printing to the screen and a file simultaneously 3.");
        System.out.println("Printing to the screen and a file simultaneously 4.");
    }
}
