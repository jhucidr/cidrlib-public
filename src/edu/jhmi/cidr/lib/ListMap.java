/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import com.google.common.base.Function;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * As an alternative, consider using guava's ArrayListMultiMap. The two classes
 * are very similar, but guava's is probably more tested.
 *
 * @author David Newcomer Feb 28, 2013
 */
public class ListMap<K, V> implements Iterable<Entry<K, List<V>>> {

    private final Map<K, List<V>> map = new HashMap<>();

    /**
     * Adds a key-value pair to the map. Duplicate key-value pairs do NOT
     * clobber one another.
     *
     * @param k
     * @param v
     */
    public void put(K k, V v) {
        if (false == map.containsKey(k)) {
            map.put(k, new ArrayList<V>());
        }
        map.get(k).add(v);
    }

    /**
     * Stores a collection of values with the same key.
     *
     * @param k
     * @param values
     */
    public void putAll(K k, Iterable<? extends V> values) {
        for (V v : values) {
            put(k, v);
        }
    }

    public void putAll(ListMap<K, V> toAdd) {
        map.putAll(toAdd.map);
    }

    public static <K, V> ListMap<K, V> union(ListMap<? extends K, V> map1, ListMap<? extends K, V> map2) {
        ListMap<K, V> result = new ListMap<K, V>();
        Map<K, List<V>> mapInstance1 = new HashMap<>(map1.map);
        Map<K, List<V>> mapInstance2 = new HashMap<>(map2.map);
        //Bug: putAll on some Map objects such as HashMap overwrite exisiting mappings
//        result.map.putAll(map1.map);
//        result.map.putAll(map2.map);

        for (Map.Entry<K, List<V>> en : mapInstance1.entrySet()) {
            result.putAll(en.getKey(), en.getValue());
        }

        for (Map.Entry<K, List<V>> en : mapInstance2.entrySet()) {
            result.putAll(en.getKey(), en.getValue());
        }

        return result;
    }

    /**
     * Removes the first occurrence of the specified key-value pair from this
     * map, if it is present (optional operation). If this list does not contain
     * the element, it is unchanged.
     *
     * @param k - key of entry to remove
     * @param v - value of entry to remove
     * @return true if a value was removed
     */
    public boolean remove(K k, V v) {
        if (map.containsKey(k)) {
            return map.get(k).remove(v);
        } else {
            return false;
        }
    }

    /**
     * Removes the mapping for a key from this map if it is present (optional
     * operation).
     *
     * @param k - key whose mapping is to be removed from the map
     * @return the previous value associated with key, or null if there was no
     * mapping for key.
     */
    public List<V> removeAll(K k) {
        return map.remove(k);
    }

    /**
     * Returns a copy of all values associated with a key in the map.
     *
     * @param k - key whose mapping is to be removed from the map
     * @return
     */
    public List<V> get(K k) {
        if (false == map.containsKey(k)) {
            return new ArrayList<V>();
        } else {
            return new ArrayList<V>(map.get(k));
        }
    }

    /**
     * Return all keys in the map containing value v;
     *
     * @param v
     * @return
     */
    public Set<K> getKeys(V v) {
        return filterKeys(new Finder(v)).keySet();
    }

    /**
     * Returns a defensive copy of the keys in the map.
     *
     * @return
     */
    public Set<K> keySet() {
        return new HashSet<K>(map.keySet());
    }

    /**
     * Returns a List of all Values in the Map. Note: There may be duplicates.
     *
     * @return
     */
    public List<V> values() {
        List<V> result = new ArrayList<V>();
        for (List<V> values : map.values()) {
            result.addAll(values);
        }
        return result;
    }

    /**
     * Returns a copy of the backing map.
     *
     * @return
     */
    public Map<K, List<V>> getMap() {
        return new HashMap<K, List<V>>(map);
    }

    public ListMap<K, V> filter(Function<Tuple<K, V>, Boolean> f) {
        ListMap<K, V> result = new ListMap<K, V>();
        for (K k : keySet()) {
            for (V v : get(k)) {
                if (f.apply(new Tuple<K, V>(k, v))) {
                    result.put(k, v);
                }
            }
        }
        return result;
    }

    /**
     * Returns all values which satisfy a predicate.
     *
     * @param op
     * @return
     */
    public ListMap<K, V> filterVal(Function<V, Boolean> op) {
        ListMap<K, V> result = new ListMap<K, V>();
        for (K k : keySet()) {
            result.putAll(filterVal(op, k));
        }
        return result;
    }

    /**
     * Returns all values associated with a key that satisfy a predicate.
     *
     * @param op
     * @param k
     * @return
     */
    public ListMap<K, V> filterVal(Function<V, Boolean> op, K k) {
        ListMap<K, V> result = new ListMap<K, V>();
        for (V v : get(k)) {
            if (op.apply(v)) {
                result.put(k, v);
            }
        }
        return result;
    }

    public <M> ListMap<K, M> map(Function<Tuple<K, V>, M> f) {
        ListMap<K, M> result = new ListMap<K, M>();
        for (Entry<K, List<V>> entry : map.entrySet()) {
            K k = entry.getKey();
            List<V> list = entry.getValue();
            for (V v : list) {
                result.put(k, f.apply(new Tuple<K, V>(k, v)));
            }
        }
        return result;
    }

    public <M> List<M> reduce(Function<Tuple<K, V>, M> f) {
        return map(f).values();
    }

    /**
     * Filters this map by retaining only keys satisfying a predicate.
     *
     * @param op
     * @return a new ListMap consisting only of those key value pairs of this
     * map where the key satisfies the predicate p.
     */
    public ListMap<K, V> filterKeys(Function<K, Boolean> op) {
        ListMap<K, V> result = new ListMap<K, V>();
        for (K k : map.keySet()) {
            if (op.apply(k)) {
                result.putAll(k, get(k));
            }
        }
        return result;
    }

    @Override
    public Iterator<Entry<K, List<V>>> iterator() {
        return map.entrySet().iterator();
    }

    /**
     * Retrieves all duplicate values found in the map associated with any key.
     * If a value V has been added once to keys K_1 and K_2, then it will be
     * returned as a duplicate. A value V added twice to K_1 will also be
     * returned as a duplicate.
     *
     * This method does not indicate which key(s) had each value as a duplicate,
     * but that can easily be determined via a call to getKeys(V v).
     *
     * @return
     */
    public Set<V> findDuplicates() {
        return new HashSet<V>(filterVal(new DuplicateFinder()).values());
    }

    /**
     * Retrieves the duplicate values mapped to a specific key.
     *
     * @param k
     * @return
     */
    public Set<V> findDuplicates(K k) {
        return new HashSet<V>(filterVal(new DuplicateFinder(), k).values());
    }

    @Override
    public String toString() {
        return map.toString();
    }

    private class Finder implements Function<K, Boolean> {

        private final V v;

        public Finder(V k) {
            this.v = k;
        }

        @Override
        public Boolean apply(K k) {
            return get(k).contains(v);
        }
    }

    private class DuplicateFinder implements Function<V, Boolean> {

        private final Set<V> found = new HashSet<V>();

        @Override
        public Boolean apply(V input) {
            return !found.add(input);
        }
    }
}