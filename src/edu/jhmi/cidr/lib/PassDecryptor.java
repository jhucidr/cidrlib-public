package edu.jhmi.cidr.lib;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author Michael Barnhart
 * @version 1.0
 */
public class PassDecryptor {

    //Add your own
    private static final String EMAIL = "";
    private static final String SELECT_ONLY = "";
    private static final String SELECTALL = "";

    //Add your own key
    private static byte[] getKey() {
        return new byte[]{};
    }

    private static String decrypt(String toDecrypt, SecretKey key) {
        try {
            Cipher dcipher = Cipher.getInstance("DES");
            
            dcipher.init(Cipher.DECRYPT_MODE, key);
            byte[] decA = new Base64().decode(toDecrypt.getBytes());
            
            return new String(dcipher.doFinal(decA), "UTF8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getCidrEmailPass() {
        SecretKeySpec key = new SecretKeySpec(getKey(), "DES");
        return decrypt(EMAIL, key);
    }

    public static String getSelectOnlyPass() {
        SecretKeySpec key = new SecretKeySpec(getKey(), "DES");
        return decrypt(SELECT_ONLY, key);
    }

    public static String getSelectAllPass() {
        SecretKeySpec key = new SecretKeySpec(getKey(), "DES");
        return decrypt(SELECTALL, key);
    }
}
