/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import java.util.Collection;

/**
 * Jan 12, 2012
 */
public class HtmlUtils {

    public static StringBuilder buildHtmlList(Collection<?> collection, boolean ordered, int limit) {
        if (collection == null) {
            throw new NullPointerException("collection must not be null");
        }
        if (collection.isEmpty()) {
            return new StringBuilder("[no items found]");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(ordered ? "<ol>" : "<ul>");
        int i = 0;
        for (Object object : collection) {
            i++;
            if (limit < 0 || i <= limit) {
                sb.append("<li>");
                if (object instanceof HtmlBuilder) {
                    HtmlBuilder h = (HtmlBuilder) object;
                    sb.append(h.sb);
                } else {
                    sb.append(Utils.display(object));
                }
                sb.append("</li>");
            } else {
                sb.append("<li>").append((collection.size() - i)).append(" more...</li>");
                break;
            }
        }
        sb.append(ordered ? "</ol>" : "</ul>");
        return sb;
    }

    public static String buildOrderredHtmlList(Collection<?> collection) {
        if (collection == null) {
            throw new NullPointerException("collection must not be null");
        }
        return buildHtmlList(collection, true, -1).toString();
    }

    public static String buildUnorderredHtmlList(Collection<?> collection) {
        if (collection == null) {
            throw new NullPointerException("collection must not be null");
        }
        return buildHtmlList(collection, false, -1).toString();
    }

    public static class HtmlBuilder {

        private final StringBuilder sb;
        private static final int DEFAULT_LIMITED_LIST_NUMBER = 15;
        public static final String BREAK = "<br>";

        public HtmlBuilder(String text) {
            this.sb = new StringBuilder(text);
        }

        public HtmlBuilder(HtmlBuilder html) {
            this.sb = new StringBuilder(html.sb);
        }

        public HtmlBuilder() {
            this.sb = new StringBuilder();
        }

        public HtmlBuilder append(Object o) {
            sb.append(Utils.display(o));
            return this;
        }

        public HtmlBuilder append(HtmlBuilder h) {
            append(h.sb);
            return this;
        }

        public HtmlBuilder newLine() {
            append(BREAK);
            return this;
        }

        public HtmlBuilder append(Collection<?> collection, boolean ordered) {
            append(collection, ordered, -1);
            return this;
        }

        public HtmlBuilder append(Collection<?> collection, boolean ordered, int limit) {
            append(buildHtmlList(collection, ordered, limit));
            return this;
        }

        //Default implementation: Unorderred
        public HtmlBuilder append(Collection<?> collection) {
            append(collection, false);
            return this;
        }

        public HtmlBuilder appendLimited(Collection<?> collection) {
            append(collection, false, DEFAULT_LIMITED_LIST_NUMBER);
            return this;
        }

        public HtmlBuilder append(Collection<?> collection, int limit) {
            append(collection, false, limit);
            return this;
        }

        public HtmlBuilder appendBold(Object o) {
            append("<b>");
            append(o);
            append("</b>");
            return this;
        }

        public String toPlainText() {
            String result = sb.toString();
            result = result.replaceAll("<ol>.*?<li>", " (").replaceAll("</ol>", ") ");
            result = result.replaceAll("<ul>.*?<li>", " (").replaceAll("</ul>", ") ");
            result = result.replaceAll("<li>", ", ").replaceAll("</li>", "");
            result = result.replaceAll("<br>", System.getProperty("line.separator"));
            result = result.replaceAll("<b>", "").replaceAll("</b>", "");
            result = result.replaceAll("<pre>", "").replaceAll("</pre>", "");
            result = removeLeadingAndTrailingHtmlTags(result);
            return result;
        }

        @Override
        public String toString() {
            StringBuilder sbToo = new StringBuilder();
            sbToo.append("<html>");
            sbToo.append(removeNewLinesAndReplaceWithLineBreaks(removeLeadingAndTrailingHtmlTags(sb.toString())));
            sbToo.append("</html>");
            return sbToo.toString();
        }

        private static String removeNewLinesAndReplaceWithLineBreaks(String contents) {
            return contents.replaceAll("\r\n", "<br>").replaceAll("\n", "<br>");
        }

        //Prevents repeat <html> tags when using multiple HtmlBuilders.
        private static String removeLeadingAndTrailingHtmlTags(String contents) {
            return contents.replaceAll("<html>", "").replaceAll("</html>", "");
        }
    }
}
