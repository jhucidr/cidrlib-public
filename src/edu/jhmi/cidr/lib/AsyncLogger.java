/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author cidr
 */
public class AsyncLogger {

    private final String pathToLogFile;
    private final BufferedWriter out;
    private final ExecutorService logService;
    
    public AsyncLogger(String pathToLogFile, boolean append) throws IOException {
        if (pathToLogFile == null) {
            throw new NullPointerException("The path to the log file must not be null");
        }
        File f = new File(pathToLogFile);
        if (f.exists()) {
            if (f.canWrite() == false) {
                throw new IllegalArgumentException("Unable to write to file: " + f.getAbsolutePath());
            }
        }
        this.pathToLogFile = pathToLogFile;
        out = new BufferedWriter(new FileWriter(f, append));
        this.logService = Executors.newSingleThreadExecutor();
    }

    public String getPathToLogFile() {
        return pathToLogFile;
    }

    public void logMessage(String msg, String id, Severity severity) {
        if (msg == null) {
            throw new NullPointerException("msg must not be null");
        }
        if (id == null) {
            throw new NullPointerException("id must not be null");
        }
        if (severity == null) {
            throw new NullPointerException("severity must not be null");
        }
        logService.submit(new LogTask(msg, id, severity, out));
    }

    public void shutDown() throws InterruptedException, IOException {
        logService.shutdown();
        logService.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        if (out != null) {
            out.close();
        }
    }

    private static final class LogTask implements Callable<Void> {

        private final String msgToWrite;
        private final BufferedWriter out;
        private static final String CRLF = "\r\n";
        private static final String LF = "\n";
        private static final String DELIMITER = "|";

        public LogTask(String msg, String taskId, Severity severity, BufferedWriter writer) {
            StringBuilder sb = new StringBuilder();
            sb.append(severity.toString());
            sb.append(DELIMITER);
            sb.append("[");
            sb.append(new Date().toString());
            sb.append("]");
            sb.append(DELIMITER);
            sb.append("[");
            sb.append(taskId);
            sb.append("]");
            sb.append(DELIMITER);
            sb.append("[");
            sb.append(msg.replaceAll(CRLF, " ").replaceAll(LF, " "));
            sb.append("]");
            sb.append(System.getProperty("line.separator"));
            this.msgToWrite = sb.toString();
            this.out = writer;
        }

        public Void call() throws Exception {
            out.write(msgToWrite);
            out.flush();
            return null;
        }
    }

    public static enum Severity {

        INFO("[Info]"),
        WARNING("[Warning]"),
        ERROR_RECOVERABLE("[Recoverable Error]"),
        ERROR_FATAL("[Fatal Error]"),
        UNKNOWN("[Severity Unknown]"),;
        private final String s;

        private Severity(String s) {
            this.s = s;
        }

        @Override
        public String toString() {
            return s;
        }
    }
}
