/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import edu.jhmi.cidr.lib.Utils;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is abstract since subclassing is required for providing easy access
 * to enclosing class members.
 * 
 * Example usage in a toString() implementation:
 * 
 * @Override
 * public String toString() {
 *    return new Dumper(this){}.dump();
 * }
 * 
 * Nov 13, 2013
 * @author dleary
 */
public abstract class Dumper {
    private final Object toDump;

    public Dumper(Object toDump) {
        this.toDump = toDump;
    }

    public String dump() {
        Field[] fields = this.getClass().getEnclosingClass().getDeclaredFields();
        Map<String, String> dumpMap = new HashMap<>();
        for (Field field : fields) {
            String fieldName = field.getName();
            String fieldValue;
            if (false == field.isAccessible()) {
                field.setAccessible(true);
            }
            try {
                //fieldValue = String.valueOf(field.get(toDump));
                fieldValue = Utils.display(field.get(toDump));
            } catch (IllegalArgumentException ex) {
                fieldValue = "ERROR";
            } catch (IllegalAccessException ex) {
                fieldValue = "ILLEGAL-ACCESS";
            }
            dumpMap.put(fieldName, fieldValue);
        }
        return Utils.display(dumpMap);
    }

}
