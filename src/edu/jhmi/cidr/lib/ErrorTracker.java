/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import com.google.common.base.*;
import edu.jhmi.cidr.lib.HtmlUtils.HtmlBuilder;
import edu.jhmi.cidr.lib.file.FileUtils;
import edu.jhmi.cidr.lib.gui.GuiUtils;
import edu.jhmi.cidr.lib.gui.GuiUtils.ErrorMessage;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author David Newcomer Nov 13, 2012
 */
public class ErrorTracker<T> implements Iterable<T> {

    private final Map<T, List<String>> errorMap = new LinkedHashMap<T, List<String>>();
    private final String delimiter;
    private static final String SEMI_COLON = ";";

    public ErrorTracker() {
        this.delimiter = FileUtils.CSV_DELIMITER;
    }

    public ErrorTracker(String delimiter) {
        if (delimiter == null) {
            throw new NullPointerException("delimiter must not be null");
        }
        if (delimiter.equals(SEMI_COLON)) {
            throw new IllegalArgumentException("Delimiter must not be a semi-colon.");
        }
        this.delimiter = delimiter;
    }

    public String buildErrorReport() {
        return buildErrorReport(new DefaultReporter());
    }

    public String buildErrorReport(Function<T, String> fun) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<T, List<String>> entry : errorMap.entrySet()) {
            T t = entry.getKey();
            List<String> list = entry.getValue();
            String result = fun.apply(t);
            for (String string : list) {
                sb.append(result);
                sb.append(delimiter);
                sb.append(string);
                sb.append(Constants.NEW_LINE);
            }
        }
        return sb.toString();
    }

    public Map<T, List<String>> getErrorMap() {
        return Collections.unmodifiableMap(errorMap);
    }

    public void addError(T unit, String error) {
        if (false == errorMap.containsKey(unit)) {
            errorMap.put(unit, new ArrayList<String>());
        }
        errorMap.get(unit).add(error.replaceAll(delimiter, SEMI_COLON));
    }

    public List<T> getErrorFreeItems(Collection<T> items) {
        List<T> retVal = new ArrayList<T>();
        for (T item : items) {
            if (false == errorMap.containsKey(item)) {
                retVal.add(item);
            }
        }
        return retVal;
    }

    @Override
    public Iterator<T> iterator() {
        return errorMap.keySet().iterator();
    }

    public boolean hasErrors(T t) {
        return getErrors(t).isEmpty() ? false : true;
    }

    public List<String> getErrors(T t) {
        if (errorMap.containsKey(t)) {
            return errorMap.get(t);
        } else {
            return new ArrayList<String>();
        }
    }

    public String toHtml() {
        return toHtml(new DefaultReporter());
    }

    public String toHtml(Function<T, String> fun) {
        HtmlBuilder html = new HtmlBuilder();
        for (T t : this) {
            html.append(fun.apply(t));
            html.append(getErrors(t));
        }
        return html.toString();
    }

    @Override
    public String toString() {
        return new HtmlBuilder().append(toHtml()).toPlainText();
    }

    public boolean isEmpty() {
        return errorMap.isEmpty();
    }

    public ErrorMessage getErrorMessage(String header) {
        return new GuiUtils.ErrorMessage(toHtml(new DefaultReporter()), header);
    }

    public ErrorMessage getErrorMessage(String header, Function<T, String> fun) {
        return new GuiUtils.ErrorMessage(toHtml(fun), header);
    }

    private class DefaultReporter implements Function<T, String> {

        @Override
        public String apply(T t) {
            return Utils.display(t);
        }
    }
}
