/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import com.google.common.base.Optional;
import java.awt.Component;
import javax.swing.JOptionPane;

/**
 *
 * @author David Newcomer Oct 17, 2012
 */
public class Choices {

    private Choices() {
    }
    public static final Choice YES = Yes.INSTANCE;
    public static final Choice NO = No.INSTANCE;

    public static Choice fromBoolean(boolean reportSuccessAndDisplayOutput) {
        if (reportSuccessAndDisplayOutput) {
            return YES;
        } else {
            return NO;
        }
    }

    public static Choice Prompt(String message) {
        return new Prompt(message);
    }

    public static interface Choice {

        public boolean isYes();
    }

    private static class Yes implements Choice {

        public static final Yes INSTANCE = new Yes();

        private Yes() {
        }

        @Override
        public boolean isYes() {
            return true;
        }
    }

    private static class No implements Choice {

        public static final No INSTANCE = new No();

        private No() {
        }

        @Override
        public boolean isYes() {
            return false;
        }
    }

    private static class Prompt implements Choice {

        private final String message;
        private boolean doPrompt = true;
        private boolean isYes = false;
        private final Optional<Component> component;

        private Prompt(String message) {
            if (message == null) {
                throw new NullPointerException("message must not be null");
            }
            this.message = message;
            this.component = Optional.absent();
        }

        private Prompt(Component c, String message) {
            if (message == null) {
                throw new NullPointerException("message must not be null");
            }
            this.message = message;
            this.component = Optional.of(c);
        }

        @Override // synchronized added 11/13/12
        public synchronized boolean isYes() { // if prompt() is synchronized this must be! In order to protect shared mutable state!
            return doPrompt ? prompt() : isYes;
        }

        private synchronized boolean prompt() {
            if (doPrompt) {
                Component c = component.isPresent() ? component.get() : Application.getComponentOrNull();
                int selection = JOptionPane.showConfirmDialog(c,//
                        message,
                        "", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (JOptionPane.YES_OPTION == selection) {
                    isYes = true;
                } else {
                    isYes = false;
                }
                doPrompt = false;
            }
            return isYes;
        }
    }
}
