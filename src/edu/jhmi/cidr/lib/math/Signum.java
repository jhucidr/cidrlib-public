/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.math;

import java.util.Date;

/**
 *
 * @author David Newcomer Aug 18, 2011 March 13, 2012: Modified to prevent
 * oh-no-verflow
 */
public class Signum {

    private Signum() {
    }

    public static int getSign(long a, long b) {
        if (a > b) {
            return 1;
        } else if (a < b) {
            return -1;
        } else if (a == b) {
            return 0;
        } else {
            throw new AssertionError("Invalid longs: " + a + ", " + b);
        }
    }

    public static int getSign(Date a, Date b) {
        if (a == b) {
            //If they are both null or same reference, returns 0.
            return 0;
        } else if (a != null && b != null) {
            return getSign(a.getTime(), b.getTime());
        } else if (a == null) {
            return 1;
        } else if (b == null) {
            return -1;
        } else {
            throw new AssertionError("Someone's made a logic error...: " + a + ", " + b);
        }
    }
    
    public static int getSign(double a, double b) {
//        if (Double.isNaN(a)) {
//            a = Double.MAX_VALUE;
//        }
//        if (Double.isNaN(b)) {
//            b = Double.MAX_VALUE;
//        }
        if (a > b) {
            return 1;
        } else if (a < b) {
            return -1;
        } else if (a == b) {
            return 0;
        } else {
            throw new IllegalArgumentException("Invalid double(s): " + a + ", " + b);
        }
    }

    public static int getSign(long difference) {
        return difference > 0L ? 1 : (difference < 0L ? -1 : 0);
    }
    public static void main(String[] args) {
        System.out.println(Double.MIN_VALUE < 0.0d);
        System.out.println(Double.MIN_VALUE > 0.0d);
        System.out.println(Double.MIN_VALUE == Double.MIN_VALUE);
        System.out.println(Double.MIN_VALUE == Double.MAX_VALUE);
        System.out.println(Double.MIN_VALUE == Double.MAX_VALUE);
        System.out.println(Double.MIN_VALUE == 0.0d);
        System.out.println(getSign(Double.MIN_VALUE));
    }
    public static int getSign(double difference) {
        if (Double.isNaN(difference)) {
            throw new IllegalArgumentException("Not a number");
        }
        return difference > 0.0d ? 1 : (difference < 0.0d ? -1 : 0);
    }

    public static int getSign(String a, String b) {
        if (a == b) {
            return 0;
        } else if (a != null && b != null) {
            return getSign(a.compareToIgnoreCase(b));
        } else if (a == null) {
            return 1;
        } else if (b == null) {
            return -1;
        } else {
            throw new AssertionError("Someone's made a logic error...: " + a + ", " + b);
        }
    }
}
