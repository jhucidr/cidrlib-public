/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.math;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This is very similar to FrequencyMap, but also a little different. Perhaps
 * the functionality of these two classes should be combined?
 *
 * @author David Newcomer
 */
public class Counter<T> {

    private final Map<T, Long> counterMap = new HashMap<>();

    public Counter<T> addAll(Counter<T> toAdd) {
        for (Map.Entry<T, Long> entry : toAdd.counterMap.entrySet()) {
            T t = entry.getKey();
            Long long1 = entry.getValue();
            addMany(t, long1);
        }
        return this;
    }

    public long add(T t) {
        return addMany(t, 1);
    }

    public long addMany(T t, long quantity) {
        if (false == counterMap.containsKey(t)) {
            counterMap.put(t, 0L);
        }
        long newValue = counterMap.get(t) + quantity;
        counterMap.put(t, newValue);
        return newValue;
    }

    public long subtract(T t) {
        return subtractMany(t, 1);
    }

    public long subtractMany(T t, long quantity) {
        if (false == counterMap.containsKey(t)) {
            counterMap.put(t, 0L);
        }
        long newValue = counterMap.get(t) - quantity;
        counterMap.put(t, newValue);
        return newValue;
    }

    public Set<T> getKeys() {
        return new HashSet<>(counterMap.keySet());
    }

    public long getCount(T t) {
        Long count = counterMap.get(t);
        return count == null ? 0 : count;
    }

    public double getPercentageOfTotal(T t) {
        return (double) getCount(t) / (double) getTotalCount();
    }

    public long getTotalCount() {
        long result = 0;
        for (Map.Entry<T, Long> entry : counterMap.entrySet()) {
            result += entry.getValue();
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        for (Map.Entry<T, Long> entry : counterMap.entrySet()) {
            T k = entry.getKey();
            Long count = entry.getValue();
            output.append(k).append("\t").append(count).append("\n");
        }
        return output.toString();
    }
}
