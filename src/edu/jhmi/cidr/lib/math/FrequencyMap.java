/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.math;

import edu.jhmi.cidr.lib.Utils;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class FrequencyMap<T extends Comparable<T>> {

    private final Map<T, Long> map;
    private long countTotal = 0;

    public FrequencyMap() {
        /*
         * Since this relies on a TreeMap, T must be comparable
         */
        map = new HashMap<T, Long>();
    }

    public Set<T> getCategories() {
        return map.keySet();
    }

    public long getCountTotal() {
        return countTotal;
    }

    public Long getCount(T category) {
        return map.get(category);
    }

    public double getPercentageOfTotal(T category) {
        if (getCountTotal() == 0) {
            // Avoid the div/0
            return 0.0d;
        }
        return ((double) getCount(category)) / ((double) getCountTotal());
    }

    public String getPercentageOfTotalAsFormattedString(T category) {
        return Utils.formatDoubleAsPercentage(getPercentageOfTotal(category) * 100d, 2);
    }

    public void incrementCountForCategory(T category) {
        countTotal++;
        if (map.containsKey(category)) {
            Long count = map.get(category);
            count = count + 1;
            map.put(category, count);
        } else {
            map.put(category, 1L);
        }
    }

    public void incrementCountForCategory(T category, long increment) {
        countTotal += increment;
        if (map.containsKey(category)) {
            Long count = map.get(category);
            count = count + increment;
            map.put(category, count);
        } else {
            map.put(category, increment);
        }
    }

    public Set<T> getDuplicatedCategories() {
        Set<T> result = new HashSet<T>();
        for (Map.Entry<T, Long> entry : map.entrySet()) {
            T category = entry.getKey();
            Long count = entry.getValue();
            if (count != null && count > 1) {
                result.add(category);
            }
        }
        return result;
    }
    
}
