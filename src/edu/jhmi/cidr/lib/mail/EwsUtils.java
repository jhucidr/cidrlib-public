/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.mail;

import edu.jhmi.cidr.lib.PassDecryptor;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import microsoft.exchange.webservices.data.*;

/**
 *
 * @author cidr
 */
public class EwsUtils {

    public static final String EXCHANGE_SERVER_URL = "";
    public static final String LOGIN_NOTIFICATION_EMAIL_ADDRESS = "";

    /**
     * This method opens an input dialog and prompts the user to input their
     * JHED credentials. Returns a Credentials instance upon the user clicking
     * "OK," and returns null if the user cancels the dialog. The caller is
     * responsible for checking the validity of the user-supplied username
     * String and password array.
     *
     * @return Credentials
     */
    public static Credentials promptUserForCredentials() {
        JLabel usernameLabel = new JLabel("JHED ID:");
        JTextField userNameField = new JTextField();
        JLabel passwordLabel = new JLabel("Password:");
        JPasswordField password = new JPasswordField();
        Object[] ra = {usernameLabel, userNameField, passwordLabel, password};
        userNameField.requestFocusInWindow();
        int result = JOptionPane.showConfirmDialog(
                null,
                ra,
                "JHED Login",
                JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            return new Credentials(userNameField.getText(), password.getPassword());
        } else {
            return null;
        }
    }

    public static MailConfiguration getCidrSoftwareMailConfiguration() {
        //Add your own credentials
        return new MailConfiguration(new Credentials("", PassDecryptor.getCidrEmailPass().toCharArray()));
    }

    public static class MailConfiguration implements Serializable {

        private final Credentials credentials;
        private final List<String> recipients;
        private String subject;
        private String messageBody;
        private boolean plainText = false;
        private static final long serialVersionUID = 42L;

        public MailConfiguration(Credentials credentials) {
            if (credentials == null) {
                throw new NullPointerException("Credentials must not be null.");
            }
            this.credentials = credentials;
            this.recipients = new ArrayList<>();
        }

        @Override
        public String toString() {
            return String.format("{receipients=%s, subject=%s, messageBody=%s}", recipients.toString(), subject, messageBody.toString());
        }

        public Credentials getCredentials() {
            return credentials;
        }

        /**
         * Only add usernames here. The e-mail addresses will be automatically
         * constructed.
         *
         * @param recipient
         * @return
         */
        public MailConfiguration addRecipient(String recipient) {
            if (recipient == null) {
                throw new NullPointerException("recipient must not be null");
            }
            recipients.add(recipient);
            return this;
        }

        public MailConfiguration addRecipients(List<String> recipients) {
            if (recipients == null) {
                throw new NullPointerException("recipients must not be null");
            }
            for (String recipient : recipients) {
                addRecipient(recipient);
            }
            return this;
        }

        public List<String> getRecipients() {
            return recipients;
        }

        public String getSubject() {
            return subject;
        }

        public MailConfiguration setSubject(String subject) {
            if (subject == null) {
                throw new NullPointerException("subject must not be null");
            }
            this.subject = subject;
            return this;
        }

        public String getMessageBody() {
            return messageBody;
        }

        public MailConfiguration setMessageBody(String messageBody) {
            if (messageBody == null) {
                throw new NullPointerException("messageBody must not be null");
            }
            this.messageBody = messageBody;
            return this;
        }

        public void setPlainText(boolean plainText) {
            this.plainText = plainText;
        }

        public boolean isPlainText() {
            return plainText;
        }

    }

    public static void sendEmail(String recipient, String subject, String messageBody, boolean plainText) throws Exception {
        MailConfiguration configuration = EwsUtils.getCidrSoftwareMailConfiguration();
        configuration.setMessageBody(messageBody);
        configuration.setSubject(subject);
        configuration.addRecipient(recipient);
        configuration.setPlainText(plainText);
        sendEmail(configuration);
    }

    public static void sendEmail(String recipient, String subject, String messageBody) throws Exception {
        MailConfiguration configuration = EwsUtils.getCidrSoftwareMailConfiguration();
        configuration.setMessageBody(messageBody);
        configuration.setSubject(subject);
        configuration.addRecipient(recipient);
        sendEmail(configuration);
    }

    public static void sendEmail(MailConfiguration conf) throws Exception {
        if (conf == null) {
            throw new NullPointerException("E-mail configuration must not be null.");
        }
        new EmailMessageWithRetry(conf).send();
    }

    public static class EmailMessageWithRetry {

        private final MailConfiguration conf;
        private int numAttempts = 0;
        //
        private static final int MAX_NUM_ATTEMPTS = 30;
        private static final int RETRY_DELAY = 1000;

        private EmailMessageWithRetry(MailConfiguration conf) {
            this.conf = conf;
        }

        public void send() throws Exception {
            System.out.println("conf = " + conf);
            System.out.println("Send method");
            try {
                prepareMessageAndSend();
            } catch (Exception ex) {
                ex.printStackTrace();
                retry(ex);
            }
        }

        private void retry(Exception ex) throws Exception {
            numAttempts++;
            Thread.sleep(RETRY_DELAY);
            if (numAttempts > MAX_NUM_ATTEMPTS) {
                throw new IllegalStateException("max num attempts exceeded: " + MAX_NUM_ATTEMPTS, ex);
            } else {
                System.out.println("attempt #" + (numAttempts + 1) + " to send email " + conf.hashCode());
                send();
            }
        }

        private void prepareMessageAndSend() throws Exception {
            EmailMessage msg = new EmailMessage(initService());
            msg.setSubject(conf.getSubject());
            msg.setBody(MessageBody.getMessageBodyFromText(conf.getMessageBody()));
            if (conf.isPlainText()) {
                msg.getBody().setBodyType(BodyType.Text);
            }
            for (String recipient : conf.getRecipients()) {
                msg.getToRecipients().add(recipient);
            }
            msg.send();
            System.out.println("Email Sent");
        }

        private ExchangeService initService() throws URISyntaxException {
            ExchangeService service = new ExchangeService();
            ExchangeCredentials credentials = new WebCredentials(//
                    conf.getCredentials().getUsername(),//
                    new String(conf.getCredentials().getPassword()));
            service.setCredentials(credentials);
            service.setUrl(new URI(EXCHANGE_SERVER_URL));
            return service;
        }
    }

    public static Credentials jhedLogin() throws Exception {
        return jhedLogin(promptUserForCredentials());
    }

    public static Credentials jhedLogin(Credentials cred) throws Exception {

        if (cred != null) {
            MailConfiguration conf = new MailConfiguration(cred);
            conf.setSubject("JHED Login");
            conf.setMessageBody("User: " //
                    + cred.getUsername() //
                    + " has logged into JHED");
            conf.addRecipient(LOGIN_NOTIFICATION_EMAIL_ADDRESS);
            System.out.println("Trying to send email");
            new EmailMessageWithRetry(conf).send();
        }
        return cred;
    }

    public static class Credentials implements Serializable {

        private final String username;
        private final transient char[] password;
        private static final long serialVersionUID = 42L;

        public Credentials(String username, char[] password) {
            if (username == null) {
                throw new NullPointerException("Username must not be null");
            }
            if (password == null) {
                throw new NullPointerException("Password must not be null");
            }
            this.username = username.trim();
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        // This is private so that we limit access to the password
        private char[] getPassword() {
            return password;
        }

        /**
         * Only call this method if you are using an instance of this class
         * *solely* for authentication.
         */
        public void clearPassword() {
            if (password != null) {
                for (int i = 0; i < password.length; i++) {
                    password[i] = '0';
                }
            }
        }
    }

    private EwsUtils() {
        // No instantiate me!
    }
    
}
