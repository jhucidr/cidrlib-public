/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Utility class that copies an {@link InputStream} to an {@link OutputStream} Please note that this class
 * is NOT thread safe: i.e., no synchronization is performed on either the supplied {@link InputStream} or
 * {@link OutputStream}.
 * @author cidr
 */
public class StreamCopier {

    private static final int ONE_MEBIBYTE = 1024 * 1024;
    private static final int DEFAULT_BUFFER_SIZE = 1024 * 8;

    /**
     * Copies the contents of an {@link InputStream}
     * to an {@link OutputStream} using the specified buffer size. Developers calling this method should
     * pay particular attention to the fact that this method is <em>NOT</em> thread-safe
     * @param in InputStream
     * @param out OutputStream
     * @param bufferSize int
     * @throws java.io.IOException
     */
    public static void copy(InputStream in, OutputStream out, int bufferSize) throws IOException {
        if (in == null) {
            throw new NullPointerException("InputStream must not be null");
        }
        if (out == null) {
            throw new NullPointerException("OutputStream must not be null");
        }
        if (bufferSize < 1 || bufferSize > ONE_MEBIBYTE) {
            throw new IllegalArgumentException("Buffer size must be between 1 and " + ONE_MEBIBYTE + " bytes.");
        }
        byte[] buffer = new byte[bufferSize];
        int numBytes = -1;
        while ((numBytes = in.read(buffer)) != -1) {
            out.write(buffer, 0, numBytes);
        }
        out.flush();
    }

    /**
     * Copies the contents of an {@link InputStream}
     * to an {@link OutputStream} using a buffer size of 8 KiB.
     * @param in InputStream
     * @param out OutputStream
     * @throws java.io.IOException
     */
    public static void copy(InputStream in, OutputStream out) throws IOException {
        copy(in, out, DEFAULT_BUFFER_SIZE);
    }

    private StreamCopier() {/*No Instantiation*/

    }
}