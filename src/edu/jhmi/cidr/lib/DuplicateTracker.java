/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import edu.jhmi.cidr.lib.HtmlUtils.HtmlBuilder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This is a useful tool for error checking if you're trying to see whether some
 * set of data sources (e.g., records in a file) contain duplicate values.
 *
 * @param <T> the type of the values we are checking for uniqueness
 * @param <K> is the type of the data source that contains T. Sources of type K
 * must provide values of T
 * @author David Newcomer Mar 12, 2012
 */
public class DuplicateTracker<T, K> implements Iterable<T>{

    private final Map<T, List<K>> entryMap;

    public DuplicateTracker() {
        this.entryMap = new HashMap<T, List<K>>();
    }

    public void addEntry(T uniqueValue, K dataSource) {
        List<K> dataSources;
        if (entryMap.containsKey(uniqueValue)) {
            dataSources = entryMap.get(uniqueValue);
        } else {
            dataSources = new ArrayList<K>();
            entryMap.put(uniqueValue, dataSources);
        }
        dataSources.add(dataSource);
    }

    public boolean hasDuplicates() {
        return false == getDuplicates().isEmpty();
    }

    public boolean hasDuplicates(T uniqueValue) {
        if (entryMap.containsKey(uniqueValue)) {
            return entryMap.get(uniqueValue).size() > 1;
        } else {
            return false;
        }
    }

    public List<K> getDataSourcesForValue(T uniqueKey) {
        if (entryMap.containsKey(uniqueKey)) {
            return Collections.unmodifiableList(entryMap.get(uniqueKey));
        } else {
            return Collections.unmodifiableList(new ArrayList<K>());
        }
    }

    public Set<T> getKeys() {
        return entryMap.keySet();
    }

    public List<K> getValues() {
        List<K> result = new ArrayList<K>();
        for (List<K> value : entryMap.values()) {
            result.addAll(value);
        }
        return Collections.unmodifiableList(result);
    }

    public Map<T, List<K>> getDuplicates() {
        Map<T, List<K>> duplicates = new HashMap<T, List<K>>();
        for (Map.Entry<T, List<K>> en : entryMap.entrySet()) {
            T uniqueValue = en.getKey();
            List<K> dataSources = Collections.unmodifiableList(en.getValue());
            if (dataSources.size() > 1) {
                duplicates.put(uniqueValue, dataSources);
            }
        }
        return Collections.unmodifiableMap(duplicates);
    }

    public Set<T> getDuplicateKeys() {
        return getDuplicates().keySet();
    }

    public List<K> getDuplicateValues() {
        List<K> sourcesWithDuplicates = new ArrayList<K>();
        for (Map.Entry<T, List<K>> en : entryMap.entrySet()) {
            List<K> dataSources = en.getValue();
            if (dataSources.size() > 1) {
                sourcesWithDuplicates.addAll(dataSources);
            }
        }
        return Collections.unmodifiableList(sourcesWithDuplicates);
    }

    public static <T> Set<T> findDuplicates(Collection<T> collectionToCheck) {
        HashSet<T> values = new HashSet<T>();
        HashSet<T> duplicates = new HashSet<T>();
        for (T t : collectionToCheck) {
            if (values.contains(t)) {
                duplicates.add(t);
            } else {
                values.add(t);
            }
        }
        return Collections.unmodifiableSet(duplicates);
    }

    /**
     * This method will return all the K sources that yield duplicate values
     * after performing a transformation.
     */
    public static <T, K> List<K> findDuplicates(Collection<K> sourceData, Function<K, T> f) {
        DuplicateTracker<T, K> dt = new DuplicateTracker<T, K>();
        for (K k : sourceData) {
            dt.addEntry(f.apply(k), k);
        }
        return dt.getDuplicateValues();
    }

    public static <T> Map<String, List<T>> findDuplicatesMapIgnoreCase(Collection<T> collectionToCheck) {
        DuplicateTracker<String, T> dt = new DuplicateTracker<String, T>();
        for (T object : collectionToCheck) {
            dt.addEntry(String.valueOf(object).toLowerCase(), object);
        }
        return dt.getDuplicates();
    }

    public static Set<String> findDuplicatesIgnoreCase(Collection<String> toCheck) {
        Set<String> retVal = new HashSet<String>();
        for (List<String> dups : findDuplicatesMapIgnoreCase(toCheck).values()) {
            retVal.addAll(dups);
        }
        return retVal;
    }

    public Optional<String> duplicationErrors() {
        return duplicationErrors(new Function<K, String>() {
            @Override
            public String apply(K f) {
                return String.valueOf(f);
            }
        });
    }

    /**
     * This method accepts a function to allow custom implementations of
     * toString for the datasource K.
     */
    public Optional<String> duplicationErrors(Function<K, String> f) {
        Map<T, List<K>> map = getDuplicates();
        if (map.isEmpty()) {
            return Optional.absent();
        } else {
            HtmlBuilder html = new HtmlBuilder();
            for (Map.Entry<T, List<K>> entry : map.entrySet()) {
                List<String> dups = new ArrayList<String>();
                for (K k : entry.getValue()) {
                    dups.add(f.apply(k));
                }
                html.append(entry.getKey()).append(" was found duplicated in the following sources:").append(dups);
            }
            return Optional.of(html.toString());
        }
    }

    @Override
    public Iterator<T> iterator() {
        return entryMap.keySet().iterator();
    }
}
