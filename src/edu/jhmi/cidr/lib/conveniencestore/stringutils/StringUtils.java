/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.conveniencestore.stringutils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kduerr1
 */
public class StringUtils {

    public static final String NEW_LINE = System.getProperty("line.separator");
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");

    public static String join(Collection<String> lines, String delimiter) {
        StringBuilder sb = new StringBuilder();

        for (String string : lines) {
            sb.append(string)
                    .append(delimiter);
        }

        if (sb.length() > delimiter.length()) {
            return sb.substring(0, sb.length() - delimiter.length());
        }

        return "";
    }

    public static String addCommas(Number number) {
        return addCommas(String.valueOf(number));
    }

    public static String addCommas(String number) {
        if (false == number.matches("\\d{4,}\\.?\\d*")) { //at least four digits and maybe decimals
            return number;
        }

        String decimalsFormat = "";
        if (number.contains(".")) {
            decimalsFormat = "." + number.split("\\.")[1].replace("\\d", "#"); //preserve decimals in the format
        }

        DecimalFormat formatter = new DecimalFormat("#,###" + decimalsFormat); //only one comma is necessary
        double doubleNumber = Double.parseDouble(number);

        return formatter.format(doubleNumber);
    }

    public static List<String> split(String line, String delimiter) {
        String hasQuotes = ".*\".*\".*";
        List<String> defaultSplit = Arrays.asList(line.split(delimiter));

        if (false == line.matches(hasQuotes)) {
            return defaultSplit;
        }

        String delimiterNotBetweenQuotes = delimiter + "(?=(?:[^\"]*\"[^\"]*\")*[^\"]*\\Z)"; //http://stackoverflow.com/a/11503678
        List<String> preservedSplit = new ArrayList<>();

        for (String element : line.split(delimiterNotBetweenQuotes)) {
            preservedSplit.add(element.replace("\"", "")); //clean out quotes
        }

        return preservedSplit;
    }

    public static String formatDecimals(double number, int decimalCount) {
        String formatString = "#";
        if (decimalCount > 0) {
            formatString += ".";
        }
        for (int i = 0; i < decimalCount; i++) {
            formatString += "#";
        }

        String formattedNumber = new DecimalFormat(formatString).format(number);

        // force padding of zeroes
        if (decimalCount > 0) {
            int decimalPlace = formattedNumber.indexOf(".");
            if (decimalPlace == -1) {
                formattedNumber += ".";
                decimalPlace = formattedNumber.length() - 1;
            }

            int formattedDecimalCount = formattedNumber.substring(decimalPlace + 1).length();
            if (formattedDecimalCount < decimalCount) {
                for (int i = 0; i < decimalCount - formattedDecimalCount; i++) {
                    formattedNumber += "0";
                }
            }
        }

        return formattedNumber;
    }

    public static String getISO8601timestamp() {
        return new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss").format(new Date());
    }

    public static void main(String... args) throws Exception {
        List<String> originalArray = Arrays.asList("orange", "red", "purple");
        String joined = StringUtils.join(originalArray, ",");
        System.out.println("join - " + joined);

        System.out.println("");
        String withCommas = StringUtils.addCommas(3000000.123);
        System.out.println("addCommas - " + withCommas);
        
        System.out.println("");
        String originalString = "blue,\"green,dark green\",yellow";
        List<String> splitted = StringUtils.split(originalString, ",");
        System.out.println("split (preserves quoted strings) - ");
        for (String string : splitted) {
            System.out.println(string);
        }

        System.out.println("");
        String formattedDecimals = formatDecimals(5.1, 3);
        System.out.println("formattedDecimals - " + formattedDecimals);

        System.out.println("");
        System.out.println("getISO8601timestamp - " + StringUtils.getISO8601timestamp());
    }
}
