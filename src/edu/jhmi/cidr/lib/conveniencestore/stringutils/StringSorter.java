/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.jhmi.cidr.lib.conveniencestore.stringutils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Brad Tibbils
 */
public class StringSorter {

    private static final String REGEX_NUMS = "[0-9]+"; //1 or more numbers
    private static final String REGEX_SPLITTER = "(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)"; //any time a non-digit comes right before a digit or (pipe character) vice-versa, split the string (when used with .split())

    public StringSorter() {
    }

    public static <T> void sort(List<T> objects) {
        Collections.sort(objects, new StringComparator<T>());
    }

    //made public so it can be used in a stream with .sorted(Comparator<>)
    public static class StringComparator<T> implements Comparator<T> {

        //split up the strings into digit and non-digit groups (using the regex above)
        //then loop through the groups of the first string comparing them to the groups in the second
        //the first time the groups aren't equal on both sides, determine the order by comparing that particular pair of strings
        
        @Override
        public int compare(T o1, T o2) {
            String string1 = o1.toString();
            String string2 = o2.toString();
            String[] first = string1.split(REGEX_SPLITTER);
            String[] second = string2.split(REGEX_SPLITTER);

            for (int i = 0; i < first.length; i++) {

                if (first[i].equals("")) { //always move empty strings to the front
                    return -1;
                }

                if (i >= second.length) {
                    continue;
                }

                if (second[i].equals("")) { //auto move anything compared to an empty string forward
                    return 1;
                }

                if (false == first[i].equalsIgnoreCase(second[i])) {

                    boolean firstIsNum = first[i].matches(REGEX_NUMS);
                    boolean secondIsNum = second[i].matches(REGEX_NUMS);

                    if (firstIsNum && secondIsNum) {
                        Double firstNum = Double.parseDouble(first[i]);
                        Double secondNum = Double.parseDouble(second[i]);

                        return firstNum > secondNum ? 1 : -1;

                    } else if (!firstIsNum && !secondIsNum) {
                        return first[i].compareToIgnoreCase(second[i]);

                    } else {
                        return secondIsNum ? 1 : -1;
                    }
                }
            }

            return 0;
        }
    }

    public static void main(String... args) throws Exception {
        List<String> strings = Arrays.asList("11", "i33k-0", "i33j-10", "i33j-11", "i33j-01", "", "1", "11", "11a", "11abcd", "0", "10", "c", "b", "a");
        StringSorter.sort(strings);
        System.out.println("strings = " + strings);
        
        List<TestObject> objects = new ArrayList<>();
        for (String string : strings) {
            objects.add(new TestObject(string));
        }
        StringSorter.sort(objects);
        System.out.println("objects = " + objects);
    }
    
    private static class TestObject {
        private final String s;

        public TestObject(String s) {
            this.s = s;
        }

        @Override
        public String toString() {
            return s;
        }
    }
}
