/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.conveniencestore.stringutils;

import java.util.Arrays;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Brad Tibbils
 */
public class EnumMaker {

    private static final String CR = StringUtils.NEW_LINE;
    private static final String TAB = "\t";

    private StringBuilder output;

    private final String className;
    private final List<String> enums;

    public EnumMaker(String className, List<String> enums) {
        this.className = className;
        this.enums = enums;
    }

    public void start() {
        output = new StringBuilder();

        buildClassName();
        buildEnums();
        buildGuts();
        addFinalBrace();
    }

    private void buildClassName() {
        output.append(CR)
                .append("public enum ")
                .append(className)
                .append(" {")
                .append(CR).append(CR);
    }

    private void buildEnums() {
        int counter = 0;
        for (String enumName : enums) {
            counter++;
            output.append(TAB)
                    .append(enumName.toUpperCase())
                    .append("(")
                    .append("\"")
                    .append(enumName)
                    .append("\"")
                    .append(")");

            if (counter < enums.size()) {
                output.append(",");
            } else {
                output.append(";");
            }

            output.append(CR);
        }
    }

    private void buildGuts() {
        output.append(CR).append(TAB)
                .append("private final String displayName;")
                .append(CR);

        output.append(CR).append(TAB)
                .append("private ")
                .append(className)
                .append(" (String displayName) {")
                .append(CR);

        output.append(TAB).append(TAB)
                .append("this.displayName = displayName;")
                .append(CR);

        output.append(TAB)
                .append("}")
                .append(CR);

        output.append(CR).append(TAB)
                .append("public String getDisplayName() {")
                .append(CR);

        output.append(TAB).append(TAB)
                .append("return displayName;")
                .append(CR);

        output.append(TAB)
                .append("}")
                .append(CR);
    }

    private void addFinalBrace() {
        output.append("}");
    }

    public String getOutput() {
        return output.toString();
    }

    public static void main(String... args) throws Exception {
        List<String> salutations = Arrays.asList("hello", "goodbye", "hola", "adios");
        EnumMaker enumMaker = new EnumMaker("Salutations", salutations);
        enumMaker.start();
        System.out.println(enumMaker.getOutput());
    }
}
