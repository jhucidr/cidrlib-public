/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.fileutils.drivemapper;

import edu.jhmi.cidr.lib.conveniencestore.fileutils.drivemapper.ProcessLaunchingUtils.ProcessOutputHandler;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Kevin Duerr
 */
public class DriveMapper {

    private final Map<String, String> driveMap = new HashMap<>();

    public DriveMapper() {
    }

    public void setupDriveMap() throws Exception {
        File[] roots = File.listRoots();
        int uncPathIndex = 11;

        for (File file : roots) {
            String driveLetter = file.getAbsolutePath().substring(0, 2);
            String commandOutput = launchCommandAndGetResults("wmic netuse where LocalName='" + driveLetter + "' get RemoteName /value");
            if (!commandOutput.isEmpty()) {
                String uncPath = commandOutput.substring(uncPathIndex, commandOutput.length());
                driveMap.put(driveLetter, uncPath);
            } else {
            }
        }
    }

    public String getUncPath(String path) {
        for (Map.Entry<String, String> entry : driveMap.entrySet()) {
            String driveLetter = entry.getKey();
            String uncPath = entry.getValue();

            if (path.startsWith(driveLetter)) {
                path = path.replace(driveLetter, uncPath);
                break;
            }
        }

        return path;
    }

    public String getUncPath(File path) {
        return getUncPath(path.getAbsolutePath());
    }

    private String launchCommandAndGetResults(String command) throws Exception {
        ProcessOutputHandler outputHandler = new ProcessOutputHandler(
                null, false, null, false, false, true);
        return ProcessLaunchingUtils.launchProcess(command, outputHandler, true);
    }

    public static void main(String[] args) throws Exception {
        DriveMapper driveMapper = new DriveMapper();
        driveMapper.setupDriveMap();
        String mappedPath = "";
        String uncPath = driveMapper.getUncPath(mappedPath);
        
        System.out.println(uncPath); //prints \\cidr.jhu.edu\CIDR\Files\Informatics\file.csv
    }
}
