/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.conveniencestore.fileutils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;
import edu.jhmi.cidr.lib.conveniencestore.stringutils.StringUtils;
import java.io.File;
import java.util.Arrays;

/**
 *
 * @author Brad Tibbils
 */
public class FileWriter {

    private final static int TIMELINE_INCREMENT_MILLIS = 500;
    private final File outputFile;
    private final List<List<String>> content;
    private boolean writingToFile;
    private String delimiter = ",";
    private Timeline timeline;
    private boolean usingTimeline = false;
    private boolean openWhenDone = true;

    public FileWriter(List<List<String>> contentToWrite, File outputFile) {
        this.content = contentToWrite;
        this.outputFile = outputFile;
        this.writingToFile = false;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    public void setCallbackAfterOpeningFile(final Runnable callback) {
        usingTimeline = true;
        timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(TIMELINE_INCREMENT_MILLIS), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                if (!writingToFile) {
                    callback.run();
                    timeline.stop();
                }
            }
        }));
    }

    public static boolean fileIsLocked(File file) {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public boolean isOutputFileLocked() {
        return fileIsLocked(outputFile);
    }

    public void write() {
        openWhenDone = false;
        writeAndOpen();
    }

    public void writeAndOpen() {
        if (usingTimeline) {
            timeline.play();
        }

        //Writing to file runs in a separate thread so .waitFor() can be used to block the thread before changing writingToFile to false.
        //The timeline (which runs on the JavaFX thread) waits for this variable to change before running the callback (if one is set).
        //This way a please wait animation can be shown without being blocked and removed only when the file is open.
        new Thread(new Runnable() {
            @Override
            public void run() {
                writingToFile = true;

                try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "utf-8"))) {
                    for (List<String> contentLine : content) {
                        String delimitedLine = StringUtils.join(contentLine, delimiter);
                        out.write(delimitedLine + StringUtils.NEW_LINE);
                    }

                    out.close(); //must close stream before opening file in excel (which is probably where it will open)

                    if (openWhenDone) {
                        Process openFileProcess = Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL " + outputFile);
                        openFileProcess.waitFor();
                    } else {
                        openWhenDone = true; //reset it just in case the instance gets used again to call writeAndOpen
                    }

                    writingToFile = false;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    public static void main(String... args) throws Exception {
        List<List<String>> contentToWrite = Arrays.asList(
                Arrays.asList("column name 1", "column name 2"), //header row of column names
                Arrays.asList("value 1", "value 2") //first row of values
        );
        File outputFile = new File("writetest.csv");
        
        FileWriter fileWriter = new FileWriter(contentToWrite, outputFile);
        fileWriter.setCallbackAfterOpeningFile(new Runnable() {
            @Override
            public void run() {
                System.out.println("The file should be open now.");
            }
        });
        
        if (fileWriter.isOutputFileLocked()) {
            System.out.println("Woah, that file is still open! Please close it.");
        } else {
            fileWriter.writeAndOpen();
        }
    }
}
