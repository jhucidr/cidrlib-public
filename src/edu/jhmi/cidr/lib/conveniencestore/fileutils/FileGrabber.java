/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.jhmi.cidr.lib.conveniencestore.fileutils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Brad Tibbils
 */
public class FileGrabber {
    private FileGrabber() {    
    }
    
    public static List<File> grab(File directory, final String prefix, final String suffix) {
        List<File> foundFiles = new ArrayList<>();

        if (directory.exists()) {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.startsWith(prefix) && name.endsWith(suffix);
                }
            };

            foundFiles = Arrays.asList(directory.listFiles(filenameFilter));
        }

        return foundFiles;
    }
    
    public static List<File> grabWithRegex(File directory, final String regex) {
        List<File> foundFiles = new ArrayList<>();
        
        if (directory.exists()) {
            FilenameFilter filenameFilter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    return name.matches(regex);
                }
            };
            
            foundFiles = Arrays.asList(directory.listFiles(filenameFilter));
        }
        
        return foundFiles;
    }
    
    public static void main(String... args) throws Exception {
        File directory = new File("test_dir");
        String prefix = "";
        String suffix = "s";
        List<File> files = FileGrabber.grab(directory, prefix, suffix);
        System.out.println("files = " + files);
        
        String regex = ".*S";
        List<File> regexedFiles = FileGrabber.grabWithRegex(directory, regex);
        System.out.println("regexedFiles = " + regexedFiles);
    }
}
 