/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic;

import com.google.common.collect.Table;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParseOmaticStrategy;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParseOmaticColumn;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.parseomaticstrategy.ColumnsFileParser;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.parseomaticstrategy.EnumColumnParser;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.parseomaticstrategy.StringColumnParser;
import java.io.File;
import java.util.List;
import java.util.Map;
import edu.jhmi.cidr.lib.conveniencestore.stringutils.StringUtils;

/**
 * \
 *
 * @author Brad
 */
public class ParseOmatic {

    private ParseOmatic() {
    }

    public static StringColumnParser getStringColumnParser(File inputFile, int headerLine, String delimiter, List<String> requestedColumns, List<String> requiredColumns) {
        return new StringColumnParser(inputFile, headerLine, delimiter, requestedColumns, requiredColumns);
    }

    public static <E extends ParseOmaticColumn> EnumColumnParser<E> getEnumColumnParser(File inputFile, int headerLine, String delimiter, List<E> requestedColumns, List<E> requiredColumns) {
        return new EnumColumnParser<>(inputFile, headerLine, delimiter, requestedColumns, requiredColumns);
    }

    public static ColumnsFileParser getColumnsFileParser(File inputFile, int headerLine, String delimiter, File columnsFile, ColumnsFileParser.ColumnsDirection columnsDirection, int columnsFileStartingRow, int columnsFileStartingColumn) {
        return new ColumnsFileParser(inputFile, headerLine, delimiter, columnsFile, columnsDirection, columnsFileStartingRow, columnsFileStartingColumn);
    }

    public static <T> void printDebug(ParseOmaticStrategy<T> parser) {
        Table<Integer, T, String> parsedContent = parser.getParsedContent();

        if (null == parsedContent) {
            System.err.println("ParseOmatic error: parsedContent is empty. Please run .parse()");
            return;
        }
        
        StringBuilder sb = new StringBuilder();

        for (Map.Entry<Integer, Map<T, String>> entry : parsedContent.rowMap().entrySet()) {
            Integer lineNumber = entry.getKey();
            Map<T, String> parsedLineMap = entry.getValue();

            sb.append(" - LINE ")
                    .append(lineNumber).append(" - ")
                    .append(StringUtils.NEW_LINE);

            for (Map.Entry<T, String> cell : parsedLineMap.entrySet()) {
                T column = cell.getKey();
                String value = cell.getValue();

                sb.append(column)
                        .append(" => ")
                        .append(value)
                        .append(StringUtils.NEW_LINE);
            }
        }

        sb.append(StringUtils.NEW_LINE);
        sb.append("Missing Required Columns - ").append(parser.getMissingRequiredColumns()).append(StringUtils.NEW_LINE);
        sb.append("Duplicate Columns - ").append(parser.getDuplicateColumns()).append(StringUtils.NEW_LINE);
        sb.append("Empty Columns - ").append(parser.getEmptyColumns()).append(StringUtils.NEW_LINE);

        System.out.println(sb.toString());
    }
}
