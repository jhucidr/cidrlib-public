/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.parseomaticstrategy;

import com.google.common.collect.Table;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.exceptions.MissingDelimiterException;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParseOmaticStrategy;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParseOmaticColumn;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParsedLinePostProcessor;
import edu.jhmi.cidr.lib.conveniencestore.stringutils.StringUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Brad Tibbils
 * @param <E>
 */
public class EnumColumnParser<E extends ParseOmaticColumn> implements ParseOmaticStrategy<E> {

    private final File inputFile;
    private final int headerLine;
    private final String delimiter;
    private final List<E> requestedColumns;
    private final List<E> requiredColumns;

    private final List<E> missingRequiredColumns = new ArrayList<>();
    private final List<String> duplicateColumns = new ArrayList<>();
    private final List<String> emptyColumns = new ArrayList<>();
    private final List<String> foundHeaderColumns = new ArrayList<>();

    private final Map<String, E> requestedColumnMap = new HashMap<>();
    private final LineParser<E> lineParser;
    private boolean caseSensitive = false;

    public EnumColumnParser(File inputFile, int headerLine, String delimiter, List<E> requestedColumns, List<E> requiredColumns) {
        this.inputFile = inputFile;
        this.headerLine = headerLine;
        this.delimiter = delimiter;
        this.requestedColumns = requestedColumns;
        this.requiredColumns = requiredColumns;
        this.lineParser = new LineParser<>(foundHeaderColumns, requestedColumnMap, duplicateColumns, emptyColumns, delimiter, caseSensitive);
    }

    @Override
    public void parse() throws Exception {
        int lineNumber = 0;
        String line;

        setupRequestedColumnMap();
        lineParser.setup(inputFile, headerLine);

        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            while ((line = reader.readLine()) != null) {
                lineNumber++;

                if (lineNumber == headerLine) {
                    processHeader(line);
                } else if (lineNumber > headerLine) {
                    lineParser.processLine(line, lineNumber);
                }
            }
        }
    }

    private void setupRequestedColumnMap() {
        for (E requestedColumn : requestedColumns) {
            String displayName = caseSensitive ? requestedColumn.getDisplayName() : requestedColumn.getDisplayName().toUpperCase();
            requestedColumnMap.put(displayName, requestedColumn);
        }
    }

    private void processHeader(String line) throws MissingDelimiterException {
        if (false == line.contains(delimiter)) {
            throw new MissingDelimiterException();
        }

        lineParser.processHeader(StringUtils.split(line, delimiter));

        for (E column : requiredColumns) {
            String columnName = caseSensitive ? column.getDisplayName() : column.getDisplayName().toUpperCase();

            if (false == foundHeaderColumns.contains(columnName)) {
                missingRequiredColumns.add(column);
            }
        }
    }

    @Override
    public Table<Integer, E, String> getParsedContent() {
        return lineParser.getParsedContent();
    }

    @Override
    public List<E> getMissingRequiredColumns() {
        return missingRequiredColumns;
    }

    @Override
    public List<String> getDuplicateColumns() {
        return duplicateColumns;
    }

    @Override
    public List<String> getEmptyColumns() {
        return emptyColumns;
    }

    @Override
    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    @Override
    public void setParsedLinePostProcessor(ParsedLinePostProcessor<E> parsedLinePostProcessor) {
        lineParser.setParsedLinePostProcessor(parsedLinePostProcessor);
    }
}
