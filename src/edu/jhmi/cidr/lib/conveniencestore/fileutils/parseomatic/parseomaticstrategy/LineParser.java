/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.parseomaticstrategy;

import com.google.common.collect.ArrayTable;
import com.google.common.collect.Table;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParsedLinePostProcessor;
import edu.jhmi.cidr.lib.conveniencestore.stringutils.StringUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Brad Tibbils
 */
class LineParser<T> {

    private final List<String> foundHeaderColumns;
    private final Map<String, T> requestedColumnMap;
    private final List<String> duplicateColumns;
    private final List<String> emptyColumns;
    private final String delimiter;
    private final boolean caseSensitive;

    private Table<Integer, T, String> parsedContent;
    private ParsedLinePostProcessor<T> parsedLinePostProcessor;

    public LineParser(List<String> foundHeaderColumns, Map<String, T> requestedColumnMap, List<String> duplicateColumns, List<String> emptyColumns, String delimiter, boolean caseSensitive) {
        this.foundHeaderColumns = foundHeaderColumns;
        this.requestedColumnMap = requestedColumnMap;
        this.duplicateColumns = duplicateColumns;
        this.emptyColumns = emptyColumns;
        this.delimiter = delimiter;
        this.caseSensitive = caseSensitive;
    }

    public Table<Integer, T, String> getParsedContent() {
        return parsedContent;
    }

    public void setup(File inputFile, int headerLine) throws FileNotFoundException {
        missingFileCheck(inputFile);

        if (null == parsedLinePostProcessor) {
            //ArrayTable is much faster than List<Map<,>> but you have to set its sizes before using it.
            List<Integer> rowKeys = new ArrayList<>();
            int lineCount = 0;
            String line;

            try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
                while ((line = reader.readLine()) != null) {
                    lineCount++;
                    if (lineCount > headerLine && false == line.trim().isEmpty()) { //don't count empty lines after the header
                        rowKeys.add(lineCount);
                    }
                }
            } catch (Exception ex) {
            }

            parsedContent = ArrayTable.create(rowKeys, requestedColumnMap.values());
        }
    }

    public void processHeader(List<String> parsedHeaderLine) {
        for (String parsedColumnName : parsedHeaderLine) {
            parsedColumnName = parsedColumnName.trim();
            String casedColumnName = caseSensitive ? parsedColumnName : parsedColumnName.toUpperCase();

            if (false == foundHeaderColumns.contains(casedColumnName)) {
                foundHeaderColumns.add(casedColumnName);
                emptyColumns.add(casedColumnName); //if values are found later, parsedColumnName will be removed from emptyColumns
            } else {
                foundHeaderColumns.add(casedColumnName + "_duplicate_placeholder_" + Math.round(Math.random() * 10));
                duplicateColumns.add(parsedColumnName);
            }
        }
    }

    public void processLine(String line, int lineNumber) {
        if (false == line.trim().isEmpty()) {
            List<String> lineParts = StringUtils.split(line, delimiter);
            Map<T, String> lineMap = new HashMap<>();

            for (int i = 0; i < foundHeaderColumns.size(); i++) {
                //move accross the line, staying in sync with the column names.
                //the line may be shorter than the header line, so check before grabbing the value at that point from it
                String columnName = foundHeaderColumns.get(i);
                String value = i < lineParts.size() ? lineParts.get(i).trim() : "";

                if (requestedColumnMap.containsKey(columnName)) {
                    T columnKey = requestedColumnMap.get(columnName);
                    if (null != parsedLinePostProcessor) {
                        lineMap.put(columnKey, value);
                    } else {
                        parsedContent.put(lineNumber, columnKey, value);
                    }
                }

                if (false == value.isEmpty()) {
                    emptyColumns.remove(columnName);
                }
            }

            if (null != parsedLinePostProcessor) {
                parsedLinePostProcessor.process(lineMap);
            }
        }
    }

    public void setParsedLinePostProcessor(ParsedLinePostProcessor<T> parsedLinePostProcessor) {
        this.parsedLinePostProcessor = parsedLinePostProcessor;
    }

    private void missingFileCheck(File inputFile) throws FileNotFoundException {
        if (false == inputFile.exists()) {
            throw new FileNotFoundException("No file was found at this path - " + inputFile.getAbsolutePath());
        }
    }
}
