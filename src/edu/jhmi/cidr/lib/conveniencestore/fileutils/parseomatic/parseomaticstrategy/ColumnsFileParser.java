/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.parseomaticstrategy;

import com.google.common.collect.Table;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParseOmaticStrategy;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.ParseOmatic;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParsedLinePostProcessor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import edu.jhmi.cidr.lib.conveniencestore.stringutils.StringUtils;

/**
 *
 * @author Brad Tibbils
 */
public class ColumnsFileParser implements ParseOmaticStrategy<String> {

    private final File inputFile;
    private final int headerLine;
    private final String delimiter;
    private final File columnsFile;
    private final ColumnsDirection columnsDirection;
    private final int columnsFileStartingRow;
    private final int columnsFileStartingColumn;

    private Table<Integer, String, String> parsedContent;
    private List<String> missingRequiredColumns;
    private List<String> duplicateColumns;
    private List<String> emptyColumns;

    private boolean caseSensitive = false;
    private ParsedLinePostProcessor<String> parsedLinePostProcessor;
    private final List<String> columns = new ArrayList<>();

    public ColumnsFileParser(File inputFile, int headerLine, String delimiter, File columnsFile, ColumnsDirection columnsDirection, int columnsFileStartingRow, int columnsFileStartingColumn) {
        this.inputFile = inputFile;
        this.headerLine = headerLine;
        this.delimiter = delimiter;
        this.columnsFile = columnsFile;
        this.columnsDirection = columnsDirection;
        this.columnsFileStartingRow = columnsFileStartingRow;
        this.columnsFileStartingColumn = columnsFileStartingColumn;
    }

    @Override
    public void parse() throws Exception {
        int lineNumber = 0;
        String line;

        try (BufferedReader reader = new BufferedReader(new FileReader(columnsFile))) {
            while ((line = reader.readLine()) != null) {
                lineNumber++;
                List<String> lineParts = StringUtils.split(line, ",");

                switch (columnsDirection) {
                    case HORIZONTAL:
                        if (lineNumber == columnsFileStartingRow) {
                            List<String> columnNames = lineParts.subList(columnsFileStartingColumn - 1, lineParts.size());

                            for (String columnName : columnNames) {
                                columns.add(columnName.trim());
                            }
                        }
                        break;
                    case VERTICAL:
                        if (lineNumber >= columnsFileStartingRow) {
                            String columnName = lineParts.get(columnsFileStartingColumn - 1).trim();
                            columns.add(columnName);
                        }
                        break;
                    default:
                        System.err.println("Unknown columns direction: " + columnsDirection);
                }
            }

            StringColumnParser parser = ParseOmatic.getStringColumnParser(inputFile, headerLine, delimiter, columns, columns);
            parser.setCaseSensitive(caseSensitive);
            parser.setParsedLinePostProcessor(parsedLinePostProcessor);
            parser.parse();

            parsedContent = parser.getParsedContent();
            missingRequiredColumns = parser.getMissingRequiredColumns();
            duplicateColumns = parser.getDuplicateColumns();
            emptyColumns = parser.getEmptyColumns();
        }
    }

    @Override
    public Table<Integer, String, String> getParsedContent() {
        return parsedContent;
    }

    @Override
    public List<String> getMissingRequiredColumns() {
        return missingRequiredColumns;
    }

    @Override
    public List<String> getDuplicateColumns() {
        return duplicateColumns;
    }

    @Override
    public List<String> getEmptyColumns() {
        return emptyColumns;
    }

    @Override
    public void setCaseSensitive(boolean caseSensitive) {
        this.caseSensitive = caseSensitive;
    }

    @Override
    public void setParsedLinePostProcessor(ParsedLinePostProcessor<String> parsedLinePostProcessor) {
        this.parsedLinePostProcessor = parsedLinePostProcessor;
    }

    public List<String> getColumns() {
        return columns;
    }
    
    public enum ColumnsDirection {

        HORIZONTAL, VERTICAL;
    }
}
