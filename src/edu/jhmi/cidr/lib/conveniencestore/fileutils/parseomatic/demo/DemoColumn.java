/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.demo;

import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.ParseOmatic;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParseOmaticColumn;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.parseomaticstrategy.ColumnsFileParser;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParseOmaticStrategy;
import edu.jhmi.cidr.lib.conveniencestore.fileutils.parseomatic.interfaces.ParsedLinePostProcessor;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Brad Tibbils
 */
public enum DemoColumn implements ParseOmaticColumn {
    COLUMN_ONE("Column1"),
    COLUMN_TWO("Column2"),
    COLUMN_THREE("Column3"),
    COLUMN_SEVENTEEN("Column17");

    private static final File INPUT_FILE = new File("testing.csv");
    private static final int HEADER_LINE = 1;
    private static final String DELIMITER = ",";
    private final String displayName;

    private DemoColumn(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    public static void main(String... args) throws Exception {
        stringParserTest();
        enumParserTest();
        columnsFileParserTest();
    }

    private static void stringParserTest() throws Exception {
        System.out.println("STRING PARSER TEST");
        List<String> requestedColumns = Arrays.asList("Column1", "Column2", "Column3");
        List<String> requiredColumns = Arrays.asList("Column1", "Column2", "Column3", "Column17");
        ParseOmaticStrategy<String> parser = ParseOmatic.getStringColumnParser(INPUT_FILE, HEADER_LINE, DELIMITER, 
                requestedColumns, requiredColumns);

        parser.parse();
        ParseOmatic.printDebug(parser);
        
        System.out.println("STRING PARSER TEST (with parsedLinePostProcessor)");
        final SimpleIntegerProperty col1ValCount = new SimpleIntegerProperty(0);
        parser.setParsedLinePostProcessor(new ParsedLinePostProcessor<String>() {
            @Override
            public void process(Map<String, String> lineMap) {
                if (false == lineMap.get("Column1").isEmpty()) {
                    col1ValCount.set(col1ValCount.get() + 1);
                }
            }
        });
        parser.parse();
        System.out.println("Column1 contains " + col1ValCount.get() + " values\n");
    }

    private static void enumParserTest() throws Exception {
        System.out.println("ENUM PARSER TEST");
        List<DemoColumn> requestedColumns = Arrays.asList(COLUMN_ONE, COLUMN_TWO, COLUMN_THREE);
        List<DemoColumn> requiredColumns = Arrays.asList(DemoColumn.values());
        ParseOmaticStrategy<DemoColumn> parser = ParseOmatic.getEnumColumnParser(INPUT_FILE, HEADER_LINE, DELIMITER, 
                requestedColumns, requiredColumns);

        parser.parse();
        ParseOmatic.printDebug(parser);
    }

    private static void columnsFileParserTest() throws Exception {
        System.out.println("COLUMNS FILE PARSER TEST");
        File columnsFile = new File("ColumnsFileExample.csv");
        ParseOmaticStrategy<String> parser = ParseOmatic.getColumnsFileParser(INPUT_FILE, HEADER_LINE, DELIMITER, 
                columnsFile, ColumnsFileParser.ColumnsDirection.VERTICAL, 1, 1);

        parser.parse();
        ParseOmatic.printDebug(parser);
    }
}
