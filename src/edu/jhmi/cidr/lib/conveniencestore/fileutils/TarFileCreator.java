/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.fileutils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.utils.IOUtils;

/**
 *
 * @author Alice Sanchez
 */
public class TarFileCreator {

    private final List<File> files;
    
    public static void main(String[] args) throws Exception {
        new TarFileCreator(Arrays.asList(new File("\\\\Isilon-cifs.cidr.jhu.edu\\seq_proj\\1_TO_COPY_TO_OCARINA\\Scharf_Tourette_WGHum_SeqWholeExomePlus_121010_1\\experiment.xml"),
                new File("\\\\Isilon-cifs.cidr.jhu.edu\\seq_proj\\1_TO_COPY_TO_OCARINA\\Scharf_Tourette_WGHum_SeqWholeExomePlus_121010_1\\run.xml"),
                new File("\\\\Isilon-cifs.cidr.jhu.edu\\seq_proj\\1_TO_COPY_TO_OCARINA\\Scharf_Tourette_WGHum_SeqWholeExomePlus_121010_1\\submission.xml")))
                .writeTarFile(new File("\\\\Isilon-cifs.cidr.jhu.edu\\seq_proj\\1_TO_COPY_TO_OCARINA\\Scharf_Tourette_WGHum_SeqWholeExomePlus_121010_1\\Scharf_Combined_SRA.tar"));
    }

    public TarFileCreator(List<File> files) {
        this.files = files;
    }

    public TarFileCreator() {
        this.files = new ArrayList<>();
    }
    
    public void addFile(File file){
        files.add(file);
    }
    
    public void addFiles(List<File> files){
        this.files.addAll(files);
    }
    
    public void addFiles(File... files){
        this.files.addAll(Arrays.asList(files));
    }

    public void writeTarFile(File output) throws Exception {
        try (ArchiveOutputStream tarOutputStream = new ArchiveStreamFactory().createArchiveOutputStream(ArchiveStreamFactory.TAR, new FileOutputStream(output))) {

            for (File file : files) {
                TarArchiveEntry entry = new TarArchiveEntry(file, file.getName());
                tarOutputStream.putArchiveEntry(entry);
                
                try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file))) {
                    IOUtils.copy(bufferedInputStream, tarOutputStream);
                    tarOutputStream.closeArchiveEntry();
                }
            }
        }
    }
}
