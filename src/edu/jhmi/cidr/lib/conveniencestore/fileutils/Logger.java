/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.fileutils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Brad Tibbils
 */
public class Logger {

    private static final Queue<String> logQueue = new LinkedList<>();
    private static final int MAX_THREADS = 1;
    private static final int DEFAULT_LOGGING_INCREMENT_MILLIS = 15 * 1000;
    private final ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(MAX_THREADS);
    private final String loggingFolderPath;
    private int loggingIncrement;

    public Logger(String loggingFolderPath) {
        this.loggingFolderPath = loggingFolderPath;
        this.loggingIncrement = DEFAULT_LOGGING_INCREMENT_MILLIS;
        addShutdownHook();
    }

    public Logger(String loggingFolderPath, int loggingIncrement) {
        this.loggingFolderPath = loggingFolderPath;
        this.loggingIncrement = loggingIncrement;
        addShutdownHook();
    }

    public void setLoggingIncrement(int loggingIncrementMillis) {
        this.loggingIncrement = loggingIncrementMillis;
    }

    public void start() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                purgeLogQueue();
            }
        };

        scheduledExecutor.scheduleWithFixedDelay(runnable, 0, loggingIncrement, TimeUnit.MILLISECONDS);
    }

    public static void addLine(String line) {
        System.out.println("Logger - " + line);

        DateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String now = timeFormat.format(new Date());
        String stampedLine = now + " | " + line;

        logQueue.add(stampedLine);
    }
    
    public void stop() {
        purgeLogQueue();
        scheduledExecutor.shutdownNow();
    }

    private void purgeLogQueue() {
        File logFile = getLogFile();

        try (PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(logFile, true)))) {
            while (logQueue.peek() != null) {
                String line = logQueue.poll().toString();
                out.println(line);
            }
        } catch (Exception e) {
            System.err.println("Could not write to log file: ");
            e.printStackTrace();
        }
    }

    private File getLogFile() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String today = dateFormat.format(date);
        File logFile = new File(loggingFolderPath, today + ".log");

        return logFile;
    }
    
    private void addShutdownHook() {
        Thread shutdownThread = new Thread(new Runnable() {
            @Override
            public void run() {
                purgeLogQueue();
                scheduledExecutor.shutdown(); 
            }
        });
        
        Runtime.getRuntime().addShutdownHook(shutdownThread);
    }

    public static void main(String... args) throws Exception {
        Logger logger = new Logger("Logger");
        logger.start();

        Logger.addLine("this is a line");
        Logger.addLine("this is a new line");
        Logger.addLine("this is an even newer line");

        while (true) {
            //wait for logger to write to file...
        }
    }
}
