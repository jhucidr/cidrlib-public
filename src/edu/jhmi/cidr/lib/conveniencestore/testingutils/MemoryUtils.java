/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.jhmi.cidr.lib.conveniencestore.testingutils;

import edu.jhmi.cidr.lib.conveniencestore.stringutils.StringUtils;
import java.lang.management.ManagementFactory;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Brad Tibbils
 */
public class MemoryUtils {
    private static final double DATA_UNIT_DIVISOR = 1024;
    
    public static String getCurrentUsedMemory() {
        double currentUsedBytes = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed();
        
        int unitsDeep = 0;
        while (currentUsedBytes > DATA_UNIT_DIVISOR) {
            currentUsedBytes /= DATA_UNIT_DIVISOR;
            unitsDeep++;
        }
        
        String formatted = StringUtils.addCommas(StringUtils.formatDecimals(currentUsedBytes, 3));
        return formatted + " " + DataUnit.fromId(unitsDeep);
    }
    
    private enum DataUnit{
        B(0),
        KB(1),
        MB(2),
        GB(3),
        TB(4); //just in case!
        private static final Map<Integer, DataUnit> idMap = new HashMap<>();
                
        private final int id;

        static {
            for (DataUnit dataUnit : DataUnit.values()) {
                idMap.put(dataUnit.getId(), dataUnit);
            }
        }
        
        private DataUnit(int id) {
            this.id = id;
        }

        public static DataUnit fromId(int id) {
            return idMap.get(id);
        }
        
        public int getId() {
            return id;
        }
    }
    
    public static void main(String... args) throws Exception {
        Integer[] ints = new Integer[1000000000];
        System.out.println(getCurrentUsedMemory());
    }
}
