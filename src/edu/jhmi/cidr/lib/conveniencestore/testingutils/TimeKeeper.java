/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.testingutils;

/**
 *
 * @author Brad Tibbils
 */
public class TimeKeeper {
    private boolean includeMillis;
    
    private long start;
    private long elapsedMillis;
    private StringBuilder display;

    public TimeKeeper() {
    }

    public void start() {
        start = System.currentTimeMillis();
    }

    public void stop(String message) {
        elapsedMillis = System.currentTimeMillis() - start;
        buildPrettyElapsed();
        System.out.println(message + " - " + display);
    }
    
    public void includeMillis(boolean includeMillis) {
        this.includeMillis = includeMillis;
    }

    private void buildPrettyElapsed() {
        display = new StringBuilder();

        for (TimeKeeperUnit timeKeeperUnit : TimeKeeperUnit.values()) {
            if (false == includeMillis && timeKeeperUnit == TimeKeeperUnit.MILLISECOND) {
                continue;
            }
            appendPrettyTimeUnit(timeKeeperUnit.getName(), timeKeeperUnit.getDivisor());
        }
        
        if (display.toString().length() == 0) {
            display.append("[too fast to count, try .includeMillis(true)]");
        }
    }

    private void appendPrettyTimeUnit(String timeUnitName, int timeUnitDivisor) {
        long timeValue = elapsedMillis / timeUnitDivisor;
        elapsedMillis -= timeValue * timeUnitDivisor;

        if (timeValue > 0) {
            display.append(String.valueOf(timeValue))
                    .append(" ")
                    .append(timeUnitName);

            if (timeValue > 1) {
                display.append("s");
            }

            display.append(" ");
        }
    }
    
    private static enum TimeKeeperUnit {
        DAY("day", 24 * 60 * 60 * 1000),
        HOUR("hour", 60 * 60 * 1000),
        MINUTE("minute", 60 * 1000),
        SECOND("second", 1000),
        MILLISECOND("millisecond", 1);
        
        private final String name;
        private final int divisor;

        private TimeKeeperUnit(String name, int divisor) {
            this.name = name;
            this.divisor = divisor;
        }

        public String getName() {
            return name;
        }

        public int getDivisor() {
            return divisor;
        }
    }

    public static void main(String... args) throws Exception {
//        TimeKeeper tk = new TimeKeeper();
//        tk.start = 1426413955000L;
//        tk.stop("test");
        
        TimeKeeper timeKeeper = new TimeKeeper();
        timeKeeper.includeMillis(true);
        timeKeeper.start();
        Thread.sleep(1256);
        timeKeeper.stop("finished");
    }
}
