/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.searchboxer;

import java.util.Arrays;
import java.util.List;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

/**
 *
 * @author Brad Tibbils
 */
public class SearchChoice<T> {

    private static final String BOLD_STYLE = "-fx-font-weight: bold;";
    private static final String NON_BOLD_STYLE = "-fx-text-fill: #636363";

    private FlowPane labelContainer;

    private final T originalObject;
    private final String searchString;

    public SearchChoice(T originalObject, String searchString) {
        this.originalObject = originalObject;
        this.searchString = searchString;
    }

    @Override
    public String toString() {
        return originalObject.toString();
    }

    public T getOriginalObject() {
        return originalObject;
    }

    public FlowPane getLabelContainer() {
        return labelContainer;
    }

    public void setup() {
        labelContainer = new FlowPane();
        labelContainer.setPrefWidth(1); //not sure why, but this removes the horizontal scrollbar

        String choiceTextLower = originalObject.toString().toLowerCase();
        List<String> searchWords = Arrays.asList(searchString.toLowerCase().split(" "));
        int currentIdx = 0;
        
        for (String searchWord : searchWords) {
            int searchWordIdx = choiceTextLower.indexOf(searchWord);
            
            if (currentIdx < searchWordIdx) {
                addLabel(currentIdx, searchWordIdx, NON_BOLD_STYLE);
                currentIdx = searchWordIdx;
            }
            
            addLabel(searchWordIdx, searchWordIdx + searchWord.length(), BOLD_STYLE);
            currentIdx += searchWord.length();
        }
        
        if (choiceTextLower.length() > currentIdx) {
            addLabel(currentIdx, choiceTextLower.length(), NON_BOLD_STYLE);
        }
    }

    private void addLabel(int start, int end, String style) {
        String text = originalObject.toString().substring(start, end);
        Label label = new Label();

        if (text.length() > 0) {
            label.setText(text);
            label.setStyle(style);
        }

        labelContainer.getChildren().add(label);
    }
}
