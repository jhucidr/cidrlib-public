/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.searchboxer;

import edu.jhmi.cidr.lib.conveniencestore.stringutils.StringSorter;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Parent;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 *
 * @author Brad
 */
public class SearchBoxer<T> {

    private static final String REGEX_WILDCARD = ".*";

    //TODO - do something about the horizontal scrollbar
    private boolean ignoreTextChange;
    private SearchChoice<T> selectedChoice;

    private List<T> originalObjects;
    private SearchDropdown<T> searchDropdown;
    private PromptTextLabel promptTextLabel;

    private ObjectChosenCallback<T> objectChosenCallback;

    private final TextField textField;

    public SearchBoxer(TextField textField) {
        this.textField = textField;
    }

    public void setup() {
        setupPromptTextAndDropdown();
        setupTextField();
    }

    public void setValues(List<T> values) {
        StringSorter.sort(values);
        this.originalObjects = values;
    }

    public void setPromptText(String promptText) {
        promptTextLabel.setText(" " + promptText);
    }

    public void setPromptTextFontSize(int size) {
        promptTextLabel.setFontSize(size);
    }

    public void setCallbackOnObjectChosen(ObjectChosenCallback<T> callback) {
        objectChosenCallback = callback;
    }

    public T getChosenObject() {
        try {
            return selectedChoice.getOriginalObject();
        } catch (NullPointerException npe) {
            return null;
        }
    }

    private void setupPromptTextAndDropdown() {
        ignoreTextChange = false;

        Bounds bounds = textField.getBoundsInLocal();
        double fieldWidth = bounds.getWidth();
        double fieldHeight = bounds.getHeight();
        double fieldX = textField.getLayoutX();
        double fieldY = textField.getLayoutY();

        promptTextLabel = new PromptTextLabel(fieldWidth, fieldHeight, fieldX, fieldY);
        promptTextLabel.setup();

        searchDropdown = new SearchDropdown<>(fieldWidth, fieldX, fieldY + fieldHeight);
        searchDropdown.setup();
        searchDropdown.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                keyPressed(KeyCode.ENTER);
                mouseEvent.consume();
            }
        });

        Pane closestParentPane = getClosestParentPane();
        closestParentPane.getChildren().add(promptTextLabel);
        closestParentPane.getChildren().add(searchDropdown);
    }

    private void setupTextField() {
        textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String oldVal, String newVal) {
                if (ignoreTextChange) {
                    ignoreTextChange = false;
                } else {
                    search(newVal);
                }
            }
        });

        textField.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                KeyCode keyCode = keyEvent.getCode();

                keyPressed(keyCode);

                if (keyCode == KeyCode.UP || keyCode == KeyCode.DOWN) {
                    keyEvent.consume();
                }
            }
        });

        textField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean wasFocused, Boolean isFocused) {
                if (isFocused) {
                    if (false == textField.getText().trim().isEmpty()) {
                        searchDropdown.toggle(true);
                    }
                } else {
                    searchDropdown.toggle(false);
                }
            }
        });
    }

    private void search(String searchString) {
        selectedChoice = null;

        if (searchString.length() == 0) {
            searchDropdown.toggle(false);
            promptTextLabel.toggle(true);
            return;
        }

        promptTextLabel.toggle(false);
        List<SearchChoice<T>> choices = createChoices(searchString);

        if (choices.isEmpty()) {
            searchDropdown.toggle(false);
        } else {
            searchDropdown.toggle(true);
            searchDropdown.toFront();
            searchDropdown.setItems(choices);
        }
    }

    private List<SearchChoice<T>> createChoices(String searchString) {
        List<SearchChoice<T>> choices = new ArrayList<>();
        String searchStringRegex = REGEX_WILDCARD + searchString.toLowerCase().replaceAll(" ", REGEX_WILDCARD) + REGEX_WILDCARD;

        for (T originalObject : originalObjects) {
            boolean matches = originalObject.toString().toLowerCase().matches(searchStringRegex);

            if (matches) {
                SearchChoice<T> searchChoice = new SearchChoice<>(originalObject, searchString);
                searchChoice.setup();
                choices.add(searchChoice);
            }
        }

        return choices;
    }

    private void keyPressed(KeyCode keyCode) {
        switch (keyCode) {
            case UP:
                searchDropdown.moveSelectionUp();
                break;

            case DOWN:
                searchDropdown.moveSelectionDown();
                break;

            case ESCAPE:
                searchDropdown.toggle(false);
                break;

            case ENTER:
                makeSelection();
                break;
        }
    }

    private void makeSelection() {
        selectedChoice = searchDropdown.getSelection();

        if (null != selectedChoice) {
            String selectedChoiceText = selectedChoice.toString();

            if (false == textField.getText().equals(selectedChoiceText)) {
                ignoreTextChange = true;
            }

            textField.setText(selectedChoiceText);
            textField.positionCaret(selectedChoiceText.length());
            searchDropdown.toggle(false);

            if (null != objectChosenCallback) {
                objectChosenCallback.run(selectedChoice.getOriginalObject());
            }
        }
    }

    private Pane getClosestParentPane() {
        Parent currentParent = textField.getParent();

        while (false == currentParent instanceof Pane) {
            currentParent = currentParent.getParent();
        }

        Pane parentPane = (Pane) currentParent;

        return parentPane;
    }
}
