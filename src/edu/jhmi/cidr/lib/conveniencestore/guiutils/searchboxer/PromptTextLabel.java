/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.searchboxer;

import javafx.scene.control.Label;

/**
 *
 * @author Brad Tibbils
 */
public class PromptTextLabel extends Label {
    private static final String DEFAULT_PROMPT_TEXT = "begin typing to search...";
    private static final String PROMPT_TEXT_CSS_CLASS = "searchBoxerPromptText";
    private static final int DEFAULT_FONT_SIZE = 13;
    private static final int LEFT_PADDING = 6;
    private static final int TOP_PADDING = 0;

    private final double width;
    private final double height;
    private final double layoutX;
    private final double layoutY;

    public PromptTextLabel(double width, double height, double layoutX, double layoutY) {
        super();
        this.width = width;
        this.height = height;
        this.layoutX = layoutX;
        this.layoutY = layoutY;
    }

    public void setup() {
        setPrefWidth(width);
        setPrefHeight(height);
        setLayoutX(layoutX + LEFT_PADDING);
        setLayoutY(layoutY + TOP_PADDING);
        
        getStyleClass().add(PROMPT_TEXT_CSS_CLASS);
        setFontSize(DEFAULT_FONT_SIZE);
        setMouseTransparent(true);
                
        setText(DEFAULT_PROMPT_TEXT);
    }

    public void setFontSize(int size) {
        setStyle("-fx-font-size: " + size + "px;");
    }

    public void toggle(boolean visible) {
        setVisible(visible);
    }
}
