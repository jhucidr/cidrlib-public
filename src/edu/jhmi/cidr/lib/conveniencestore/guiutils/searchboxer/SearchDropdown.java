/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.searchboxer;

import java.util.List;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

/**
 *
 * @author Brad Tibbils
 */
public class SearchDropdown<T> extends ListView<SearchChoice<T>> {

    private static final int DROPDOWN_HEIGHT = 148; //148 - no scroll bar, 160 - scroll bar
    private static final int DROPDOWN_TOP_PADDING = 3;
    private static final String DROPDOWN_ID = "searchBoxerDropdown";
    private static final String DROPSHADOW_CSS_CLASS = "searchBoxerDropshadow";

    private int selectedIndex;

    private final double width;
    private final double layoutX;
    private final double layoutY;

    public SearchDropdown(double width, double layoutX, double layoutY) {
        super();
        this.width = width;
        this.layoutX = layoutX;
        this.layoutY = layoutY;
    }

    public void setup() {
        setVisuals();
        setCellFactory();
        resetSelectedIndex();
        toggle(false);
    }

    public void setItems(List<SearchChoice<T>> choices) {
        getItems().setAll(choices);
        resetSelectedIndex();
    }

    public void toggle(boolean visible) {
        setVisible(visible);
    }

    public void moveSelectionUp() {
        selectedIndex--;
        moveSelection();
        scrollToSelection();
    }

    public void moveSelectionDown() {
        selectedIndex++;
        moveSelection();
        scrollToSelection();
    }

    public SearchChoice<T> getSelection() {
        try {
            return getItems().get(selectedIndex);
        } catch (ArrayIndexOutOfBoundsException ex) {
            return null;
        }
    }

    private void setVisuals() {
        setId(DROPDOWN_ID);
        setPrefWidth(width);
        setPrefHeight(DROPDOWN_HEIGHT);
        setLayoutX(layoutX);
        setLayoutY(layoutY + DROPDOWN_TOP_PADDING);
        getStyleClass().add(DROPSHADOW_CSS_CLASS);
    }

    private void setCellFactory() {
        setCellFactory(new Callback<ListView<SearchChoice<T>>, ListCell<SearchChoice<T>>>() {
            @Override
            public ListCell<SearchChoice<T>> call(ListView<SearchChoice<T>> p) {
                final SearchChoiceListCell<T> searchChoiceListCell = new SearchChoiceListCell<>();

                searchChoiceListCell.addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent mouseEvent) {
                        if (mouseEvent.getEventType() == MouseEvent.MOUSE_ENTERED) {
                            selectedIndex = searchChoiceListCell.getIndex();
                            moveSelection();
                            mouseEvent.consume();
                        } else if (mouseEvent.getEventType() == MouseEvent.MOUSE_EXITED) {
                            if (selectedIndex == 0 || selectedIndex == getItems().size() - 1) {
                                selectedIndex = -1;
                                getSelectionModel().clearSelection();
                                mouseEvent.consume();
                            }
                        }
                    }
                });

                return searchChoiceListCell;
            }
        });
    }

    private void resetSelectedIndex() {
        selectedIndex = -1;
    }

    private void moveSelection() {
        if (selectedIndex < -1) {
            selectedIndex = getItems().size() - 1;
        } else if (selectedIndex == getItems().size()) {
            selectedIndex = -1;
        }

        getSelectionModel().select(selectedIndex);
    }
    
    private void scrollToSelection() {
        scrollTo(selectedIndex - 3); //keep it centered
    }
}
