/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

/**
 *
 * @author Brad Tibbils
 */
public class RemovableListViewCell<T> extends ListCell<T> {

    private final HBox hBox = new HBox();
    private final Label cellText = new Label("");
    private final Pane separator = new Pane();
    private final Button removeButton = new Button("X");

    private final Runnable removeItemCallback;

    public RemovableListViewCell(Runnable removeItemCallback) {
        super();
        this.removeItemCallback = removeItemCallback;
        setupRemoveButton();
        setupHbox();
    }

    private void setupHbox() {
        cellText.setStyle("-fx-font-size: 15px;");
        hBox.getChildren().addAll(cellText, separator, removeButton);
        HBox.setHgrow(separator, Priority.ALWAYS);
    }

    private void setupRemoveButton() {
        Tooltip tooltip = new Tooltip("Remove");
        tooltip.setStyle("-fx-text-fill: white; -fx-font-size: 12px;");
        removeButton.setTooltip(tooltip);
        removeButton.getStyleClass().add("removeListItemButton");

        final RemovableListViewCell thisCell = this;

        removeButton.setOnAction(new EventHandler() {
            @Override
            public void handle(Event t) {
                ListView listView = thisCell.getListView();
                listView.getItems().remove(thisCell.getIndex());

                if (null != removeItemCallback) {
                    removeItemCallback.run();
                }
            }
        });
    }

    @Override
    protected void updateItem(T item, boolean empty) {
        super.updateItem(item, empty);

        setText(null); // Clear text from label of super class b/c it sits on top of this custom cell

        if (empty) {
            setGraphic(null);
        } else {
            cellText.setText(item.toString());
            setGraphic(hBox);
        }
    }
}
