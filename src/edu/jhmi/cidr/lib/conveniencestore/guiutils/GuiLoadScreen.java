/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils;

import edu.jhmi.cidr.lib.conveniencestore.guiutils.guihelperutils.Centering;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.guihelperutils.GridGlassPane;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.guihelperutils.GuiAnimations;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 *
 * @author Brad Tibbils
 */
public class GuiLoadScreen {

    private static final String DEFAULT_MESSAGE = "Please wait...";
    private static final String MESSAGE_STYLE = "-fx-font-size: 22px; -fx-text-fill: white;";
    private static final String MESSAGE_WINDOW_STYLE = "-fx-background-color: black; -fx-border-radius: 25 25 25 25; -fx-background-radius: 25 25 25 25; -fx-effect: dropshadow(gaussian, rgba(0,0,0,0.75), 10, 0, 3, 3)";
    private static final String PATH_TO_LOADER_GIF = "/edu/jhmi/cidr/lib/conveniencestore/guiutils/style/images/loader.gif";
    private static final int PADDING = 30;

    private GridGlassPane glassPane;
    private Runnable backendProcess;
    private boolean backendProcessing = false;

    private final Pane rootPane;

    public GuiLoadScreen(Stage stage) {
        this.rootPane = (Pane) stage.getScene().getRoot();
    }

    public GuiLoadScreen(Pane rootPane) {
        this.rootPane = rootPane;
    }

    public void show() {
        start(DEFAULT_MESSAGE);
    }

    public void show(String message) {
        start(message);
    }

    public void hide() {
        killLoadScren();
    }

    public void setBackgroundProcess(Runnable backendProcess) {
        this.backendProcess = backendProcess;
    }

    private void start(String message) {
        killLoadScren(); //in case there's already a loadscreen there

        glassPane = new GridGlassPane(rootPane);
        glassPane.getChildren().add(buildMessagePane(message));

        rootPane.getChildren().add(glassPane);
        rootPane.getScene().setCursor(Cursor.WAIT);

        glassPane.setOpacity(0);
        GuiAnimations.fadeIn(glassPane);

        if (backendProcess != null) {
            startBackendProcess();
        }
    }

    private StackPane buildMessagePane(String message) {
        StackPane messagePane = new StackPane();

        messagePane.setStyle(MESSAGE_WINDOW_STYLE);
        messagePane.setPadding(new Insets(PADDING));

        messagePane.getChildren().add(buildContentVBox(message));

        return messagePane;
    }

    private VBox buildContentVBox(String message) {
        VBox content = new VBox();
        Pane vSeparator = new Pane();

        ImageView loaderGifImageView = buildLoaderGifImageView();
        Label messageLabel = buildMessageLabel(message);
        HBox imageHBox = Centering.wrapNodeWithHBox(loaderGifImageView);
        vSeparator.setPrefSize(0, PADDING / 2);
        HBox messageHBox = Centering.wrapNodeWithHBox(messageLabel);

        content.getChildren().add(imageHBox);
        content.getChildren().add(vSeparator);
        content.getChildren().add(messageHBox);

        return content;
    }

    private ImageView buildLoaderGifImageView() {
        ImageView loaderGifImageView = new ImageView();

        loaderGifImageView.setImage(new Image(PATH_TO_LOADER_GIF));
        loaderGifImageView.setFitWidth(60);
        loaderGifImageView.setFitHeight(60);

        return loaderGifImageView;
    }

    private Label buildMessageLabel(String message) {
        Label messageLabel = new Label(message);

        messageLabel.setAlignment(Pos.CENTER);
        messageLabel.setStyle(MESSAGE_STYLE);

        return messageLabel;
    }

    private void killLoadScren() {
        if (glassPane != null) {
            GuiAnimations.fadeOut(glassPane, 300, new Runnable() {
                @Override
                public void run() {
                    rootPane.getChildren().remove(glassPane);
                    rootPane.getScene().setCursor(Cursor.DEFAULT);
                }
            });
        }
    }

    private void startBackendProcess() {
        final Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(500), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!backendProcessing) {
                    killLoadScren();
                    timeline.stop();
                }
            }
        }));

        Thread backendThread = new Thread(new Runnable() {
            @Override
            public void run() {
                backendProcessing = true;
                backendProcess.run();
                backendProcessing = false;
            }
        });

        timeline.play();
        backendThread.start();
    }
}
