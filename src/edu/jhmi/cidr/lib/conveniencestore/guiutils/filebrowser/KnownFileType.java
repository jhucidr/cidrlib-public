/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.filebrowser;

/**
 *
 * @author Brad Tibbils
 */
public enum KnownFileType implements FileTypeRestriction {

    ALL_FILES("All Files", "*"),
    CSV("CSV Files (*.csv)", "csv"),
    FASTA("FASTA Files (*.fasta)", "fasta"),
    VCF("VCF Files (*.vcf)", "vcf"),
    XLSX("XLSX Files (*.xlsx)", "xlsx");

    private final String display;
    private final String extension;

    private KnownFileType(String displayName, String extension) {
        this.display = displayName;
        this.extension = extension;
    }

    @Override
    public String getDisplay() {
        return display;
    }

    @Override
    public String getExtension() {
        return extension;
    }
}
