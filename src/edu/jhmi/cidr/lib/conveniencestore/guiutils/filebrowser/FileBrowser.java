/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.filebrowser;

import edu.jhmi.cidr.lib.conveniencestore.fileutils.drivemapper.DriveMapper;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.FileChooserBuilder;
import javafx.stage.Stage;
import javax.swing.filechooser.FileSystemView;
import edu.jhmi.cidr.lib.conveniencestore.stringutils.StringUtils;

/**
 *
 * @author Brad Tibbils
 */
public class FileBrowser {

    private final Stage stage;
    private final Map<TextField, File> rememberedLocationMap = new HashMap<>();
    private boolean useDriveMapper = false;
    private File defaultInitialDirectory = FileSystemView.getFileSystemView().getHomeDirectory();
    private static DriveMapper driveMapper;

    public FileBrowser(Stage stage) {
        this.stage = stage;
    }

    public FileBrowser(Stage stage, File defaultInitialDirectory) {
        this.stage = stage;
        if (defaultInitialDirectory.isDirectory()) {
            this.defaultInitialDirectory = defaultInitialDirectory;
        }
    }

    public void setUseDriveMapper(boolean usingDriveMapper) {
        useDriveMapper = usingDriveMapper;

        if (usingDriveMapper && driveMapper == null) {
            try {
                driveMapper = new DriveMapper();
                driveMapper.setupDriveMap();
            } catch (Exception ex) {
                System.out.println("DriveMapper could not be setup properly by FileBrowser.");
                useDriveMapper = false;
            }
        }
    }

    public void selectFolder(TextField pathField, String windowTitle) {
        goBrowse(pathField, windowTitle, BrowseType.SELECT_FOLDER, null);
    }

    public void openFile(TextField pathField, String windowTitle, FileTypeRestriction fileTypeRestriction) {
        goBrowse(pathField, windowTitle, BrowseType.OPEN_FILE, fileTypeRestriction);
    }

    public void openMultipleFiles(TextField pathField, String windowTitle, FileTypeRestriction fileTypeRestriction, String delimiter) {
        File rememberedLocation = getRememberedLocation(pathField);
        FileChooser multipleFileChooser = getFileChooser(windowTitle, rememberedLocation, fileTypeRestriction);
        List<File> chosenFiles = multipleFileChooser.showOpenMultipleDialog(stage);

        if (chosenFiles != null && chosenFiles.size() > 0) {
            List<String> stringPaths = new ArrayList<>();

            for (File file : chosenFiles) {
                String translatedPath = translatePath(file.getAbsolutePath());
                stringPaths.add(translatedPath);
            }

            pathField.setText(StringUtils.join(stringPaths, delimiter));
            addLocationToMap(pathField, chosenFiles.get(0));
        }
    }

    public void saveFile(TextField pathField, String windowTitle, FileTypeRestriction fileTypeRestriction) {
        goBrowse(pathField, windowTitle, BrowseType.SAVE_FILE, fileTypeRestriction);
    }

    private void goBrowse(TextField pathField, String windowTitle, BrowseType browseType, FileTypeRestriction fileTypeRestriction) {
        File chosen;
        File rememberedLocation = getRememberedLocation(pathField);

        switch (browseType) {
            case SELECT_FOLDER:
                DirectoryChooser directoryChooser = new DirectoryChooser();

                directoryChooser.setTitle(windowTitle);
                directoryChooser.setInitialDirectory(rememberedLocation);
                chosen = directoryChooser.showDialog(stage);
                break;
            case SAVE_FILE:
                FileChooser fileSaveChooser = getFileChooser(windowTitle, rememberedLocation, fileTypeRestriction);
                chosen = fileSaveChooser.showSaveDialog(stage);

                //FileChooser does not automatically add extension on new file, so add it here if user did not type it in
                boolean chosenFileIsNew = null != chosen && !chosen.exists();
                boolean fileTypeIsRestricted = fileTypeRestriction != KnownFileType.ALL_FILES;

                if (chosenFileIsNew && fileTypeIsRestricted) {
                    String chosenFilePath = chosen.getAbsolutePath();
                    String extension = "." + fileTypeRestriction.getExtension();
                    boolean extensionWasNotIncluded = !chosenFilePath.substring(chosenFilePath.length() - 4).equalsIgnoreCase(extension);

                    if (extensionWasNotIncluded) {
                        chosen = new File(chosen.getAbsolutePath().concat(extension));
                    }
                }
                break;
            case OPEN_FILE:
                FileChooser fileChooser = getFileChooser(windowTitle, rememberedLocation, fileTypeRestriction);
                chosen = fileChooser.showOpenDialog(stage);
                break;
            default:
                chosen = null;
                break;
        }

        if (chosen != null) {
            String translatedPath = translatePath(chosen.getAbsolutePath());
            pathField.setText(translatedPath);

            addLocationToMap(pathField, new File(translatedPath));
        }
    }

    private String translatePath(String path) {
        if (useDriveMapper) {
            return driveMapper.getUncPath(path);
        }

        return path;
    }

    private FileChooser getFileChooser(String windowTitle, File rememberedLocation, FileTypeRestriction fileTypeRestriction) {
        return FileChooserBuilder.create()
                .title(windowTitle)
                .initialDirectory(rememberedLocation)
                .extensionFilters(new FileChooser.ExtensionFilter(
                                fileTypeRestriction.getDisplay(), "*." + fileTypeRestriction.getExtension()
                        )).build();
    }

    private File getRememberedLocation(TextField pathField) {
        //if available, use the browsing location used the last time the button for this field was clicked.
        //Else see if a location was set for another field and use that.
        //Else if the browse button hasn't been clicked yet but there's a path in the field (testing scenario), go to that path
        //Else use the desktop
        if (rememberedLocationMap.get(pathField) != null) {
            return rememberedLocationMap.get(pathField);
        } else if (new File(pathField.getText()).exists()) {
            return new File(pathField.getText()).getParentFile();
        } else {
            for (File rememberedPath : rememberedLocationMap.values()) {
                if (rememberedPath != null) {
                    return rememberedPath;
                }
            }
        }

        return defaultInitialDirectory;
    }

    private void addLocationToMap(TextField pathField, File file) {
        File locationToAdd = file.getParentFile().exists() ? file.getParentFile() : file;

        rememberedLocationMap.put(pathField, locationToAdd);
    }
}
