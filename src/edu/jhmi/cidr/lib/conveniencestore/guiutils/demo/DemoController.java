/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.demo;

import edu.jhmi.cidr.lib.conveniencestore.guiutils.filebrowser.FileBrowser;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.GuiDialog;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.GuiLoadScreen;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.demo.progressor.Progressor;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.guiprogressscreen.GuiProgressScreen;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.demo.table.JFXTableDemo;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.demo.table.JFXTableObject;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.filebrowser.FileTypeRestriction;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.filebrowser.KnownFileType;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.guiprogressscreen.ObservantTask;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.searchboxer.SearchBoxer;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.searchboxer.SearchBoxerTestObject;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author btibbil1
 */
public class DemoController implements Initializable {

    private Stage stage;
    private FileBrowser fileBrowser;
    private SearchBoxer<SearchBoxerTestObject> searchBoxer;
    private JFXTableDemo jfxTableDemo;

    @FXML
    private TabPane tabPane;

    //FileBrowser
    @FXML
    private TextField saveFileTextField;
    @FXML
    private TextField selectFileTextField;
    @FXML
    private TextField selectMultipleFilesTextField;
    @FXML
    private TextField selectFolderTextField;

    //Overlays
    @FXML
    private Button guiDialogButton;
    @FXML
    private Button guiLoadScreenButton;
    @FXML
    private Button guiProgressScreenButton;
    @FXML
    private Button guiProgressScreenOTaskButton;

    //SearchBoxer
    @FXML
    private TextField searchBoxerTextField;
    @FXML
    private Label searchBoxerLabel;

    //JFX Table
    @FXML
    private TableView<JFXTableObject> demoTable;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        fileBrowser = new FileBrowser(stage);

        jfxTableDemo = new JFXTableDemo(demoTable);
        jfxTableDemo.addItem(new JFXTableObject(true, 1, "I am error"));
        jfxTableDemo.addItem(new JFXTableObject(false, 1, "Win"));
    }

    //
    // FileBrowser
    //
    @FXML
    void saveFileButtonClicked(ActionEvent event) {
        fileBrowser.saveFile(saveFileTextField, "Select Save File", KnownFileType.CSV);
    }

    @FXML
    void selectFileButtonClicked(ActionEvent event) {
        fileBrowser.openFile(selectFileTextField, "Select File", FileTypeRestriction.Utils.create("Open Office Document (*.odt)", "odt"));
    }

    @FXML
    void selectMultipleFilesButtonClicked(ActionEvent event) {
        fileBrowser.openMultipleFiles(selectMultipleFilesTextField, "Select Multiple Files (hold Ctrl while selecting)", KnownFileType.ALL_FILES, ", ");
    }

    @FXML
    void selectFolderButtonClicked(ActionEvent event) {
        fileBrowser.selectFolder(selectFolderTextField, "Select Folder");
    }

    //
    // Overlays
    //
    private void toggleOverlaysUI(boolean enabled) {
        guiDialogButton.setDisable(!enabled);
        guiLoadScreenButton.setDisable(!enabled);
        guiProgressScreenButton.setDisable(!enabled);
        guiProgressScreenOTaskButton.setDisable(!enabled);
    }

    @FXML
    void guiDialogButtonClicked(ActionEvent event) {
        toggleOverlaysUI(false);

        List<String> errorsToDisplay = Arrays.asList(
                "This is a GuiDialog !",
                "The next 4 errors will not be shown because guiDialog.setMaxLinesToDisplay(2) was used.",
                "Hidden error",
                "Hidden error 2.",
                "Hidden 3",
                "Hidden 4"
        );
        GuiDialog guiDialog = new GuiDialog(stage);
        guiDialog.setMaxLinesToDisplay(2);

        guiDialog.setCallBackOnResponse(GuiDialog.Response.OK, new Runnable() {
            @Override
            public void run() {
                toggleOverlaysUI(true);
                System.out.println("OK was chosen.");
            }
        });
        guiDialog.setCallBackOnResponse(GuiDialog.Response.CANCEL, new Runnable() {
            @Override
            public void run() {
                toggleOverlaysUI(true);
                System.out.println("CANCEL was chosen.");
            }
        });

        guiDialog.showDialog(errorsToDisplay, GuiDialog.GuiDialogButton.OK_CANCEL);
    }

    @FXML
    void guiLoadScreenButtonClicked(ActionEvent event) {
        toggleOverlaysUI(false);

        GuiLoadScreen guiLoadScreen = new GuiLoadScreen(stage);
        guiLoadScreen.setBackgroundProcess(new Runnable() {
            @Override
            public void run() {
                Integer i = 0;
                int total = 50000;
                while (i < total) {
                    i++;
                    System.out.println(i + " / " + total);
                }
                toggleOverlaysUI(true);
            }
        });

        guiLoadScreen.show("Counting numbers...");
    }

    @FXML
    void guiProgressScreenButtonClicked(ActionEvent event) {
        toggleOverlaysUI(false);

        final Task task = new Task() {
            @Override
            protected Object call() throws Exception {
                int total = 100;
                for (int i = 1; i <= total; i++) {
                    int percentComplete = (int) Math.floor((float) i / total * 100);

                    String title;
                    if (percentComplete < 33) {
                        title = "Doing a thing...";
                    } else if (percentComplete < 66) {
                        title = "Doing another thing...";
                    } else if (percentComplete < 100) {
                        title = "Finishing up...";
                    } else {
                        title = "Complete !";
                    }

                    updateTitle(title); //label above progress bar
                    updateMessage(percentComplete + "%"); //label on progress bar
                    updateProgress(i, total); //progress bar value
                    Thread.sleep(50);
                }
                Thread.sleep(500); //pause for effect
                return true;
            }
        };

        GuiProgressScreen guiProgressScreen = new GuiProgressScreen(task, stage);
        guiProgressScreen.setCallback(new Runnable() {
            @Override
            public void run() {
                toggleOverlaysUI(true);
                System.out.println("Progress screen is done.");
            }
        });
        guiProgressScreen.setCancelButtonCallback(new Runnable() {
            @Override
            public void run() {
                toggleOverlaysUI(true);
                task.cancel();
                System.out.println("Progress screen was cancelled!");
            }
        });
        guiProgressScreen.start();
    }

    @FXML
    void guiProgressScreenWithObservantTaskButtonClicked(ActionEvent event) {
        toggleOverlaysUI(false);

        ObservantTask observantTask = new ObservantTask();
        GuiProgressScreen guiProgressScreen = new GuiProgressScreen(observantTask, stage);
        guiProgressScreen.setCallback(new Runnable() {
            @Override
            public void run() {
                toggleOverlaysUI(true);
            }
        });
        guiProgressScreen.start();

        Progressor progressor = new Progressor();
        progressor.addObserver(observantTask);

        new Thread(progressor).start();
    }

    //
    // SearchBoxer
    //
    public void setupSearchBoxer() {
        searchBoxer = new SearchBoxer<>(searchBoxerTextField);
        searchBoxer.setup();

        List<String> strings = Arrays.asList(
                "apple", "adam", "adamantium", "adamant", "ardvark",
                "astroturf", "astro", "abbreviated", "abbreviation",
                "apocalyptic", "always", "ardent", "abhorent", "antimatter",
                "aloysius", "adirondack", "astrid", "annoyance", "appomattox",
                "already", "alright", "astronomical", "antietam"
        );

//        List<String> strings = Arrays.asList(
//                "really long name for something here so I can test if it will wrap around and also what it will look like so I'll just keep typing.",
//                "this is another thing that got typed in and is also prettttttttly long. I wonder if it will wrap around once or twice? I guess we'll see!"
//        );
        List<SearchBoxerTestObject> testObjects = new ArrayList<>();
        int i = 0;
        for (String string : strings) {
            i++;
            testObjects.add(new SearchBoxerTestObject(string, i));
        }

        searchBoxer.setPromptText("Start typing a word that starts with 'a' (or any part of that word).");
        searchBoxer.setValues(testObjects);
    }

    @FXML
    void searchBoxerButtonClicked(ActionEvent event) {
        String content;
        try {
            content = searchBoxer.getChosenObject().getContents();
        } catch (NullPointerException npe) {
            content = "(Nothing chosen.)";
        }

        searchBoxerLabel.setText(content);
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
