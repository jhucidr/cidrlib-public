/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.jhmi.cidr.lib.conveniencestore.guiutils.demo.table;

import edu.jhmi.cidr.lib.conveniencestore.guiutils.jfxtable.JFXTableView;
import java.util.Arrays;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;

/**
 *
 * @author Alice Sanchez
 */
public class JFXTableDemo extends JFXTableView<JFXTableObject> {

    public JFXTableDemo(TableView<JFXTableObject> tableView) {
        super(tableView, Arrays.asList(JFXTableObject.JFXTableObjectColumn.values()));
    }

    @Override
    protected void setUpTableSettings(TableView<JFXTableObject> tableView) {
        super.setUpTableSettings(tableView);
        
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    @Override
    protected ContextMenu getContextMenu(TableView<JFXTableObject> tableView) {
        MenuItem item = new MenuItem("Hi am a context menu hehe");
        ContextMenu menu = new ContextMenu(item);
        
        return menu;
    }

}
