/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.jhmi.cidr.lib.conveniencestore.guiutils.demo.table;

import edu.jhmi.cidr.lib.conveniencestore.guiutils.jfxtable.JFXTableColumn;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.jfxtable.cells.JFXTableCellType;
import javafx.scene.control.TableColumn;

/**
 *
 * @author Alice Sanchez
 */
public class JFXTableObject {
    private final boolean failure;
    private final long id;
    private final String name;

    public JFXTableObject(boolean failure, long id, String name) {
        this.failure = failure;
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isFailure() {
        return failure;
    }
    
    public enum JFXTableObjectColumn implements JFXTableColumn<JFXTableObject>{

        FAILURE("Failure", "failure", JFXTableCellType.BOOLEAN_ICON),
        ID("ID", "id", JFXTableCellType.LONG),
        NAME("Name", "name", JFXTableCellType.STRING);
            
           private final String columnName;
           private final String fieldName;
           private final JFXTableCellType cellType;

        private JFXTableObjectColumn(String columnName, String fieldName, JFXTableCellType cellType) {
            this.columnName = columnName;
            this.fieldName = fieldName;
            this.cellType = cellType;
        }
           
        @Override
        public String getColumnName() {
            return columnName;
        }

        @Override
        public String getFieldName() {
            return fieldName;
        }

        @Override
        public JFXTableCellType getTableCellType() {
           return cellType;
        }

        @Override
        public TableColumn<JFXTableObject, ?> getTableColumn() {
            return cellType.getTableColumn(columnName, fieldName);
        }

    }
}
