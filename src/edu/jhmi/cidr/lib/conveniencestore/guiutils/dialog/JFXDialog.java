/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.dialog;

import java.util.List;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.GridPaneBuilder;
import javafx.scene.layout.HBox;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;
import javafx.scene.paint.Color;

/**
 *
 * @author Alice Sanchez
 */
public class JFXDialog {

    private static final String DEFAULT_BACKGROUND_STYLE = "-fx-background-color: rgba(240,240,240,0.8)";
    private static final String DEFAULT_DIALOG_BOX_STYLE = "-fx-background-color: rgba(240,240,240);-fx-border-radius: 10 10 10 10;-fx-background-radius: 10 10 10 10;";

    private final Pane rootPane;
    private final Pane backgroundPane;
    private final Pane dialogBox;

    public JFXDialog(Pane rootPane, Pane backgroundPane, Pane dialogBox) {
        this.rootPane = rootPane;
        this.backgroundPane = backgroundPane;
        this.dialogBox = dialogBox;
    }

    private GridPane getDefaultBackground(String style) {
        return GridPaneBuilder.create()
                .alignment(Pos.CENTER)
                .style(style)
                .build();
    }

    private GridPane getDefaultBackground() {
        return getDefaultBackground(DEFAULT_BACKGROUND_STYLE);
    }

    private VBox getDefaultDialogBox(String style) {
        return VBoxBuilder.create()
                .alignment(Pos.CENTER)
                .spacing(5.0f)
                .padding(new Insets(12.0f))
                .effect(new DropShadow(32.0f, Color.BLACK))
                .style(style)
                .build();
    }

    private VBox getDefaultDialogBox() {
        return getDefaultDialogBox(DEFAULT_DIALOG_BOX_STYLE);
    }

    private EventHandler<ActionEvent> getDefaultOnActionEvent() {
        return new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                closeDialog();
            }
        };
    }

    private HBox getButtonPane(List<Button> buttons) {
        HBox buttonBox = HBoxBuilder.create()
                .alignment(Pos.CENTER)
                .spacing(10.0f)
                .padding(new Insets(10.0f))
                .build();

        if (buttons.isEmpty()) {
            Button defaultButton = new Button("OK");

            defaultButton.setOnAction(getDefaultOnActionEvent());
            buttonBox.getChildren().add(defaultButton);
        } else {
            for (Button button : buttons) {
                button.addEventHandler(ActionEvent.ACTION, getDefaultOnActionEvent());
                buttonBox.getChildren().add(button);
            }
        }

        return buttonBox;
    }

    protected JFXDialog(JFXDialogBuilder builder) {
        if (builder.rootPane == null) {
            throw new NullPointerException("rootPane can not be null");
        }

        this.rootPane = builder.rootPane;
        this.dialogBox = getDefaultDialogBox();
        this.backgroundPane = getDefaultBackground();

        if (builder.backgroundStyle != null) {
            this.backgroundPane.setStyle(builder.backgroundStyle);
        }

        if (builder.dialogBoxStyle != null) {
            this.dialogBox.setStyle(builder.dialogBoxStyle);
        }

        if (builder.label != null) {
            this.dialogBox.getChildren().add(HBoxBuilder.create()
                    .alignment(Pos.CENTER)
                    .children(builder.label)
                    .build());
        }

        if (builder.dialogBoxWidth != null) {
//            this.dialogBox.setMaxWidth(builder.dialogBoxWidth);
            this.dialogBox.setMinWidth(builder.dialogBoxWidth);
        }

        if (builder.dialogBoxHeight != null) {
//            this.dialogBox.setMaxHeight(builder.dialogBoxHeight);
            this.dialogBox.setMinHeight(builder.dialogBoxHeight);
        }

        this.dialogBox.getChildren().addAll(builder.nodes);
        this.dialogBox.getChildren().add(getButtonPane(builder.buttons));

//        this.dialogBox.autosize();
        this.backgroundPane.getChildren().add(this.dialogBox);
    }

    public Pane getDialogPane() {
        return backgroundPane;
    }

    public void showDialog() {
        rootPane.getChildren().add(backgroundPane);
//        backgroundPane.autosize();
    }

    public void closeDialog() {
        rootPane.getChildren().remove(backgroundPane);
    }

}
