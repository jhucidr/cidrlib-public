/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.jhmi.cidr.lib.conveniencestore.guiutils.dialog;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author Alice Sanchez
 */
public class JFXDialogBuilder {
    private static final Font DEFAULT_FONT = Font.font("System", FontWeight.BOLD, 18.0f);
    
    protected Pane rootPane = null;
    protected String backgroundStyle = null;
    protected String dialogBoxStyle = null;
    protected Label label = null;
    protected Double dialogBoxWidth = null;
    protected Double dialogBoxHeight = null;
    protected final List<Node> nodes = new LinkedList<>();
    protected final List<Button> buttons = new LinkedList<>();

    public JFXDialogBuilder(Pane rootPane) {
        this.rootPane = rootPane;
    }
    
//    public JFXDialogBuilder rootPane(Pane rootPane) {
//        this.rootPane = rootPane;
//        return this;
//    }

    public JFXDialogBuilder backgroundStyle(String backgroundStyle) {
        this.backgroundStyle = backgroundStyle;
        return this;
    }

    public JFXDialogBuilder dialogBoxStyle(String dialogBoxStyle) {
        this.dialogBoxStyle = dialogBoxStyle;
        return this;
    }
    
    public JFXDialogBuilder dialogBoxWidth(double width){
        this.dialogBoxWidth = width;
        return this;
    }
    
    public JFXDialogBuilder dialogBoxHeight(double height){
        this.dialogBoxHeight = height;
        return this;
    }
  
    public JFXDialogBuilder label(Label label){
        this.nodes.add(label);
        return this;
    }
    
    public JFXDialogBuilder title(String text){
        Label newLabel = new Label(text);

        newLabel.setFont(DEFAULT_FONT);
        this.label = newLabel;
        
        return this;
    }
    
    public JFXDialogBuilder textField(String label, TextField textField){
        Label textFieldLabel = new Label(label);
        HBox hBox = new HBox();
        
        hBox.setSpacing(10.0);
        hBox.getChildren().addAll(textFieldLabel, textField);
        HBox.setHgrow(textField, Priority.ALWAYS);
        
        this.nodes.add(hBox);
        
        return this;
    }
    
    public JFXDialogBuilder node(Node node){
        this.nodes.add(node);
        return this;
    }
    
    public JFXDialogBuilder nodes(Node... nodes){
        this.nodes.addAll(Arrays.asList(nodes));
        return this;
    }
    
    public JFXDialogBuilder button(Button button){
        this.buttons.add(button);
        return this;
    }
    
    public JFXDialogBuilder button(Button... buttons){
        this.buttons.addAll(Arrays.asList(buttons));
        return this;
    }
    
    public JFXDialog build(){
        return new JFXDialog(this);
    }
}
