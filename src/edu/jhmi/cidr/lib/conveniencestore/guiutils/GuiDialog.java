/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils;

import edu.jhmi.cidr.lib.conveniencestore.guiutils.guihelperutils.Centering;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.guihelperutils.GridGlassPane;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.guihelperutils.GuiAnimations;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Brad Tibbils
 */
public class GuiDialog {

    private static final long FADE_DURATION = 300;
    private static final String BUTTON_CSS_CLASS = "clearWhiteButton";
    private static final String DROPSHADOW_CSS_CLASS = "dropShadow";
    private static final String DISPLAY_TEXT_CSS_CLASS = "whiteBoldText";
    private static final String ROUNDED_CORNERS_CSS_CLASS = "roundedCorners";
    private static final String DIALOG_WINDOW_DEFAULT_COLOR = "#005DE8";

    private HBox buttonWrap;
    private VBox dialogWindow;
    private int messageLimit = 5;
    private String windowColor = DIALOG_WINDOW_DEFAULT_COLOR;

    //TODO - Give option to set your own button text (custom interface).
    private final Pane backgroundPane;
    private final Button okButton = new Button("OK");
    private final Button cancelButton = new Button("Cancel");

    private final Pane root;

    public GuiDialog(Stage stage) {
        this.root = (Pane) stage.getScene().getRoot();
        this.backgroundPane = new GridGlassPane(stage);
        setDefaultButtonActions();
    }

    public GuiDialog(Pane root) {
        this.root = root;
        this.backgroundPane = new GridGlassPane(root);
        setDefaultButtonActions();
    }

    public void showDialog(String dialogText, GuiDialog.GuiDialogButton buttonChoices) {
        showDialog(Arrays.asList(dialogText), buttonChoices);
    }

    public void showDialog(List<String> dialogText, GuiDialog.GuiDialogButton buttonChoices) {
        root.getChildren().add(backgroundPane);

        buildDialogWindow();
        addText(dialogText);
        addButons(buttonChoices);
        addResizeListeners();

        okButton.setDefaultButton(true);
    }

    public void setWindowColor(String windowColorHexCode) {
        this.windowColor = windowColorHexCode;
    }

    public void setMaxLinesToDisplay(int maxLines) {
        this.messageLimit = maxLines;
    }

    public void setCallBackOnResponse(GuiDialog.Response response, final Runnable callback) {
        EventHandler<ActionEvent> handler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                closeDialog();
                callback.run();
            }
        };

        switch (response) {
            case OK:
                okButton.setOnAction(handler);
                break;
            case CANCEL:
                cancelButton.setOnAction(handler);
                break;
        }
    }

    private void setDefaultButtonActions() {
        EventHandler<ActionEvent> handler = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                closeDialog();
            }
        };
        
        okButton.setOnAction(handler);
        cancelButton.setOnAction(handler);
    }
    
    private void buildDialogWindow() {
        dialogWindow = new VBox();
        backgroundPane.getChildren().add(dialogWindow);
        sizeDialogWindow();
        styleDialogWindow();
    }

    private void styleDialogWindow() {
        dialogWindow.setStyle("-fx-background-color: " + windowColor);
        dialogWindow.getStyleClass().add(ROUNDED_CORNERS_CSS_CLASS);
        dialogWindow.getStyleClass().add(DROPSHADOW_CSS_CLASS);
    }

    private void sizeDialogWindow() {
        dialogWindow.setPrefWidth(root.getWidth() * 0.7);
        dialogWindow.setPrefHeight(root.getHeight() * 0.7);
        Centering.centerGuiElementOnStage(root, dialogWindow);
    }

    private void addText(List<String> dialogText) {
        List<String> truncatedDialogText = truncateErrors(dialogText);
        List<Label> labels = buildLabelsForDialogText(truncatedDialogText);
        dialogWindow.getChildren().addAll(labels);
    }

    private void addButons(GuiDialog.GuiDialogButton buttonChoices) {
        setupButtonArea();

        Pane leftSpacer = new Pane();
        Pane rightSpacer = new Pane();
        HBox.setHgrow(leftSpacer, Priority.SOMETIMES);
        HBox.setHgrow(rightSpacer, Priority.SOMETIMES);

        buttonWrap.getChildren().add(leftSpacer);

        okButton.getStyleClass().add(BUTTON_CSS_CLASS);
        buttonWrap.getChildren().add(okButton);

        if (buttonChoices == GuiDialogButton.OK_CANCEL) {
            Pane hSpacer = new Pane();
            HBox.setHgrow(hSpacer, Priority.SOMETIMES);
            buttonWrap.getChildren().add(hSpacer);

            cancelButton.getStyleClass().add(BUTTON_CSS_CLASS);
            buttonWrap.getChildren().add(cancelButton);
        }

        buttonWrap.getChildren().add(rightSpacer);
    }

    private void setupButtonArea() {
        buttonWrap = new HBox();

        Pane dialogSpacer = new Pane();
        Pane buttonSpacer = new Pane();

        VBox.setVgrow(dialogSpacer, Priority.SOMETIMES);
        VBox.setVgrow(buttonSpacer, Priority.SOMETIMES);

        dialogWindow.getChildren().add(dialogSpacer);
        dialogWindow.getChildren().add(buttonWrap);
        dialogWindow.getChildren().add(buttonSpacer);
    }

    private void addResizeListeners() {
        root.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number oldValue, Number newValue) {
                sizeDialogWindow();
            }
        });

        root.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number oldValue, Number newValue) {
                sizeDialogWindow();
            }
        });
    }

    private List<String> truncateErrors(List<String> errors) {
        List<String> truncatedErrors = new ArrayList<>();

        if (errors.size() > messageLimit) {
            int extraErrorCount = errors.size() - messageLimit;

            truncatedErrors.addAll(errors.subList(0, messageLimit));
            truncatedErrors.add(extraErrorCount + " more...");
        } else {
            truncatedErrors.addAll(errors);
        }

        return truncatedErrors;
    }

    private void closeDialog() {
        GuiAnimations.fadeOut(backgroundPane, FADE_DURATION, new Runnable() {
            @Override
            public void run() {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        okButton.setDefaultButton(false);
                        backgroundPane.setVisible(false);
                        root.getChildren().remove(backgroundPane);
                    }
                });
            }
        });
    }

    private List<Label> buildLabelsForDialogText(List<String> dialogText) {
        List<Label> labels = new ArrayList<>();

        for (String text : dialogText) {
            Label label = new Label(text);
            label.getStyleClass().add(DISPLAY_TEXT_CSS_CLASS);
            label.wrapTextProperty().setValue(true);
            labels.add(label);
        }

        return labels;
    }

    public enum GuiDialogButton {

        OK, OK_CANCEL;
    }

    public enum Response {

        OK, CANCEL;
    }
}
