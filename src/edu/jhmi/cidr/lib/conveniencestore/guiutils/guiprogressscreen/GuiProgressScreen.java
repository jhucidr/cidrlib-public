/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.guiprogressscreen;

import edu.jhmi.cidr.lib.conveniencestore.guiutils.guihelperutils.GridGlassPane;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Brad Tibbils
 */
public class GuiProgressScreen {

    private static final int PROGRESS_BAR_HEIGHT = 30;
    private static final int CANCEL_BUTTON_WIDTH = 150;
    private static final String CSS_CLASS = "gui-progress-screen-progress-bar";
    private static final String THREAD_NAME = "GuiProgressBar Background Thread";
    
    private Runnable completedCallback;
    private Runnable cancelledCallback;
    private boolean running = false;

    private final Task task;
    private final Stage stage;
    private final Pane rootPane;
    private final GridGlassPane gridPane;

    public static int calculatePercentComplete(int currentProgress, int progressGoal) {
        return (int) Math.floor(((double) currentProgress / progressGoal) * 100);
    }
    
    public GuiProgressScreen(Task backgroundTask, Stage stage) {
        this.task = backgroundTask;
        this.stage = stage;
        this.rootPane = (Pane) stage.getScene().getRoot();
        this.gridPane = new GridGlassPane(stage);
    }

    public void start() {
        setup();

        Thread backgroundThread = new Thread(task, THREAD_NAME);
        backgroundThread.start();
    }
    
    public void close() {
        closeProgressScreen();
    }

    public void setCallback(Runnable callback) {
        this.completedCallback = callback;
    }

    public void setCancelButtonCallback(Runnable callback) {
        this.cancelledCallback = callback;
    }

    public boolean isRunning() {
        return running;
    }

    private void setup() {
        running = true;
        int progressBarWidth = (int) stage.getWidth() / 2;

        Label statusLabel = createStatusLabel();
        Label progressLabel = createProgressLabel();
        ProgressBar progressBar = createProgressBar(progressBarWidth);
        StackPane stackPane = new StackPane();

        stackPane.getChildren().addAll(progressBar, progressLabel);
        setupGridPane(statusLabel, stackPane);
        rootPane.getChildren().add(gridPane);

        if (null != cancelledCallback) {
            setupCancelButton();
        }

        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                closeProgressScreen();
                if (null != completedCallback) {
                    completedCallback.run();
                }
            }
        });
    }

    private Label createStatusLabel() {
        Label statusLabel = new Label();

        statusLabel.setStyle("-fx-font-style: italic; -fx-font-size: 13px;");
        statusLabel.setAlignment(Pos.CENTER_LEFT);
        statusLabel.setPrefWidth(stage.getWidth() / 2);
        statusLabel.textProperty().bind(task.titleProperty());

        return statusLabel;
    }

    private Label createProgressLabel() {
        Label progressLabel = new Label();

        progressLabel.setStyle("-fx-font-size: 17px; -fx-padding: 0 0 3 0");
        progressLabel.textProperty().bind(task.messageProperty());

        return progressLabel;
    }

    private ProgressBar createProgressBar(int width) {
        ProgressBar progressBar = new ProgressBar();

        progressBar.setPrefWidth(width);
        progressBar.setPrefHeight(PROGRESS_BAR_HEIGHT);
        progressBar.getStyleClass().add(CSS_CLASS);

        progressBar.progressProperty().bind(task.progressProperty());

        return progressBar;
    }

    private void setupGridPane(Label statusLabel, StackPane stackPane) {
        RowConstraints row1 = new RowConstraints();
        row1.setPercentHeight(37);
        row1.setValignment(VPos.BOTTOM);

        RowConstraints row2 = new RowConstraints(33);
        row2.setValignment(VPos.BOTTOM);

        gridPane.getRowConstraints().addAll(row1, row2);

        gridPane.add(statusLabel, 0, 0);
        gridPane.add(stackPane, 0, 1);

        gridPane.setPrefSize(stage.getWidth(), stage.getHeight());
        gridPane.setAlignment(Pos.TOP_CENTER);
        gridPane.setStyle("-fx-background-color: rgba(173, 173, 173, 0.99);");
    }

    private void setupCancelButton() {
        Button cancelButton = new Button("CANCEL");
        cancelButton.getStyleClass().add("cancelButton");
        cancelButton.setPrefWidth(CANCEL_BUTTON_WIDTH);
        cancelButton.setPrefHeight(25);
        cancelButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                cancelledCallback.run();
                closeProgressScreen();
            }
        });

        gridPane.add(cancelButton, 0, 2);
        GridPane.setHalignment(cancelButton, HPos.CENTER);
        RowConstraints cancelButtonRow = new RowConstraints(60);
        gridPane.getRowConstraints().add(cancelButtonRow);
    }

    private void closeProgressScreen() {
        running = false;
        rootPane.getChildren().remove(gridPane);
    }
}
