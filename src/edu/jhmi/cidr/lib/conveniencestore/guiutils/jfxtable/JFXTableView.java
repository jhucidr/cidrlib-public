/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.jfxtable;

import com.google.common.base.Optional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Alice Sanchez
 * @param <T>
 */
public class JFXTableView<T> extends Observable {
    
    private final TableView<T> tableView;
    private final Map<JFXTableColumn, TableColumn<T, ?>> columnMap = new HashMap<>();
    private Optional<TableColumn> defaultSortColumn = Optional.absent();
    private final IntegerProperty numberSelectedRowsProperty = new SimpleIntegerProperty();
    private final IntegerProperty numberRowsProperty = new SimpleIntegerProperty();
    
    public JFXTableView(TableView<T> tableView, List<? extends JFXTableColumn> columns) {
        this.tableView = tableView;
        
        setUpTable(columns);
    }
    
    public TableView<T> getTableView() {
        return tableView;
    }
    
    public void addItem(T problem) {
        tableView.getItems().add(problem);
    }
    
    public void setItems(Collection<T> items) {
        tableView.setItems(FXCollections.observableArrayList(items));
        refreshTable(false);
    }
    
    public void removeItem(T item) {
        tableView.getItems().remove(item);
    }
    
    public T getSelectedItem() {
        return tableView.getSelectionModel().getSelectedItem();
    }
    
    public List<T> getSelectedItems() {
        return tableView.getSelectionModel().getSelectedItems();
    }
    
    protected void setDefaultSortColumn(TableColumn tableColumn) {
        this.defaultSortColumn = Optional.of(tableColumn);
    }
    
    private void setUpTable(List<? extends JFXTableColumn> columns) {
        setUpTableColumns(tableView, columns);
        setUpContextMenu(tableView);
        setUpTableSettings(tableView);
        
        tableView.getSelectionModel().getSelectedItems().addListener(new ListChangeListener<T>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends T> change) {
                numberSelectedRowsProperty.set(tableView.getSelectionModel().getSelectedItems().size());
            }
        });
//        tableView.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
//            
//            @Override
//            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
//            }
//        });
        
        tableView.getItems().addListener(new ListChangeListener<T>() {

            @Override
            public void onChanged(ListChangeListener.Change<? extends T> change) {
                numberRowsProperty.set(tableView.getItems().size());
                tableView.getSelectionModel().clearSelection();
            }
        });
    }

    public IntegerProperty numberSelectedRowsProperty() {
        return numberSelectedRowsProperty;
    }

    public IntegerProperty numberRowsProperty() {
        return numberRowsProperty;
    }
    
    protected void setUpTableSettings(TableView<T> tableView) {
        //NO-OP
    }
    
    protected void setUpTableColumns(TableView<T> tableView, List<? extends JFXTableColumn> columns) {
        for (JFXTableColumn column : columns) {
            @SuppressWarnings("unchecked")
            TableColumn<T, ?> tableColumn = column.getTableColumn();
            
            tableView.getColumns().add(tableColumn);
            columnMap.put(column, tableColumn);
        }
    }
    
    protected void setUpContextMenu(final TableView<T> tableView) {
        ContextMenu menu = getContextMenu(tableView);
        
        if (menu != null) {
            tableView.setContextMenu(menu);
            
            tableView.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                
                @Override
                public void handle(MouseEvent t) {
                    if (t.getButton() == MouseButton.SECONDARY) {
                        tableView.getContextMenu().show(tableView, t.getSceneX(), t.getSceneY());
                    }
                }
            });
        }
    }
    
    protected ContextMenu getContextMenu(TableView<T> tableView) {
        return null;
    }
    
    public Map<JFXTableColumn, TableColumn<T, ?>> getColumnMap() {
        return columnMap;
    }
    
    public void refreshTable(boolean informObservers) {
        if (!tableView.getColumns().isEmpty()) {
            TableColumn<T, ?> column = tableView.getColumns().get(0);
            
            column.setVisible(false);
            column.setVisible(true);
  
            if (!tableView.getSortOrder().isEmpty()) {
                List<TableColumn<T, ?>> sortOrder = new ArrayList<>(tableView.getSortOrder());
                
                tableView.getSortOrder().clear();
                tableView.getSortOrder().addAll(sortOrder);
            } else if (defaultSortColumn.isPresent()) {
                tableView.getSortOrder().add(defaultSortColumn.get());
            }
            
            if (informObservers) {
                updateObservers(JFXTableMessage.TABLE_REFRESH);
            }
        }
    }
    
    protected void updateObservers(Message msg) {
        setChanged();
        notifyObservers(msg);
    }
    
    public interface Message {
        
    }
    
    protected enum JFXTableMessage implements Message {
        
        TABLE_REFRESH;
    }
}
