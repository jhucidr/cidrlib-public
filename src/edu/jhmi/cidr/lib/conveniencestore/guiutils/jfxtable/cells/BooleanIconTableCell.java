/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.jfxtable.cells;

import javafx.scene.control.TableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Alice Sanchez
 */
public class BooleanIconTableCell<T> extends TableCell<T, Boolean> {

    private static final Image CROSS_ICON;
    private static final Image CHECK_MARK_ICON;

    static {
        CROSS_ICON = new Image(BooleanIconTableCell.class.getResourceAsStream("cross.png"));
        CHECK_MARK_ICON = new Image(BooleanIconTableCell.class.getResourceAsStream("check_mark.png"));
    }

    private final ImageView icon = new ImageView();

    @Override
    protected void updateItem(Boolean t, boolean bln) {
        super.updateItem(t, bln);

        if (t != null) {
            icon.setPreserveRatio(true);
            icon.setFitHeight(16);

            if (t == false) {
                icon.setImage(CROSS_ICON);
            } else {
                icon.setImage(CHECK_MARK_ICON);
            }

            this.setGraphic(icon);
        }
    }

//    @Override
//    public void updateSelected(boolean bln) {
//        if (this.getItem() != null && this.getItem().isSelectable()) {
//            super.updateSelected(bln);
//        }
//    }
}
