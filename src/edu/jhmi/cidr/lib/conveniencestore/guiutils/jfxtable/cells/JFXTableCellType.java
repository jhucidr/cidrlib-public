/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.jfxtable.cells;

import edu.jhmi.cidr.lib.Displayable;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

/**
 *
 * @author Alice Sanchez
 */
public enum JFXTableCellType {

    STRING(new JFXTableCellFactory<String>() {

        @Override
        public <T> Callback<TableColumn<T, String>, TableCell<T, String>> getCallback(TableColumn<T, String> tableColumn) {
            return new Callback<TableColumn<T, String>, TableCell<T, String>>() {

                @Override
                public TableCell<T, String> call(TableColumn<T, String> p) {
                    return new TableCell<T, String>() {

                        @Override
                        protected void updateItem(String t, boolean bln) {
                            super.updateItem(t, bln);

                            if (t != null) {
                                this.setText(t);
                            } else {
                                this.setText("");
                            }
                        }

                    };
                }
            };
        }

        @Override
        public <T> PropertyValueFactory<T, String> getValueFactory(String fieldName) {
            return new PropertyValueFactory<>(fieldName);
        }

    }),
    OBJECT(new JFXTableCellFactory<Object>() {

        @Override
        public <T> Callback<TableColumn<T, Object>, TableCell<T, Object>> getCallback(TableColumn<T, Object> tableColumn) {
            return new Callback<TableColumn<T, Object>, TableCell<T, Object>>() {

                @Override
                public TableCell<T, Object> call(TableColumn<T, Object> p) {
                    return new TableCell<T, Object>() {

                        @Override
                        protected void updateItem(Object t, boolean bln) {
                            super.updateItem(t, bln);

                            if (t != null) {
                                this.setText(t.toString());
                            } else {
                                this.setText("");
                            }
                        }

                    };
                }
            };
        }

        @Override
        public <T> PropertyValueFactory<T, Object> getValueFactory(String fieldName) {
            return new PropertyValueFactory<>(fieldName);
        }
    }),
    DISPLAYABLE(new JFXTableCellFactory<Displayable>() {

        @Override
        public <T> Callback<TableColumn<T, Displayable>, TableCell<T, Displayable>> getCallback(TableColumn<T, Displayable> tableColumn) {
            return new Callback<TableColumn<T, Displayable>, TableCell<T, Displayable>>() {

                @Override
                public TableCell<T, Displayable> call(TableColumn<T, Displayable> p) {
                    return new TableCell<T, Displayable>() {

                        @Override
                        protected void updateItem(Displayable t, boolean bln) {
                            super.updateItem(t, bln);

                            if (t != null) {
                                this.setText(t.getDisplayString());
                            } else {
                                this.setText("");
                            }
                        }

                    };
                }
            };
        }

        @Override
        public <T> PropertyValueFactory<T, Displayable> getValueFactory(String fieldName) {
            return new PropertyValueFactory<>(fieldName);
        }
    }),
    LONG(new JFXTableCellFactory<Long>() {

        @Override
        public <T> Callback<TableColumn<T, Long>, TableCell<T, Long>> getCallback(TableColumn<T, Long> tableColumn) {
            return new Callback<TableColumn<T, Long>, TableCell<T, Long>>() {

                @Override
                public TableCell<T, Long> call(TableColumn<T, Long> p) {
                    return new TableCell<T, Long>() {

                        @Override
                        protected void updateItem(Long t, boolean bln) {
                            super.updateItem(t, bln);

                            if (t != null) {
                                this.setText(String.valueOf(t));
                            } else {
                                this.setText("");
                            }
                        }

                    };
                }
            };
        }

        @Override
        public <T> PropertyValueFactory<T, Long> getValueFactory(String fieldName) {
            return new PropertyValueFactory<>(fieldName);
        }

    }),
    DOUBLE(new JFXTableCellFactory<Double>() {    
        @Override
        public <T> Callback<TableColumn<T, Double>, TableCell<T, Double>> getCallback(TableColumn<T, Double> tableColumn) {
            return new Callback<TableColumn<T, Double>, TableCell<T, Double>>() {

                @Override
                public TableCell<T, Double> call(TableColumn<T, Double> p) {
                    return new TableCell<T, Double>() {

                        @Override
                        protected void updateItem(Double t, boolean bln) {
                            super.updateItem(t, bln);

                            if (t != null) {
                                this.setText(String.valueOf(t));
                            } else {
                                this.setText("");
                            }
                        }

                    };
                }
            };
        }

        @Override
        public <T> PropertyValueFactory<T, Double> getValueFactory(String fieldName) {
            return new PropertyValueFactory<>(fieldName);
        }

    }),
    BOOLEAN_ICON(new JFXTableCellFactory<Boolean>() {

        @Override
        public <T> Callback<TableColumn<T, Boolean>, TableCell<T, Boolean>> getCallback(TableColumn<T, Boolean> tableColumn) {
            return new Callback<TableColumn<T, Boolean>, TableCell<T, Boolean>>() {

                @Override
                public TableCell<T, Boolean> call(TableColumn<T, Boolean> p) {
                    return new BooleanIconTableCell<>();
                }
            };
        }

        @Override
        public <T> PropertyValueFactory<T, Boolean> getValueFactory(String fieldName) {
            return new PropertyValueFactory<>(fieldName);
        }
    }),
    CHECKBOX(new JFXTableCellFactory<Boolean>() {

        @Override
        public <T> Callback<TableColumn<T, Boolean>, TableCell<T, Boolean>> getCallback(TableColumn<T, Boolean> tableColumn) {
            return CheckBoxTableCell.forTableColumn(tableColumn);
        }

        @Override
        public <T> PropertyValueFactory<T, Boolean> getValueFactory(String fieldName) {
            return new PropertyValueFactory<>(fieldName);
        }
    }),
    ROW_INDEX(new JFXTableCellFactory<Void>() {

        @Override
        public <T> Callback<TableColumn<T, Void>, TableCell<T, Void>> getCallback(TableColumn<T, Void> tableColumn) {
            return new Callback<TableColumn<T, Void>, TableCell<T, Void>>() {

                @Override
                public TableCell<T, Void> call(TableColumn<T, Void> p) {
                    return new TableCell<T, Void>() {

                        @Override
                        protected void updateItem(Void t, boolean bln) {
                            super.updateItem(t, bln); //To change body of generated methods, choose Tools | Templates.

                            if (this.getTableRow() != null) {
                                setText((this.getTableRow().getIndex() + 1) + "");
                            } else {
                                setText("");
                            }
                        }

                    };
                }
            };
        }

        @Override
        public <T> PropertyValueFactory<T, Void> getValueFactory(String fieldName) {
            return null;
        }

    });

    private final JFXTableCellFactory tableCellFactory;

    private JFXTableCellType(JFXTableCellFactory tableCellFactory) {
        this.tableCellFactory = tableCellFactory;
    }

    public JFXTableCellFactory getTableCellFactory() {
        return tableCellFactory;
    }

    public <T, S> TableColumn<T, S> getTableColumn(String columnName, String fieldName) {
        TableColumn<T, S> column = new TableColumn<>(columnName);
        @SuppressWarnings("unchecked")
        Callback<TableColumn<T, S>, TableCell<T, S>> cellFactoryCallback = tableCellFactory.<T>getCallback(column);
        @SuppressWarnings("unchecked")
        Callback<CellDataFeatures<T, S>, ObservableValue<S>> cellValueCallback = tableCellFactory.<T>getValueFactory(fieldName);

//        @SuppressWarnings("unchecked")
        column.setCellFactory(cellFactoryCallback);
        if (cellValueCallback != null) {
            column.setCellValueFactory(cellValueCallback);
        }

        return column;
    }

}
