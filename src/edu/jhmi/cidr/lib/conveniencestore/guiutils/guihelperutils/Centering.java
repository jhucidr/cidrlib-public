/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.guihelperutils;

import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author Brad Tibbils
 */
public class Centering {

    public static void centerStage(Stage stage) {
        Screen screen = Screen.getPrimary();
        double screenWidth = screen.getVisualBounds().getWidth();
        double screenHeight = screen.getVisualBounds().getHeight();

        stage.sizeToScene();
        stage.setX(screenWidth / 2 - stage.getWidth() / 2);
        stage.setY(screenHeight / 2 - stage.getHeight() / 2);
    }

    public static void centerGuiElementOnStage(Stage stage, Node node) {
        Pane root = (Pane) stage.getScene().getRoot();
        centerGuiElementOnStage(root, node);
    }

    public static void centerGuiElementOnStage(Pane root, Node node) {
        double centerX = root.getWidth() / 2;
        double centerY = root.getHeight() / 2;

        double newX = centerX - node.getLayoutBounds().getWidth() / 2;
        double newY = centerY - node.getLayoutBounds().getHeight() / 2;

        node.setLayoutX(newX);
        node.setLayoutY(newY);
    }

    public static HBox wrapNodeWithHBox(Node nodeToWrap) {
        HBox hBox = new HBox();
        Pane leftSeparatorPane = new Pane();
        Pane rightSeparatorPane = new Pane();

        hBox.getChildren().add(leftSeparatorPane);
        hBox.getChildren().add(nodeToWrap);
        hBox.getChildren().add(rightSeparatorPane);

        HBox.setHgrow(leftSeparatorPane, Priority.SOMETIMES);
        HBox.setHgrow(rightSeparatorPane, Priority.SOMETIMES);

        return hBox;
    }
}
