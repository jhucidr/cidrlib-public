/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.guiutils.guihelperutils;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 *
 * @author Brad Tibbils
 */
public class GlassPane extends Pane {

    private static final Double DEFAULT_OPACITY = 0.75;

    public GlassPane(Stage stage) {
        this(stage, DEFAULT_OPACITY);
    }

    public GlassPane(Pane root) {
        this(root, DEFAULT_OPACITY);
    }
    
    public GlassPane(Stage stage, Double opacity) {
        super();
        this.setStyle("-fx-background-color: rgba(173, 173, 173, " + opacity + ");");
        this.setPrefSize(stage.getWidth(), stage.getHeight());
        addResizeListener((Pane) stage.getScene().getRoot());
    }

    public GlassPane(Pane root, Double opacity) {
        super();
        this.setStyle("-fx-background-color: rgba(173, 173, 173, " + opacity + ");");
        this.setPrefSize(root.getWidth(), root.getHeight());
        addResizeListener(root);
    }
    
    private void addResizeListener(final Pane root) {
        root.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                setPrefSize(root.getWidth(), root.getHeight());
            }
        });
        
        root.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                setPrefSize(root.getWidth(), root.getHeight());
            }
        });
    }
}
