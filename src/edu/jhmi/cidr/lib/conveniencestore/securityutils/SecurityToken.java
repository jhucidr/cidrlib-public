/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.jhmi.cidr.lib.conveniencestore.securityutils;

import java.io.UnsupportedEncodingException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Alice Sanchez
 */
public class SecurityToken {
    private static final long INTERVAL_COUNT = 60000;
    private static final String HASH_ALGORITHM = "HmacMD5";
    private static final String CHAR_SET = "UTF8";
    
    public static void main(String[] args) throws Exception {
        String key = "hello";
        String token = generate(key);
        
        System.out.println("token = " + token);
        System.out.println("Valid Token: " + isTokenValid(key, token));
    }
    
    public static String generate(String key) throws Exception {
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(CHAR_SET), HASH_ALGORITHM);
        Mac mac = Mac.getInstance(HASH_ALGORITHM);
        byte[] hashResult;
        
        mac.init(keySpec);
        hashResult = mac.doFinal(getIntervals());
        
        return Base64.encodeBase64String(hashResult);
    }
    
    public static boolean isTokenValid(String key, String token) throws Exception{
        String compareToken = generate(key);
        
        return compareToken.equals(token);
    }
    
    private static byte[] getIntervals() throws UnsupportedEncodingException{
        long intervals = System.currentTimeMillis()/INTERVAL_COUNT;
        String strValue = String.valueOf(intervals);
        
        return strValue.getBytes(CHAR_SET);
    }
}
