/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.securityutils.ldapauth;

import edu.jhmi.cidr.lib.conveniencestore.guiutils.GuiDialog;
import edu.jhmi.cidr.lib.conveniencestore.guiutils.GuiLoadScreen;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * FXML Controller class
 *
 * @author Alice Sanchez
 */
public class LoginDialogViewController implements Initializable {

    private GuiLoadScreen loadScreen;
    private Authenticator authenticator;
    private LoginCallback callback;
    private Stage stage;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private StackPane stackPane;

    @FXML
    private Button loginButton;

    public void setAuthenticator(Authenticator authenticator) {
        this.authenticator = authenticator;
    }

    public void setCallback(LoginCallback callback) {
        this.callback = callback;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
//        this.loadScreen = new GuiLoadScreen(stage);
    }

    @FXML
    void onLogin(ActionEvent event) {
        loadScreen.show("Authenticating...");
        new Thread(new Runnable() {

            @Override
            public void run() {
                String username = usernameField.getText();
                String password = passwordField.getText();

                try {
                    if (authenticator.login(username, password)) {
                        callback.onSuccess(username);
                        Platform.runLater(new Runnable() {

                            @Override
                            public void run() {
                                stage.close();
                            }
                        });
                    } else {
                        if (callback.onFailure(username)) {
                            Platform.runLater(new Runnable() {

                                @Override
                                public void run() {
                                    GuiDialog dialog = new GuiDialog(stackPane);
                                    dialog.setCallBackOnResponse(GuiDialog.Response.OK, new Runnable() {

                                        @Override
                                        public void run() {
                                            Platform.runLater(new Runnable() {

                                                @Override
                                                public void run() {
                                                    System.out.println("loginButton = " + loginButton.isDefaultButton());
                                                    loginButton.setDefaultButton(true);
                                                }
                                            });
                                        }
                                    });
                                    dialog.showDialog("Wrong username/password", GuiDialog.GuiDialogButton.OK);
                                }
                            });
                        } else {
                            stage.close();
                        }
                    }

                    Platform.runLater(new Runnable() {

                        @Override
                        public void run() {
                            loadScreen.hide();
                        }
                    });
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }).start();
    }

    @FXML
    void onCancel(ActionEvent event) {
        System.out.println("callback = " + callback);
        callback.onCanceled();
        stage.close();
    }

    private static boolean turnOnExplictExitMode() {
        boolean turnedOn = false;

        if (Platform.isImplicitExit()) {
            Platform.setImplicitExit(false);
            turnedOn = true;
        }

        return turnedOn;
    }

    private static void setupShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

            @Override
            public void run() {
                if (Platform.isImplicitExit()) {
                    Platform.setImplicitExit(false);
                }
            }
        }));
    }

    public static void showDialog(final Authenticator authenticator, final LoginCallback callback) {
        boolean explictExit = turnOnExplictExitMode();

        if (explictExit) {
            setupShutdownHook();
        }

        new JFXPanel();
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("LoginDialogView.fxml"));
                    Parent root = (Parent) loader.load();
                    LoginDialogViewController controller = (LoginDialogViewController) loader.getController();
                    Scene scene = new Scene(root);
                    Stage stage = new Stage();

                    controller.setAuthenticator(authenticator);
                    controller.setCallback(callback);

                    stage.setScene(scene);
                    controller.setStage(stage);
                    stage.setTitle("CIDR Login");
                    stage.setResizable(false);
                    stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

                        @Override
                        public void handle(WindowEvent t) {
                            callback.onCanceled();
                        }
                    });
                    stage.show();
                    stage.sizeToScene();
                    stage.centerOnScreen();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        usernameField.requestFocus();
        loadScreen = new GuiLoadScreen(stackPane);
    }

    public static void main(String[] args) {
        LoginDialogViewController.showDialog(new LdapAuthenticator(), new LoginCallback() {

            @Override
            public void onSuccess(String username) {
                System.out.println(username + " logged in");
                System.exit(0);
            }

            @Override
            public boolean onFailure(String username) {
                System.out.println(username + "login failed");
                System.out.println("Please try again");
                return true;
            }

            @Override
            public void onCanceled() {
                System.out.println("User canceled");
                System.exit(0);
            }
        });

    }

    public interface LoginCallback {

        public void onSuccess(String username);

        //Return true to try again
        public boolean onFailure(String username);

        public void onCanceled();
    }
}
