/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.conveniencestore.securityutils.ldapauth;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import javax.naming.AuthenticationException;
import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

/**
 *
 * @author Alice Sanchez
 */
public class LdapAuthenticator implements Authenticator {

    private static final String LDAP_CONTEXT_FACTORY_NAME = "com.sun.jndi.ldap.LdapCtxFactory";

    //Add your own LDAP hosts Ex: "ldap://panda.power.edu/"
    private static final List<String> LDAP_HOSTS = Arrays.asList();
    private static final String AUTH_MODE = "simple";
    //Add domain EX: "@panda.edu"
    private static final String DEFAULT_DOMAIN = "";

    private final String ldapFactory;
    private final String authMode;
    private final String domain;
    private final Map<String, UserCredentials> offlineUsers = new HashMap<>();

    public LdapAuthenticator() {
        this.ldapFactory = LDAP_CONTEXT_FACTORY_NAME;
        this.authMode = AUTH_MODE;
        this.domain = DEFAULT_DOMAIN;
    }

    public LdapAuthenticator(List<UserCredentials> offlineUsers) {
        this.ldapFactory = LDAP_CONTEXT_FACTORY_NAME;
        this.authMode = AUTH_MODE;
        this.domain = DEFAULT_DOMAIN;
        populateOfflineUserMap(offlineUsers);
    }

    private void populateOfflineUserMap(List<UserCredentials> offlineUsers) {
        for (UserCredentials userCredentials : offlineUsers) {
            this.offlineUsers.put(userCredentials.getUsername().toLowerCase(), userCredentials);
        }
    }

    private boolean loginAsOfflineUser(String username, String password) {
        boolean accepted = false;
        UserCredentials credentials = offlineUsers.get(username.toLowerCase());

        if (credentials != null) {
            accepted = password.equals(credentials.getPassword());
        }

        return accepted;
    }

    private boolean loginAsLdapUser(final String username, final String password) throws Exception {
        Hashtable<String, Object> env = new Hashtable<>();
        DirContext dirContext;
        int maxRetries = 30;

        env.put(Context.INITIAL_CONTEXT_FACTORY, ldapFactory);
        env.put(Context.SECURITY_AUTHENTICATION, authMode);
        env.put(Context.SECURITY_PRINCIPAL, username + domain);
        env.put(Context.SECURITY_CREDENTIALS, password);

        for (int i = 0; i < maxRetries; i++) {
            env.put(Context.PROVIDER_URL, LDAP_HOSTS.get(i % LDAP_HOSTS.size()));

            try {
                dirContext = new InitialDirContext(env);
                dirContext.close();

                return true;
            } catch (AuthenticationException ex) {
                return false;
            } catch (CommunicationException ex) {
                System.out.println("Retry");
            }

            Thread.sleep(1000);
        }

        throw new Exception("Max login retry reached");
    }

    @Override
    public boolean login(String username, String password) throws Exception {
        if (username.isEmpty() || password.isEmpty()) {
            return false;
        }

        if (loginAsOfflineUser(username, password)) {
            return true;
        }

        return loginAsLdapUser(username, password);
    }

    public class UserCredentials {

        public final String username;
        public final String password;

        public UserCredentials(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }

    }
}
