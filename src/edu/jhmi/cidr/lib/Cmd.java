/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author cidr
 */
public class Cmd {

    private static final Lock LOGGER_LOCK = new ReentrantLock();
    private static AsyncLogger PROCESSS_LOGGER = null;

    public static void provideLogggerPath(String path) throws IOException {
        LOGGER_LOCK.lock();
        try {
            if (PROCESSS_LOGGER != null) {
                throw new IllegalStateException("The process logger may only be assigned once.");
            }
            if (path == null) {
                throw new NullPointerException("The process logger path must not be null");
            }
            PROCESSS_LOGGER = new AsyncLogger(path, false);
        } finally {
            LOGGER_LOCK.unlock();
        }

    }

    private static void logProcessInformation(String toLog) {
        LOGGER_LOCK.lock();
        try {
            if (PROCESSS_LOGGER != null) {
                PROCESSS_LOGGER.logMessage(toLog, "COMMAND", AsyncLogger.Severity.INFO);
            }
        } finally {
            LOGGER_LOCK.unlock();
        }
    }

    /**
     * This method tries to execute the supplied command as a separate process.
     * If the supplied boolean has a value of
     * <code>true</code>, this method will block until either the process
     * launched by the command terminates or an Exception is thrown. Internally,
     * this method uses the {@link StreamEater} class to try to avoid the
     * external process blocking, hanging, or deadlocking.
     *
     * @param command String
     * @param handler ProcessOutputHandler specifies whether to print to the
     * console and provides output streams to which to forward the launched
     * process's err and out streams.
     * @param waitFor boolean
     * @throws IOException
     * @throws InterruptedException
     * @throws NullPointerException if the command is null.
     */
    public static void launchProcess(String command, ProcessOutputHandler handler,
            boolean waitFor) throws IOException, InterruptedException {
        if (command == null) {
            throw new NullPointerException("Command must not be null.");
        }
        if (handler == null) {
            throw new NullPointerException("Process output handler must not be null");
        }
        System.out.println("Attempting to execute: " + command);
        logProcessInformation(command);
        Process p = Runtime.getRuntime().exec(command);
        Thread err = new StreamEater(
                p.getErrorStream(),
                handler.getErr(),
                StreamEater.Type.STDERR, handler.isPrintErrToConsole(),
                handler.isBinaryOutput());
        err.setPriority(Thread.MAX_PRIORITY);
        err.start();
        Thread out = new StreamEater(
                p.getInputStream(),
                handler.getOut(),
                StreamEater.Type.STDOUT, handler.isPrintOutToConsole(),
                handler.isBinaryOutput());
        out.setPriority(Thread.MAX_PRIORITY);
        out.start();
        if (waitFor) {
            p.waitFor();
        }
    }

    public static Process launchAndReturnProcess(String cmd) throws IOException {
        return Runtime.getRuntime().exec(cmd);
    }

    public static class ProcessOutputHandler {

        private final OutputStream err;
        private final OutputStream out;
        private final boolean printErrToConsole;
        private final boolean printOutToConsole;
        private final boolean binaryOutput;

        public ProcessOutputHandler(OutputStream err, boolean printErrToConsole,
                OutputStream out, boolean printOutToConsole, boolean binaryOutput) {
            this.err = err;
            this.out = out;
            this.printErrToConsole = printErrToConsole;
            this.printOutToConsole = printOutToConsole;
            this.binaryOutput = binaryOutput;
        }

        public OutputStream getErr() {
            return err;
        }

        public OutputStream getOut() {
            return out;
        }

        public boolean isBinaryOutput() {
            return binaryOutput;
        }

        public boolean isPrintErrToConsole() {
            return printErrToConsole;
        }

        public boolean isPrintOutToConsole() {
            return printOutToConsole;
        }
    }
//    public static void launchJar(JreInstallation installation, String jarFilePath, List<String> jvmArgs, List<String> argsForJarFileToLaunch, ProcessOutputHandler handler, boolean waitFor) throws Exception {
//        if (jarFilePath == null) {
//            throw new NullPointerException("Jar File Path must not be null");
//        }
//        StringBuilder sb = new StringBuilder();
//        sb.append(installation.buildPathToExecutable(JreInstallation.Executable.JAVA));
//        sb.append(GlobalConstants.SPACE);
//        sb.append(JreInstallation.CommandLineArg.JAR.getArg());
//        if (jvmArgs != null) {
//            for (String arg : jvmArgs) {
//                sb.append(GlobalConstants.SPACE);
//                sb.append(arg);
//            }
//        }
//        sb.append(GlobalConstants.SPACE);
//        sb.append(jarFilePath);
//        if (argsForJarFileToLaunch != null) {
//            for (String arg : argsForJarFileToLaunch) {
//                sb.append(GlobalConstants.SPACE);
//                sb.append(arg);
//            }
//        }
//        String cmd = sb.toString();
//        launchProcess(cmd, handler, waitFor);
//    }
//        public static void shutdownLogger() {
//        try {
//            if(null != PROCESSS_LOGGER) {
//                PROCESSS_LOGGER.shutDown();
//            }
//        } catch (Exception ex) {
//            Main.serr("The process logger has not shut down in an orderly fashion.", AsyncLogger.Severity.WARNING);
//        }
//    }
}
