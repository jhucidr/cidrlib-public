/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.view;

import edu.jhmi.cidr.lib.gui.GuiUtils;
import edu.jhmi.cidr.lib.gui.GuiUtils.ErrorMessage;
import edu.jhmi.cidr.lib.gui.table.CellEditing;
import javax.swing.JOptionPane;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author David Newcomer
 * Apr 4, 2012
 */
/*package access */
class EditingStrategy implements CellEditing<ViewRecord> {

    private final DataField dataField;

    public EditingStrategy(DataField dataField) {
        if (dataField == null) {
            throw new NullPointerException("dataField must not be null");
        }
        this.dataField = dataField;
    }

    @Override
    public TableCellEditor getCellEditor(ViewRecord dataSource) {
        return dataField.getEditableDataType().getCellEditor(dataSource, dataField);
    }

    @Override
    public void notifyEditingStopped(Object valueToSet, ViewRecord rowDataSource) {
        if (valueToSet != null) {
            try {
                EditableDataType editableDataType = dataField.getEditableDataType();
                if (editableDataType.isConvertableDataType()) {
                    //Converts and validates the user-supplied value.
                    valueToSet = editableDataType.convertObject(valueToSet);
                }
                Object originalValue = dataField.getObject(rowDataSource);
                if (valueToSet.equals(originalValue) == false) {
                    //If a change was made, set that value into the object.
                    dataField.setValueIntoObject(rowDataSource.getDataSource(), valueToSet);
                    JOptionPane.showMessageDialog(null, dataField.getMemberName() + " successfully updated.", dataField.getMemberName() + " successfully updated", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception e) {
                rowDataSource.updateView(dataField);
                ErrorMessage em = new ErrorMessage("Unable to update " + dataField.getMemberName(), "Error updating field", e, GuiUtils.ContactPwogwammaws.YES);
                e.printStackTrace();
                //GuiUtils.reportError(null, em);
                JOptionPane.showMessageDialog(null, em.getMessage(), em.getTitle(), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void notifyEditingCancelled(Object o, ViewRecord dataSource) {
        //No-Op
    }

    @Override
    public boolean canEdit(ViewRecord userData) {
        return dataField.isEditable();
    }
}
