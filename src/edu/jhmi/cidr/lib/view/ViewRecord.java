/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.view;

/**
 * If I have multiple instances of the same dataSource object, I want to display them all.
 * This layer is necessary, because it makes all dataSources unique from the perspective
 * of the Gui Table framework (even if they aren't REALLY unique.).  It also allows 
 * additional information in the view, such as the index & default toString().
 * @author David Newcomer
 * Apr 3, 2012
 */
public class ViewRecord {

    private final Object dataSource;
    private final long index;
    private DisplayTable table;

    public ViewRecord(Object dataSource, long index) {
        //Null dataSource is allowed.
        if (index < 0) {
            throw new IllegalArgumentException("index must be >= zero.");
        }
        this.dataSource = dataSource;
        this.index = index;
    }

    public long getIndex() {
        return index;
    }

    /**
     * May return a null value.
     */
    public Object getDataSource() {
        return dataSource;
    }

    /**
     * This is done ON PURPOSE.  
     * DO NOT delegate to the dataSource's equals method!!!
     */
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    /**
     * This is done ON PURPOSE.  
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /*package access*/ void setTable(DisplayTable table) {
        if (table == null) {
            throw new NullPointerException("table must not be null");
        }
        this.table = table;
    }

    public void updateView(DataField dataField) {
        table.updateRow(this, dataField);
    }

    public boolean isNull() {
        return dataSource == null;
    }

    /*package access*/ DisplayTable getTable() {
        return table;
    }
}