/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * DisplayDialog.java
 *
 * Created on Apr 6, 2012, 12:21:56 PM
 */
package edu.jhmi.cidr.lib.view;


import java.awt.Window;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.UIManager;

/**
 *
 * @author David Newcomer
 */
public class DisplayDialog extends javax.swing.JDialog {
    
    private final DisplayTable guiTable;

    /**
     * Creates new form DisplayDialog
     */
    public DisplayDialog(Window parent, Viewer viewer) {
        super(parent, "View fields for Object: " + viewer.getDataType().getSimpleName(), ModalityType.DOCUMENT_MODAL);
        if (viewer == null) {
            throw new NullPointerException("v must not be null");
        }
        this.guiTable = new DisplayTable(viewer);
        initComponents();
        guiTable.addToScrollPane(jScrollPane1);
        
        for (ViewRecord record : viewer.getDataToView()) {
            guiTable.addRow(record);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 848, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 488, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    public static void displayEntities(final List<?> entities) throws Exception {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

//        /* Create and display the dialog */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            @Override
//            public void run() {
                Viewer v = new Viewer(entities);
                DisplayDialog dialog = new DisplayDialog(new JFrame(), v);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
//            }
//        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
