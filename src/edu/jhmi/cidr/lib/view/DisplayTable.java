/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.view;

import edu.jhmi.cidr.lib.gui.table.GuiTable;

/**
 *
 * @author David Newcomer
 * Mar 30, 2012
 */
/*package access*/ class DisplayTable extends GuiTable<ViewRecord> {

    private final Viewer viewer;

    public DisplayTable(Viewer v) {
        super(v.getDataFields());
        this.viewer = v;
        for (DataField df : v.getDataFields()) {
            addListener(df, new PopupListener(df));
        }
        for (ViewRecord vr : v.getDataToView()) {
            vr.setTable(this);
        }
    }

    public Viewer getViewer() {
        return viewer;
    }
}
