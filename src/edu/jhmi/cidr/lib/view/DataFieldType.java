/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.view;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author David Newcomer
 * Mar 29, 2012
 */
public enum DataFieldType {

    SIMPLE("Simple", "Simple, or scalar, properties have a single value that may be retrieved or modified. The underlying property type might be a Java language primitive (such as int, a simple object (such as a java.lang.String), or a more complex object whose class is defined either by the Java language, by the application, or by a class library included with the application.") {

        @Override
        public boolean isType(Class clazz) {
            if (INDEXED.isType(clazz) || MAPPED.isType(clazz)) {
                return false;
            }
            return true;
        }

        @Override
        public TableCellEditor getCellEditor(DataField dataSource) {
            Class dataType = dataSource.getMyDataType();
            if (EditableDataType.ENUMERATED.isType(dataType)) {
                final JComboBox<Object> typesCombo = new JComboBox<>();
                for (Object enumValue : dataType.getEnumConstants()) {
                    typesCombo.addItem(enumValue);
                }
                return new DefaultCellEditor(typesCombo);
            }
            return null;
        }

        @Override
        public Viewer buildView(Object toView) {
            return new Viewer(toView);
        }

        @Override
        public boolean isEmpty(Object toView) {
            return toView.equals(DataField.NULL_SOURCE);
        }
    }, INDEXED("Indexed", "An indexed property stores an ordered collection of objects (all of the same type) that can be individually accessed by an integer-valued, non-negative index (or subscript). Alternatively, the entire set of values may be set or retrieved using an array. As an extension to the JavaBeans specification, the BeanUtils package considers any property whose underlying data type is java.util.List (or an implementation of List) to be indexed as well.") {

        @Override
        public boolean isType(Class clazz) {
            if (clazz.isArray()) {
                return true;
            }
            if (Iterable.class.isAssignableFrom(clazz)) {
                return true;
            }
            return false;
        }

        @Override
        public TableCellEditor getCellEditor(DataField dataSource) {
            return null;
        }

        @Override
        public Viewer buildView(Object toView) {
            Collection<Object> c = new ArrayList<Object>();
            if (toView != null) {
                if (toView.getClass().isArray()) {
                    //This below does not work  - primitive[] can not be cast to Object[].
//                    c = Arrays.asList((Object[]) toView); 
                    for (int i = 0; i < Array.getLength(toView); i++) {
                        c.add(Array.get(toView, i));
                    }
                } else if (toView instanceof Collection<?>) {
                    c.addAll((Collection<?>)toView);
                } else {
                    throw new IllegalStateException("O is not an indexed class.  " + toView.getClass());
                }
            }
            return new Viewer(c);
        }

        @Override
        public boolean isEmpty(Object toView) {
            if (toView != null) {
                if (toView.getClass().isArray()) {
                    return Array.getLength(toView) == 0;
                } else if (toView instanceof Collection<?>) {
                    return ((Collection<?>) toView).isEmpty();
                }
            }
            return true;
        }
    }, MAPPED("Mapped", "As an extension to standard JavaBeans APIs, the BeanUtils package considers any property whose underlying value is a java.util.Map to be \"mapped\". You can set and retrieve individual values via a String-valued key.") {

        @Override
        public boolean isType(Class clazz) {
            if (Map.class.isAssignableFrom(clazz)) {
                return true;
            }
            return false;
        }

        @Override
        public TableCellEditor getCellEditor(DataField dataSource) {
            return null;
        }

        @Override
        public Viewer buildView(Object toView) {
            if (toView instanceof Map) {
                Map map = (Map) toView;
                return new Viewer(map);
            } else {
                throw new IllegalStateException("O is not a mapped class.  " + toView.getClass());
            }
        }

        @Override
        public boolean isEmpty(Object toView) {
            if (toView instanceof Map) {
                Map map = (Map) toView;
                return map.isEmpty();
            }
            return true;
        }
    };
    private final String value;
    private final String description;

    private DataFieldType(String value, String description) {
        this.value = value;
        this.description = description;
    }

    public abstract boolean isType(Class clazz);

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public static DataFieldType getType(Class clazz) {
        for (DataFieldType type : values()) {
            if (type.isType(clazz)) {
                return type;
            }
        }
        throw new IllegalStateException("Unknown Type for class " + clazz.getSimpleName());
    }

    public abstract TableCellEditor getCellEditor(DataField dataSource);

    public abstract Viewer buildView(Object toView);

    public boolean isEditable() {
        return this == DataFieldType.SIMPLE;
    }

    public abstract boolean isEmpty(Object dataSource);
}
