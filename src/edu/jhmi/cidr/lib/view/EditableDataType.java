/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.view;

import java.util.EnumSet;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author David Newcomer
 * Apr 4, 2012
 */
public enum EditableDataType {

    STRING {

        @Override
        public boolean isType(Class clazz) {
            return String.class.equals(clazz);
        }

        @Override
        public Object convertObject(Object toConvert) throws Exception {
            return toConvert.toString();
        }
    }, BOOLEAN {

        @Override
        public boolean isType(Class clazz) {
            return Boolean.class.equals(clazz) || boolean.class.equals(clazz);
        }

        @Override
        public Object convertObject(Object toConvert) throws Exception {
            //This receives special handling, which is addressed in 
            //This is due to a custom TableCellEditor in the Editing Strategy.
            throw new UnsupportedOperationException();
//            String value = toConvert.toString();
//            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("T")) {
//                return true;
//            } else if (value.equalsIgnoreCase("false") || value.equalsIgnoreCase("F")) {
//                return false;
//            } else {
//                throw new IllegalStateException(toConvert + " is not a valid boolean.");
//            }
        }

        @Override
        public TableCellEditor getCellEditor(ViewRecord dataSource, DataField dataField) {
            final JComboBox<Object> typesCombo = new JComboBox<>();
            typesCombo.addItem(Boolean.TRUE);
            typesCombo.addItem(Boolean.FALSE);
            return new DefaultCellEditor(typesCombo);
        }
    }, BYTE {

        @Override
        public boolean isType(Class clazz) {
            return Byte.class.equals(clazz) || byte.class.equals(clazz);
        }

        @Override
        public Object convertObject(Object toConvert) throws Exception {
            return Byte.parseByte(toConvert.toString());
        }
    }, SHORT {

        @Override
        public boolean isType(Class clazz) {
            return Short.class.equals(clazz) || short.class.equals(clazz);
        }

        @Override
        public Object convertObject(Object toConvert) throws Exception {
            return Short.parseShort(toConvert.toString());
        }
    }, INTEGER {

        @Override
        public boolean isType(Class clazz) {
            return Integer.class.equals(clazz) || int.class.equals(clazz);
        }

        @Override
        public Object convertObject(Object toConvert) throws Exception {
            return Integer.parseInt(toConvert.toString());
        }
    }, LONG {

        @Override
        public boolean isType(Class clazz) {
            return Long.class.equals(clazz) || long.class.equals(clazz);
        }

        @Override
        public Object convertObject(Object toConvert) throws Exception {
            return Long.parseLong(toConvert.toString());
        }
    }, FLOAT {

        @Override
        public boolean isType(Class clazz) {
            return Float.class.equals(clazz) || float.class.equals(clazz);
        }

        @Override
        public Object convertObject(Object toConvert) throws Exception {
            return Float.parseFloat(toConvert.toString());
        }
    }, DOUBLE {

        @Override
        public boolean isType(Class clazz) {
            return Double.class.equals(clazz) || double.class.equals(clazz);
        }

        @Override
        public Object convertObject(Object toConvert) throws Exception {
            return Double.parseDouble(toConvert.toString());
        }
    }, CHARACTER {

        @Override
        public boolean isType(Class clazz) {
            return Character.class.equals(clazz) || char.class.equals(clazz);
        }

        @Override
        public Object convertObject(Object toConvert) throws Exception {
            String character = toConvert.toString();
            if (character.length() == 1) {
                return character.charAt(0);
            } else {
                throw new IllegalArgumentException(toConvert + " is not a valid character.");
            }
        }
    }, ENUMERATED {

        @Override
        public boolean isType(Class clazz) {
            if (clazz == null) {
                return false;
            }
            return clazz.isEnum();
        }

        @Override
        public Object convertObject(Object toConvert) throws Exception {
            //This requires special handling, and is taken care of in the calling context.
            //This is due to a custom TableCellEditor in the Editing Strategy.
            throw new UnsupportedOperationException();
        }

        @Override
        public TableCellEditor getCellEditor(ViewRecord dataSource, DataField dataField) {
            final JComboBox<Object> typesCombo = new JComboBox<>();
            for (Object enumValue : dataField.getMyDataType().getEnumConstants()) {
                typesCombo.addItem(enumValue);
            }
            return new DefaultCellEditor(typesCombo);
        }
    }, NOT_EDITABLE {

        @Override
        public boolean isType(Class clazz) {
            for (EditableDataType edt : EnumSet.complementOf(EnumSet.of(NOT_EDITABLE))) {
                if (edt.isType(clazz)) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public Object convertObject(Object toConvert) throws Exception {
            throw new UnsupportedOperationException();
        }
    };
    private static final EnumSet<EditableDataType> PRIMITIVES =
            EnumSet.of(BOOLEAN, BYTE, CHARACTER, DOUBLE, FLOAT, INTEGER, LONG, SHORT);
    private static final EnumSet<EditableDataType> CONVERTABLE_TYPES =
            EnumSet.complementOf(EnumSet.of(BOOLEAN, ENUMERATED, NOT_EDITABLE));

    public abstract boolean isType(Class clazz);

    public static boolean isEditableDataType(Class clazz) {
        if (clazz != null) {
            EditableDataType edt = getEditableDataType(clazz);
            return edt != NOT_EDITABLE;
        }
        return false;
    }

    public static EditableDataType getEditableDataType(Class clazz) {
        if (clazz != null) {
            for (EditableDataType edt : EditableDataType.values()) {
                if (edt.isType(clazz)) {
                    return edt;
                }
            }
        }
        throw new IllegalStateException("Unable to determinate EditableDataType for Class " + clazz.getName());
    }

    public boolean isEditable() {
        return this != NOT_EDITABLE;
    }

    public TableCellEditor getCellEditor(ViewRecord dataSource, DataField dataField) {
        return null;
    }

    public abstract Object convertObject(Object toConvert) throws Exception;

    public boolean isPrimitiveOrString() {
        return PRIMITIVES.contains(this) || this == STRING;
    }

    public boolean isConvertableDataType() {
        return CONVERTABLE_TYPES.contains(this);
    }
}
