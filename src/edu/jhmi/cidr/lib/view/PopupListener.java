/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.view;

import edu.jhmi.cidr.lib.gui.table.GuiTable;
import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 *
 * @author David Newcomer Apr 3, 2012
 */
/*package access */ class PopupListener implements GuiTable.GuiTablePopupListener<ViewRecord> {

    private final JPopupMenu popUpMenu;
    private final DataField dataField;

    public PopupListener(DataField dataField) {
        if (dataField == null) {
            throw new NullPointerException("dataField must not be null");
        }
        this.popUpMenu = new JPopupMenu();
        this.dataField = dataField;
    }

    @Override
    public JPopupMenu getPopupMenu(ViewRecord dataSource, GuiTableColumn<ViewRecord> column) {
        popUpMenu.removeAll();
        if (dataField.isMoreInfoAvailable(dataSource)) {
            popUpMenu.add(new ViewMenuItem(dataSource, "More Info"));
            return popUpMenu;
        }
        return null;
    }

    public class ViewMenuItem extends JMenuItem implements ActionListener {

        private final ViewRecord source;

        public ViewMenuItem(ViewRecord source, String menuItemText) {
            super(menuItemText);
            this.source = source;
            addActionListener(this);
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            Object o = dataField.getObject(source);
            DataFieldType type = DataFieldType.getType(o.getClass());
            final Viewer v = type.buildView(o);
            new Thread() {
                @Override
                public void run() {
                    try {
                        DisplayDialog displayView = new DisplayDialog(getWindow(), v);
                        displayView.setVisible(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }

        private Window getWindow() {
            Component component = source.getTable().getJTable();
            while (component instanceof Window == false) {
                component = component.getParent();
            }
            return (Window) component;
        }
    }
}
