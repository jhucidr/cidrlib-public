/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.view;

import java.lang.reflect.Method;

/**
 * This is a custom DataField that pulls information out of the ViewRecord
 * Rather than the dataSource Object contained within the ViewRecord.
 * @author David Newcomer
 * Apr 4, 2012
 */
public class ViewRecordDataField extends DataField {

    public ViewRecordDataField(Method getter) {
        super(getter);
    }

    @Override
    public Object getObject(ViewRecord dataSource) {
        try {
            Object o = getGetter().invoke(dataSource);
            return o == null ? "NULL VALUE" : o;
        } catch (Exception e) {
            e.printStackTrace();
            return "Error getting " + getMyDataType() + ".";
        }
    }
}
