/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.view;

import edu.jhmi.cidr.lib.HtmlUtils.HtmlBuilder;
import edu.jhmi.cidr.lib.gui.GuiUtils;
import edu.jhmi.cidr.lib.gui.GuiUtils.ErrorMessage;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * This allows you to view Objects, and homogenous collections of objects.
 *
 * @author David Newcomer Mar 27, 2012
 */
public class Viewer {

    private final List<ViewRecord> dataToView;
    private final List<DataField> dataFields;
    private final Class dataType;

    public Viewer(Object toView) {
        this(toCollection(toView));
    }

    private static Collection<Object> toCollection(Object toView) {
        if (toView == null) {
            throw new NullPointerException("toView must not be null");
        }
        List<Object> objects = new ArrayList<Object>();
        objects.add(toView);
        return objects;
    }

    public Viewer(Map<?, ?> toView) {
        this(buildDataToView(toView));
    }

    public Viewer(Collection<?> toView) {
        if (toView == null) {
            throw new NullPointerException("toView must not be null");
        }
        if (toView.isEmpty()) {
            throw new IllegalStateException("Attempting to view an empty collection...");
        }
        //TODO: Consider iterating through all the Objects and determining the 
        //Lowest common Derived type (LCD).
        Object source = toView.iterator().next();
        this.dataType = source.getClass();
        boolean editingIsEnabled = true;
        this.dataFields = buildDataFieldList(editingIsEnabled);
        this.dataToView = new ArrayList<ViewRecord>();
        long index = 0;
        for (Object t : toView) {
            if (t == null || dataType.isInstance(t)) {
                dataToView.add(new ViewRecord(t, index));
                index++;
            } else {
                HtmlBuilder html = new HtmlBuilder();
                html.append("Viewer is not compatible with homogenous collections.  Expected class=");
                html.append(dataType.getSimpleName());
                html.append(".  Incompatable class=");
                html.append(t.getClass().getSimpleName());
                html.append(".");
                ErrorMessage em = new ErrorMessage(html.toPlainText(), "Can't view contents...", GuiUtils.ContactPwogwammaws.NO);
//                em.displayToUser();
                throw new IllegalArgumentException(em);
            }
        }
    }

    private static List<Object> buildDataToView(Map<?, ?> toView) {
        if (toView == null) {
            throw new NullPointerException("toView must not be null");
        }
        if (toView.isEmpty()) {
            throw new IllegalStateException("Attempting to view an empty collection...");
        }
        List<Object> data = new ArrayList<Object>();
        for (Entry<?, ?> entry : toView.entrySet()) {
            data.add(new MapEntry(entry));
        }
        return data;
    }

    private List<DataField> buildDataFieldList(boolean editingIsEnabled) {
        List<DataField> myDataFields = new ArrayList<DataField>();
        try {

            //Shows information contained in the ViewRecord
            BeanInfo viewRecordBeanInfo = Introspector.getBeanInfo(ViewRecordDataField.class);
            for (Method m : dataType.getMethods()) {
                if (m.getName().equalsIgnoreCase("toString")) {
                    //Constructs a custom ViewRecordDataField, 
                    //that pulls meta-data out of the VeiwRecord.
                    DataField dataField = new DataField(m);
                    myDataFields.add(dataField);
                }
            }
            for (PropertyDescriptor propertyDescriptor : viewRecordBeanInfo.getPropertyDescriptors()) {
                String readMethodName = propertyDescriptor.getReadMethod().getName();
                if (readMethodName.equalsIgnoreCase("getDataSource")) {
                    //Constructs a custom ViewRecordDataField, 
                    //that pulls meta-data out of the VeiwRecord.
                    DataField dataField = new ViewRecordDataField(propertyDescriptor.getReadMethod());
                    myDataFields.add(dataField);
                }
            }
            BeanInfo beanInfo = Introspector.getBeanInfo(dataType);
            for (PropertyDescriptor propertyDescriptor : beanInfo.getPropertyDescriptors()) {
                if (propertyDescriptor.getReadMethod() == null) {
                    continue;
                }
                if (propertyDescriptor.getReadMethod().getName() == null) {
                    continue;
                }
                if (propertyDescriptor.getReadMethod().getName().equalsIgnoreCase("getClass")) {
                    continue;
                }
                DataField dataField = new DataField(propertyDescriptor, editingIsEnabled);
                myDataFields.add(dataField);
            }
            return myDataFields;
        } catch (Exception e) {
            throw new IllegalStateException("Problem Instrospecting...", e);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(buildHeader());
        sb.append(System.getProperty("line.separator"));
        for (ViewRecord t : dataToView) {
            sb.append(getView(t));
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    public List<String> buildHeader() {
        List<String> headerValues = new ArrayList<String>();
        for (DataField m : dataFields) {
            headerValues.add(m.getMemberName());
        }
        return headerValues;
    }

    public List<ViewRecord> getDataToView() {
        return dataToView;
    }

    public List<DataField> getDataFields() {
        return dataFields;
    }

    public List<String> getView(ViewRecord toView) {
        List<String> view = new ArrayList<String>();
        for (DataField dataField : dataFields) {
            Object toAdd = dataField.getObject(toView);
            view.add(toAdd == null ? "nullValue" : toAdd.toString());
        }
        return view;
    }

    public Class getDataType() {
        return dataType;
    }

    /*package access*/ DisplayTable getTable() {
        return dataToView.get(0).getTable();
    }
}