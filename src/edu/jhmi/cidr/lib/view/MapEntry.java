/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.view;

import java.util.Map.Entry;

/**
 * This layer is necessary because calling invoke() on a map Entry's
 * getter/setter throws an IllegalAccessException.
 *
 * @author David Newcomer Apr 3, 2012
 */
/*package access */
class MapEntry {

    private Object key;
    private Object value;

    public MapEntry(Entry<?, ?> entry) {
        if (entry == null) {
            this.key = null;
            this.value = null;
        } else {
            this.key = entry.getKey();
            this.value = entry.getValue();
        }
    }

    public Object getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }
}
