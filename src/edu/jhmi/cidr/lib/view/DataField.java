/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.view;

import edu.jhmi.cidr.lib.gui.table.CellEditing;
import edu.jhmi.cidr.lib.gui.table.CellRendering;
import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;
import java.awt.Color;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author David Newcomer
 * Mar 27, 2012
 */
public class DataField implements GuiTableColumn<ViewRecord>, CellRendering<ViewRecord> {

    private final Method getter;
    private final Method setter;
    private final Class myDataType;
    private final String memberName;
    private final DataFieldType dataFieldType;
    private final EditableDataType editableDataType;
    private final CellEditing<ViewRecord> strategy;
    private final boolean editingIsEnabled;
    private static final String GET = "GET";
    public static final String NULL_SOURCE = "NULL SOURCE";
    private static final DefaultTableCellRenderer EDITABLE_CELL_RENDERER;
    private static final DefaultTableCellRenderer AVAILABLE_INFO_CELL_RENDERER;
    private static final DefaultTableCellRenderer EDITABLE_AVAILABLE_INFO_CELL_RENDERER;

    static {
        EDITABLE_CELL_RENDERER = new DefaultTableCellRenderer();
        EDITABLE_CELL_RENDERER.setBackground(new Color(0xCCCCEE));
        AVAILABLE_INFO_CELL_RENDERER = new DefaultTableCellRenderer();
        AVAILABLE_INFO_CELL_RENDERER.setBackground(new Color(0xEECCCC));
        EDITABLE_AVAILABLE_INFO_CELL_RENDERER = new DefaultTableCellRenderer();
        EDITABLE_AVAILABLE_INFO_CELL_RENDERER.setBackground(new Color(0xCCEECC));
    }

    public DataField(PropertyDescriptor propertyDescriptor, boolean editingIsEnabled) {
        if (propertyDescriptor == null) {
            throw new NullPointerException("methodDescriptor must not be null");
        }
        this.getter = propertyDescriptor.getReadMethod();
        this.setter = propertyDescriptor.getWriteMethod();
        this.myDataType = propertyDescriptor.getPropertyType();
        this.dataFieldType = DataFieldType.getType(myDataType);
        this.memberName = buildName(getter);
        this.editableDataType = EditableDataType.getEditableDataType(myDataType);
        this.strategy = new EditingStrategy(this);
        this.editingIsEnabled = editingIsEnabled;
    }

    public DataField(Method getter) {
        if (getter == null) {
            throw new NullPointerException("getter must not be null");
        }
        this.getter = getter;
        this.setter = null;
        this.myDataType = getter.getReturnType();
        this.dataFieldType = DataFieldType.getType(myDataType);
        this.memberName = buildName(getter);
        this.strategy = new CellEditing.NoOp<ViewRecord>();
        this.editableDataType = EditableDataType.getEditableDataType(myDataType);
        this.editingIsEnabled = false;
    }

    private static String buildName(Method getter) {
        String getterName = getter.getName();
        StringBuilder sb = new StringBuilder();
        if (getterName.toUpperCase().startsWith(GET)) {
            getterName = getterName.substring(GET.length());
        }
        for (char c : getterName.toCharArray()) {
            if (Character.isUpperCase(c) && (sb.toString().isEmpty() == false)) {
                sb.append(" ");
            }
            sb.append(c);
        }
        return sb.toString();
    }

    public String getMemberName() {
        return memberName;
    }

    @Override
    public Object getObject(ViewRecord rowDataSource) {
        if (rowDataSource == null) {
            throw new NullPointerException("rowDataSource must not be null");
        }
        Object dataSource = rowDataSource.getDataSource();
        if (dataSource == null) {
            return NULL_SOURCE;
        }
        try {
            Object o = getter.invoke(dataSource);
            return o == null ? "NULL VALUE" : o;
        } catch (Exception e) {
            e.printStackTrace();
            return "Error getting " + myDataType + ".";
        }
    }

    public boolean hasSetter() {
        return setter == null ? false : true;
    }

    public void setValueIntoObject(Object obj, Object valueToSet) throws Exception {
        if (hasSetter() == false) {
            throw new IllegalArgumentException(getMemberName() + " does not have an publicly available setter.");
        }
        setter.invoke(obj, valueToSet);
        System.out.println("Set " + valueToSet + " of " + valueToSet.getClass() + " into " + obj);
    }

    public DataFieldType getDataFieldType() {
        return dataFieldType;
    }

    @Override
    public String getColumnHeader() {
        return getMemberName();
    }

    /**
     * Required for the GUI Table framework - this method is misleading, and only
     * causes problems.  It should either be removed or made optional. -JDN
     * @return 
     */
    @Override
    public Class getDataType() {
        return Object.class;
//        return dataType; //This causes issues.  Specifically, I think it disallows 
        // heterogonous data types from being used, such as null values and Strings,
        // if the dataType is, say, an Enum.
    }

    @Override
    public CellEditing<ViewRecord> getCellEditing(ViewRecord rowDataSource) {
        if (isEditable() && (rowDataSource.isNull() == false)) {
            return strategy;
        } else {
            return new CellEditing.NoOp<ViewRecord>();
        }
    }

    public Method getGetter() {
        return getter;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Name=");
        sb.append(memberName);
        sb.append("\tgetter=");
        sb.append(getter.getName());
        sb.append("\tsetter=");
        sb.append(setter == null ? "None" : setter.toString());
        sb.append("\tdataType=");
        sb.append(myDataType.getSimpleName());
        return sb.toString();
    }

    @Override
    public TableCellRenderer getCellRenderer(ViewRecord dataSource) {
        if (dataSource.isNull() == false) {
            if (isEditable()) {
                if (isMoreInfoAvailable(dataSource)) {
                    return EDITABLE_AVAILABLE_INFO_CELL_RENDERER;
                }
                return EDITABLE_CELL_RENDERER;
            }
        }
        if (isMoreInfoAvailable(dataSource)) {
            return AVAILABLE_INFO_CELL_RENDERER;
        }
        return null;
    }

    public boolean isEditable() {
        if (editingIsEnabled) {
            if (hasSetter()) {
                if (dataFieldType.isEditable()) {
                    if (editableDataType.isEditable()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public EditableDataType getEditableDataType() {
        return editableDataType;
    }

    public Class getMyDataType() {
        return myDataType;
    }

    public boolean isEmpty(ViewRecord dataSource) {
        Object toCheck = getObject(dataSource);
        return dataFieldType.isEmpty(toCheck);
    }

    public boolean isMoreInfoAvailable(ViewRecord record) {
        //Don't allow more info on classes.
        if (myDataType.equals(Class.class) == false) {
            //Don't allow more info for primitives or Strings.
            if (editableDataType.isPrimitiveOrString() == false) {
                //Don't allow more info on empty maps, collections, arrays, or null values.
                if (isEmpty(record) == false) {
                    Object source = record.getDataSource();
                    if (source != null) {
                        //Don't allow more info on the object currently being viewed.
                        if (source.equals(getObject(record)) == false) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
