/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.concurrent;

import edu.jhmi.cidr.lib.annotations.ThreadSafe;
import java.util.*;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * This class is an ExecutorService (thread pool) that can track which tasks
 * started, but did not complete, should there be an interruption or
 * cancellation.
 *
 * <pre>
 * Example usage:
 *
 * ExecutorService myService = Executors.newFixedThreadPool(5);
 *
 * TrackingExecutor tracker = new TrackingExecutor(myService);
 *
 * List<Callable<Object>> list = new ArrayList<Callable<Object>>();
 *
 * ...
 *
 * for (Callable<Object> c : list) {
 *     tracker.submit(c);
 * }
 *
 * List<Runnable> neverStarted = tracker.shutdownNow();
 * tracker.awaitTermination(1, TimeUnit.DAYS);
 *
 * List<Runnable> cancelled = tracker.getCancelledTasks();
 *
 *
 * </pre>
 * The shutdownNow method, in practice, will most likely be wrapped in a listener.
 * An example would be a user pressing a Cancel button during a series of long-running
 * background tasks. The application would then call shutdownNow(), await the
 * (abrupt) termination of the tracking service, then call the getCancelledTasks()
 * method. Then, the application would take whatever action is appropriate in its context,
 * for example, logging those tasks that did not start, as well as those tasks that
 * started, but did not complete.
 * @author cidr
 */
@ThreadSafe
public class TrackingExecutor<T extends ExecutorService> extends AbstractExecutorService {
    private final T service;
    private final Set<Runnable> cancelledTasks;

    /**
     * Construct your TrackingExecutor with any non-null ExecutorService.
     * @param service
     * @throws NullPointerException if the supplied service is null.
     */
    public TrackingExecutor(T service) {
        if (service == null) {
            throw new NullPointerException();
        }
        this.service = service;
        this.cancelledTasks = Collections.synchronizedSet(new HashSet<Runnable>());
    }

    /**
     * Returns a List of Runnable which contains the tasks that were submitted to this
     * executor, but which did not finish. Due to the nature of the Executor framework,
     * there is an unavoidable race condition that can make this method yield false
     * positives, i.e., tasks that have completed, but are identified as cancelled.
     * However, if the tasks are idempotent, this race condition is not a problem.
     * Caveat Programmor.
     * @return java.util.List
     * @throws IllegalStateException if this method is called before {@link #isTerminated()} returns true.
     */
    public List<Runnable> getCancelledTasks() {
        if (isTerminated() == false) {
            throw new IllegalStateException("The ExecutorService has not yet terminated.");
        }
        return new ArrayList<Runnable>(cancelledTasks);
    }

    @Override
    public void execute(final Runnable runnable) {
        service.execute(new Runnable() {

            @Override
            public void run() {
                try {
                    runnable.run();
                } finally {
                    if (isShutdown() && Thread.currentThread().isInterrupted()) {
                        cancelledTasks.add(runnable);
                    }
                }
            }

        });
    }

    @Override
    public void shutdown() {
        service.shutdown();
    }

    @Override
    public List<Runnable> shutdownNow() {
        return service.shutdownNow();
    }

    @Override
    public boolean isShutdown() {
        return service.isShutdown();
    }

    @Override
    public boolean isTerminated() {
        return service.isTerminated();
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        return service.awaitTermination(timeout, unit);
    }

    public T getService() {
        return service;
    }
}

