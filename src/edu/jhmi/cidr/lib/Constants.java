/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Houses application-wide constants and enum values
 *
 * @author dleary
 */
public class Constants {

    public static final String NEW_LINE = System.getProperty("line.separator");
    //We have many date formats in our DateFormatter class.  The below should 
    //either be consolidated into that class, or replaced with an entry from it. -JDN
    public static final DateFormat DATE_FORMAT_FOR_PI_FILE_CIDR_COMPLIANT_NAME = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss.SSS");
    public static final long ONE_KIBIBYTE = 1024L;
    public static final long ONE_MEBIBYTE = ONE_KIBIBYTE * ONE_KIBIBYTE;
    public static final long ONE_GIBIBYTE = ONE_KIBIBYTE * ONE_MEBIBYTE;
    public static final long MAX_FILE_BYTES_LONG_BLOB_SIZE = 4 * ONE_GIBIBYTE;
    public static final int LONG_BLOB_THRESHOLD_SIZE = 16777215; // 2^24 - 1
    public static final long DEFAULT_DATABASE_ID = -1;
    // Primarily for running on locally on development machine without CIDR network connection:
//    public static boolean USE_ONLY_PHOENIX_DB_IF_PROJMAN_IS_UNREACHABLE = true;
    public static String PROGRAMMERS_EMAIL = "cidr_programmers@lists.johnshopkins.edu";
    public static String PHOENIX_EMAIL_SUBJECT_LINE_PREFIX = "[PHOENIX]";
    public static final String CSV_EXTENSION = ".csv";
    public static final String TXT_EXTENSION = ".txt";
    public static final String NOT_APPLICABLE = "N/A";
    // in case users ever want to change what appears in GUI fields, separately from what's in files and reports
    public static final String NOT_APPLICABLE_GUI = NOT_APPLICABLE;
    public static final String BLANK = "";
    public static DateFormatter.ComparableDateString COMPARABLE_DATE_NULL = DateFormatter.ComparableDateString.getNullObject(Constants.NOT_APPLICABLE_GUI);
    public static String DEFAULT_EMAIL_ADDRESS = "";
//    public static String RAD_STUDY_NAME_SQL_PREFIX = "RD\\_";
    public static final long ONE_DAY_IN_MILLIS = 24 * 60 * 60 * 1000;
    public static final long CONTAINER_CHECK_EXPIRY = ONE_DAY_IN_MILLIS;
//    public static String CONTAINER_CHECK_EXPIRY_DESCRIPTION = "24 hours";
//    public static long FINGERPRINT_PLATE_CHECK_EXPIRY = CONTAINER_CHECK_EXPIRY;
//    public static String FINGERPRINT_PLATE_CHECK_EXPIRY_DESCRIPTION = CONTAINER_CHECK_EXPIRY_DESCRIPTION;
//    public static long PLATE_CHECK_EXPIRY = CONTAINER_CHECK_EXPIRY;
//    public static String PLATE_CHECK_EXPIRY_DESCRIPTION = CONTAINER_CHECK_EXPIRY_DESCRIPTION;
//    public static final String FINGERPRINT_PROJECT_NAME_RE = "[Ff]_"; // See PCC_6.7, page 6
//    public static final String FINGERPRINT_PROJECT_NAME_SQL_PREFIX = "F\\_"; // See PCC_6.7, page 6
    //    
    // TODO switch this mode when deploying
    //Turns on Stack-trace E-Mailing, user-login authentication, and resource paths (e.g., icon, DFM JAR).
    public static boolean DEVELOPMENT_MODE = true;
}
