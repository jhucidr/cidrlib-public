/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

/**
 *
 * @author Sean Griffith
 * Mar 4, 2014
 */

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class BijectiveMap<L, R> extends ConcurrentHashMap<L, R> {

    private final Map<R, L> rToLMap;
    private final Lock lock = new ReentrantLock();

    public BijectiveMap() {
        this.rToLMap = new ConcurrentHashMap<>();
    }

    public R putLeft(L lValue, R rValue) {
        if (lValue == null) {
            throw new NullPointerException("L value must not be null.");
        }
        if (rValue == null) {
            throw new NullPointerException("R value must not be null.");
        }
        if (super.containsValue(rValue)) {
            throw new IllegalArgumentException("The supplied R value is already associated with an L value.");
        }
        lock.lock();
        try {
            rToLMap.put(rValue, lValue);
            return super.put(lValue, rValue);
        } finally {
            lock.unlock();
        }
    }

    public L putRight(R rValue, L lValue) {
        if (rValue == null) {
            throw new NullPointerException("R value must not be null.");
        }
        if (lValue == null) {
            throw new NullPointerException("L value must not be null.");
        }
        if (rToLMap.containsValue(lValue)) {
            throw new IllegalArgumentException("The supplied L value is already associated with an R value.");
        }
        lock.lock();
        try {
            super.put(lValue, rValue);
            return rToLMap.put(rValue, lValue);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public R put(L key, R value) {
        throw new UnsupportedOperationException("Please use one of putLeft(L, R) or putRight(R, L).");
    }

    @Override
    public R get(Object key) {
        throw new UnsupportedOperationException("Please use one of getLeft(L) or getRight(R).");
    }

    public L getLeft(R rValue) {
        lock.lock();
        try {
            return rToLMap.get(rValue);
        } finally {
            lock.unlock();
        }
    }

    public R getRight(L lValue) {
        lock.lock();
        try {
            return super.get(lValue);
        } finally {
            lock.unlock();
        }
    }
}
