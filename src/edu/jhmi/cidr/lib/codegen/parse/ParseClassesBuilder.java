/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen.parse;

import edu.jhmi.cidr.lib.file.WritableFile;
import edu.jhmi.cidr.lib.file.FileUtils;
import java.io.File;

/**
 *
 * @author David Newcomer Oct 31, 2012
 */
public class ParseClassesBuilder {

    private final String delimiter;
    private final File delimitedFileToParse;
    private static final String COLUMN_CLASS_NAME = "Column";
    private static final String ROW_CLASS_NAME = "Row";
    private static final String GUI_TABLE_CLASS_NAME = "Table";
    private static final String EDITING_STRATEGY_CLASS_NAME = "EditingStrategy";
    private static final String PACKAGE_NAME = "edu.jhmi.cidr.lib.module.ui.admin.sessiondetails";
    private static final File PACKAGE_DIRECTORY = new File(".\\src\\edu\\jhmi\\cidr\\phoenix\\module\\ui\\admin\\sessiondetails");

    public static void main(String[] args) {
        String delimiter = ",";
        File delimitedFileToParse = new File("Path\\To\\File.csv");
        ParseClassesBuilder builder = new ParseClassesBuilder(delimiter, delimitedFileToParse);
        builder.writeClassToFile();    
    }

    public ParseClassesBuilder(String delimiter, File delimitedFileToParse) {
        this.delimiter = delimiter;
        this.delimitedFileToParse = delimitedFileToParse;
    }

    public void writeClassToFile() {
//        writeToFile(COLUMN_CLASS_NAME, buildColumnClass());
//        writeToFile(ROW_CLASS_NAME, buildRowClass());
//        writeToFile(GUI_TABLE_CLASS_NAME, buildTableClass());
//        writeToFile(EDITING_STRATEGY_CLASS_NAME, buildEditingStrategy());
    }

    private void writeToFile(String className, String content) {
        WritableFile out = null;
        try {
            out = new WritableFile(new File(PACKAGE_DIRECTORY, className + ".java"), FileUtils.OverwritePolicy.NO);
            out.writeLine("package " + PACKAGE_NAME + ";");
            out.write(content);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (out != null) {
                out.flushAndClose();
            }
        }
    }
}
