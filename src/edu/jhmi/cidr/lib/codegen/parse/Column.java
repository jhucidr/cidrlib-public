/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen.parse;

import edu.jhmi.cidr.lib.gui.table.CellEditing;
import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;

/**
 *
 * @author cidr
 */
/**
 * package access
 */
enum Column implements GuiTableColumn<Row> {

    FIELDNAME("Fieldname", Object.class, EditingStrategy.FIELDNAME) {
        @Override
        public Object getObject(Row row) {
            return row.getParsableColumn().getFieldName();
        }
    },
    TYPE("Type", Object.class, EditingStrategy.TYPE) {
        @Override
        public Object getObject(Row row) {
            return row.getParsableColumn().getTypeString();
        }
    },;
    private final String columnHeader;
    private final Class dataType;
    private CellEditing<Row> strategy;

    private Column(String columnHeader, Class dataType, CellEditing<Row> strategy) {
        this.columnHeader = columnHeader;
        this.dataType = dataType;
        this.strategy = strategy;
    }

    @Override
    public String getColumnHeader() {
        return columnHeader;
    }

    @Override
    public Class getDataType() {
        return dataType;
    }

    @Override
    public CellEditing<Row> getCellEditing(Row row) {
        return strategy;
    }

    @Override
    public String toString() {
        return columnHeader;
    }
}
