/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen.parse;

/**
 *
 * @author David Newcomer Oct 31, 2012
 */
public class ParseStrategyBuilder {

    private final String DELIMITER;
    private final String className;

    public ParseStrategyBuilder(String DELIMITER, String className) {
        this.DELIMITER = DELIMITER;
        this.className = className;
    }
    
}
//Is each column required?  aka, present in the file, or must not be empty in the file.
//What if you could get records of either type?  It should be fairly straightforward to 
//transform an Record<Column> that wraps an EnumMap into a Record.

//I'll write a seperate code generator that can be pointed at one of these column classes
//and generate the corresponding record with validation.

//In the meantime, I should use the record framework I created for Exemplar.