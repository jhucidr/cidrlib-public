/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen.parse;

import edu.jhmi.cidr.lib.codegen.enumgen.EnumNameConverter;
import javax.swing.table.TableCellEditor;
import edu.jhmi.cidr.lib.codegen.parse.ParsableColumn.Type;
import edu.jhmi.cidr.lib.gui.combobox.EnumComboBox;
import edu.jhmi.cidr.lib.gui.table.CellEditing;
import javax.swing.DefaultCellEditor;
import javax.swing.JOptionPane;

/**
 *
 * @author cidr
 */
enum EditingStrategy implements CellEditing<Row> {

    FIELDNAME {
        @Override
        public void notifyEditingStopped(Object o, Row row) {
            row.getParsableColumn().setFieldName(String.valueOf(o));
            row.update(Column.FIELDNAME);
        }
    },
    TYPE {
        @Override
        public void notifyEditingStopped(Object o, Row row) {
            if (o instanceof Type) {
                Type t = (Type) o;
                if (Type.OTHER == t) {
                    String answer = JOptionPane.showInputDialog("Please enter the class name that will fufill your wishes");
                    if (answer != null) {
                        row.getParsableColumn().setType(t);
                        row.getParsableColumn().setOtherTypeInfo(answer);
                    }
                } else {
                    row.getParsableColumn().setType((Type) o);
                }
            }
            row.update(Column.TYPE);
        }

        @Override
        public TableCellEditor getCellEditor(Row row) {
            return new DefaultCellEditor(new EnumComboBox(Type.class));
        }
    },;
    public static final CellEditing<Row> NO_OP = new CellEditing.NoOp<Row>();

    @Override
    public boolean canEdit(Row row) {
        return true;
    }

    @Override
    public TableCellEditor getCellEditor(Row row) {
        return null;
    }

    @Override
    public void notifyEditingCancelled(Object o, Row row) {
        // no-op                                                                          
    }

    @Override
    public String toString() {
        return EnumNameConverter.translateEnumNameIntoHumanReadableValue(super.toString());
    }
}
