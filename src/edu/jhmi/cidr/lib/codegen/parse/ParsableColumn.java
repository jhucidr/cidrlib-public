/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen.parse;

import edu.jhmi.cidr.lib.codegen.enumgen.EnumNameConverter;

/**
 *
 * @author David Newcomer Dec 13, 2012
 */
public class ParsableColumn {

    private String fieldName;
    private Type type = Type.STRING;
    private String otherTypeInfo = "";

    public ParsableColumn(String fieldName) {
        this.fieldName = fieldName;
    }

    void setType(Type type) {
        this.type = type;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Type getType() {
        return type;
    }

    public void setOtherTypeInfo(String otherTypeInfo) {
        this.otherTypeInfo = otherTypeInfo;
    }

    public String getTypeString() {
        if (type == Type.OTHER) {
            return otherTypeInfo;
        } else {
            return type.toString();
        }
    }

    static enum Type {

        STRING,
        INTEGER,
        LONG,
        DOUBLE,
        FLOAT,
        OTHER;
        private final String name;

        private Type() {
            this.name = EnumNameConverter.translateEnumNameIntoHumanReadableValue(super.name());
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
