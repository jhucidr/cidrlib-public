/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen.guitable;

import edu.jhmi.cidr.lib.file.WritableFile;
import edu.jhmi.cidr.lib.file.FileUtils;
import java.io.File;

/**
 *
 * @author David Newcomer May 14, 2012
 */
public class GuiTableClassesBuilder {

    private final String columnValues;
    private final String editingStrategyValues;
    private final String rowDataSource;

    private static final String COLUMN_CLASS_NAME = "Column";
    private static final String ROW_CLASS_NAME = "Row";
    private static final String GUI_TABLE_CLASS_NAME = "Table";
    private static final String EDITING_STRATEGY_CLASS_NAME = "EditingStrategy";
    private static final String PACKAGE_NAME = "edu.jhmi.cidr.projman.gui.project";
    private static final File PACKAGE_DIRECTORY = new File("C:\\cvs_workdir\\ProjMan2013\\src\\edu\\jhmi\\cidr\\projman\\gui\\project");
    private static final String DATA_SOURCE = "ProjectDetailsTuple";

    public static void main(String[] args) {
        String columnList = "Category,Entry Date,Comment";
        String editableList = "Entry Date,Comment";
        GuiTableClassesBuilder classesBuilder = new GuiTableClassesBuilder(columnList, editableList, DATA_SOURCE);
        classesBuilder.writeClassToFile();
    }

    public GuiTableClassesBuilder(String columnValues, String editingStrategyValues, String rowDataSource) {
        this.columnValues = columnValues;
        this.editingStrategyValues = editingStrategyValues;
        this.rowDataSource = rowDataSource;
    }

    public void writeClassToFile() {
        writeToFile(COLUMN_CLASS_NAME, buildColumnClass());
        writeToFile(ROW_CLASS_NAME, buildRowClass());
        writeToFile(GUI_TABLE_CLASS_NAME, buildTableClass());
        writeToFile(EDITING_STRATEGY_CLASS_NAME, buildEditingStrategy());
    }

    private String buildColumnClass() {
        StringBuilder sb = new StringBuilder();
        GuiColumnBuilder columnBuilder = new GuiColumnBuilder(columnValues, COLUMN_CLASS_NAME, ROW_CLASS_NAME, EDITING_STRATEGY_CLASS_NAME, rowDataSource, editingStrategyValues);
        sb.append(columnBuilder.build());
        return sb.toString();
    }

    private String buildRowClass() {
        StringBuilder sb = new StringBuilder();
        GuiRowBuilder rowBuilder = new GuiRowBuilder(rowDataSource, ROW_CLASS_NAME, GUI_TABLE_CLASS_NAME, COLUMN_CLASS_NAME);
        sb.append(rowBuilder.build());
        return sb.toString();
    }

    private String buildTableClass() {
        StringBuilder sb = new StringBuilder();
        GuiTableBuilder tableBuilder = new GuiTableBuilder(GUI_TABLE_CLASS_NAME, ROW_CLASS_NAME, COLUMN_CLASS_NAME);
        sb.append(tableBuilder.build());
        return sb.toString();
    }

    private String buildEditingStrategy() {
        StringBuilder sb = new StringBuilder();
        GuiEditingStrategyBuilder editingStrategyBuilder = new GuiEditingStrategyBuilder(editingStrategyValues, EDITING_STRATEGY_CLASS_NAME, ROW_CLASS_NAME, rowDataSource, COLUMN_CLASS_NAME);
        sb.append(editingStrategyBuilder.build());
        return sb.toString();
    }

//    private String buildSeparator(String className) {
//        StringBuilder sb = new StringBuilder();
//        sb.append("\n");
//        sb.append("==============================================\n");
//        sb.append("       ****** ").append(className).append(" ****** \n");
//        sb.append("==============================================\n");
//        sb.append("\n");
//        return sb.toString();
//    }
    private void writeToFile(String className, String content) {
        WritableFile out = null;
        try {
            out = new WritableFile(new File(PACKAGE_DIRECTORY, className + ".java"), FileUtils.OverwritePolicy.NO);
            out.writeLine("package " + PACKAGE_NAME + ";");
            out.write(content);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (out != null) {
                out.flushAndClose();
            }
        }

    }
}
