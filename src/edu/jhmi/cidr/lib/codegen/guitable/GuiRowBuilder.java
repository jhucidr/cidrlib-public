/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen.guitable;

/**
 *
 * @author David Newcomer May 14, 2012
 */
/*
 * package access
 */ class GuiRowBuilder {

    private final String myDataSource;
    private final String myClassName;
    private final String guiTableClassName;
    private final String columnClassName;

    public static void main(String[] args) {
        //Example Implementation:
        GuiRowBuilder builder = new GuiRowBuilder("String");
        System.out.println(builder.build());
    }

    public GuiRowBuilder(String myDataSource) {
        this(myDataSource, "Row", "Table", "Column");
    }

    public GuiRowBuilder(String myDataSource, String myClassName, String GuiTableClassName, String columnClassName) {
        this.myDataSource = myDataSource;
        this.myClassName = myClassName;
        this.guiTableClassName = GuiTableClassName;
        this.columnClassName = columnClassName;
    }

    public String build() {
        StringBuilder sb = new StringBuilder();
        sb.append(buildImports());
        sb.append(buildClassDeclarationAndMembers());
        sb.append(buildConstructor());
        sb.append(buildMethods());
        return sb.toString();
    }

    private String buildImports() {
        StringBuilder sb = new StringBuilder();
        sb.append("import edu.jhmi.cidr.lib.gui.Changeable;                           \n");
        sb.append("import edu.jhmi.cidr.lib.gui.table.HasAdjustableHeight;            \n");
        sb.append("                                                                             \n");
        sb.append("                                                                             \n");
        return sb.toString();
    }

    private String buildClassDeclarationAndMembers() {
        StringBuilder sb = new StringBuilder();
        sb.append("/* package access */ class ").append(myClassName);
        sb.append(" implements HasAdjustableHeight, Changeable<").append(columnClassName).append("> {     \n");
        sb.append("                                                                                       \n");
        sb.append("    private final ").append(guiTableClassName).append(" table;                         \n");
        sb.append("    private final ChangeTracker<").append(columnClassName).append("> changeTracker;    \n");
        sb.append("    private final ").append(myDataSource).append(" ").append(myDataSource).append(";   \n");
        sb.append("                                                                                       \n");
        return sb.toString();
    }

    private String buildConstructor() {
        StringBuilder sb = new StringBuilder();
        sb.append("    public ").append(myClassName).append("(");
        sb.append(myDataSource).append(" dataSource, ").append(guiTableClassName).append(" table) {          \n");
        sb.append("        if (dataSource == null) {                                                         \n");
        sb.append("            throw new NullPointerException(\"dataSource must not be null\");              \n");
        sb.append("        }                                                                                 \n");
        sb.append("        if (table == null) {                                                              \n");
        sb.append("            throw new NullPointerException(\"table must not be null\");                   \n");
        sb.append("        }                                                                                 \n");
        sb.append("        this.").append(myDataSource).append(" = dataSource;                               \n");
        sb.append("        this.table = table;                                                               \n");
        sb.append("        this.changeTracker = new ChangeTracker<").append(columnClassName).append(">();    \n");
        sb.append("    }                                                                                     \n");
        sb.append("                                                                                          \n");
        return sb.toString();
    }

    private String buildMethods() {
        StringBuilder sb = new StringBuilder();
        sb.append("    public ").append(guiTableClassName).append(" getTable() {                              \n");
        sb.append("        return table;                                                                      \n");
        sb.append("    }                                                                                      \n");
        sb.append("                                                                                           \n");
        sb.append("    public ").append(myDataSource).append(" get").append(myDataSource).append("(){         \n");
        sb.append("        return ").append(myDataSource).append(";                                           \n");
        sb.append("    }                                                                                      \n");
        sb.append("                                                                                           \n");
        sb.append("    @Override                                                                              \n");
        sb.append("    public int getHeightMultiplier() {                                                     \n");
        sb.append("        return 1;                                                                          \n");
        sb.append("    }                                                                                      \n");
        sb.append("                                                                                           \n");
        sb.append("    @Override                                                                              \n");
        sb.append("    public ChangeTracker<").append(columnClassName).append("> getChanges() {               \n");
        sb.append("        return changeTracker;                                                              \n");
        sb.append("    }                                                                                      \n");
        sb.append("                                                                                           \n");
        sb.append("    public void update(").append(columnClassName).append("... columns) {                   \n");
        sb.append("        getTable().updateRow(this, columns);                                               \n");
        sb.append("    }                                                                                      \n");
        sb.append("                                                                                           \n");
        sb.append("    @Override                                                                              \n");
        sb.append("    public boolean equals(Object o) {                                                      \n");
        sb.append("        if (o instanceof ").append(myClassName).append(") {                                \n");
        sb.append("            ").append(myClassName).append(" that = (").append(myClassName).append(") o;    \n");
        sb.append("            return this.get").append(myDataSource).append("().equals(that.get").append(myDataSource).append("());      \n");
        sb.append("        }                                                                                  \n");
        sb.append("        return false;                                                                      \n");
        sb.append("    }                                                                                      \n");
        sb.append("                                                                                           \n");
        sb.append("    @Override                                                                              \n");
        sb.append("    public int hashCode() {                                                                \n");
        sb.append("        return get").append(myDataSource).append("().hashCode();                           \n");
        sb.append("    }                                                                                      \n");
        sb.append("}                                                                                          \n");
        return sb.toString();
    }
}
