/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen.guitable;

import edu.jhmi.cidr.lib.codegen.enumgen.EnumNameConverter;

/**
 *
 * @author David Newcomer May 14, 2012
 */
/*
 * package access
 */ class GuiEditingStrategyBuilder {

    private final String myClassName;
    private final String rowClassName;
    private final String rowDataSourceName;
    private final String columnClassName;
    private final String editingStrategyValues;

    public static void main(String[] args) {
        //Example Implementation:
        GuiEditingStrategyBuilder builder = new GuiEditingStrategyBuilder("Example");
        System.out.println(builder.build());
    }

    public GuiEditingStrategyBuilder(String enumValues) {
        this(enumValues, "EditingStrategy", "Row", "dataSource", "Column");
    }

    public GuiEditingStrategyBuilder(String enumValues, String myClassName, String rowClassName, String rowDataSourceName, String columnClassName) {
        this.editingStrategyValues = enumValues;
        this.myClassName = myClassName;
        this.rowClassName = rowClassName;
        this.rowDataSourceName = rowDataSourceName;
        this.columnClassName = columnClassName;
    }

    public String build() {
        StringBuilder sb = new StringBuilder();
        sb.append(getImports());
        sb.append(getEnumData());
        sb.append(getMethodsAndMembers());
        return sb.toString();
    }

    private String getImports() {
        StringBuilder sb = new StringBuilder();
        sb.append("import edu.jhmi.cidr.lib.gui.table.CellEditing;                    \n");
        sb.append("import javax.swing.table.TableCellEditor;                                    \n");
        sb.append("import edu.jhmi.cidr.lib.codegen.enumgen.EnumNameConverter;        \n");
        sb.append("                                                                             \n");
        sb.append("/**                                                                          \n");
        sb.append(" *                                                                           \n");
        sb.append(" * @author cidr                                                              \n");
        sb.append(" */                                                                          \n");
        return sb.toString();
    }

    private String getEnumData() {
        StringBuilder sb = new StringBuilder();
        sb.append("enum ").append(myClassName).append(" implements CellEditing<").append(rowClassName).append("> {  \n");
        sb.append("                                                                                                 \n");
           for (String enumValue : editingStrategyValues.split(",")) {
            String enumName = EnumNameConverter.getEnumNameReplacingNumbersWithWords(enumValue);
            sb.append("    ").append(enumName).append("{         \n");
            sb.append("                                                                                                   \n");
            sb.append("                                                                                                   \n");
            sb.append("        @Override                                                                                  \n");
            sb.append("        public void notifyEditingStopped(Object o, ").append(rowClassName).append(" row) {         \n");
            sb.append("//          if (false == ").append(columnClassName).append(".").append(enumName).append(".getObject(row).equals(o)) {           \n");
            sb.append("//              row.get").append(rowDataSourceName).append("().set").append(enumValue).append("(o);                             \n");
            sb.append("//              row.getChanges().setHasUnsavedChanges(").append(columnClassName).append(".").append(enumName).append(", true);  \n");
            sb.append("//          }                                                                                      \n");
            sb.append("//          row.update(").append(columnClassName).append(".").append(enumName).append(");          \n");
            sb.append("        }                                                                                          \n");
            sb.append("    },                                                                                             \n");
        }
        sb.append(";\n");
        return sb.toString();
    }

    private String getMethodsAndMembers() {
        StringBuilder sb = new StringBuilder();
        sb.append("    public static final CellEditing<").append(rowClassName);
        sb.append("> NO_OP = new CellEditing.NoOp<").append(rowClassName).append(">();\n");
        sb.append("                                                                                              \n");
        sb.append("        @Override                                                                             \n");
        sb.append("        public boolean canEdit(").append(rowClassName).append(" row) {                        \n");
        sb.append("            return true;                                                                      \n");
        sb.append("        }                                                                                     \n");
        sb.append("                                                                                              \n");
        sb.append("        @Override                                                                             \n");
        sb.append("        public TableCellEditor getCellEditor(").append(rowClassName).append(" row) {          \n");
        sb.append("            return null;                                                                      \n");
        sb.append("        }                                                                                     \n");
        sb.append("                                                                                              \n");
        sb.append("        @Override                                                                             \n");
        sb.append("        public void notifyEditingCancelled(Object o, ").append(rowClassName).append(" row) {  \n");
        sb.append("            // no-op                                                                          \n");
        sb.append("        }                                                                                     \n");
        sb.append("                                                                                              \n");
        sb.append("    @Override                                                                                 \n");
        sb.append("    public String toString() {                                                                \n");
        sb.append("        return EnumNameConverter.translateEnumNameIntoHumanReadableValue(super.toString());   \n");
        sb.append("    }                                                                                         \n");
        sb.append("}                                                                                             \n");
        return sb.toString();
    }
}
