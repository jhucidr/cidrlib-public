/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen.guitable;

import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.codegen.enumgen.EnumNameConverter;
import java.util.List;

/**
 *
 * @author David Newcomer May 14, 2012
 */
/*
 * package access
 */ class GuiColumnBuilder {

    private final String columnsToConvert;
    private final String columnClassName;
    private final String rowClassName;
    private final String editingStrategyClassName;
    private final String rowDataSource;
    private final List<String> editingStrategyValues;

    public static void main(String[] args) {
        //Example Implementation:
        GuiColumnBuilder builder = new GuiColumnBuilder("column1,column2,column3,column4");
        System.out.println(builder.build());
    }

    public GuiColumnBuilder(String columnsToConvert) {
        this(columnsToConvert, "Column", "Row", "EditingStrategy", "dataSource", "");
    }

    public GuiColumnBuilder(String columnsToConvert, String columnClassName, String rowClassName, String editingStrategyClassName, String rowDataSource, String editingStrategyValues) {
        this.columnsToConvert = columnsToConvert;
        this.columnClassName = columnClassName;
        this.rowClassName = rowClassName;
        this.editingStrategyClassName = editingStrategyClassName;
        this.rowDataSource = rowDataSource;
        this.editingStrategyValues = Utils.asList(editingStrategyValues.split(","));
    }

    public String build() {
        StringBuilder sb = new StringBuilder();
        sb.append(getImports());
        sb.append(getEnumValues());
        sb.append(getMethodsAndMembers());
        return sb.toString();
    }

    private String getImports() {
        StringBuilder sb = new StringBuilder();
        sb.append("import edu.jhmi.cidr.lib.gui.table.CellEditing;              \n");
//        sb.append("import edu.jhmi.cidr.lib.gui.table.CellRendering;            \n");
        sb.append("import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;           \n");
        sb.append("import edu.jhmi.cidr.lib.codegen.enumgen.EnumNameConverter;  \n");
        sb.append("import javax.swing.table.TableCellRenderer;                            \n");
        sb.append("                                                                       \n");
        sb.append("/**                                                                    \n");
        sb.append(" *                                                                     \n");
        sb.append(" * @author cidr                                                        \n");
        sb.append(" */                                                                    \n");
        return sb.toString();
    }

    private String getEnumValues() {
        StringBuilder sb = new StringBuilder();
        sb.append("/** package access*/ enum ").append(columnClassName);
        sb.append(" implements GuiTableColumn<").append(rowClassName).append(">");
//        sb.append(", CellRendering<").append(rowClassName).append("> ");
        sb.append("{\n");
        for (String enumValue : columnsToConvert.split(",")) {
            String enumName = EnumNameConverter.getEnumNameReplacingNumbersWithWords(enumValue);
            String editingStrategy = editingStrategyValues.contains(enumValue) ? enumName : "NO_OP";
            sb.append("    ").append(enumName).append("(\"").append(enumValue).append("\", Object.class, ");
            sb.append(editingStrategyClassName).append(".").append(editingStrategy).append(") {        \n");
            sb.append("                                                                                                      \n");
            sb.append("        @Override                                                                                     \n");
            sb.append("        public Object getObject(").append(rowClassName).append(" row) {                               \n");
            sb.append("            //return row.get").append(rowDataSource).append("().get").append(enumValue).append("();   \n");
            sb.append("            throw new UnsupportedOperationException(\"Not yet supported.\");                          \n");
            sb.append("        }                                                                                             \n");
            sb.append("                                                                                                      \n");
            sb.append("    },                                                                                                \n");
        }
        sb.append(";\n");
        return sb.toString();
    }

    private String getMethodsAndMembers() {
        StringBuilder sb = new StringBuilder();
        sb.append("    private final String columnHeader;                                               \n");
        sb.append("    private final Class dataType;                                                    \n");
        sb.append("    private CellEditing<").append(rowClassName).append("> strategy;                  \n");
        sb.append("                                                                                     \n");
        sb.append("    private ").append(columnClassName).append("(String columnHeader, Class dataType, CellEditing<");
        sb.append(rowClassName).append("> strategy) {                                                   \n");
        sb.append("        this.columnHeader = columnHeader;                                            \n");
        sb.append("        this.dataType = dataType;                                                    \n");
        sb.append("        this.strategy = strategy;                                                    \n");
        sb.append("    }                                                                                \n");
        sb.append("                                                                                     \n");
        sb.append("    @Override                                                                        \n");
        sb.append("    public String getColumnHeader() {                                                \n");
        sb.append("        return columnHeader;                                                         \n");
        sb.append("    }                                                                                \n");
        sb.append("                                                                                     \n");
        sb.append("    @Override                                                                        \n");
        sb.append("    public Class getDataType() {                                                     \n");
        sb.append("        return dataType;                                                             \n");
        sb.append("    }                                                                                \n");
        sb.append("                                                                                     \n");
        sb.append("    @Override                                                                        \n");
        sb.append("    public CellEditing<").append(rowClassName).append("> getCellEditing(").append(rowClassName).append(" row) {          \n");
        sb.append("        return strategy;                                                             \n");
        sb.append("    }                                                                                \n");
        sb.append("                                                                                     \n");
//        sb.append("    @Override                                                                        \n");
//        sb.append("    public TableCellRenderer getCellRenderer(").append(rowClassName).append(" row) { \n");
//        sb.append("        return Rendering.getDefaultRenderer(this, row);                              \n");
//        sb.append("    }                                                                                \n");
//        sb.append("                                                                                     \n");
        sb.append("    @Override                                                                        \n");
        sb.append("    public String toString() {                                                       \n");
        sb.append("        return columnHeader;                                                         \n");
        sb.append("    }                                                                                \n");
        sb.append("}                                                                                    \n");
        return sb.toString();
    }
}
