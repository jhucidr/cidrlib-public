/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen.guitable;

/**
 *
 * @author David Newcomer
 * May 14, 2012
 */
public class GuiTableBuilder {

    private final String myClassName;
    private final String rowClassName;
    private final String columnClassName;

    public static void main(String[] args) {
        //Example Implementation:
        GuiTableBuilder builder = new GuiTableBuilder();
        System.out.println(builder.build());
    }

    public GuiTableBuilder() {
        this("Table", "Row", "Column");
    }

    public GuiTableBuilder(String myClassName, String rowClassName, String columnClassName) {
        this.myClassName = myClassName;
        this.rowClassName = rowClassName;
        this.columnClassName = columnClassName;
    }

    public String build() {
        StringBuilder sb = new StringBuilder();
        sb.append(buildImports());
        sb.append(buildDeclarationAndConstructor());
        sb.append(buildMethods());
        return sb.toString();
    }

    private String buildImports() {
        StringBuilder sb = new StringBuilder();
        sb.append("import edu.jhmi.cidr.lib.gui.table.GuiTable;                           \n");
        sb.append("import java.util.ArrayList;                                            \n");
        sb.append("import java.util.Iterator;                                             \n");
        sb.append("import java.util.List;                                                 \n");
        sb.append("                                                                       \n");
        sb.append("/**                                                                    \n");
        sb.append(" *                                                                     \n");
        sb.append(" * @author cidr                                                        \n");
        sb.append(" */                                                                    \n");
        sb.append("/* package access */                                                   \n");
        return sb.toString();
    }

    private String buildDeclarationAndConstructor() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ");
        sb.append(myClassName);
        sb.append(" extends GuiTable<").append(rowClassName).append("> {                  \n");
        sb.append("                                                                       \n");
        sb.append("    public ").append(myClassName).append("() {                         \n");
        sb.append("        super(").append(columnClassName).append(".values());           \n");
        sb.append("    }                                                                  \n");
        sb.append("                                                                       \n");
        return sb.toString();
    }

    private String buildMethods() {
        StringBuilder sb = new StringBuilder();
        sb.append("                                                                       \n");
//        sb.append("    public ClientContext getClientContext() {                          \n");
//        sb.append("        return context;                                                \n");
//        sb.append("    }                                                                  \n");
        sb.append("}                                                                      \n");
        return sb.toString();
    }
}
