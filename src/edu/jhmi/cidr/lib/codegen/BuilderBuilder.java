/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen;

/**
 *
 * @author jnewcome
 */
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author David Newcomer May 24, 2012
 */
public class BuilderBuilder {

    private final List<Meadow> fields;
    private final Class clazz;
    private final boolean includeBuildIfValidMethod = true;

    public static void main(String[] args) {
        System.out.println(BuilderBuilder.constructABuilder(Object.class));
    }

    private BuilderBuilder(Class clazz) {
        if (clazz == null) {
            throw new NullPointerException("clazz must not be null");
        }
        this.clazz = clazz;
        this.fields = grasslandWhichIsNotGrazedByDomesticLivestockButRatherAllowedToGrowUncheckedInOrderToMakeHay(clazz);
    }

    private boolean isEntity() {
        @SuppressWarnings("unchecked")
        boolean retVal = (clazz.getAnnotation(javax.persistence.Entity.class) != null);
        return retVal;
    }

    private static List<Meadow> grasslandWhichIsNotGrazedByDomesticLivestockButRatherAllowedToGrowUncheckedInOrderToMakeHay(Class clazz) {
        List<Meadow> pasture = new ArrayList<Meadow>();
        for (Field field : clazz.getDeclaredFields()) {
            pasture.add(new Meadow(field));
        }
        return pasture;
    }

    public static String constructABuilder(Class clazz) {
        BuilderBuilder mason = new BuilderBuilder(clazz);
        return mason.builderBuilder();
    }
    private static final List<String> ENTITY_FIELDNAMES_TO_EXCLUDE = Arrays.asList("id", "version", "serialVersionUID");

    private void performEntitySpecificStuff() {
        List<Meadow> toRemove = new ArrayList<Meadow>();
        for (Meadow m : fields) {
            for (String name : ENTITY_FIELDNAMES_TO_EXCLUDE) {
                if (m.field.getName().equalsIgnoreCase(name)) {
                    toRemove.add(m);
                }
            }
        }
        fields.removeAll(toRemove);
    }

    private String builderBuilder() {
        StringBuilder sb = new StringBuilder();
        if (isEntity()) {
            performEntitySpecificStuff();
        }
        sb.append(buildHeader());
        sb.append(buildMembers());
        sb.append(buildConstructor());
        sb.append(buildMethods());
        sb.append(buildExternalConstructor());
        return sb.toString();
    }

    private String buildHeader() {
        return "public static class Builder {\n\n";
    }

    private String buildMembers() {
        StringBuilder sb = new StringBuilder();
        for (Meadow meadow : fields) {
            sb.append(meadow.buildMember()).append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    private String buildConstructor() {
        return "public Builder(){}\n\n";
    }

    private String buildMethods() {
        StringBuilder sb = new StringBuilder();
        for (Meadow meadow : fields) {
            sb.append(meadow.buildMethod()).append(System.getProperty("line.separator"));
        }
        sb.append(buildValidateRequiredFieldsMethod());
        sb.append(isEntity() ? buildBuildAndMergeMethod() : buildBuildMethod()).append("\n");
        if (includeBuildIfValidMethod) {
            sb.append(buildIfValidMethod());
        }
        sb.append("}\n");
        return sb.toString();
    }

    private String buildIfValidMethod() {
        StringBuilder sb = new StringBuilder();
        sb.append("        public Optional<").append(clazz.getSimpleName()).append("> buildIfValid(TypeCaster v, ClientContext cc) {   \n");
        sb.append("            if (v.noErrorsDetected()) {                                                                            \n");
        sb.append("                try {                                                                                              \n");
        sb.append("                    return Optional.of(build());                                                                   \n");
        sb.append("                } catch (NullPointerException e) {                                                                 \n");
        sb.append("                    ErrorMessage error = new ErrorMessage(\"\", \"Required fields were missing...\", e);           \n");
        sb.append("                    GuiUtils.reportError(cc, error);                                                               \n");
        sb.append("                }                                                                                                  \n");
        sb.append("            } else {                                                                                               \n");
        sb.append("                v.reportErrors(cc);                                                                                \n");
        sb.append("            }                                                                                                      \n");
        sb.append("            return Optional.absent();                                                                              \n");
        sb.append("        }                                                                                                          \n");
        return sb.toString();
    }

    private String buildValidateRequiredFieldsMethod() {
        StringBuilder sb = new StringBuilder();
        sb.append("private void validateRequiredFields(){\n");
        sb.append("        List<String> nullRequiredFields = new ArrayList<String>();                                                \n");
        for (Meadow meadow : fields) {
            if (meadow.isRequired()) {
                sb.append(meadow.fieldCheck());
            }
        }
        sb.append("        if(nullRequiredFields.isEmpty() == false){                                                                \n");
        sb.append("            throw new NullPointerException(\"The following required fields were missing: \" + nullRequiredFields);\n");
        sb.append("        }                                                                                                         \n");
        sb.append("   }                                                                                                              \n");
        return sb.toString();
    }

    private String buildBuildMethod() {
        StringBuilder sb = new StringBuilder();
        sb.append("public ").append(clazz.getSimpleName()).append(" build() {\n");
        sb.append("    return new ").append(clazz.getSimpleName()).append("(this);\n}\n");
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }

    private String buildBuildAndMergeMethod() {
        StringBuilder sb = new StringBuilder();
        sb.append("public ").append(clazz.getSimpleName()).append(" buildAndMerge(ClientContext context) throws Exception {\n");
        sb.append("            if (context == null) {                                                    \n");
        sb.append("                throw new NullPointerException(\"ClientContext must not be null\");\n");
        sb.append("            }                                                                    \n");
        sb.append("            return SessionUtils.merge(context, new ").append(clazz.getSimpleName()).append("(this));\n}\n");
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }

    private String buildExternalConstructor() {
        StringBuilder sb = new StringBuilder();
        sb.append("private ").append(clazz.getSimpleName()).append("(Builder builder) {\n");

        sb.append("builder.validateRequiredFields();\n");
        for (Meadow meadow : fields) {
            sb.append(meadow.buildConstructorAssignment());
        }
        sb.append("}\n");
        return sb.toString();
    }

    public static class Meadow {

        private final Field field;

        public Meadow(Field field) {
            if (field == null) {
                throw new NullPointerException("field must not be null");
            }
//            field.setAccessible(true);
            this.field = field;
        }

        public Class getType() {
            return field.getType();
        }

        public String typeName() {
            return getTypeName(field.getGenericType());
        }

        public static String getTypeName(Type type) {
            if (type instanceof Class) {
                Class clazz = (Class) type;
                return clazz.getSimpleName();
            } else if (type instanceof ParameterizedType) {
                StringBuilder sb = new StringBuilder();
                ParameterizedType pType = (ParameterizedType) type;
                for (Type t : pType.getActualTypeArguments()) {
                    sb.append(sb.length() == 0 ? getTypeName(pType.getRawType()) + "<" : ",");
                    sb.append(getTypeName(t));
                }
                sb.append(sb.length() == 0 ? "" : ">");
                return sb.toString();
            } else {
                throw new IllegalStateException("Unrecognized type class: " + type);
            }
        }

        public boolean isFinal() {
            return Modifier.isFinal(field.getModifiers());
        }

        public String buildMember() {
            StringBuilder sb = new StringBuilder();
            sb.append("private ").append(typeName()).append(" ").append(field.getName()).append(";");
            return sb.toString();
        }

        public String buildMethod() {
            StringBuilder sb = new StringBuilder();
            sb.append("public Builder ").append(field.getName()).append("(");
            sb.append(typeName()).append(" ").append(field.getName()).append("){\n");
//            sb.append(nullcheck(field));
            sb.append("this.").append(field.getName()).append(" = ").append(field.getName()).append(";\n");
            sb.append("return this;\n}\n");
            return sb.toString();
        }

        private String nullcheck(Field field) {
            if (getType().isPrimitive()) {
                return "";
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("if (");
                sb.append(field.getName());
                sb.append(" == null) {\n");
                sb.append("    throw new NullPointerException(\"").append(field.getName()).append(" must not be null\");\n");
                sb.append("}\n");
                return sb.toString();
            }
        }

        private String fieldCheck() {
            if (getType().isPrimitive()) {
                return "";
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("if (").append(field.getName()).append(" == null) {\n");
                sb.append("    nullRequiredFields.add(\"").append(field.getName()).append("\");\n");
                sb.append("}\n");
                return sb.toString();
            }
        }

        public boolean isRequired() {
//            edu.jhmi.cidr.lib.api.annotations.Required required = field.getAnnotation(edu.jhmi.cidr.lib.api.annotations.Required.class);
//            if (required != null) {
//                return true;
//            }
            if (Modifier.isFinal(field.getModifiers())) {
                return true;
            }
            return isNullable() ? false : true;
        }

        public boolean isNullable() {
            javax.persistence.JoinColumn joinColumn = field.getAnnotation(javax.persistence.JoinColumn.class);
            if (joinColumn != null) {
                return joinColumn.nullable();
            }
            javax.persistence.Column column = field.getAnnotation(javax.persistence.Column.class);
            if (column != null) {
                return column.nullable();
            }
            return true;
        }

        public String buildConstructorAssignment() {
            StringBuilder sb = new StringBuilder();
            sb.append("this.").append(field.getName()).append(" = ").append("builder.").append(field.getName()).append(";\n");
            return sb.toString();
        }
    }
}
