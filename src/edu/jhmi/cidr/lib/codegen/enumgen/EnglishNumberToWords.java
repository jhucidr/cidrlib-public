/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen.enumgen;

/**
 * 
 * @author David Newcomer
 * Oct 24, 2011
 */
public class EnglishNumberToWords {

    /**
     * testing
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("*** " + EnglishNumberToWords.convert(-999));
        System.out.println("*** " + EnglishNumberToWords.convert(0));
        System.out.println("*** " + EnglishNumberToWords.convert(1));
        System.out.println("*** " + EnglishNumberToWords.convert(16));
        System.out.println("*** " + EnglishNumberToWords.convert(100));
        System.out.println("*** " + EnglishNumberToWords.convert(118));
        System.out.println("*** " + EnglishNumberToWords.convert(200));
        System.out.println("*** " + EnglishNumberToWords.convert(219));
        System.out.println("*** " + EnglishNumberToWords.convert(800));
        System.out.println("*** " + EnglishNumberToWords.convert(801));
        System.out.println("*** " + EnglishNumberToWords.convert(1316));
        System.out.println("*** " + EnglishNumberToWords.convert(1000000));
        System.out.println("*** " + EnglishNumberToWords.convert(2000000));
        System.out.println("*** " + EnglishNumberToWords.convert(3000200));
        System.out.println("*** " + EnglishNumberToWords.convert(700000));
        System.out.println("*** " + EnglishNumberToWords.convert(9000000));
        System.out.println("*** " + EnglishNumberToWords.convert(9001000));
        System.out.println("*** " + EnglishNumberToWords.convert(123456789));
        System.out.println("*** " + EnglishNumberToWords.convert(2147483647));
        System.out.println("*** " + EnglishNumberToWords.convert(3000000010L));
    }

    private static String convertLessThanOneThousand(int number) {
        if (number >= 1000) {
            throw new IllegalArgumentException("This method can only convert numbers less than or equal to 100.  " + number + " is an illegal argument.");
        }
        if (number == 0) {
            return "";
        }
        StringBuilder retVal = new StringBuilder();
        if (number > 99) {
            retVal.append(ValueUnderTwenty.getValue(number / 100).getKey());
            retVal.append(" hundred");
        }
        Tens tensValue = Tens.getValue(number);
        if (tensValue != Tens.TEN) {
            retVal.append(tensValue.getKey());
        }
        retVal.append(ValueUnderTwenty.getValue(number).getKey());
        return retVal.toString();
    }

    public static String convert(long numberToConvert) {
        if (numberToConvert == 0) {
            return "zero";
        }
        StringBuilder retVal = new StringBuilder();
        if (numberToConvert < 0) {
            retVal.append("negative");
            numberToConvert = Math.abs(numberToConvert);
        }
        for (Magnitude m : Magnitude.values()) {
            retVal.append(m.getValue(numberToConvert));
        }
        return retVal.toString().trim();
    }

    private enum Magnitude {

        TRILLIONS("trillion", tenToThe(12)),
        BILLIONS("billion", tenToThe(9)),
        MILLIONS("million", tenToThe(6)),
        HUNDRED_THOUSANDS("thousand", tenToThe(3)),
        THOUSANDS("", tenToThe(0)),;
        private final String magnitude;
        private final long minimumValue;

        private Magnitude(String magnitude, long minimumValue) {
            this.magnitude = magnitude;
            this.minimumValue = minimumValue;
        }

        private static long tenToThe(int power) {
            return (long) Math.pow(10, power);
        }

        public long getMinimumValue() {
            return minimumValue;
        }

        public String getMagnitude() {
            return magnitude;
        }

        public String getValue(long numberToParse) {
            if (numberToParse >= this.getMinimumValue()) {
                numberToParse = numberToParse / this.getMinimumValue();
                int valueToParse = (int) (numberToParse % 1000);
                if (valueToParse > 0) {
                    return convertLessThanOneThousand(valueToParse) + " " + this.getMagnitude();
                }
            }
            return "";
        }
    }

    public static enum Tens {

        ZERO(""),
        TEN(" ten"),
        TWENTY(" twenty"),
        THIRTY(" thirty"),
        FORTY(" forty"),
        FIFTY(" fifty"),
        SIXTY(" sixty"),
        SEVENTY(" seventy"),
        EIGHTY(" eighty"),
        NINETY(" ninety"),;
        private final String key;

        private Tens(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }

        public static Tens getValue(int valueToGet) {
            int tensDigit = (valueToGet % 100) / 10;
            return Tens.values()[tensDigit];
        }
    }

    public enum ValueUnderTwenty {

        ZERO(""),
        ONE(" one"),
        TWO(" two"),
        THREE(" three"),
        FOUR(" four"),
        FIVE(" five"),
        SIX(" six"),
        SEVEN(" seven"),
        EIGHT(" eight"),
        NINE(" nine"),
        TEN(" ten"),
        ELEVEN(" eleven"),
        TWELVE(" twelve"),
        THIRTEEN(" thirteen"),
        FOURTEEN(" fourteen"),
        FIFTEEN(" fifteen"),
        SIXTEEN(" sixteen"),
        SEVENTEEN(" seventeen"),
        EIGHTEEN(" eighteen"),
        NINETEEN(" nineteen"),;
        private final String key;

        private ValueUnderTwenty(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }

        public static ValueUnderTwenty getValue(int valueToGet) {
            int valueUnderNinetyNine = valueToGet % 100;
            if (valueUnderNinetyNine < 20) {
                return ValueUnderTwenty.values()[valueUnderNinetyNine];
            } else {
                int valueUnderTen = valueToGet % 10;
                return ValueUnderTwenty.values()[valueUnderTen];
            }
        }
    }
}
