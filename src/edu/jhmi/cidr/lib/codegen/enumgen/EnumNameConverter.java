/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.codegen.enumgen;

//import edu.jhmi.cidr.exemplar.plugins.utils.enm.EnglishNumberToWords;
/**
 *
 * @author David Newcomer Oct 21, 2011
 */
public class EnumNameConverter {

    private static String toConvert = "Assessment problem,Chromosome anomaly,Contamination problem,DNA source problem,Documentation problem,Duplicate problem,Gender problem,Performance problem,Receipt problem,Relatedness problem";

    public static void main(String[] args) {
        System.out.println(buildEnum("CanonicalProblemDescription", toConvert, true));
    }

    public static String buildEnum(String className, String fields, boolean convertNumbersToWords) {
        return buildEnum(className, fields.split(","), true);
    }

    public static String buildEnum(String className, String[] fields) {
        return buildEnum(className, fields, true);
    }

    public static String buildEnum(String className, String[] fields, boolean convertNumbersToWords) {
        StringBuilder sb = new StringBuilder();
        sb.append("public enum ").append(className).append(" {               \n");
        sb.append(getEnumValues(fields, convertNumbersToWords));
        sb.append(";                                                         \n");
        sb.append("    private final String key;                             \n");
        sb.append("    private ").append(className).append("(String key) {   \n");
        sb.append("        this.key = key;                                   \n");
        sb.append("    }                                                     \n");
        sb.append("    public String getKey() {                              \n");
        sb.append("        return key;                                       \n");
        sb.append("    }                                                     \n");
        sb.append("    public boolean isLast() {                             \n");
        sb.append("        return ordinal() == values().length - 1;          \n");
        sb.append("    }                                                     \n");
        sb.append("} ");
        return sb.toString();
    }

    public static String getEnumValues(String[] values) {
        return getEnumValues(values, true);
    }

    public static String getEnumValues(String[] values, boolean convertNumbersToWords) {
        StringBuilder sb = new StringBuilder();
        for (String string : values) {
            sb.append(convertNumbersToWords ? getEnumNameReplacingNumbersWithWords(string) : getEnumName(string));
            sb.append("(\"");
            sb.append(string);
            sb.append("\"),");
            sb.append(System.getProperty("line.separator"));
        }
        return sb.toString();
    }

    public static String getEnumNameReplacingNumbersWithWords(String nameToConvert) {
        String enumName = getEnumName(nameToConvert);
        enumName = replaceDigitsWithWords(enumName);
        if (enumName.startsWith("_")) {
            //If the enum name started with a number, it will have
            //a leading underscore.  This removes it.
            enumName = enumName.substring(1);
        }
        return enumName;
    }

    public static String getEnumName(String nameToConvert) {
        if (nameToConvert == null) {
            throw new NullPointerException("nameToConvert must not be null");
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < nameToConvert.length(); i++) {
            char c = nameToConvert.charAt(i);
            if (sb.toString().isEmpty() == false) {
                if (sb.charAt(sb.length() - 1) != '_') {
                    //The enum name will not have any repeating underscores
                    if (meetsAnyCondition(nameToConvert, i)) {
                        sb.append("_");
                    }
                }
            } else if (Character.isDigit(c)) {
                //If the first character in the name to convert is a numerical digit,
                //Then the enum name will start with an underscore.
                sb.append("_");
            }
            if (Character.isLetterOrDigit(c)) {
                sb.append(c);
            }
        }
        return sb.toString().toUpperCase();
    }

    private static boolean meetsAnyCondition(String toTest, int index) {
        for (Condition condition : Condition.values()) {
            if (condition.isMet(toTest, index)) {
                return true;
            }
        }
        return false;
    }

    private static String replaceDigitsWithWords(String enumName) {
        if (enumName == null) {
            throw new NullPointerException("enumName must not be null");
        }
        String[] values = enumName.split("_");
        StringBuilder retVal = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            if (i > 0) {
                retVal.append("_");
            }
            String string = values[i];
            try {
                int value = Integer.parseInt(string);
                String numberName = EnglishNumberToWords.convert(value);
                retVal.append(getEnumName(numberName));
            } catch (NumberFormatException ex) {
                retVal.append(string);
            }
        }
        return retVal.toString();
    }

    public static String translateEnumNameIntoHumanReadableValue(String toTranslate) {
        StringBuilder sb = new StringBuilder();
        for (String word : toTranslate.split("_")) {
            sb.append(sb.length() == 0 ? "" : " ");
            for (int i = 0; i < word.toCharArray().length; i++) {
                char c = word.toCharArray()[i];
                sb.append(i == 0 ? Character.toUpperCase(c) : Character.toLowerCase(c));
            }
        }
        return sb.toString();
    }

    public static enum Condition {

        UPPER_CASE_LETTER_AFTER_A_NOT_UPPER_CASE_LETTER {
            @Override
            public boolean isMet(String toTest, int index) {
                if (index > 0) {
                    char c = toTest.charAt(index);
                    if (Character.isUpperCase(c)) {
                        if (Character.isUpperCase(toTest.charAt(index - 1)) == false) {
                            return true;
                        }
                    }
                }
                return false;
            }
        },
        UPPER_CASE_LETTER_BEFORE_A_LOWER_CASE_LETTER {
            @Override
            public boolean isMet(String toTest, int index) {
                if (index < toTest.length() - 2) {
                    char c = toTest.charAt(index);
                    if (Character.isUpperCase(c)) {
                        if (Character.isLowerCase(toTest.charAt(index + 1))) {
                            return true;
                        }
                    }
                }
                return false;
            }
        },
        LETTER_AFTER_NON_LETTER_CHAR {
            @Override
            public boolean isMet(String toTest, int index) {
                if (index > 0) {
                    char c = toTest.charAt(index);
                    if (Character.isLetter(c)) {
                        if (Character.isLetter(toTest.charAt(index - 1)) == false) {
                            return true;
                        }
                    }
                }
                return false;
            }
        },
        NUMBER_AFTER_NON_NUMBER_CHAR {
            @Override
            public boolean isMet(String toTest, int index) {
                if (index > 0) {
                    char c = toTest.charAt(index);
                    if (Character.isDigit(c)) {
                        if (Character.isDigit(toTest.charAt(index - 1)) == false) {
                            return true;
                        }
                    }
                }
                return false;
            }
        },
        INVALID_CHARACTER {
            @Override
            public boolean isMet(String toTest, int index) {
                char c = toTest.charAt(index);
                return Character.isLetterOrDigit(c) ? false : true;

            }
        },;

        public abstract boolean isMet(String toTest, int index);
    }
}
