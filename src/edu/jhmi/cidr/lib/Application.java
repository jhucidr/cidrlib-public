/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import edu.jhmi.cidr.lib.gui.GuiUtils;
import edu.jhmi.cidr.lib.gui.GuiUtils.ErrorMessage;
import edu.jhmi.cidr.lib.mail.EwsUtils;
import edu.jhmi.cidr.lib.message.MessageType;
import java.awt.Component;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author David Newcomer Jul 11, 2013
 */
public class Application {

    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger("cidrlib");
    private static ExceptionHandler handler = new HandlerWithEmail();
    private static Component component = null;

    public static void message(MessageType type, String text) {
        switch (type) {
            case INFO:
                Application.getLogger().info(text);
                break;
            case WARNING:
                Application.getLogger().warn(text);
                break;
            case ERROR:
                Application.getLogger().error(text);
                break;
            default:
                IllegalArgumentException ex = new IllegalArgumentException("Unhandled MessageType '" + type + "'");
                Application.getLogger().error("MessageHandler.submit('" + text + "')", ex);
                throw ex;
        }
    }

    public static void reportSuccess(String message) {
        reportSuccess(null, message);
    }

    public static void reportSuccess(Component c, String message) {
        if (message == null) {
            throw new NullPointerException("message must not be null");
        }
        HtmlUtils.HtmlBuilder html = new HtmlUtils.HtmlBuilder().append(message);
        message(MessageType.INFO, html.toPlainText());
        if (c == null) {
            c = getComponentOrNull();
        }
        if (c != null) {
            JOptionPane.showMessageDialog(c,
                    html.toString(), "Success", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public static void reportWarning(String message) {
        reportWarning(null, message);
    }

    public static void reportWarning(Component c, String message) {
        if (message == null) {
            throw new NullPointerException("message must not be null");
        }
        HtmlUtils.HtmlBuilder html = new HtmlUtils.HtmlBuilder().append(message);
        Application.message(MessageType.WARNING, html.toPlainText());
        if (c == null) {
            c = getComponentOrNull();
        }
        if (c != null) {
            JOptionPane.showMessageDialog(c,
                    html.toString(), "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }

    public static void reportError(String descriptionOfFailure, String dialogTitle, Exception ex) {
        reportError(null, descriptionOfFailure, dialogTitle, ex);
    }

    public static void reportError(Component c, String descriptionOfFailure, String dialogTitle, Exception ex) {
        reportError(c, descriptionOfFailure, dialogTitle, ex, GuiUtils.ContactPwogwammaws.YES);
    }

    public static void reportError(GuiUtils.ErrorMessage errorMessage) {
        reportError(null, errorMessage);
    }

    public static void reportError(Component c, GuiUtils.ErrorMessage errorMessage) {
        if (errorMessage == null) {
            throw new NullPointerException("errorMessage must not be null");
        }
        Application.handleException(c, errorMessage);
    }

    public static void reportError(String descriptionOfFailure, String title, Throwable t, GuiUtils.ContactPwogwammaws contactProgrammers) {
        reportError(null, descriptionOfFailure, title, t, contactProgrammers);
    }

    public static void reportError(Component c, String descriptionOfFailure, String title, Throwable t, GuiUtils.ContactPwogwammaws contactProgrammers) {
        reportError(c, new GuiUtils.ErrorMessage(descriptionOfFailure, title, t, contactProgrammers));
    }

    private static boolean emailProgrammers(String summary, String descriptionOfFailure, Throwable t) {
        if (Constants.DEVELOPMENT_MODE) {
            return false;
        }
        EwsUtils.MailConfiguration mailConfig = EwsUtils.getCidrSoftwareMailConfiguration();
        mailConfig.addRecipient(Constants.DEFAULT_EMAIL_ADDRESS);
        Object contextComponent = Application.getComponentOrNull();
        String contextComponentClassName = (contextComponent != null) ? contextComponent.getClass().getSimpleName() : "Unknown Context";
        StringBuilder subject = new StringBuilder();
        subject.append(Constants.PHOENIX_EMAIL_SUBJECT_LINE_PREFIX);
        subject.append(" Error Encountered in ");
        subject.append(contextComponentClassName);
        mailConfig.setSubject(subject.toString());
        HtmlUtils.HtmlBuilder body = new HtmlUtils.HtmlBuilder();
        body.append("<pre>");
        body.append("Automated E-Mail:").newLine().newLine();
        body.append("Summary: ");
        body.append(summary).newLine().newLine();
        body.append("Error description: ");
        body.append(descriptionOfFailure);
        if (t != null) {
            body.newLine().newLine().append("Stack Trace:").newLine();
            body.append(Utils.getStackTraceAsHtml(t));
        }
        body.append("</pre>");
        mailConfig.setMessageBody(body.toString());
        try {
            EwsUtils.sendEmail(mailConfig);
            return true;
        } catch (Exception ex) {
            Application.handleException(ex);
            return false;
        }
    }

    public static void setComponent(Component component) {
        Application.component = component;
    }

    public static Component getComponentOrNull() {
        return component;
    }

    public static void setLogger(org.slf4j.Logger LOGGER) {
        Application.LOGGER = LOGGER;
    }

    public static Logger getLogger() {
        return LOGGER;
    }

    public static boolean hasGui() {
        return component != null;
    }

    public static void handleException(Throwable t) {
        handleException(null, t);
    }

    public static void handleException(Component c, Throwable t) {
        handler.handle(c, t);
    }

    public static interface ExceptionHandler {

        public void handle(Component c, Throwable ex);
    }

    private static class HandlerWithEmail implements ExceptionHandler {

        @Override
        public void handle(Component c, Throwable ex) {
            if (false == Application.hasGui()) {
                ex.printStackTrace();
            }
            if (false == (ex instanceof ErrorMessage)) {
                ex = new ErrorMessage(ex.getMessage(), "An Exception has occurred", ex, GuiUtils.ContactPwogwammaws.YES);
            }
            ErrorMessage errorMessage = (ErrorMessage) ex;
            String descriptionOfFailure = errorMessage.getMessage();
            String title = errorMessage.getTitle();
            Throwable t = errorMessage.getCause();
            GuiUtils.ContactPwogwammaws contactProgrammers = errorMessage.reportErrorToProgrammers();
            // NOTE: t can be null
            if (descriptionOfFailure == null) {
                throw new NullPointerException("descriptionOfFailure must not be null");
            }
            if (title == null) {
                throw new NullPointerException("title must not be null");
            }
            HtmlUtils.HtmlBuilder html = new HtmlUtils.HtmlBuilder();
            html.append(descriptionOfFailure);
            html.newLine();
            html.appendBold(Utils.buildUserFriendlyErrorMessage(t));
            if (contactProgrammers == GuiUtils.ContactPwogwammaws.YES) {
                html.newLine().newLine();
                html.append("An error has occurred.");
                boolean success = emailProgrammers(title, descriptionOfFailure, t);
                if (success) {
                    html.newLine();
                    html.append("The programmers have been notified of your problem.");
                } else {
                    ex.printStackTrace();
                    html.newLine();
                    html.append("The application was unable to email the programmers automatically.");
                    html.newLine();
                    html.append(" Please report this error to a programmer (");
                    html.append(Constants.PROGRAMMERS_EMAIL);
                    html.append(").");
                }
            }
            System.out.println("HTML for JOptionPane:\n" + html.toString());
            //Defaults to the component passed in; otherwise uses the Application's.
            if (c == null) {
                c = Application.getComponentOrNull();
            }
            Application.message(MessageType.ERROR, "[" + title + "] " + html.toPlainText());
            System.out.println("GUI_UTILS: clientContext.hasGui() = " + Application.hasGui());
            if (c != null) {
                JOptionPane optionPane = new JOptionPane(html.toString(), JOptionPane.WARNING_MESSAGE);
                JDialog errorDialog = optionPane.createDialog(c, title);

//                errorDialog.setAlwaysOnTop(true);
                errorDialog.setVisible(true);
                errorDialog.requestFocus();

                //Bug: Scared error message dialogs were hiding behind other windows
//                JOptionPane.showMessageDialog(c,
//                        html.toString(), title, JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
