/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import edu.jhmi.cidr.lib.HtmlUtils.HtmlBuilder;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;
import javax.swing.SwingUtilities;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author dleary
 */
public class Utils {

    private static final ReentrantLock OUT_LOCK = new ReentrantLock();
    /**
     * This describes any one of the following characters invalid for Windows
     * file names: \ / : * ? " < > |
     *
     */
    private static String INVALID_FILENAME_CHARS_REGEX = "[\\\\/:\\*\\?\"<>\\|]";

//    /**
//     * Caveat Programmor! This will only work on Windows!!!!!!ONE
//     *
//     * @param fullyQualifiedPath
//     * @throws IOException
//     */
//    public static void displayOutput(String fullyQualifiedPath) throws IOException {
//        if (fullyQualifiedPath == null) {
//            throw new NullPointerException("The path to the file to display must not be null");
//        }
//        Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL "
//                + new File(fullyQualifiedPath).getAbsolutePath());
//    }
    public static boolean null_or_empty(String s) {
        if (s == null) {
            return true;
        } else {
            return s.trim().isEmpty();
        }
    }

    public static int count_nonempty_values_in_string_array(String[] strRa) {
        int count = 0;
        for (int i = 0; i < strRa.length; i++) {
            String string = strRa[i];
            if (false == (null_or_empty(string))) {
                count++;
            }
        }
        return count;
    }

    public static String make_valid_file_name(String s) {
        String retVal = s.replaceAll(INVALID_FILENAME_CHARS_REGEX, "_");
        return retVal;
    }

    public static String[] remove_trailing_empty_values_from_string_array(String[] strRa) {
        int i;
        for (i = strRa.length - 1; i >= 0; i--) {
            String string = strRa[i];
            if (false == (null_or_empty(string))) {
                break;
            }
        }
        String[] retVal = new String[i + 1];
        System.arraycopy(strRa, 0, retVal, 0, i + 1);
        return retVal;
    }

    @Deprecated
    public static boolean not_null(Object o) {
        return o != null;
    }

    /**
     * returns an array containing the number of values split including empty
     * strings.
     *
     * @param regex
     * @param string
     * @return
     */
    public static String[] split(String regex, String string) {
        return string.split(regex, -1);
    }

    public static StringBuilder concatenateDisplayableItems(
            String separator,
            Collection<?> displyables,
            boolean encloseInQuotes,
            boolean includeAnd) {
        StringBuilder result = new StringBuilder();
        StringBuilder item = null;
        for (Object o : displyables) {
            if (item != null) {
                result.append(item);
                if (displyables.size() > 2) {
                    result.append(separator);
                }
                result.append(' ');
            }
            item = new StringBuilder();
            if (encloseInQuotes) {
                item.append('\'');
            }
            item.append(Utils.display(o));
            if (encloseInQuotes) {
                item.append('\'');
            }
        }
        if (includeAnd) {
            if (result.length() > 0) {
                result.append("and ");
            }
        }
        result.append(item);
        return result;
    }

    public static String join(String delimiter, Collection<?> values) {
        return join(delimiter, values.toArray(new Object[]{}));
    }

    public static String join(String delimiter, Object... values) {
        StringBuilder result = new StringBuilder();
        boolean isFirst = true;
        for (Object val : values) {
            if (isFirst) {
                isFirst = false;
            } else {
                result.append(delimiter);
            }
            result.append(display(val));
        }
        return result.toString();
    }

    public static String display(Object... objs) {
        return join("", objs);
    }

    public static String display(Map<?, ?> displayables) {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (Map.Entry<?, ?> e : displayables.entrySet()) {
            sb.append(sb.length() > 1 ? ", " : "");
            sb.append(display(e.getKey()));
            sb.append("=");
            sb.append(display(e.getValue()));
        }
        sb.append("}");
        return sb.toString();
    }

    public static String display(Collection<?> os) {
        return join(",", os);
    }

    public static String display(Object toDisplay) {
        if (toDisplay instanceof Displayable) {
            return ((Displayable) toDisplay).getDisplayString();
        } else if (toDisplay instanceof Collection) {
            return display((Collection) (toDisplay));
        } else if (toDisplay instanceof Map) {
            return display((Map) (toDisplay));
        } else {
            if (toDisplay instanceof Object[]) {
                return display(Arrays.asList((Object[]) (toDisplay)));
            } else {
                return String.valueOf(toDisplay);
            }
        }
    }

    public static String transformToValidFileName(String attemptedFileName) {
        return attemptedFileName.replaceAll(INVALID_FILENAME_CHARS_REGEX, "_");
    }

    public static String removeTrailingCharacters(String line, String charactersToRemove) {
        return line.replaceAll("([" + Pattern.quote(charactersToRemove) + " ]*)$", "");
    }

    public static StringBuilder padWithDelimiters(String line, int numDelimitersRequired, String delimiter) {
        if (delimiter.length() > 1) {
            throw new IllegalStateException("Unable to pad for delimiter with size > 1: " + delimiter);
        }
        StringBuilder result = new StringBuilder();
        int numRemainingDelimitersNeeded = numDelimitersRequired;
        for (char c : line.toCharArray()) {
            if (c == delimiter.charAt(0)) {
                --numRemainingDelimitersNeeded;
            }
            result.append(c);
        }
        while (numRemainingDelimitersNeeded > 0) {
            result.append(delimiter);
            --numRemainingDelimitersNeeded;
        }
        return result;
    }

    public static boolean stringIsEmptyOrContainsOnlyDelimiters(String line, String delimiter) {
        if (delimiter == null) {
            throw new NullPointerException("delimiter must not be null");
        }
        if (line == null) {
            throw new NullPointerException("line must not be null");
        }
        line = line.trim();
        return line.isEmpty() || line.matches("^" + Pattern.quote(delimiter) + "*$");
    }

    public static void println(Object o) {
        OUT_LOCK.lock();
        try {
            Application.getLogger().info(Utils.display(o));
        } finally {
            OUT_LOCK.unlock();
        }
    }

    public static void println(Object... objs) {
        println(join(";", objs));
    }

    @SafeVarargs
    public static <T> Set<T> getAsSet(T first, T... others) {
        if (first == null) {
            throw new NullPointerException("first must not be null");
        }
        Set<T> result = new HashSet<T>();
        result.add(first);
        if (others != null) {
            List<T> toAdd = Arrays.asList(others);
            result.addAll(toAdd);
        }
        return result;
    }

    public static String getStackTrace(Throwable t) {
        return new HtmlBuilder().append(getStackTraceAsHtml(t)).toPlainText();
    }

    public static String getStackTraceAsHtml(Throwable t) {
        HtmlBuilder html = new HtmlBuilder();
        html.append("<pre>");
        Throwable current = t;
        boolean isChained = false;
        while (current != null) {
            if (isChained) {
                html.append("Caused by: ");
            }
            html.append(current.getClass().getName());
            html.append(": ");
            if (current.getMessage() != null) {
                html.append(current.getMessage());
            }
            html.newLine();
            for (StackTraceElement elem : current.getStackTrace()) {
                html.append("     at ");
                html.append(elem.toString()).newLine();
            }
            current = current.getCause();
            isChained = true;
        }
        html.append("</pre>");
        return html.toString();
    }

    public static String buildUserFriendlyHtmlMessage(Throwable t) {
        HtmlBuilder html = new HtmlBuilder();
        html.append("<pre>");
        Throwable current = t;
        while (current != null) {
            if (current.getMessage() != null) {
                if (current.getCause() == null) {
                    html.append(current.getMessage());
                } else {
                    Throwable next = current.getCause();
                    if (false == current.getMessage().contains(next.getClass().getSimpleName())) {
                        html.append(current.getMessage());
                        html.newLine();
                    }
                }
            }
            current = current.getCause();
        }
        html.append("</pre>");
        return html.toString();
    }

    public static String buildUserFriendlyErrorMessage(Throwable t) {
        return new HtmlBuilder(buildUserFriendlyHtmlMessage(t)).toPlainText();
    }

    public static String formatDoubleAsPercentage(double d, int i) {
        if (i < 0) {
            throw new IllegalArgumentException("number of digits to the right of decimal point must be non-negative");
        }
        return String.format("%1$." + i + "f%%", d);
    }

    public static <T> List<T> asList(T[] tRa) {
        return Arrays.asList(tRa);
    }

    public static <T> List<T> asList(Collection<T> t) {
        return new ArrayList<T>(t);
    }

    @SafeVarargs
    public static <T> List<T> asList(T t, T... tRa) {
        List<T> myTs = new ArrayList<T>();
        myTs.add(t);
        myTs.addAll(asList(tRa));
        return myTs;
    }

    @SafeVarargs
    public static <T> Set<T> asSet(T... tRa) {
        return new HashSet<T>(asList(tRa));
    }

    @SafeVarargs
    public static <T> Set<T> asSet(Collection<T>... tRa) {
        Set<T> result = new HashSet<>();
        for (Collection<T> collection : tRa) {
            result.addAll(asSet(collection));
        }
        return result;
    }

    public static <T> Set<T> asSet(Collection<T> t) {
        return new HashSet<>(t);
    }

    public static <T> List<T> flatMap(Collection<? extends Collection<T>> c) {
        List<T> result = new ArrayList<>();
        for (Collection<T> subC : c) {
            result.addAll(subC);
        }
        return result;
    }

    public static <T extends Comparable<? super T>> List<T> sort(Collection<T> t) {
        List<T> toSort = new ArrayList<>(t);
        Collections.sort(toSort);
        return toSort;
    }

    public static <T> List<T> sort(Collection<T> t, Comparator<? super T> c) {
        List<T> toSort = new ArrayList<>(t);
        Collections.sort(toSort, c);
        return toSort;
    }

    public static <T> List<T> distinct(Collection<T> t) {
        return asList(asSet(t));
    }

    private static <T> List<T> _findAll(List<T> col, T t) {
        int i = col.indexOf(t);
        if (i != -1 && i < col.size()) {
            T item = col.get(i);
            List<T> subList = col.subList(i + 1, col.size());
            List<T> found = _findAll(subList, t);
            found.add(item);
            return found;
        } else {
            return new ArrayList<>();
        }
    }

    public static <T> List<T> findAll(List<T> col, T t) {
        List<T> found = _findAll(col, t);
        Collections.reverse(found);
        return found;
    }

    /**
     * Ensures that an object reference passed as a parameter to the calling
     * method is not null.
     *
     * @param reference an object reference
     * @return the non-null reference that was validated
     * @throws NullPointerException if {@code reference} is null
     */
    public static <T> T checkNotNull(T reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
        return reference;
    }

    /**
     * Ensures that an object reference passed as a parameter to the calling
     * method is not null.
     *
     * @param reference an object reference
     * @param errorMessage the exception message to use if the check fails; will
     * be converted to a string using {@link String#valueOf(Object)}
     * @return the non-null reference that was validated
     * @throws NullPointerException if {@code reference} is null
     */
    public static <T> T checkNotNull(T toCheck, Object errorMessage) {
        if (toCheck == null) {
            throw new NullPointerException(String.valueOf(errorMessage));
        }
        return toCheck;
    }
    private static AtomicLong THREAD_ID = new AtomicLong(1L);

    public static long getNextThreadId() {
        return THREAD_ID.getAndIncrement();
    }

    public static String translateClassName(Class clazz) {
        if (clazz.isAnonymousClass()) {
            //The simple name of anonymous classes is empty.
            if (clazz.getSuperclass() == Object.class && clazz.getInterfaces().length > 0) {
                List<String> classNames = new ArrayList<String>();
                for (Class clazz2 : clazz.getInterfaces()) {
                    classNames.add(translateClassName(clazz2));
                }
                return classNames.toString();
            } else {
                clazz = clazz.getSuperclass();
            }
        }
        String cName = clazz.getSimpleName();
        String panel = "panel";
        if (cName.toLowerCase().endsWith(panel)) {
            cName = cName.substring(0, cName.length() - panel.length());
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < cName.length(); i++) {
            char c = cName.charAt(i);
            if (i != 0) {
                if (Character.isUpperCase(c)) {
                    sb.append(" ");
                }
            }
            sb.append(c);
        }
        return sb.toString();
    }

    public static String ucfirst(String s) {
        StringBuilder sb = new StringBuilder();
        sb.append(Character.valueOf(s.charAt(0)).toString().toUpperCase());
        sb.append(s.subSequence(1, s.length()));
        return sb.toString();
    }

    public static Thread getEdt() throws Exception {
        return EdtRetriever.getEdt();
    }

    public static void waitForEdtTaskCompletion() {
        if (false == SwingUtilities.isEventDispatchThread()) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
                    public void run() {
                        //Does nothing.
                    }
                });
            } catch (InterruptedException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean startsWithIgnoreCase(String string, String prefix) {
        if (string == null) {
            throw new NullPointerException("string must not be null");
        }
        if (prefix == null) {
            throw new NullPointerException("prefix must not be null");
        }
        if (prefix.isEmpty()) {
            throw new IllegalArgumentException("prefix must not be empty");
        }
        if (prefix.length() > string.length()) {
            return false;
        }
        return string.substring(0, prefix.length()).equalsIgnoreCase(prefix);
    }

    public static boolean endsWithIgnoreCase(String string, String suffix) {
        if (string == null) {
            throw new NullPointerException("string must not be null");
        }
        if (suffix == null) {
            throw new NullPointerException("suffix must not be null");
        }
        if (suffix.isEmpty()) {
            throw new IllegalArgumentException("suffix must not be empty");
        }
        if (suffix.length() > string.length()) {
            return false;
        }
        return string.substring(string.length() - suffix.length(), string.length()).equalsIgnoreCase(suffix);
    }

    public static List<String> splitIgnoreCase(String string, String toSplitUpon) {
        List<String> result = new ArrayList<>();
        int counter = 0;
        for (String s : string.toLowerCase().split(toSplitUpon.toLowerCase(), -1)) {
            result.add(string.substring(counter, counter + s.length()));
            counter += (s.length() + toSplitUpon.length());
        }
        return result;
    }

    public static String replaceFirstIgnoreCase(String string, String toFind, String toReplace) {
        if (string == null) {
            throw new NullPointerException("string must not be null");
        }
        if (toFind == null) {
            throw new NullPointerException("toFind must not be null");
        }
        if (toReplace == null) {
            throw new NullPointerException("toReplace must not be null");
        }
        if (toFind.isEmpty()) {
            throw new IllegalArgumentException("toFind must not be empty.");
        }
        int index = string.toLowerCase().indexOf(toFind.toLowerCase());
        if (index >= 0) {
            return string.substring(0, index) + toReplace + string.substring(index + toFind.length(), string.length());
        } else {
            return string;
        }
    }

    public static String replaceIgnoreCase(String string, String toFind, String toReplace) {
        if (string == null) {
            throw new NullPointerException("string must not be null");
        }
        if (toFind == null) {
            throw new NullPointerException("toFind must not be null");
        }
        if (toReplace == null) {
            throw new NullPointerException("toReplace must not be null");
        }
        if (toFind.isEmpty()) {
            throw new IllegalArgumentException("toFind must not be empty.");
        }
        StringBuilder sb = new StringBuilder();
        List<String> splitsville = splitIgnoreCase(string, toFind);
        for (int i = 0; i < splitsville.size(); i++) {
            String piece = splitsville.get(i);
            if (i > 0) {
                sb.append(toReplace);
            }
            sb.append(piece);
        }
        return sb.toString();
    }

    /**
     * Returns a substring of the provided String that starts with the specified
     * String. Includes the provided String.
     */
    public static String substring(String string, String toStart) {
        return string.substring(string.indexOf(toStart));
    }

    public static String substringIgnoreCase(String string, String toStart) {
        return string.substring(string.toLowerCase().indexOf(toStart.toLowerCase()));
    }

    public static String before(String string, String endingChars) {
        return string.substring(0, string.indexOf(endingChars));
    }

    public static String beforeIgnoreCase(String string, String endingChars) {
        return string.substring(0, string.toLowerCase().indexOf(endingChars.toLowerCase()));
    }

    public static String beforeLast(String string, String endingChars) {
        return string.substring(0, string.lastIndexOf(endingChars));
    }

    public static String beforeLastIgnoreCase(String string, String endingChars) {
        return string.substring(0, string.toLowerCase().lastIndexOf(endingChars.toLowerCase()));
    }

    public static String after(String string, String everythingAfter) {
        return string.substring(string.indexOf(everythingAfter) + everythingAfter.length());
    }

    public static String afterIgnoreCase(String string, String everythingAfter) {
        return string.substring(string.toLowerCase().indexOf(everythingAfter.toLowerCase()) + everythingAfter.length());
    }

    public static String afterLast(String string, String everythingAfter) {
        return string.substring(string.lastIndexOf(everythingAfter) + everythingAfter.length());
    }

    public static String afterLastIgnoreCase(String string, String everythingAfter) {
        return string.substring(string.toLowerCase().lastIndexOf(everythingAfter.toLowerCase()) + everythingAfter.length());
    }

    //TODO: Consider throwing an exception rather than returning the empty string.
    public static String between(String string, String start, String stop) {
        int startIndex = string.indexOf(start);
        if (startIndex < 0) {
            return "";
        }
        int stopIndex = string.substring(startIndex).indexOf(stop);
        if (stopIndex < 0) {
            return "";
        }
        return string.substring(startIndex + start.length(), stopIndex + startIndex);
    }

    //TODO: Consider throwing an exception rather than returning the empty string.
    public static String betweenIgnoreCase(String string, String start, String stop) {
        String upper = string.toUpperCase();
        start = start.toUpperCase();
        stop = stop.toUpperCase();
        int startIndex = upper.indexOf(start);
        if (startIndex < 0) {
            return "";
        }
        int stopIndex = upper.substring(startIndex).indexOf(stop);
        if (stopIndex < 0) {
            return "";
        }
        return string.substring(startIndex + start.length(), stopIndex + startIndex);
    }

    public static <T> boolean intersect(Set<T> l, Set<T> r) {
        // Alternative implementation would be Collections.disjoint()
        return false == intersection(l, r).isEmpty();
    }

    public static <T> Set<T> intersection(Set<T> l, Set<T> r) {
        if (l.isEmpty() || r.isEmpty()) {
            return new HashSet<T>();
        }
        Set<T> result = asSet(l); // defensive copy
        result.retainAll(r);
        return result;
    }

    public static <T> Set<T> complement(Set<T> superset, Set<T> subset) {
        Set<T> result = new HashSet<T>(superset);
        result.removeAll(subset);
        return result;
    }

    /**
     * inclusive range
     */
    public static int[] range(int start, int finish) {
        int[] result = new int[finish - start + 1];
        for (int i = 0; i < result.length; i++) {
            result[i] = start + i;
        }
        return result;
    }

    /**
     * preserves case of the first inserted value (of pair of duplicates), but
     * ignores case when determining duplication
     */
    public static Set<String> getCasesInsensitiveSet(Collection<String> source) {
        Set<String> result = new HashSet<>();
        Set<String> ucSet = new HashSet<>();
        for (String s : source) {
            String ucS = s.toUpperCase();
            if (false == ucSet.contains(ucS)) {
                result.add(s);
                ucSet.add(ucS);
            }
        }
        return result;
    }

    public static List<Integer> rangeList(int start, int finish) {
        return asList(ArrayUtils.toObject(range(start, finish)));
    }

    public static <K, V> HashMap<V, Set<K>> invertMultiMap(Map<K, Set<V>> multiMap) {
        HashMap<V, Set<K>> result = new HashMap<>();
        for (Map.Entry<K, Set<V>> entry : multiMap.entrySet()) {
            K k = entry.getKey();
            Set<V> vs = entry.getValue();
            for (V v : vs) {
                if (false == result.containsKey(v)) {
                    result.put(v, new HashSet<K>());
                }
                result.get(v).add(k);
            }
        }
        return result;
    }

    private static class EdtRetriever implements Runnable {

        private EdtRetriever() {
        }
        private static EdtRetriever INSTANCE;
        Thread edt = null;

        @Override
        public void run() {
            edt = Thread.currentThread();
        }

        public static Thread getEdt() throws Exception {
            if (INSTANCE == null) {
                INSTANCE = new EdtRetriever();
                if (SwingUtilities.isEventDispatchThread()) {
                    INSTANCE.edt = Thread.currentThread();
                } else {
                    SwingUtilities.invokeAndWait(INSTANCE);
                }
            }
            return INSTANCE.edt;
        }
    }
}
