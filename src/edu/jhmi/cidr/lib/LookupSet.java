/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import edu.jhmi.cidr.lib.Utils;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author cidr
 */
public class LookupSet<T> implements Iterable<T> {
    private final Map<T, T> map = new HashMap<>();

    public int size() {
        return map.size();
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

    public boolean contains(T t) {
        return map.containsKey(t);
    }

    @Override
    public Iterator<T> iterator() {
        return map.keySet().iterator();
    }

    public T add(T t) {
        return map.put(t, t);
    }

    public T remove(T t) {
        return map.remove(t);
    }

    public boolean containsAll(Collection<?> c) {
        return map.keySet().containsAll(c);
    }

    public void addAll(Collection<? extends T> c) {
        for (T t : c) {
            add(t);
        }
    }

    public void removeAll(Collection<? extends T> c) {
        for (T t : c) {
            remove(t);
        }
    }

    public void clear() {
        map.clear();
    }

    public Set<T> asSet() {
        return Utils.asSet(map.keySet());
    }

    public T get(T unit) {
        return map.get(unit);
    }
    
}
