/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.message;

import edu.jhmi.cidr.lib.Application;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author cidr
 */
class MessageHandler {

    private ExecutorService messageService = Executors.newSingleThreadExecutor();
    private static final MessageHandler INSTANCE = new MessageHandler();
    

    private MessageHandler() {
    }

    public static MessageHandler getInstance() {
        return INSTANCE;
    }

    public void shutdown() {
        messageService.shutdown();
    }

    public void submit(final MessageType type, final String text) {
        messageService.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println(type.toString() + "> " + text);
                } catch (Exception ex) {
                    Application.getLogger().error("MessageHandler.submit()", ex);
                    // no sense calling rootclientcontext.logThrowable() if this is called from there
                    ex.printStackTrace();
                }
            }
        });
        switch (type) {
            case INFO:
                Application.getLogger().info(text);
                break;
            case WARNING:
                Application.getLogger().warn(text);
                break;
            case ERROR:
                Application.getLogger().error(text);
                break;
            default:
                IllegalArgumentException ex = new IllegalArgumentException("Unhandled MessageType '" + type + "'");
                Application.getLogger().error("MessageHandler.submit('" + text + "')", ex);
                throw ex;
        }
    }
}
