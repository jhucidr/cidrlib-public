/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.message;

import java.awt.Color;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 *
 * @author cidr
 */
public enum MessageType {
    INFO {
        @Override
        public SimpleAttributeSet getAttributes() {
            SimpleAttributeSet result = new SimpleAttributeSet();
            StyleConstants.setFontFamily(result, "SansSerif");
            StyleConstants.setForeground(result, new Color(0x55BB55));
            StyleConstants.setFontSize(result, 12);
            return result;
        }
    }, WARNING {
        @Override
        public SimpleAttributeSet getAttributes() {
            SimpleAttributeSet result = new SimpleAttributeSet();
            StyleConstants.setFontFamily(result, "SansSerif");
            StyleConstants.setForeground(result, new Color(0xBB9900));
            StyleConstants.setFontSize(result, 12);
            return result;
        }
    }, ERROR {
        @Override
        public SimpleAttributeSet getAttributes() {
            SimpleAttributeSet result = new SimpleAttributeSet();
            StyleConstants.setFontFamily(result, "SansSerif");
            StyleConstants.setForeground(result, Color.red);
            StyleConstants.setFontSize(result, 12);
            return result;
        }
    };

    public abstract SimpleAttributeSet getAttributes();
    
}
