/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import java.util.Comparator;

/**
 *
 * @author dleary
 */
public interface Displayable {

    public String getDisplayString();

    public static enum Case {

        SENSITIVE,
        INSENSITIVE;
    }

    /**
     * CAVEAT PROGRAMMOR: When using this class on a set (heterogeneous or not)
     * of Displayables, each object must be uniquely identifiable by its display
     * string in order to adhere to the TreeSet/TreeMap contract.
     *
     * NOTE: If the only purpose of using TreeSet or TreeMap is sorting, use
     * Collections.sort instead. Otherwise, there is a good probably data will
     * be lost.
     *
     * For example, inserting 2 Displayables into a TreeMap with the same
     * display string will result in a TreeMap of size 1 containing only the
     * first inserted Displayable.
     */
    public static class DisplayableComparator implements Comparator<Displayable> {

        private final boolean ignoreCase;
        //
        public static final DisplayableComparator CASE_SENSITIVE = new DisplayableComparator(Case.SENSITIVE);
        public static final DisplayableComparator CASE_INSENSITIVE = new DisplayableComparator(Case.INSENSITIVE);

        public DisplayableComparator() {
            ignoreCase = false;
        }

        public DisplayableComparator(Case caze) {
            this.ignoreCase = caze == Case.SENSITIVE ? false : true;
        }

        @Override
        public int compare(Displayable o1, Displayable o2) {
            if (ignoreCase) {
                return o1.getDisplayString().compareToIgnoreCase(o2.getDisplayString());
            } else {
                return o1.getDisplayString().compareTo(o2.getDisplayString());
            }
        }
    }
}
