/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author David Newcomer Apr 22, 2011 Updated May 31, 2011 to include
 * lineNumber, and access to data by lineNumber. Updated June 28, 2011 to
 * include a boolean method to filter out empty lines (and thus only affect
 * lines with data). Updated August 12, 2011 minor refactor of constructors.
 * Changed String member inputFilePath to File member inputFile. Allowed
 * affectFileLine to throw Exceptions. Updated November 22, 2011 added a
 * post-parse method to the strategy.
 */
public class Parser {

    private final File inputFile;
    private final FilteringStrategy strategy;
    private long lineCount;
    private boolean isParsed;

    public Parser(String fullyQualifiedPathToInputFile, FilteringStrategy strategy) throws Exception {
        this(getFile(fullyQualifiedPathToInputFile), strategy);
    }

    private static File getFile(String fullyQualifiedFilePath) {
        if (fullyQualifiedFilePath == null) {
            throw new NullPointerException("fullyQualifiedFilePath must not be null");
        }
        return new File(fullyQualifiedFilePath);
    }

    /**
     * Takes an input .txt file, an output path that does not already exist, and
     * a strategy. Copies lines to new filepath according to the strategy.
     *
     * @param inputFile
     * @param strategy
     * @throws Exception
     */
    public Parser(File inputFile, FilteringStrategy strategy) throws Exception {
        if (inputFile == null) {
            throw new NullPointerException("Filepath must not be null.");
        }
        if (strategy == null) {
            throw new NullPointerException("FilteringStrategy must not be null.");
        }
        if (inputFile.exists() == false) {
            throw new IllegalArgumentException("File " + inputFile + " must exist");
        }
        if (inputFile.isFile() == false) {
            throw new IllegalArgumentException("File " + inputFile + "must be a text file.");
        }
        this.inputFile = inputFile;
        this.strategy = strategy;
        this.lineCount = 0;
        this.isParsed = false;
    }

    /**
     * Parses the .txt file, adding lines to an List<String> according to
     * strategy,
     *
     * @throws Exception
     */
    public void parse() throws Exception {
        if (isParsed == false) {
            try {
                long lineNumber = 0;
                String line = "";
                BufferedReader in = null;
                try {
                    in = new BufferedReader(new FileReader(inputFile));
                    while (strategy.keepParsing()) {
                        line = in.readLine();
                        if (line == null) {
                            break;
                        }
                        lineNumber++;
                        if (strategy.doNotAffectEmptyLines() && line.trim().isEmpty()) {
                            continue;
                        }
                        strategy.affectFileLine(line, lineNumber);
                    }
                } catch (Exception e) {//Catch exception if any
                    strategy.handleException(e);
                } finally {
                    if (in != null) {
                        try {
                            in.close();
                        } catch (Exception ignore) {
                        }
                    }
                }
                this.lineCount = lineNumber;
                this.isParsed = true;
            } finally {
                strategy.postParse();
            }
        } else {
            throw new IllegalArgumentException("ERROR: you are attempting to parse " + inputFile.getAbsolutePath() + " multiple times!");
        }
    }

    /**
     * Returns true if the parse() method has completed.
     *
     * @return
     */
    public boolean isParsed() {
        return isParsed;
    }

    public static String getLineFromFile(File file, long lineNumber) {
        if (file == null) {
            throw new NullPointerException("file must not be null");
        }
        if (lineNumber <= 0) {
            throw new IllegalArgumentException("lineNumber must be > zero.");
        }
        StringValueFromLineNumberFilter stringValueGetter = new StringValueFromLineNumberFilter(lineNumber);
        Parser parser = null;
        try {
            parser = new Parser(file, stringValueGetter);
            parser.parse();
        } catch (Exception ignore) {
        }
        if (stringValueGetter.getString() != null) {
            return stringValueGetter.getString();
        } else {
            StringBuilder errorMessage = new StringBuilder();
            errorMessage.append("ERROR: line ");
            errorMessage.append(lineNumber);
            errorMessage.append(" is out of bounds.  ");
            errorMessage.append(parser.inputFile.getAbsolutePath());
            errorMessage.append(" contains only ");
            errorMessage.append(parser.lineCount);
            errorMessage.append(" lines.");
            throw new IndexOutOfBoundsException(errorMessage.toString());
        }
    }

    /**
     * This method returns the String found on the given lineNumber. For better
     * performance, use getFileContents() to get the entire File as an
     * ArrayList<String> or create a custom FilteringStrategy that uses
     * affectFileLine to store the results in a list, and access lines from the
     * cached values.
     *
     * @param lineNumber
     * @return
     * @throws Exception if index is out of bounds, or if there's a problem
     * parsing the file.
     */
    public String getLine(long lineNumber) throws Exception {
        if (lineNumber <= 0) {
            throw new IllegalArgumentException("lineNumber must be > zero.");
        }
        if (lineNumber > getLineCount()) {
            StringBuilder sb = new StringBuilder();
            sb.append("ERROR: ");
            sb.append(lineNumber);
            sb.append(" is out of bounds.  ");
            sb.append(inputFile.getAbsolutePath());
            sb.append(" contains only ");
            sb.append(this.lineCount);
            sb.append(" lines.");
            throw new IndexOutOfBoundsException(sb.toString());
        }
        StringValueFromLineNumberFilter stringValueGetter = new StringValueFromLineNumberFilter(lineNumber);
        Parser parser = new Parser(inputFile, stringValueGetter);
        parser.parse();
        return stringValueGetter.getString();
    }

    /**
     * Returns the total number of lines in the file being parsed.
     *
     * @return
     * @throws Exception
     */
    public static long getLineCount(File file) throws Exception {
        Parser p = new Parser(file, new NoFilteringStrategy());
        return p.getLineCount();
    }

    /**
     * Returns the total number of lines in the file being parsed.
     *
     * @return
     * @throws Exception
     */
    public long getLineCount() throws Exception {
        if ((isParsed == false) && (this.lineCount == 0)) {
            Parser sizeGetter = new Parser(inputFile, new NoFilteringStrategy());
            sizeGetter.parse();
            this.lineCount = sizeGetter.lineCount;
        }
        return lineCount;
    }

    /**
     * Parses a file, and returns a List<String> with all of the line values
     * cached.
     *
     * @param fullyQualifiedPathToTextFile
     * @return
     * @throws Exception
     */
    public static List<String> getFileContents(String fullyQualifiedPathToTextFile) throws Exception {
        final List<String> valueList = new ArrayList<String>();
        Parser parser = new Parser(fullyQualifiedPathToTextFile, new FilteringStrategyAdaptor() {
            @Override
            public void affectFileLine(String line, long lineNumber) {
                valueList.add(line);
            }
        });
        parser.parse();
        return valueList;
    }

    public static interface FilteringStrategy {

        public void affectFileLine(String line, long lineNumber) throws Exception;

        public boolean keepParsing();

        public void handleException(Exception ex) throws Exception;

        public boolean doNotAffectEmptyLines();

        public void postParse();
    }

    /**
     * The FilteringStrategyAdaptor allows the user to only define only the
     * AffectFileLine method. It is a convenience class.
     */
    public static abstract class FilteringStrategyAdaptor implements FilteringStrategy {

        @Override
        public boolean keepParsing() {
            return true;
        }

        @Override
        public void handleException(Exception ex) throws Exception {
            throw ex;
        }

        @Override
        public boolean doNotAffectEmptyLines() {
            return false;
        }

        public void postParse() {
            //does nothing.
        }
    }

    private static class NoFilteringStrategy extends FilteringStrategyAdaptor {

        @Override
        public void affectFileLine(String line, long lineNumber) {
            //Does nothing.
        }
    }

    /**
     * This FilteringStrategy parses until it reaches the desired lineNumber,
     * saves the String found at that lineNumber, and stops parsing.
     */
    private static class StringValueFromLineNumberFilter extends FilteringStrategyAdaptor {

        private final long lineNumber;
        private boolean keepParsing = true;
        private String lineToGet;

        public StringValueFromLineNumberFilter(long lineNumber) {
            this.lineNumber = lineNumber;
        }

        @Override
        public void affectFileLine(String line, long lineNumber) {
            if (this.lineNumber == lineNumber) {
                keepParsing = false;
                lineToGet = line;
            }
        }

        @Override
        public boolean keepParsing() {
            return keepParsing;
        }

        public String getString() {
            return lineToGet;
        }
    }
}
