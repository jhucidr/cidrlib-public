/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file;

import edu.jhmi.cidr.lib.Application;
import edu.jhmi.cidr.lib.Constants;
import edu.jhmi.cidr.lib.DateFormatter;
import edu.jhmi.cidr.lib.HtmlUtils;
import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.gui.FileBrowseListener;
import edu.jhmi.cidr.lib.gui.GuiUtils;
import edu.jhmi.cidr.lib.gui.table.GuiTable;
import static edu.jhmi.cidr.lib.Utils.transformToValidFileName;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import javax.swing.JFileChooser;

public class FileUtils {

    public static final String CSV_DELIMITER = ",";
    public static final String TAB_DELIMITER = "\t";
    public static final String NEW_LINE = Constants.NEW_LINE;
    public static final Set<Character> FILE_PATH_ENTRY_CHARS = Collections.unmodifiableSet(Utils.asSet(':', '/', '\\', File.pathSeparatorChar, File.separatorChar));
    private static final File TEMP_OUTPUT_DIR = new File(System.getProperty("java.io.tmpdir") + "/Phoenix/", DateFormatter.DateFormatType.FILENAME.format());
    private static boolean DELETES_TEMP_FILES_ON_EXIT = false;

    static {
        try {
            Runtime.getRuntime().addShutdownHook(new Deleter());
        } catch (Exception e) {
            throw new IllegalStateException("An error occurred while attempting to delete files.", e);
        }
    }

    public static void setDeleteFilesOnExit(boolean b) {
        DELETES_TEMP_FILES_ON_EXIT = b;
    }

    private static class Deleter extends Thread {

        @Override
        public void run() {
            if (DELETES_TEMP_FILES_ON_EXIT) {
                //DELETE ALL THE FILES.
                List<File> filesToDel = getFiles(TEMP_OUTPUT_DIR);
                Collections.reverse(filesToDel);
                for (File file : filesToDel) {
                    System.out.println((file.delete() ? "Deleted " : "Could not delete ") + file.getAbsolutePath());
                }
            }
        }
    }

    public static long getLineCount(File file) throws Exception {
        return Parser.getLineCount(file);
    }

    public static String getLineFromFile(File file, long lineNumber) throws Exception {
        return Parser.getLineFromFile(file, lineNumber);
    }
    
    public static List<String> getFileContents(File file) throws Exception{
        return Parser.getFileContents(file.getAbsolutePath());
    }

    public static List<File> getFiles(File start) {
        return getFiles(start, null);
    }

    //breadth-first search for all sub-folders and sub-files
    public static List<File> getFiles(File start, FileFilter filter) {
        List<File> result = new ArrayList<>();
        // Use LinkedList (as opposed to ArrayList) b/c when using ArrayList.remove(0),
        // the array is copied. LinkedList.removeFirst() handles this better.
        LinkedList<File> toHandle = new LinkedList<>();
        toHandle.add(start);
        while (false == toHandle.isEmpty()) {
            File file = toHandle.removeFirst();
            if (filter == null || filter.accept(file)) {
                result.add(file);
            }
            if (file.isDirectory()) {
                toHandle.addAll(Utils.asList(file.listFiles()));
            }
        }
        return result;
    }

    public static String transformToValidFileName(String string) {
        return Utils.transformToValidFileName(string);
    }

    public static class FileValidationException extends Exception {

        public FileValidationException(String msg) {
            super(msg);
        }
    }

    public static boolean isFileValid(File file) {
        //Nullcheck is in validateFileExists() method.
        boolean result = true;
        try {
            validateFileExists(file);
        } catch (Exception ex) {
            result = false;
        }
        return result;
    }

    // JLG 7/09/2012
    public static boolean isExcelWorkbook(File file) {
        String path = file.getAbsolutePath().trim().toUpperCase();
        if (path.endsWith(".XLS")
                || path.endsWith(".XLSX")) {
            return true;
        } else {
            return false;
        }
    }

    private static boolean isNotFullyQualifiedFilePath(String filePath) {
        if (filePath.trim().length() < 2) {
            return true;
        }
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("windows")) {
            return !FILE_PATH_ENTRY_CHARS.contains(filePath.charAt(1));
        } else {
            return !FILE_PATH_ENTRY_CHARS.contains(filePath.charAt(0));
        }
    }

    public static File validateFileExists(String filePath) throws FileValidationException {
        if (filePath == null) {
            throw new NullPointerException("File path must not be null");
        }
        if (filePath.trim().isEmpty()) {
            throw new FileValidationException("File path must not be empty.");
        }
        if (isNotFullyQualifiedFilePath(filePath)) {
            throw new FileValidationException("File path '" + filePath + "' is not valid.");
        }
        return validateFileExists(new File(filePath));
    }

    public static <T extends File> T validateFileExists(T file) throws FileValidationException {
        if (file == null) {
            throw new NullPointerException("file must not be null");
        }
        if (file.exists() == false) {
            throw new FileValidationException("File: " + file.getAbsolutePath() + " does not exist.");
        }
        if (file.isDirectory()) {
            throw new FileValidationException("File: " + file.getAbsolutePath() + " must not be a directory.");
        }
        return file;
    }

    public static boolean isDirectoryValid(File file) {
        //Nullcheck is in validateDirectory() method.
        boolean result = true;
        try {
            validateDirectory(file);
        } catch (Exception ex) {
            result = false;
        }
        return result;
    }

    public static File validateDirectory(String directoryPath) throws FileValidationException {
        if (directoryPath == null) {
            throw new NullPointerException("Directory must not be null");
        }
        if (directoryPath.trim().isEmpty()) {
            throw new FileValidationException("Directory Path must not be empty.");
        }
        if (isNotFullyQualifiedFilePath(directoryPath)) {
            throw new IllegalArgumentException("Directory Path '" + directoryPath + "' is not valid.");
        }
        return validateDirectory(new File(directoryPath));
    }

    public static <T extends File> T validateDirectory(T directory) throws FileValidationException {
        if (directory == null) {
            throw new NullPointerException("Directory must not be null");
        }
        if (directory.exists() == false) {
            throw new FileValidationException("Directory: " + directory.getAbsolutePath() + " does not exist.");
        }
        if (directory.isDirectory() == false) {
            throw new FileValidationException("Directory: " + directory.getAbsolutePath() + " must be a directory.");
        }
        return directory;
    }

    public static <T extends File> T mkdirs(T file) {
        if (file == null) {
            throw new NullPointerException("file must not be null");
        }
        file.mkdirs();
        return file;
    }

    public static <T extends File> T mkParentDirs(T file) {
        if (file == null) {
            throw new NullPointerException("file must not be null");
        }
        file.getParentFile().mkdirs();
        return file;
    }

    public static String getPathToUserDesktop() {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("windows")) {
            return System.getenv("USERPROFILE") + File.separator + "Desktop";
        } else {
            return System.getProperty("user.home");

        }
    }

    public static boolean lineContainsOnlyCommas(String line) {
        return line.matches("^(,|\\s)+$");
    }

    /**
     * Provides a fully-qualified file path that is terminated by the provided
     * file name that is guaranteed to be unique and accessible to the calling
     * context for reading and writing.
     *
     * @param fileName the name terminating the fully-qualified path of the
     * returned fileName or either the empty string or null if the path,
     * including the terminating filename, can be auto-generated
     * @return a unique, fully-qualified path accessible to the client
     * @throws IOException if a path that provides read and write access to the
     * client is unavailable
     */
    public static File getNewTempFile(String fileName) throws IOException {
        if (fileName == null || fileName.isEmpty()) {
            fileName = UUID.randomUUID().toString();
        }
        File directory = new File(getTempDirPath(), UUID.randomUUID().toString());
        if (false == directory.mkdirs()) {
            throw new IllegalStateException("unable to create temp dir: " + directory);
        }
        return new File(directory, transformToValidFileName(fileName));
    }

    public static File getNewTempFile() throws IOException {
        return getNewTempFile(null);
    }

    public static String getTempDirPath() {
        return getTempDir().getAbsolutePath();
    }

    public static File getTempDir() {
        TEMP_OUTPUT_DIR.mkdirs();
        return TEMP_OUTPUT_DIR;
    }

    /**
     * Copies the source file to the destination file.
     *
     * @param source the source file
     * @param destination the destination location
     * @param overwritePolicy whether or not to overwrite or append to an
     * existing file, if there is a already a file at the specified destination
     * location.
     * @throws FileNotFoundException if the source file cannot be found
     * @throws Exception if a file already exists at the specified destination
     * and the overwrite policy is NEVER
     * @throws IOException if an unexpected IO condition is encountered while
     * copying the file
     * @throws RemoteException if there is a communication problem between the
     * client and server
     */
    public static void copy(File source, File destination,
            OverwritePolicy overwritePolicy) throws
            FileNotFoundException,
            Exception,
            IOException {
        if (destination.exists() && overwritePolicy != OverwritePolicy.YES) {
            throw new Exception("Cannot copy file; a file already exists at the destination location: "
                    + destination);
        }
        FileCopier xerox = new FileCopier(source, destination, overwritePolicy);
        try {
            xerox.call();
        } catch (Exception ex) {
            ex.printStackTrace();
            
            throw new Exception("Problem copying file '" + source
                    + "' to location '" + destination + "' with overwrite policy "
                    + overwritePolicy, ex);
        }
    }

    /*
     * Snatched from sequencing pipeline: @author cidr
     */
    private static class FileCopier implements Callable<File> {

        private final File sourceFile;
        private final File destinationFile;
        private final OverwritePolicy overwritePolicy;
        private static final int THIRTY_TWO_KIBIBYTES = 1024 * 32;

        public FileCopier(File sourceFile, File destinationFile, OverwritePolicy overwritePolicy) {
            if (sourceFile == null) {
                throw new NullPointerException("sourceFile must not be null");
            }
            if (destinationFile == null) {
                throw new NullPointerException("destinationFile must not be null");
            }
            if (overwritePolicy == null) {
                throw new NullPointerException("overwritePolicy must not be null");
            }
            this.sourceFile = sourceFile;
            this.destinationFile = destinationFile;
            this.overwritePolicy = overwritePolicy;
        }

        @Override
        public File call() throws Exception {
            return copy();
        }

        public File copy() throws FileNotFoundException, IOException {
            if (destinationFile.exists()) {
                if (overwritePolicy == OverwritePolicy.NO || overwritePolicy == OverwritePolicy.APPEND) {
                    return destinationFile;
                }
            }
            InputStream in = null;
            OutputStream out = null;
            try {
                in = new FileInputStream(sourceFile);
                out = new FileOutputStream(destinationFile);
                copyStream(in, out, THIRTY_TWO_KIBIBYTES);
                return destinationFile;
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException ignore) {
                    }
                }
                if (out != null) {
                    try {
                        out.flush();
                    } catch (IOException ignore) {
                    }
                    try {
                        out.close();
                    } catch (IOException ignore) {
                    }
                }
            }
        }

        private void copyStream(InputStream in, OutputStream out, long bufferSize) throws IOException {
            if (in == null) {
                throw new NullPointerException("InputStream must not be null");
            }
            if (out == null) {
                throw new NullPointerException("OutputStream must not be null");
            }
            if (bufferSize < 1) {
                throw new IllegalArgumentException("bufferSize must be > 0.");
            }
            if (bufferSize > Integer.MAX_VALUE) {
                throw new IllegalArgumentException("bufferSize must not be greater than: " + Integer.MAX_VALUE + ".");
            }
            byte[] buffer = new byte[(int) bufferSize];
            int i;
            while ((i = in.read(buffer)) != -1) {
                out.write(buffer, 0, i);
            }
            out.flush();
        }
    }

    public static void exportTableToCsv(final GuiTable<?> table) {
        new Thread() {
            @Override
            public void run() {
                try {
                    FileBrowseListener listener = new FileBrowseListener("Save",
                            "Save as", JFileChooser.FILES_ONLY);
                    File file = listener.showFileSelectionDialog();
                    if (file == null) {
                        Application.reportError(new GuiUtils.ErrorMessage("No output file was selected.",
                                "Input Required", GuiUtils.ContactPwogwammaws.NO));
                    } else {
                        String name = file.getName();
                        if (false == name.toLowerCase().endsWith(Constants.CSV_EXTENSION)) {
                            name += Constants.CSV_EXTENSION;
                        }
                        File toWrite = new File(file.getParentFile(), name);
                        String fileContents = table.getContentsAsCsvFileString();
                        WritableFile writer = new WritableFile(toWrite, OverwritePolicy.PROMPT);
                        if (writer.canWriteFile()) {
                            writer.write(fileContents);
                            writer.flushAndClose();
                            HtmlUtils.HtmlBuilder html = new HtmlUtils.HtmlBuilder();
                            html.append("Your file has been saved to: ");
                            html.newLine();
                            html.append(toWrite.getAbsolutePath());
                            Application.reportSuccess(html.toString());
                            GuiUtils.displayOutput(toWrite);
                        } else {
                            Application.reportWarning("Your file was not saved.");
                        }
                    }
                } catch (Exception e) {
                    Application.reportError("There was a problem saving the table contents to a file", "Uh oh...", e, GuiUtils.ContactPwogwammaws.YES);
                }
            }
        }.start();
    }

    public static enum OverwritePolicy {

        YES("overwrite allowed"),
        APPEND("append to file"),
        NO("overwrite not allowed"),
        @Deprecated // http://165.112.177.106/show_bug.cgi?id=212 -- use NO instead, as a unique file name will be created automatically
        PROMPT("prompts the user to decide"),;
        private final String desc;

        private OverwritePolicy(String desc) {
            this.desc = desc;
        }

        public String getDescription() {
            return desc;
        }
    }

    /**
     * Implementation suggestions: each Type value could be associated with a
     * FileContent class. A static lookup method (similar to fromString()) could
     * be used by FileContent objects to lookup which Type applies to them; each
     * FileContent can then grab the appropriate parser instance.
     *
     * All configuration for parsing any file in Phoenix can be specified in the
     * implementation of this interface. The client code need only provide the
     * result object that the parser of the file should populate.
     */
    public static enum Type implements Serializable {

        TWO_D_BARCODE("2D Barcode File"),
        MANREC("MANREC"),
        MANSENT("MANSENT"),
        MANREC_VALIDATION_REPORT("MANREC Validation Report"),;
        private final String name;
        private static final long serialVersionUID = 42L;

        private Type(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return getName();
        }
    }

    /**
     * Reads the supplied file into a byte array. USE THIS METHOD WITH CAUTION!
     * Only small-ish files should be provided to this method, as the resulting
     * byte array will be held entirely in memory (RAM), and foolish use of this
     * method -will- exhaust your system memory. If you do that, you're gonna
     * have a bad time.
     *
     * @param file
     * @return
     * @throws Exception
     */
    public static byte[] readFileIntoByteArray(File file) throws Exception {
        if (file == null) {
            throw new NullPointerException("File to read into byte array must not be null");
        }
        if (false == file.exists()) {
            throw new IllegalArgumentException("File to read into byte array must exist.");
        }
        if (file.length() > Integer.MAX_VALUE) {
            throw new IllegalArgumentException("File is too large to read into an array. Calling this method with that file is bad, and you should feel bad.");
        }
        RandomAccessFile raf = new RandomAccessFile(file, "r");
        int size = (int) raf.length();
        byte[] result = new byte[size];
        raf.readFully(result);
        return result;
    }

    //TODO: Consider ditching the OverwritePolicy.PROMPT enum value. Doing so would obviate the need for this method.
    public static WritableFile constructWritableFile(File file, FileUtils.OverwritePolicy preferredOverwritePolicy) throws Exception {
        //If we has no gui and we would prompt the user, then don't!
        boolean trouble = !Application.hasGui() && (preferredOverwritePolicy == FileUtils.OverwritePolicy.PROMPT);
        return new WritableFile(file, trouble
                ? FileUtils.OverwritePolicy.YES : preferredOverwritePolicy);
    }
}
