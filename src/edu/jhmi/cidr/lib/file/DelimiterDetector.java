/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file;


import edu.jhmi.cidr.lib.file.content.FileContent;
import edu.jhmi.cidr.lib.file.content.ParseStrategyAdapter;
import edu.jhmi.cidr.lib.file.content.ParseEngine;
import edu.jhmi.cidr.lib.Tuple;
import edu.jhmi.cidr.lib.Utils;
import java.io.File;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author David Newcomer Apr 11, 2013
 */
public class DelimiterDetector {
    //
    //To check for additional delimiters, add them to this list.

    private static final List<String> DELIMITERS = Utils.asList(",", ";", "\t");
    private final File file;

    private DelimiterDetector(File file) {
        this.file = file;
    }

    public static String determineDelimiter(String filepath) throws Exception {
        return determineTuple(new File(filepath)).get1();
    }

    public static String determineDelimiter(File file) throws Exception {
        return determineTuple(file).get1();
    }

    private static Tuple<String, Integer> determineTuple(File file) throws Exception {
        return new DelimiterDetector(file).determineDelimiter();
    }

    public Tuple<String, Integer> determineDelimiter() throws Exception {
        Strategy strat = new Strategy();
        ParseEngine.parse(file, strat);
        return strat.getTuple();
    }

    private static class Strategy extends ParseStrategyAdapter {

        private final Map<Tuple<String, Integer>, Long> frequencyMap;

        public Strategy() {
            this.frequencyMap = new HashMap<>();
        }

        @Override
        public FileContent getFileContent() {
            return new FileContent() {
            };
        }

        @Override
        public void handleLine(long lineNumber, String line) throws ParseException {
            //Don't parse every line of vary large files.
            if (lineNumber > 250) {
                continueParsing = false;
            }
            for (String delimiter : DELIMITERS) {
                if (line.contains(delimiter)) {
                    processLine(line, delimiter);
                }
            }
        }
        private boolean continueParsing = true;

        @Override
        public boolean continueParsing() {
            return continueParsing;
        }

        private void processLine(String line, String delimiter) {
            int columns = line.split(delimiter).length;
            Tuple<String, Integer> key = new Tuple<>(delimiter, columns);
            if (false == frequencyMap.containsKey(key)) {
                frequencyMap.put(key, 0L);
            }
            frequencyMap.put(key, frequencyMap.get(key) + 1);
        }

        public Tuple<String, Integer> getTuple() {
            long biggest = 0;
            Tuple<String, Integer> result = null;
            for (Map.Entry<Tuple<String, Integer>, Long> entry : frequencyMap.entrySet()) {
                Long long1 = entry.getValue();
                if (long1 > biggest) {
                    biggest = long1;
                    result = entry.getKey();
                }
//                System.out.println(entry.getKey().get2() + " columns of '" + entry.getKey().get1() + "' found " + entry.getValue() + " times ");
            }
            return result == null ? new Tuple<>(",", 1) : result;
        }
    }
}
