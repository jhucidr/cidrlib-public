/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file;

import edu.jhmi.cidr.lib.DateFormatter;
import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.file.FileUtils.OverwritePolicy;
import edu.jhmi.cidr.lib.gui.GuiUtils;
import java.io.*;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author David Newcomer Mar 30, 2011
 */
public class WritableFile implements AutoCloseable {

    private PrintWriter out = null;
    private final File file;
    private static final Pattern FILE_PATH_EXTENSION_PATTERN = Pattern.compile("(.*?)(\\..+)?");

    /**
     * Builds a WritableFile object with the policy of never overwriting
     * existing files.
     *
     * @param fullyQualifiedOutputFilePath
     * @throws Exception
     */
    public WritableFile(String fullyQualifiedOutputFilePath) throws Exception {
        this(new File(fullyQualifiedOutputFilePath));
    }

    /**
     * Builds a WritableFile object with the policy of never overwriting
     * existing files.
     *
     * @param fullyQualifiedOutputFilePath
     * @throws Exception
     */
    public WritableFile(File outputFile) throws Exception {
        this(outputFile, OverwritePolicy.NO);
    }

    /**
     * Builds a WritableFile object with the supplied overwriting policy.
     *
     * @param fullyQualifiedOutputFilePath
     * @param overwritePolicy
     * @throws Exception
     */
    public WritableFile(String fullyQualifiedOutputFilePath, OverwritePolicy overwritePolicy) throws Exception {
        this(new File(fullyQualifiedOutputFilePath), overwritePolicy);
    }

    /**
     * Builds a WritableFile object with the supplied overwriting policy.
     *
     * @param outputFile
     * @param overwritePolicy
     * @throws Exception
     */
    public WritableFile(File outputFile, OverwritePolicy overwritePolicy) throws Exception {
        if (overwritePolicy == null) {
            throw new NullPointerException("OverwritePolicy must not be null");
        }
        if (outputFile == null) {
            throw new NullPointerException("fullyQualifiedOutpoutFilePath must not be null.");
        }
        if (overwritePolicy == OverwritePolicy.NO) {
            if (outputFile.exists()) {
                outputFile = createUniqueFileName(outputFile);
            }
        }
        File parent = outputFile.getParentFile();
        if (parent.exists() == false) {
            if (parent.getParentFile().mkdirs() == false) {
                throw new IOException("Could not create output directory: " + outputFile.getAbsolutePath());
            }
        }
        boolean append = true;  //when true, the printWriter will append.
        if (overwritePolicy == OverwritePolicy.APPEND) {
            if (outputFile.exists() == true) {
                System.out.println("Your output filepath already exists.  Appending...");
            }
        } else if (overwritePolicy == OverwritePolicy.YES) {
            append = false;
        }
        if (overwritePolicy == OverwritePolicy.PROMPT) {
            append = false;
            if (outputFile.exists()) {
                int x = JOptionPane.showConfirmDialog(null,
                        "File exists: " + outputFile.getName() + ". Overwrite?", "Confirm", JOptionPane.YES_NO_OPTION);
                if (x != JOptionPane.YES_OPTION) {
                    throw new GuiUtils.ErrorMessage("You chose not to overwrite the file.", "Unable to write file", GuiUtils.ContactPwogwammaws.NO);
                }
            }
        }
        out = new PrintWriter(new BufferedWriter(new FileWriter(outputFile, append)));
        this.file = outputFile;
    }

    private File createUniqueFileName(File outputFile) throws OverwriteException {
        System.out.println("Trying to create unique file name for: " + outputFile.getAbsolutePath());
        File dir = outputFile.getParentFile();
        String fileName = outputFile.getName();
        Matcher matcher = FILE_PATH_EXTENSION_PATTERN.matcher(fileName);
        if (matcher.matches()) {
            String prefix = matcher.group(1);
            String extension = matcher.group(2);
            extension = (extension == null) ? "" : extension;
            String newNameWithDate = prefix + "." + DateFormatter.getCurrentFormattedFileNameDate() + extension;
            File newFileWithDateInPath = new File(dir, newNameWithDate);
            if (newFileWithDateInPath.exists()) {
                String newNameWithUuid = Utils.transformToValidFileName(prefix + "." + UUID.randomUUID() + extension);
                File newfileWithUuidInPath = new File(dir, newNameWithUuid);
                if (newfileWithUuidInPath.exists()) {
                    throw new OverwriteException("Your output filepath " + outputFile.getAbsolutePath() + " already exists and an attempt to create a unique file name failed.  Unable to write file.");
                } else {
                    return newfileWithUuidInPath;
                }
            } else {
                return newFileWithDateInPath;
            }
        } else {
            throw new OverwriteException("Your output filepath " + outputFile.getAbsolutePath() + " already exists.  Unable to write file.");
        }
    }

    @Override
    public void close() {
        if (out != null) {
            try {
                flush();
            } catch (Exception ignore) {
            }
            try {
                out.close();
            } catch (Exception ignore) {
            }
            out = null;
        }
    }

    public static class OverwriteException extends Exception {

        public OverwriteException(String message) {
            super(message);
        }
    }

    public void write(String content) {
        if (content == null) {
            throw new NullPointerException("line must not be null");
        }
        out.print(content);
    }

    public void writeLine(String line) {
        if (line == null) {
            return;
        }
        out.println(line);
    }

    public void newLine() {
        out.println();
    }

    public void writeLineAndFlush(String line) {
        writeLine(line);
        flush();
    }

    public void flush() {
        if (out != null) {
            out.flush();
        }
    }

    public void write(Object content) {
        out.print(Utils.display(content));
    }

    public void flushAndClose() {
        close();
    }

    public File getFile() {
        return file;
    }

    public boolean canWriteFile() {
        return (out != null) && (file != null);
    }
}
