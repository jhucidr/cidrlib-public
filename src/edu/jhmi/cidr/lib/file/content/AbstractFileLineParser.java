/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file.content;

import edu.jhmi.cidr.lib.Displayable;
import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.file.content.AbstractFileLineParser.FileLineParseError.MissingColumn;
import edu.jhmi.cidr.lib.file.content.AbstractFileLineParser.FileLineParseError.UnrecognizedColumn;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cidr
 */
public abstract class AbstractFileLineParser<R, C> {

    private List<C> columnsInOrder;
    private final List<FileLineParseError> parseErrors = new ArrayList<>();
    private final String delimiter;

    protected AbstractFileLineParser(String delimiter) {
        this.delimiter = delimiter;
    }

    protected abstract C string2Column(String headerString);

    protected abstract R getNewRecord(long lineNumber);

    protected abstract void populateRecord(long lineNumber, R record, C col, String colData) throws Exception;

    protected abstract List<C> getRequiredColumns();

    public static class FileLineParseError implements Displayable {

        private final String errorString;

        public FileLineParseError(String errorString) {
            this.errorString = errorString;
        }

        public String getErrorString() {
            return errorString;
        }

        @Override
        public String getDisplayString() {
            return getErrorString();
        }

        public static class UnrecognizedColumn extends FileLineParseError {

            public UnrecognizedColumn(String errorString) {
                super(errorString);
            }
        }

        public static class MissingColumn extends FileLineParseError {

            public MissingColumn(String errorString) {
                super(errorString);
            }
        }

        public static class UnexpectedValues extends FileLineParseError {

            public UnexpectedValues(String errorString) {
                super(errorString);
            }
        }
    }

    public void addParseError(FileLineParseError parseError) {
        parseErrors.add(parseError);
    }

    public List<String> getParseErrorStrings() {
        List<String> result = new ArrayList<>();
        for (FileLineParseError error : parseErrors) {
            result.add(error.getErrorString());
        }
        return result;
    }

    public List<FileLineParseError> getParseErrors() {
        return parseErrors;
    }

    public boolean isHeaderParsed() {
        return columnsInOrder != null;
    }

    public void parseHeaderLine(long lineNumber, String headerLine) {
        String[] headerData = Utils.split(delimiter, headerLine);
        columnsInOrder = new ArrayList<>(headerData.length);
        for (int i = 0; i < headerData.length; i++) {
            String colHeader = headerData[i].trim();
            C col = string2Column(colHeader);
            if (col == null) {
                addParseError(new UnrecognizedColumn("unrecognized column '" + colHeader + "' in column number " + (i + 1) + " on line " + lineNumber));
            }
            // NOTE: we want to add null for any unrecognized column headers so
            // that the underlying data are extracted and assigned to the appropriate headers.
            columnsInOrder.add(col);
        }
        if (false == columnsInOrder.containsAll(getRequiredColumns())) {
            addParseError(new MissingColumn(determineMissingColumns()));
        }
    }

    private String determineMissingColumns() {
        List<C> req = new ArrayList<>(getRequiredColumns());
        req.removeAll(columnsInOrder);
        StringBuilder sb = new StringBuilder();
        sb.append("The following columns were required, but are missing: ");
        sb.append(req);
        return sb.toString();
    }

    public R parse(long lineNumber, String line) throws Exception {
        if (columnsInOrder == null) {
            throw new IllegalStateException("parseHeaderLine() was not called");
        }
        line = Utils.padWithDelimiters(line, columnsInOrder.size() - 1, delimiter).toString();
        R result = getNewRecord(lineNumber);
        String[] lineData = Utils.split(delimiter, line);
        if (lineData.length > columnsInOrder.size()) {
            addParseError(new FileLineParseError.UnexpectedValues("More values found on line "//
                    + lineNumber//
                    + " than expected (according to the header line): "//
                    + columnsInOrder.size()));
        }
        for (int i = 0; i < columnsInOrder.size(); i++) {
            C col = columnsInOrder.get(i);
            if (col != null) {
                if (i < lineData.length) {
                    String colData = lineData[i].trim();
                    populateRecord(lineNumber, result, col, colData);
                } // (the else case should not occur due to the padding above)
            }
        }
        return result;
    }
}