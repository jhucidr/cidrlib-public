/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file.content;

import edu.jhmi.cidr.lib.Displayable;
import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.file.content.FileColumn;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author David Newcomer May 13, 2011
 */
public class FileRecord<T extends FileColumn> implements Displayable {

    private final long lineNumber;
    // LinkedHashMap to maintain insertion order.
    private final Map<T, String> contents = new LinkedHashMap<>();

    public FileRecord(long lineNumber) {
        this.lineNumber = lineNumber;
    }

    /*package access*/ void pad(List<T> allColumns) {
        if (false == contents.keySet().containsAll(allColumns)) {
            Map<T, String> temp = new LinkedHashMap<>();
            for (T col : allColumns) {
                String tempVal = contents.containsKey(col) ? contents.get(col) : "";
                temp.put(col, tempVal);
            }
            contents.clear();
            contents.putAll(temp);
        }
    }

    public long getLineNumber() {
        return lineNumber;
    }

    public Collection<T> getColumns() {
        return contents.keySet();
    }

    public String getValue(T key) {
        return contents.get(key);
    }

    public String setValue(T key, String value) {
        return contents.put(key, value);
    }

    @Override
    public String toString() {
        return lineNumber + " " + contents;
    }

    @Override
    public String getDisplayString() {
        return Utils.display(contents.values());
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.contents);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked") // equals check
        final FileRecord<T> other = (FileRecord<T>) obj;
        if (!Objects.equals(this.contents, other.contents)) {
            return false;
        }
        return true;
    }
}
