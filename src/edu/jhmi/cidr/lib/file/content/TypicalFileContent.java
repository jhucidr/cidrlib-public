/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file.content;

import edu.jhmi.cidr.lib.Displayable;
import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.file.FileUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author cidr
 */
public class TypicalFileContent<T extends FileColumn> implements Displayable, FileContent {
    private List<FileRecord<T>> records = new ArrayList<>();
    private final List<T> allColumns;
    private final boolean columnsProvidedAtInitialization;

    /**
     * Enforces order for entire set of columns.
     *
     * @param allColumns The columns in the file in order
     */
    public TypicalFileContent(List<T> allColumns) {
        this.allColumns = allColumns;
        this.columnsProvidedAtInitialization = allColumns.isEmpty() ? false : true;
    }

    /**
     * Adds columns to the file content as records are added. Column order
     * is based on the order in which they appear in each record. For each
     * new successively added record, new columns are added after previously
     * added columns for earlier records regardless of their order in the
     * newly added record.
     */
    public TypicalFileContent() {
        this(new ArrayList<T>());
    }

    /**
     * Pads this record and possibly all previously added records with empty
     * string for missing values of any expected columns.
     */
    public void addRecord(FileRecord<T> recordToAdd) {
        records.add(recordToAdd);
        if (false == columnsProvidedAtInitialization) {
            boolean shouldPadAll = false;
            for (T col : recordToAdd.getColumns()) {
                if (false == allColumns.contains(col)) {
                    allColumns.add(col);
                    shouldPadAll = true;
                }
            }
            if (shouldPadAll) {
                for (FileRecord<T> record : records) {
                    record.pad(allColumns);
                }
            }
        }
        recordToAdd.pad(allColumns); // ensure the last added record is always padded
    }

    public List<FileRecord<T>> getRecords() {
        return records;
    }

    @Override
    public String toString() {
        return getDisplayString();
    }

    @Override
    public String getDisplayString() {
        if (false == getRecords().isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append(Utils.join(FileUtils.CSV_DELIMITER, allColumns)).append(FileUtils.NEW_LINE);
            for (FileRecord<T> record : getRecords()) {
                sb.append(Utils.display(record)).append(FileUtils.NEW_LINE);
            }
            return sb.toString();
        } else {
            return "";
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.records);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        // equals check
        @SuppressWarnings(value = "unchecked")
        final TypicalFileContent<T> other = (TypicalFileContent<T>) obj;
        if (!Objects.equals(this.records, other.records)) {
            return false;
        }
        return true;
    }

    public static class Builder<T extends FileColumn> {

        private final TypicalFileContent<T> content;
        private FileRecord<T> currentRecord;

        public Builder(List<T> allColumns) {
            Objects.requireNonNull(allColumns, "'all columns' set must not be null");
            this.content = new TypicalFileContent<>(allColumns);
            newLine();
        }

        public Builder() {
            this.content = new TypicalFileContent<>();
            newLine();
        }

        public final Builder<T> newLine() {
            // start at 2 to count the header row
            long lineNumber;
            if (currentRecord == null) {
                lineNumber = 2L;
            } else {
                content.addRecord(currentRecord);
                lineNumber = currentRecord.getLineNumber() + 1;
            }
            currentRecord = new FileRecord<>(lineNumber);
            return this;
        }

        public final Builder<T> set(T column, String value) {
            currentRecord.setValue(column, value);
            return this;
        }

        public TypicalFileContent<T> build() {
            newLine(); // adds last record to content
            return content;
        }
    }
    
}
