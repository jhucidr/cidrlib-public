/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file.content;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author cidr
 */
public class FileParser<T extends FileColumn>
        extends SimpleParseStrategy<FileRecord<T>, T, TypicalFileContent<T>> {

    private final Map<String, T> fromString = new HashMap<>();

    public FileParser(String delimiter, List<T> validColumns, List<T> requiredColumns) {
        this(1, delimiter, validColumns, requiredColumns);
    }

    public FileParser(int headerLine, String delimiter, List<T> validColumns, List<T> requiredColumns) {
        super(headerLine, delimiter, new TypicalFileContent<T>(), requiredColumns);
        for (T t : validColumns) {
            fromString.put(t.getColumnName().trim().toLowerCase(), t);
        }
    }

    @Override
    protected void addRecordToContent(TypicalFileContent<T> content, FileRecord<T> record) {
        content.addRecord(record);
    }

    @Override
    protected void populateRecord(long lineNumber, FileRecord<T> record, T col, String colData) throws Exception {
        record.setValue(col, colData);
    }

    @Override
    protected FileRecord<T> getNewRecord(long lineNumber) {
        return new FileRecord<>(lineNumber);
    }

    @Override
    protected T convertStringToColumn(String headerString) {
        return fromString.get(headerString.trim().toLowerCase());
    }

    public TypicalFileContent<T> parse(File toParse) throws Exception {
        ParseEngine.parse(toParse, this);
        return getFileContent();
    }
}
