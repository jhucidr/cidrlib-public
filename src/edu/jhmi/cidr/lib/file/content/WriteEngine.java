/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file.content;

import edu.jhmi.cidr.lib.file.FileUtils.OverwritePolicy;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author dleary
 */
public class WriteEngine {

    public static <T extends FileContent> void writeFile(
            File destination, ContentWriterStrategy<T> strategy,
            T toWrite, OverwritePolicy policy) throws Exception {
        if (destination == null) {
            throw new NullPointerException("destination must not be null");
        }
        if (strategy == null) {
            throw new NullPointerException("strategy must not be null");
        }
        FileOutputStream fos = null;
        switch (policy) {
            case APPEND:
                fos = new FileOutputStream(destination, true);
                break;
            case YES:
                fos = new FileOutputStream(destination, false);
                break;
            case NO:
                if (destination.exists()) {
                    throw new IOException(
                            "There is already a file at the write destination "
                            + "and the OverwritePolicy is 'NO': "
                            + destination.getAbsolutePath());
                } else {
                    fos = new FileOutputStream(destination, false);
                }
                break;
            default:
                throw new Exception("OverwritePolicy not handled: " + policy);
        }
        if (fos == null) {
            throw new Exception("Uninitialized file output stream");
        }
        writeStream(new BufferedOutputStream(fos), strategy, toWrite);
    }

    public static <T extends FileContent> void writeStream(
            BufferedOutputStream os,
            ContentWriterStrategy<T> strategy,
            T toWrite) throws Exception {
        if (strategy == null) {
            throw new NullPointerException("strategy must not be null");
        }
        if (os == null) {
            throw new NullPointerException("os must not be null");
        }
        BufferedOutputStream wrappedStream = strategy.wrapStream(os);
        try {
            strategy.preWrite();
            strategy.write(wrappedStream, toWrite);
        } catch (Exception ex) {
            strategy.handleThrowable(ex);
        } finally {
            try {
                strategy.postWrite();
            } finally {
                try {
                    wrappedStream.flush();
                } catch (IOException ignore) {
                }
                try {
                    wrappedStream.close();
                } catch (IOException ignore) {
                }
            }
        }
    }
}
