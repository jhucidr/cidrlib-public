/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file.content;

import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.file.content.AbstractFileLineParser.FileLineParseError;
import java.text.ParseException;
import java.util.List;

/**
 *
 * @author cidr
 */
public abstract class SimpleParseStrategy<R, C, F extends FileContent> extends ParseStrategyAdapter<F> {

    private final F content;
    private final AbstractFileLineParser<R, C> lineParser;
    private final String delimiter;
    private final int headerLine;

    protected SimpleParseStrategy(String delimiter, F content, final List<C> requiredColumns) {
        this(1, delimiter, content, requiredColumns);
    }

    // TODO consider instead, detecting which line is the header line based on whether
    //      all values match the required columns;
    protected SimpleParseStrategy(int headerLine, String delimiter, F content, final List<C> requiredColumns) {
        if (headerLine < 1) {
            throw new IllegalArgumentException("headerLine must be >0: " + headerLine);
        }
        if (content == null) {
            throw new NullPointerException("content must not be null");
        }
        this.headerLine = headerLine;
        this.delimiter = delimiter;
        this.content = content;
        this.lineParser = new AbstractFileLineParser<R, C>(delimiter) {
            @Override
            protected C string2Column(String headerString) {
                return SimpleParseStrategy.this.convertStringToColumn(headerString);
            }

            @Override
            protected R getNewRecord(long lineNumber) {
                return SimpleParseStrategy.this.getNewRecord(lineNumber);
            }

            @Override
            protected void populateRecord(long lineNumber, R record, C col, String colData) throws Exception {
                SimpleParseStrategy.this.populateRecord(lineNumber, record, col, colData);
            }

            @Override
            protected List<C> getRequiredColumns() {
                return requiredColumns;
            }
        };
    }

    protected abstract C convertStringToColumn(String headerString);

    protected abstract R getNewRecord(long lineNumber);

    protected abstract void populateRecord(long lineNumber, R record, C col, String colData) throws Exception;

    protected abstract void addRecordToContent(F content, R record);

    @Override
    public F getFileContent() {
        return content;
    }

    @Override
    public void handleLine(long lineNumber, String line) throws Exception {
        if (Utils.stringIsEmptyOrContainsOnlyDelimiters(line, delimiter)) {
            return;
        }
        if (lineNumber < headerLine) {
            return;
        }
        parseDataLine(lineNumber, line);
    }

    private void parseDataLine(long lineNumber, String line) throws Exception {
        if (false == lineParser.isHeaderParsed()) {
            lineParser.parseHeaderLine(lineNumber, line);
        } else {
            R record = lineParser.parse(lineNumber, line);
            addRecordToContent(getFileContent(), record);
        }
    }

    public void addParseError(FileLineParseError parseError) {
        lineParser.addParseError(parseError);
    }

    public List<String> getParseErrorStrings() {
        return lineParser.getParseErrorStrings();
    }
    
    public List<FileLineParseError> getParseErrors() {
        return lineParser.getParseErrors();
    }

    public boolean hasErrors() {
        return false == getParseErrors().isEmpty();
    }
}
