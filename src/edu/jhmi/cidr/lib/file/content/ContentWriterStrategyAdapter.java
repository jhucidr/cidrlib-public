/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file.content;

import java.io.BufferedOutputStream;
import java.io.IOException;

/**
 *
 * @author cidr
 */
public class ContentWriterStrategyAdapter<T extends FileContent> implements ContentWriterStrategy<T> {

    @Override
    public void preWrite() {
        // no-op
    }

    @Override
    public void postWrite() {
        // no-op
    }

    @Override
    public void write(BufferedOutputStream out, T toWrite) throws IOException {
        // no-op
    }

    @Override
    public void handleThrowable(Throwable th) throws Exception {
        // no-op
        try {
            throw th;
        } catch (IOException ioe) {
            throw ioe;
        } catch (RuntimeException re) {
            throw re;
        } catch (Throwable t) {
            throw new Exception("unknown throwable type thrown", t);
        }
    }

    @Override
    public boolean continueWriting() {
        return true;
    }

    @Override
    public BufferedOutputStream wrapStream(BufferedOutputStream out) throws IOException {
        return out;
    }
    
}
