/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file.content;

import java.io.File;

/**
 *
 * @author cidr
 */
public abstract class ParseStrategyAdapter<T extends FileContent> implements ParseStrategy<T> {

    private File parsedFile;

    @Override
    public void setParsedFile(File file) {
        this.parsedFile = file;
    }

    @Override
    public File getParsedFile() {
        return parsedFile;
    }

    @Override
    public void preParse() throws Exception {
        return; // no-op
    }

    @Override
    public void postParse() {
        return; // no-op
    }

    @Override
    public abstract T getFileContent(); // client must implement

    // client must implement
    @Override
    public abstract void handleLine(long lineNumber, String line) throws Exception;

    @Override
    public void handleThrowable(long lineNumber, Exception ex) throws Exception {
        throw ex;
    }

    @Override
    public boolean continueParsing() {
        return true; // default
    }
}
