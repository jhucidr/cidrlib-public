/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.file;

import edu.jhmi.cidr.lib.file.content.FileContent;
import edu.jhmi.cidr.lib.file.content.ParseStrategyAdapter;
import edu.jhmi.cidr.lib.file.content.ParseEngine;
import edu.jhmi.cidr.lib.Utils;
import java.io.File;
import org.apache.poi.xssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;

import java.io.FileOutputStream;
import java.text.ParseException;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author David Newcomer Apr 11, 2013
 */
public class CsvToExcelFileConverter {

    private static final long MAX_XLS_LINES = 65535;
    private final File toConvert;
    private final Workbook wb;
    private final CellStyle style;
    private final Sheet sheet;// = wb.createSheet(toConvert.getName());
    private final Type type;

    public static void main(String... args) throws Exception {
        File csvFile = new File("big.txt");
        System.out.println(CsvToExcelFileConverter.convert(csvFile, Type.XLSX));

    }

    private CsvToExcelFileConverter(File toConvert, Type type) throws Exception {
        if (toConvert == null) {
            throw new NullPointerException("toConvert must not be null");
        }
        if (type == null) {
            throw new NullPointerException("type must not be null");
        }
        FileUtils.validateFileExists(toConvert);
        this.toConvert = toConvert;
        this.wb = type.buildNewWorkbook();
        this.style = wb.createCellStyle();
        this.sheet = wb.createSheet(toConvert.getName());
        this.type = type;
    }

    public File convert() throws Exception {
        if ((type == Type.XLS) && FileUtils.getLineCount(toConvert) >= MAX_XLS_LINES) {
            System.out.println("Conversion not supported for XLS files with > " + MAX_XLS_LINES + " lines.");
            return toConvert;
        }
        Strategy strategy = new Strategy(DelimiterDetector.determineDelimiter(toConvert));
        ParseEngine.parse(toConvert, strategy);
        File file = buildOutputFile();
        try (FileOutputStream out = new FileOutputStream(file)) {
            wb.write(out);
        }
        return file;
    }

    public static File convert(String toConvert) throws Exception {
        return convert(new File(toConvert));
    }

    public static File convert(File toConvert) throws Exception {
        return convert(toConvert, Type.XLSX);
    }

    public static File convert(String toConvert, Type type) throws Exception {
        return convert(new File(toConvert), type);
    }

    public static File convert(File toConvert, Type type) throws Exception {
        String fileName = toConvert.getName();
        for (Type t : Type.values()) {
            //Don't convert excel files to excel format.
            if (fileName.toLowerCase().trim().endsWith(t.getExtension().toLowerCase())) {
                return toConvert;
            }
        }
        return new CsvToExcelFileConverter(toConvert, type).convert();
    }

    private File buildOutputFile() {
        String file = removeExtension(toConvert.getAbsolutePath()) + ".xls";
        if (wb instanceof XSSFWorkbook) {
            file += "x";
        }
        return new File(file);
    }

    private static String removeExtension(String toModify) {
        if (toModify.contains(".")) {
            return toModify.substring(0, toModify.lastIndexOf("."));
        } else {
            return toModify;
        }
    }

    public enum Type {

        XLS(".xls"),
        XLSX(".xlsx"),;
        //
        private final String extension;

        private Type(String extension) {
            this.extension = extension;
        }

        public String getExtension() {
            return extension;
        }


        /*package access*/ Workbook buildNewWorkbook() {
            switch (this) {
                case XLS:
                    return new HSSFWorkbook();
                case XLSX:
                    return new XSSFWorkbook();
                default:
                    throw new UnsupportedOperationException("WTF?!!!!");
            }
        }
    }

    private class Strategy extends ParseStrategyAdapter {

        private final String delimiter;

        public Strategy(String delimiter) {
            this.delimiter = delimiter;
        }

        @Override
        public FileContent getFileContent() {
            return new FileContent() {
            };
        }

        @Override
        public void handleLine(long lineNumber, String line) throws ParseException {
            String[] result = line.split(delimiter);
            //title row
            Row titleRow = sheet.createRow((int) lineNumber - 1);
            List<String> strings = Utils.asList(line.split(delimiter));
            for (int i = 0; i < strings.size(); i++) {
                Cell cell = titleRow.createCell(i);
                cell.setCellType(Cell.CELL_TYPE_STRING);
                cell.setCellValue(strings.get(i));
                cell.setCellStyle(style);
            }
        }
    }
}
