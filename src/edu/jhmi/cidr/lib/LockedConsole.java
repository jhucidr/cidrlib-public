/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class writes output to System.out and System.err under the guard of a
 * lock, to prevent interleaving of messages printed to standard error or
 * standard out. Please note that all Null-type messages are ignored, i.e., they
 * are NOT printed to either standard error or standard out.
 *
 * @author cidr
 */
public class LockedConsole {

    private static final Lock OUT_LOCK = new ReentrantLock();

    /**
     * Prints the supplied message to standard out.
     *
     * @param msg
     */
    public static void sout(String msg) {
        if (msg == null) {
            return;
        }
        OUT_LOCK.lock();
        try {
            System.out.println(msg);
        } finally {
            OUT_LOCK.unlock();
        }
    }

    /**
     * Prints the supplied message to standard err.
     *
     * @param msg
     */
    public static void serr(String msg) {
        if (msg == null) {
            return;
        }
        OUT_LOCK.lock();
        try {
            System.err.println(msg);
        } finally {
            OUT_LOCK.unlock();
        }
    }
}
