/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dleary
 */
public class IndexedLookupSet<T> extends LookupSet<T> {
    private final Map<T, Integer> idxByT;
    private final Map<Integer, T> tByIdx;

    public IndexedLookupSet() {
        this.idxByT = new HashMap<>();
        this.tByIdx = new HashMap<>();
    }

    @Override
    public synchronized T add(T t) {
        if (false == contains(t)) {
            idxByT.put(t, size());
            tByIdx.put(size(), t);
        }
        return super.add(t);
    }

    @Override
    public synchronized T remove(T toRemove) {
        Integer removedIdx = idxByT.get(toRemove);
        if (removedIdx != null) {
            idxByT.remove(toRemove);
            tByIdx.remove(removedIdx);
            for (Map.Entry<T, Integer> e : idxByT.entrySet()) {
                T t = e.getKey();
                Integer idx = e.getValue();
                if (idx > removedIdx) {
                    Integer adjustedIdx = idx - 1;
                    idxByT.put(t, adjustedIdx);
                    tByIdx.put(adjustedIdx, t);
                }
            }
        }
        return super.remove(toRemove);
    }

    public synchronized int indexOf(T t) {
        Integer result = idxByT.get(t);
        return result == null ? -1 : result.intValue();
    }

    public synchronized T get(int index) {
        return tByIdx.get(index);
    }

    public synchronized List<T> asList() {
        List<T> result = new ArrayList<>(size());
        for (int i = 0; i < size(); i++) {
            T t = tByIdx.get(i);
            result.add(t);
        }
        return result;
    }

    /**
     * Intended as a more efficient way to replace items than removing an item
     * and adding back at that same index.
     *
     * See implementation of remove().
     */
    public synchronized T replace(int rowIndex, T t) {
        Integer idx = idxByT.get(t);
        if (idx != null) {
            idxByT.put(t, idx);
            tByIdx.put(idx, t);
        }
        // essentially a 'replace' for LookupSet via Map.put()
        return super.add(t);
    }
    
}
