/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui;

import com.google.common.base.Optional;
import com.toedter.calendar.JDateChooser;
//import com.toedter.calendar.JDateChooser;
import edu.jhmi.cidr.lib.Application;
import edu.jhmi.cidr.lib.HtmlUtils.HtmlBuilder;
import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.file.CsvToExcelFileConverter;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.swing.*;

/**
 *
 * @author dleary
 */
public class GuiUtils {

    private static final Color TEXT_FIELD_ERROR_COLOR = Color.RED;
    private static final Color TEXT_FIELD_DEFAULT_COLOR = Color.BLACK;
    private static final Color TEXT_FIELD_BACKGROUND_ERROR_COLOR = new Color(0xFFDDDD);
    private static final Color TEXT_FIELD_BACKGROUND_DEFAULT_COLOR = Color.WHITE;
    private static final Dimension SCREEN_DIMENSION = Toolkit.getDefaultToolkit().getScreenSize();

    public static void center(Component c) {
        int x = (SCREEN_DIMENSION.width - c.getWidth()) / 2;
        int y = (SCREEN_DIMENSION.height - c.getHeight()) / 2;
        c.setBounds(x, y, c.getWidth(), c.getHeight());
    }

    public static Optional<File> promptUserForDownloadLocation() {
        if (Application.hasGui()) {
            FileBrowseListener listener = new FileBrowseListener("Choose Download Location", "File Download", JFileChooser.DIRECTORIES_ONLY);
            File dir = listener.showFileSelectionDialog();
            if (dir == null) {
                return Optional.absent();
            } else {
                return Optional.of(dir);
            }
        } else {
            return Optional.absent();
        }
    }

    public static Optional<File> promptUserForFile() {
        if (Application.hasGui()) {
            FileBrowseListener listener = new FileBrowseListener("Choose Download Location", "File Download", JFileChooser.FILES_AND_DIRECTORIES);
            File dir = listener.showFileSelectionDialog();
            if (dir == null) {
                return Optional.absent();
            } else {
                return Optional.of(dir);
            }
        } else {
            return Optional.absent();
        }
    }

    public static Optional<String> promptUserForComment(Component parentComponent) {
        return promptUserForComment(parentComponent, "", "Enter Comment");
    }

    public static Date promptUserForDate(Date initialDate) {
        JFrame jf = null;
        JDialog dialog = new JDialog(jf, true);
        dialog.setTitle("Modify Date");
        JDateChooser chooser = new JDateChooser(initialDate);
        dialog.add(chooser);
        dialog.setSize(250, 60);
        center(dialog);
        dialog.setVisible(true);
        dialog.dispose();
        return chooser.getDate();
    }
//    
//    public static void main(String[] args) throws Exception {
//        Date d = promptUserForDate(new Date());
//        System.out.println("my date was " + d);
//    }
    public static boolean prompt(String message, String title) {
        return prompt(null, message, title);
    }

    public static boolean prompt(Component c, String message, String title) {
        if (Application.hasGui()) {
            int answer = JOptionPane.showConfirmDialog(c, message, title, JOptionPane.YES_NO_OPTION);
            return answer == JOptionPane.YES_OPTION;
        }
        return false;
    }

    public static Optional<String> promptUserForComment(String message, String title) {
        return promptUserForComment(new JPanel(), message, title);
    }

    public static Optional<String> promptUserForComment(Component parentComponent, String message, String title) {
        if (false == Application.hasGui()) {
            StringBuilder sb = new StringBuilder();
            sb.append("PromptForComment Skipped: ");
            sb.append(title);
            sb.append("; ");
            sb.append(message);
            System.out.println(sb.toString());
            return Optional.of("Automated Comment Submission: " + sb.toString());
        }
        GuiConfirmDialog gui = new GuiConfirmDialog(parentComponent, message, title);
        gui.setVisible(true);
        return gui.getText();
    }

    public static <T extends JDialog> T launchDialog(T dialog) {
        centerInScreen(dialog);
        dialog.setVisible(true);
        return dialog;
    }

    public static UserChoice doesUserWantToDiscardUnsavedChanges() {
        return doesUserWantToDiscardUnsavedChanges("Any unsaved changes will be lost. Do you want to continue and discard these changes?");
    }

    public static UserChoice doesUserWantToDiscardUnsavedChanges(String message) {
        return doesUserWantToDiscardUnsavedChanges(null, message);
    }

    public static UserChoice doesUserWantToDiscardUnsavedChanges(Component c, String message) {
        if (c == null) {
            c = Application.getComponentOrNull();
        }
        if (c == null) {
            return UserChoice.CANCEL;
        } else {
            int selection = JOptionPane.showConfirmDialog(Application.getComponentOrNull(),//
                    message,
                    "Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if (JOptionPane.YES_OPTION == selection) {
                return UserChoice.DISCARD_UNSAVED_CHANGES;
            }
            return UserChoice.CANCEL;
        }
    }

    public static enum OnEdtMode {

        BLOCKING, NON_BLOCKING;
    }

    public static void onEdt(Runnable runnable) {
        onEdt(runnable, OnEdtMode.BLOCKING);
    }

    public static void onEdt(Runnable runnable, OnEdtMode mode) {
        if (EventQueue.isDispatchThread()) {
            runnable.run();
        } else {
            if (mode == OnEdtMode.BLOCKING) {
                try {
                    EventQueue.invokeAndWait(runnable);
                } catch (InterruptedException | InvocationTargetException ex) {
                    ex.printStackTrace();
                    throw new RuntimeException("Unable to execute runnable on EDT; " + ex.getMessage(), ex);
                }
            } else {
                EventQueue.invokeLater(runnable);
            }
        }
    }

    public static enum UserChoice {

        DISCARD_UNSAVED_CHANGES,
        CANCEL;
    }

    public static enum ContactPwogwammaws {

        YES, NO;
    }

    public static void visualizeErrorOnTextField(JTextField field, boolean hasError) {
        if (hasError) {
            field.setForeground(TEXT_FIELD_ERROR_COLOR);
            field.setBackground(TEXT_FIELD_BACKGROUND_ERROR_COLOR);
        } else {
            field.setForeground(TEXT_FIELD_DEFAULT_COLOR);
            field.setBackground(TEXT_FIELD_BACKGROUND_DEFAULT_COLOR);
        }
    }

    public static void centerInScreen(java.awt.Window window) {
        centerInScreen(window, window.getSize());
    }

    public static Dimension getWindowSizeAsProportionOfScreenSize(double proportionOfScreenToCover) {
        if (proportionOfScreenToCover > 1 || proportionOfScreenToCover <= 0) {
            throw new IllegalArgumentException("proportionOfScreenToCover must be within range (0, 1]");
        }
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        double proportionOfScreen = 0.75;
        Dimension windowSize = new Dimension((int) (screenDimension.width * proportionOfScreen), (int) (screenDimension.height * proportionOfScreen));
        return windowSize;
    }

    public static void centerInScreen(java.awt.Window window, Dimension windowSize) {
        Dimension screenDimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((screenDimension.width - windowSize.width) / 2.0);
        int y = (int) ((screenDimension.height - windowSize.height) / 2.0);
        window.setBounds(x, y, windowSize.width, windowSize.height);
    }

    public static void displayOutput(String fullyQualifiedPath) {
        if (fullyQualifiedPath == null) {
            throw new NullPointerException("fullyQualifiedPath must not be null.");
        }
        displayOutput(new File(fullyQualifiedPath));
    }

    public static void displayOutput(File file) {
        displayOutput(Utils.asList(file));
    }

    public static void displayOutput(Collection<File> files) {
        for (File file : files) {
            displayOutput(file.getAbsolutePath(), true);
        }
    }

    public static void displayOutput(String fullyQualifiedPath, boolean useDfm) {
        displayOutput(null, fullyQualifiedPath, useDfm);
    }

    public static void displayOutput(Component c, String fullyQualifiedPath, boolean useDfm) {
        try {
            if (false == Application.hasGui()) {
                System.out.println("Not displaying " + fullyQualifiedPath);
                return; // no-op
            }
            if (fullyQualifiedPath == null) {
                throw new NullPointerException("The path to the file to display must not be null");
            }
            File toView = new File(fullyQualifiedPath);
            if (toView.isFile()) {
                //Converts CSV & TXT Files to .xls format.
                if (Utils.endsWithIgnoreCase(fullyQualifiedPath, ".txt") || Utils.endsWithIgnoreCase(fullyQualifiedPath, ".csv")) {
                    toView = CsvToExcelFileConverter.convert(fullyQualifiedPath, CsvToExcelFileConverter.Type.XLS);
                }
//            Caveat Programmor! This will only work on Windows!
                Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL "
                        + toView.getAbsolutePath());

//            } else {
////            Caveat Programmor! This will only work on Windows!
//            Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL "
//                    + new File(fullyQualifiedPath).getAbsolutePath());
            } else {
                throw new IllegalArgumentException(fullyQualifiedPath + " is not a valid file.");
            }

        } catch (Exception e) {
            Application.reportError(c, new ErrorMessage("Unable to display file: " + fullyQualifiedPath, "Display Error", e, GuiUtils.ContactPwogwammaws.NO));
        }
    }

    public static class ErrorMessageThatClosesGui extends ErrorMessage {

        public ErrorMessageThatClosesGui(String message, String header, ContactPwogwammaws reportErrorToProgrammers) {
            super(message, header, reportErrorToProgrammers);
        }
        
        public ErrorMessageThatClosesGui(String message, String header, Throwable cause, ContactPwogwammaws reportErrorToProgrammers) {
            super(message, header, cause, reportErrorToProgrammers);
        }
    }

    public static class MultiErrorMessageForInformationPanel extends MultiErrorMessage {

        public MultiErrorMessageForInformationPanel(ErrorMessage em) {
            super(em);
        }
    }

    public static class MultiErrorMessage extends ErrorMessage {

        private Set<ErrorMessage> errors = new LinkedHashSet<ErrorMessage>();

        public MultiErrorMessage(ErrorMessage em) {
            super(em.getMessage(), em.getTitle());
            errors.add(em);
        }

        public void addError(ErrorMessage t) {
            errors.add(t);
        }

        public Set<ErrorMessage> getErrors() {
            return errors;
        }
    }

    public static class ErrorMessage extends Exception {

        private final String title;
        private final int messageType = JOptionPane.ERROR_MESSAGE;
        private ContactPwogwammaws reportErrorToProgrammers;

        public ErrorMessage(List<String> errors, String title) {
            this(new HtmlBuilder().appendLimited(errors).toString(), title);
        }
        
        public ErrorMessage(String message, String title) {
            this(message, title, null, ContactPwogwammaws.NO);
        }

        public ErrorMessage(String message, String title, ContactPwogwammaws reportErrorToProgrammers) {
            this(message, title, null, reportErrorToProgrammers);
        }

        public ErrorMessage(String message, String title, Throwable cause) {
            this(message, title, cause, ContactPwogwammaws.NO);
        }

        public ErrorMessage(String message, String title, Throwable cause, ContactPwogwammaws reportErrorToProgrammers) {
            super(message, cause);
            this.title = title;
            this.reportErrorToProgrammers = reportErrorToProgrammers;
        }

        public int getMessageType() {
            return messageType;
        }

        public String getTitle() {
            return title;
        }

        public ContactPwogwammaws reportErrorToProgrammers() {
            return reportErrorToProgrammers;
        }
    }
}
