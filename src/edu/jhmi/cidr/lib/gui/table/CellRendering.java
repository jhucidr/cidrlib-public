/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table;

import edu.jhmi.cidr.lib.gui.Changeable;
import java.awt.Color;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author cidr
 */
public interface CellRendering<T> {

    public TableCellRenderer getCellRenderer(T row);
    public static final DefaultTableCellRenderer test = new DefaultTableCellRenderer();

    public static class Rendering {

        public static final DefaultTableCellRenderer EDITABLE_CELL_RENDERER;
        public static final DefaultTableCellRenderer AVAILABLE_INFO_CELL_RENDERER;
        public static final DefaultTableCellRenderer EDITABLE_AVAILABLE_INFO_CELL_RENDERER;
        public static final Color LAVENDER = new Color(0xCCCCEE);
        public static final Color SALMON = new Color(0xEECCCC);
        public static final Color CORAL = new Color(0xCCEECC);
        public static final Color LIGHT_YELLOW = new Color(245, 242, 175);
        public static final Color ROBINZ_EGGG_BLOO = new Color(160, 200, 250);
        public static final Color DARK_GREEN = new Color(0x66CCAA);
        public static final Color LIGHT_GREEN = new Color(183, 255, 200);

        static {
            EDITABLE_CELL_RENDERER = new DefaultTableCellRenderer();
            EDITABLE_CELL_RENDERER.setBackground(ROBINZ_EGGG_BLOO);
            AVAILABLE_INFO_CELL_RENDERER = new DefaultTableCellRenderer();
            AVAILABLE_INFO_CELL_RENDERER.setBackground(SALMON);
            EDITABLE_AVAILABLE_INFO_CELL_RENDERER = new DefaultTableCellRenderer();
            EDITABLE_AVAILABLE_INFO_CELL_RENDERER.setBackground(CORAL);
        }

        public static DefaultTableCellRenderer getBackgroundColor(Color c) {
            DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
            renderer.setBackground(c);
            return renderer;
        }

        public static DefaultTableCellRenderer getValidationRenderer(boolean isValid) {
            if (isValid) {
                return getBackgroundColor(LIGHT_GREEN);
            } else {
                return getBackgroundColor(SALMON);
            }
        }

        //@Deprecated()//This is now built into the GuiTableFramework, and does not need explicit calls.
        // DRL 2013-02-11: I actually find calling this directly to be very useful.
        //                 It's useful if you want to build on this rendering.
        //                 I'm going to undeprecate this and return it to the original implementation
        public static <C extends GuiTableColumn<R>, R extends Changeable<C>> DefaultTableCellRenderer getDefaultRenderer(C column, R row) {
            if(column.getDataType().equals(Boolean.class)){
                return null;
            }
            if (row.getChanges().hasUnsavedChanges(column)) {
                return Rendering.getBackgroundColor(Rendering.LIGHT_YELLOW);
            } else if (column.getCellEditing(row).canEdit(row)) {
                return Rendering.EDITABLE_CELL_RENDERER;
            } else {
                return null;
            }
        }
    }
}
