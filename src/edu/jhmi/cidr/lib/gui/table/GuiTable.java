/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table;

import edu.jhmi.cidr.lib.HtmlUtils.HtmlBuilder;
import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.file.FileUtils;
import edu.jhmi.cidr.lib.gui.Changeable;
import edu.jhmi.cidr.lib.gui.Changeable.ChangeTracker;
import edu.jhmi.cidr.lib.gui.GuiUtils;
import edu.jhmi.cidr.lib.gui.SelectAllButtonValue;
import edu.jhmi.cidr.lib.gui.table.CellRendering.Rendering;
import edu.jhmi.cidr.lib.gui.table.GuiTableModel.AddRowsCallback;
import edu.jhmi.cidr.lib.gui.table.filter.FilterManager;
import java.awt.Component;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.*;

/**
 * NOTE: All rows (of type T) that appear in the table must be considered unique
 * according to their equals() methods (i.e., no 2 rows shown in the table can
 * be equal()). If you add the same row twice, it will only appear once. This
 * class uses a "last commit/add wins" approach.
 *
 * @author David Newcomer Jan 25, 2012
 */
public class GuiTable<T> implements Changeable<GuiTableColumn<T>> {

    private JScrollPane backingScrollPane;
    private int initialRowHeight = -1;
    private final JTable guiTable;
    private final GuiTableModel<T> model;
    private final ListenerManager<T> listenerManager;
    private final FilterManager<T> filterManager;
    private final ColumnManager<T> columnManager;
    private final List<GuiTableColumn<T>> columns;
    private SelectAllButtonValue selectAllValue = SelectAllButtonValue.SELECT_ALL;
//
    private static final long serialVersionUID = 42L;

    @SafeVarargs
    public GuiTable(GuiTableColumn<T>... columns) {
        this(Utils.asList(columns));
    }

    public GuiTable(List<? extends GuiTableColumn<T>> columns) {
        if (columns == null) {
            throw new NullPointerException("columns must not be null");
        }
        if (columns.isEmpty()) {
            throw new IllegalStateException("Must have at least one column.");
        }
        this.model = new GuiTableModel<>(this);
        this.columns = new ArrayList<>(columns);
        this.columns.add(0, new RowNumberColumn());
        this.guiTable = initializeGuiTable();
        this.filterManager = new FilterManager<>(this);
        this.listenerManager = new ListenerManager<>(this);
        this.columnManager = new ColumnManager<>(this);
        //this.packAll();
    }

    public void clearSelections() {
        guiTable.getSelectionModel().clearSelection();
    }

    public void addRowSelections(Collection<T> toSelect) {
        for (T row : toSelect) {
            int modelIndex = model.getRowIndex(row);
            int viewIndex = guiTable.convertRowIndexToView(modelIndex);
            guiTable.getSelectionModel().addSelectionInterval(viewIndex, viewIndex);
        }
    }

    public void addListenerToScrollPane(MouseListener listener) {
        if (listener == null) {
            throw new NullPointerException("listener must not be null");
        }
        listenerManager.getMouseListeners().add(listener);
        if (backingScrollPane != null) {
            backingScrollPane.addMouseListener(listener);
        }
    }

    public void updateFilters() {
        RowFilter<TableModel, Integer> toSet = filterManager.getFilter();
        TableRowSorter<?> tableRowSorter = (TableRowSorter<?>) guiTable.getRowSorter();
        tableRowSorter.setRowFilter(toSet);
    }

    public FilterManager<T> getFilterManager() {
        return filterManager;
    }

    public List<GuiTableColumn<T>> getColumns() {
        return Collections.unmodifiableList(columns);
    }

    public List<T> getSelectedRows() {
        stopCellEditing();
        List<T> selectedRows = new ArrayList<>();
        for (int row = 0; row < guiTable.getRowCount(); row++) {
            if (guiTable.isRowSelected(row)) {
                selectedRows.add(model.getRow(guiTable.convertRowIndexToModel(row)));
            }
        }
        return selectedRows;
    }

    public void clear() {
        stopCellEditing();
        model.clear();
    }

    public void addToScrollPane(JScrollPane pane) {
        if (pane == null) {
            throw new NullPointerException("pane must not be null");
        }
        listenerManager.transferScrollPaneListeners(backingScrollPane, pane);
        backingScrollPane = pane;
        allowHorizontalScrolling();
        pane.setViewportView(guiTable);
    }

    public void removeFromScrollPane() {
        clearSelections();
        clear();
        listenerManager.removeScrollPaneListeners(backingScrollPane);
        getBackingScrollPane().remove(guiTable);
    }

    public JScrollPane getBackingScrollPane() {
        return backingScrollPane;
    }

    public void setEnabled(boolean b) {
        guiTable.setEnabled(b);
        guiTable.getTableHeader().setEnabled(b);
    }

    public List<T> getRows() {
        stopCellEditing();
        return model.getRowsAsList();
    }

    public int getRowNumber(T row) {
        return model.getRowIndex(row);
    }

    /**
     * This will throw an Exception if you attempt to retrieve a cell value that
     * is absent from the Table's view.
     */
    public Object getCellValue(T row, GuiTableColumn<T> col) {
        stopCellEditing();
        int colI = columns.indexOf(col);
        int rowI = model.getRowIndex(row);
        return guiTable.getValueAt(guiTable.convertRowIndexToView(rowI), guiTable.convertColumnIndexToView(colI));
    }

    public List<T> getFilteredRows() {
        stopCellEditing();
        List<T> filteredRows = new ArrayList<>();
        for (int row = 0; row < guiTable.getRowCount(); row++) {
            filteredRows.add(model.getRow(guiTable.convertRowIndexToModel(row)));
        }
        return filteredRows;
    }

    @Deprecated // any functionality that requires the JTable, should be added to the GuiTable class
    public JTable getJTable() {
        return guiTable;
    }

    @Override
    public ChangeTracker<GuiTableColumn<T>> getChanges() {
        stopCellEditing();
        ChangeTracker<GuiTableColumn<T>> tracker = new ChangeTracker<>();
        for (T row : model.getRowsAsList()) {
            if (row instanceof Changeable) {
                Changeable ch = (Changeable) row;
                if (ch.getChanges() != null && ch.getChanges().hasUnsavedChanges()) {
                    tracker.setHasUnsavedChanges(true, getColumns());
                    return tracker;
                }
            } else {
                return new NullChangeTracker<>();
            }
        }
        return tracker;
    }

    @Deprecated //Use addPopupMenuItem() instead.
    public void addDefaultItemsToPopupMenu(JPopupMenu popup) {
        for (Component c : listenerManager.getDefaultPopupMenuItems()) {
            popup.add(c);
        }
    }

    public String selectAll(SelectAllButtonValue buttonValue) {
        if (buttonValue == null) {
            throw new NullPointerException("selectAll buttonValue must not be null");
        }
        if (buttonValue == SelectAllButtonValue.SELECT_ALL) {
            guiTable.selectAll();
        } else if (buttonValue == SelectAllButtonValue.DESELECT_ALL) {
            guiTable.clearSelection();
        }
        this.selectAllValue = buttonValue.toggle();
        return buttonValue.toggle().getText();
    }

    public boolean hasSelectedRows() {
        return getSelectedRows().isEmpty() ? false : true;
    }

    public void addPopupMenuItem(JMenuItem menuItem) {
        if (menuItem == null) {
            throw new NullPointerException("menuItem must not be null");
        }
        listenerManager.addPopupMenuItem(menuItem);
    }

    public String getContentsAsCsvFileString() {
        stopCellEditing();
        // Header:
        StringBuilder headerSb = new StringBuilder();
        List<GuiTableColumn<T>> visibleColumns = columnManager.getVisibleColumns();
        for (GuiTableColumn<T> col : visibleColumns) {
            if (headerSb.length() > 0) {
                headerSb.append(FileUtils.CSV_DELIMITER);
            }
            headerSb.append(col.getColumnHeader());
        }
        headerSb.append(FileUtils.NEW_LINE);
        // Rows:
        StringBuilder rowsSb = new StringBuilder();
        for (T row : getFilteredRows()) {
            if (rowsSb.length() > 0) {
                rowsSb.append(FileUtils.NEW_LINE);
            }
            StringBuilder rowSb = new StringBuilder();
            for (GuiTableColumn<T> col : visibleColumns) {
                if (rowSb.length() > 0) {
                    rowSb.append(FileUtils.CSV_DELIMITER);
                }
                String cellContents = String.valueOf(this.getCellValue(row, col));
                // Must convert html to plain text BEFORE regex replaceAlls
                HtmlBuilder html = new HtmlBuilder();
                html.append(cellContents);
                cellContents = html.toPlainText();
                // convert embedded newlines to semicolons, commas to spaces.
                // Note: Constants.NEWLINE will NOT work below; a regex is required.
                cellContents = cellContents.replaceAll("\\r\\n", "; ");
                cellContents = cellContents.replaceAll("\\n", "; ");
                cellContents = cellContents.replaceAll("\\r", "; ");
                cellContents = cellContents.replaceAll(",", " ");
                rowSb.append(cellContents);
            }
            rowsSb.append(rowSb);
        }
        return new StringBuilder().append(headerSb).append(rowsSb).toString();
    }

    public ColumnManager<T> getColumnManager() {
        return columnManager;
    }

    public void allowHorizontalScrolling() {
        guiTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    }

    public SelectAllButtonValue getSelectAllButtonValue() {
        return selectAllValue;
    }

    private void stopCellEditing() {
        if (guiTable != null && guiTable.isEditing()) {
            guiTable.getCellEditor().stopCellEditing();
        }
    }

    public static interface PackColumnsCallback<T> {

        public void onPack(GuiTableColumn<T> column);
    }

    public void packAll() {
        packAll(null);
    }

    public void packAll(PackColumnsCallback<T> callback) {
        packColumns(columnManager.getVisibleColumns(), callback);
    }

    public void packColumns(Collection<? extends GuiTableColumn<T>> columns) {
        packColumns(columns, null);
    }

    public void packColumns(Collection<? extends GuiTableColumn<T>> columns, PackColumnsCallback<T> callback) {
        for (GuiTableColumn<T> col : columns) {
            if (col instanceof HasPreferredColumnWidth) {
                HasPreferredColumnWidth cw = (HasPreferredColumnWidth) (col);
                if (cw.getPreferredColumnWidth() != HasPreferredColumnWidth.DONT_BOTHER) {
                    continue;
                }
            }
            if (callback != null) {
                callback.onPack(col);
            }
            packColumn(col);
        }
        GuiUtils.onEdt(new Runnable() {
            @Override
            public void run() {
                allowHorizontalScrolling();
            }
        });
    }

    /**
     * Implementation based on JXTable.
     */
    public void packColumn(final GuiTableColumn<T> column) {
        if (column == null) {
            throw new NullPointerException("column must not be null.");
        }
        GuiUtils.onEdt(new Runnable() {
            @Override
            public void run() {

                int colModelI = columns.indexOf(column);
                int width = 0;
                final TableColumn tableColumn = guiTable.getColumn(column.getColumnHeader());
                if (tableColumn == null) {
                    throw new IllegalArgumentException("Unable to get TableColumn for header " + column.getColumnHeader());
                }
                TableCellRenderer headerRenderer = tableColumn.getHeaderRenderer();
                if (headerRenderer == null) {
                    JTableHeader header = guiTable.getTableHeader();
                    if (header != null) {
                        headerRenderer = header.getDefaultRenderer();
                    }
                }
                if (headerRenderer != null) {
                    Component comp = headerRenderer.getTableCellRendererComponent(guiTable,
                            tableColumn.getHeaderValue(), false, false, 0, colModelI);
                    width = comp.getPreferredSize().width;
                }
                if (false == getFilteredRows().isEmpty()) {
                    // Must have visible rows in order for this to work:
                    TableCellRenderer renderer = null;
                    int colViewI = guiTable.convertColumnIndexToView(colModelI);
                    if (colViewI >= 0) {
                        renderer = guiTable.getCellRenderer(0, colViewI);
                    } else {
                        throw new IllegalArgumentException("Unable to pack column not in view: " + column.getColumnHeader());
                    }
                    for (int r = 0; r < guiTable.getRowCount(); r++) {
                        Component comp = guiTable.prepareRenderer(renderer, r, colViewI);
                        width = Math.max(width, comp.getPreferredSize().width);
                    }
                }
                int defaultPackMargin = 4; // TODO consider making this configurable
                width += 2 * defaultPackMargin;
                int maxWidthOfColumn = -1; // TODO consider making this configurable
                if (maxWidthOfColumn != -1 && width > maxWidthOfColumn) {
                    width = maxWidthOfColumn;
                }
                tableColumn.setPreferredWidth(width);
            }
        });
    }

    public void setRowNumberColumnVisible(boolean visible) {
        if (!visible) {
            for (GuiTableColumn<T> column : columnManager.getVisibleColumns()) {
                if (column instanceof GuiTable.RowNumberColumn) {
                    columnManager.hideColumn(column);
                }
            }
        } else {
            for (GuiTableColumn<T> column : columnManager.getHiddenColumns()) {
                if (column instanceof GuiTable.RowNumberColumn) {
                    columnManager.showColumn(column);
                }
            }
        }
    }

    private class OurSpecialJTable extends JTable {

        @Override
        public TableCellRenderer getCellRenderer(int r, int c) {
            final T row = model.getRow(convertRowIndexToModel(r));
            final GuiTableColumn<T> col = columns.get(convertColumnIndexToModel(c));
            return getCellRenderer(r, c, col, row);
        }

        private TableCellRenderer getCellRenderer(final int r, int c, final GuiTableColumn<T> col, final T row) {
            if (false == supportsCellRendering(col)) {
                return super.getCellRenderer(r, c);
            }
            if (col instanceof CellRendering) {
                @SuppressWarnings("unchecked") // safe due to above check 
                CellRendering<T> rendering = (CellRendering<T>) col;
                TableCellRenderer renderer = rendering.getCellRenderer(row);
                if (renderer != null) {
                    return renderer;
                }
            }
            DefaultTableCellRenderer defaultGuiTableRenderer = getGuiTableDefaultRenderer(col, row);
            if (defaultGuiTableRenderer != null) {
                return defaultGuiTableRenderer;
            }
            return super.getCellRenderer(r, c);
        }

        /**
         * This method will kick in when our Row is changeable. It will
         * automagically highlight editable rows & rows that have been changed.
         * However, if CellRendering is implemented in the Column subclass, the
         * implementation will be used.
         */
        private <R extends Changeable<GuiTableColumn<T>>> DefaultTableCellRenderer getGuiTableDefaultRenderer(GuiTableColumn<T> column, T row) {
            try {
                @SuppressWarnings("unchecked") // safe because it's in a try-catch.
                R myR = (R) row;
                //This is a redundant check - belt & suspenders. -JDN
                if (false == supportsCellRendering(column)) {
                    return null;
                }
                if (myR.getChanges().hasUnsavedChanges(column)) {
                    return Rendering.getBackgroundColor(Rendering.LIGHT_YELLOW);
                } else if (column.getCellEditing(row).canEdit(row)) {
                    return Rendering.EDITABLE_CELL_RENDERER;
                }
            } catch (ClassCastException ignore) {
            }
            return null;
        }

        @Override
        public TableCellEditor getCellEditor(int r, int c) {
            r = convertRowIndexToModel(r);
            c = convertColumnIndexToModel(c);
            final T row = model.getRow(r);
            final GuiTableColumn<T> col = columns.get(c);
            final CellEditing<T> strategy = col.getCellEditing(row);
            return addCellEditor(r, c, strategy, row);
        }

        @Override
        public Component prepareRenderer(TableCellRenderer renderer,
                int rowIndex, int vColIndex) {
            Component c = null;
            try {
                c = super.prepareRenderer(renderer, rowIndex, vColIndex);
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
            if (c instanceof JComponent) {
                JComponent jc = (JComponent) c;
                Object value = getValueAt(rowIndex, vColIndex);
                if (value != null) {
                    if (value.toString().length() >= 16) {
                        jc.setToolTipText(value.toString());
                    } else {
                        jc.setToolTipText(null);
                    }
                }
            }
            return c;
        }

        private TableCellEditor addCellEditor(final int r, int c, final CellEditing<T> strategy, final T row) {
            TableCellEditor tempEditor = strategy.getCellEditor(row);
            boolean usingColumnEditor = false;
            if (tempEditor == null) {
                tempEditor = super.getCellEditor(convertRowIndexToView(r), convertColumnIndexToView(c));
                usingColumnEditor = true;
            }
            final TableCellEditor editor = tempEditor;
            CellEditorListener listener = new CellEditorListener() {
                @Override
                public void editingStopped(ChangeEvent e) {
                    strategy.notifyEditingStopped(editor.getCellEditorValue(), model.getRow(r));
                }

                @Override
                public void editingCanceled(ChangeEvent e) {
                    strategy.notifyEditingCancelled(editor.getCellEditorValue(), model.getRow(r));
                }
            };
            if (usingColumnEditor) {
                if (editor instanceof AbstractCellEditor) {
                    AbstractCellEditor ace = (AbstractCellEditor) editor;
                    for (CellEditorListener cel : ace.getCellEditorListeners()) {
                        ace.removeCellEditorListener(cel);
                    }
                }
            }
            editor.addCellEditorListener(listener);
            return editor;
        }

        private boolean supportsCellRendering(GuiTableColumn<T> col) {
            if (col.getDataType().equals(Boolean.class)) {
                return false;
            } else {
                return true;
            }
        }
    }

    // called from constructor
    private JTable initializeGuiTable() {
        JTable t = new OurSpecialJTable();
        t.setModel(model);
        t.addMouseListener(new MouseAdapterOsAgnostic());
        t.setAutoCreateRowSorter(true);
        resizeColumns(t);
        return t;
    }

    private void resizeColumns(JTable jt) {
        TableColumnModel columnModel = jt.getColumnModel();
        int columnIndex = 0;
        for (GuiTableColumn<T> column : columns) {
            if (column instanceof HasPreferredColumnWidth) {
                HasPreferredColumnWidth adjustableColumn = (HasPreferredColumnWidth) column;
                int columnWidth = adjustableColumn.getPreferredColumnWidth();
                if (columnWidth != HasPreferredColumnWidth.DONT_BOTHER) {
                    columnModel.getColumn(columnIndex).setPreferredWidth(columnWidth);
                }
            } else {
                //Set the column width to a value between 75 & 250, related to the length of the header.
                int columnWidth = column.getColumnHeader().length() * 16;
                if (columnWidth < 75) {
                    columnWidth = 75;
                } else if (columnWidth > 250) {
                    columnWidth = 250;
                }
                columnModel.getColumn(columnIndex).setPreferredWidth(columnWidth);
            }
            columnIndex++;
        }
    }

//    public void setTextBeforeTableShown(String text);
    class AddRowsCallbackOnLoad implements AddRowsCallback<T> {

        private final JLabel label;
        private final String labelText;

        public AddRowsCallbackOnLoad(JLabel label, int numRows) {
            this.label = label;
            this.labelText = "Now loading row %d of " + numRows + "...";
        }

        @Override
        public void onAddRow(T row, int rowNum) {
            label.setText(String.format(labelText, rowNum));
        }

        @Override
        public void onCompletion() {
            label.setText("Preparing table data for display...");
        }
    }

    public void load(JScrollPane pane, final List<T> rows) {
        final JLabel label = new JLabel();
        pane.setViewportView(label);
        label.setHorizontalAlignment(JLabel.CENTER);

        model.addRows(rows, new AddRowsCallbackOnLoad(label, rows.size()));

        String labelText = "Now adjusting height for row %d of " + rows.size() + "...";
        int rowNum = 1;
        for (T row : rows) {
            label.setText(String.format(labelText, rowNum));
            adjustRowHeightIfNecessary(row);
            rowNum++;
        }

        packAll(new PackColumnsCallback<T>() {
            @Override
            public void onPack(GuiTableColumn<T> column) {
                label.setText("Now adjusting '" + column.getColumnHeader() + "' column width...");
            }
        });

        label.setText("Displaying table...");
        addToScrollPane(pane);
    }

    public void addRow(T row) {
        model.addRow(row);
        adjustRowHeightIfNecessary(row);
    }

    public T getRow(int rowNumber) {
        return model.getRow(rowNumber);
    }

    public void addListener(GuiTableListener<T> listener) {
        for (int i = 0; i < columns.size(); i++) {
            GuiTableColumn<T> column = columns.get(i);
            listenerManager.addListener(column, listener);
        }
    }

    public void addListener(GuiTableColumn<T> column, GuiTableListener<T> listener) {
        listenerManager.addListener(column, listener);
    }

    /**
     * This requires that the T dataSource uniquely represent a row, as it uses
     * the .equals() method to find the old version. It also requires the row to
     * already be in the table.
     */
    @SafeVarargs
    public final void updateRow(T row, GuiTableColumn<T> firstColumn, GuiTableColumn<T>... otherColumns) {
        if (row == null) {
            throw new NullPointerException("dataSource must not be null");
        }
        if (firstColumn == null) {
            throw new NullPointerException("firstColumn must not be null");
        }
        List<GuiTableColumn<T>> columns = new ArrayList<>();
        columns.add(firstColumn);
        if (otherColumns != null) {
            columns.addAll(Arrays.asList(otherColumns));
        }
        updateRow(row, columns);
    }

    public void updateRow(T row, GuiTableColumn<T>[] columns) {
        if (row == null) {
            throw new NullPointerException("dataSource must not be null");
        }
        if (columns == null) {
            throw new NullPointerException("columns must not be null");
        }
        updateRow(row, Arrays.asList(columns));
    }

    public void updateRow(T row, Collection<? extends GuiTableColumn<T>> columns) {
        if (row == null) {
            throw new NullPointerException("dataSource must not be null");
        }
        if (columns == null) {
            throw new NullPointerException("columns must not be null");
        }
        model.updateRow(row, columns);
        adjustRowHeightIfNecessary(row);
    }

    private void adjustRowHeightIfNecessary(T row) {
        /*
         * There is a potential race condition here if we were to over
         * remove rows and add them to the end of the table model. We don't
         * ever do this, so the below should be fine.
         */
        final int rowIndex = guiTable.convertRowIndexToView(model.getRowIndex(row));

        if (rowIndex < 0) {
            throw new IllegalStateException("unable to adjust row height for non-existent row: " + row);
        }
        if (row instanceof HasAdjustableHeight) {
            HasAdjustableHeight hah = (HasAdjustableHeight) row;
            if (initialRowHeight == -1) {
                // To avoid expanding row height with every update.
                // Could be a static final, but we don't know if
                // that will work with varying screen metrics.
                initialRowHeight = guiTable.getRowHeight(rowIndex);
            }
            guiTable.setRowHeight(rowIndex, initialRowHeight * hah.getHeightMultiplier());

        }
    }

    public void removeRow(T row) {
        model.removeRow(row);
    }

    /**
     * Used by filter manager.
     */
    public static <T> GuiTableModel<T> getGuiTableModel(TableModel model) {
        if (model instanceof TableSorter) {
            model = ((TableSorter) model).getTableModel();
        }
        if (model instanceof GuiTableModel) {
            @SuppressWarnings("unchecked")//This is safe because we are creating the model and putting it into the table.
            GuiTableModel<T> retVal = (GuiTableModel<T>) model;
            return retVal;
        } else {
            if (model == null) {
                throw new NullPointerException("model must not be null");
            } else {
                throw new IllegalArgumentException("Invalid model found for jTable "//
                        + ".  Model found: " + model.getClass().getName() + ".");
            }
        }
    }

    // tagging interface
    public static interface GuiTableListener<T> {
    }

    public static interface GuiTableDoubleClickListener<T> extends GuiTableListener<T> {

        public void doubleClick(T row, GuiTableColumn<T> column);
    }

    public static interface GuiTablePopupListener<T> extends GuiTableListener<T> {

        public JPopupMenu getPopupMenu(T dataSource, GuiTableColumn<T> column);
    }

    private class MouseAdapterOsAgnostic extends MouseAdapter {

        private RowColumnTuple<T> getRowColumnTuple(MouseEvent e) {
            JTable source = (JTable) e.getSource();
            int rIdx = source.rowAtPoint(e.getPoint());
            if (rIdx < 0) {
                // no valid selection
                return null;
            }
            int cIdx = source.columnAtPoint(e.getPoint());
            if (cIdx < 0) {
                // no valid selection
                return null;
            }
            if (false == source.isRowSelected(rIdx)) {
                source.changeSelection(rIdx, cIdx, false, false);
            }
            rIdx = guiTable.convertRowIndexToModel(rIdx);
            cIdx = guiTable.convertColumnIndexToModel(cIdx);
            T row = getRow(rIdx);
            if (cIdx >= columns.size()) {
                throw new IllegalStateException("column index "
                        + cIdx + " exceeds number of columns");
            } else {
                GuiTableColumn<T> column = columns.get(cIdx);
                return new RowColumnTuple<>(row, column);
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            if (guiTable.isEnabled()) {
                RowColumnTuple<T> tuple = getRowColumnTuple(e);
                if (tuple == null) {
                    return;
                }
                popup(e, tuple);
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (guiTable.isEnabled()) {
                RowColumnTuple<T> tuple = getRowColumnTuple(e);
                if (tuple == null) {
                    return;
                }
                if (e.getButton() == MouseEvent.BUTTON1) {
                    if (e.getClickCount() == 2) {
                        // double-click with left mouse button
                        doubleClick(e, tuple);
                    }
                }
                popup(e, tuple);
            }
        }

        /**
         * Different platforms have different implementations of
         * isPopupTrigger(). By calling this method in both mouseClicked &
         * mouseReleased, we make the GUI OS-Agnostic.
         */
        private void popup(MouseEvent e, RowColumnTuple<T> tuple) {
            if (false == e.isPopupTrigger()) {
                return;
            }
            List<GuiTablePopupListener<T>> listeners = listenerManager.getPopupListeners(tuple.getRow(), tuple.getColumn());
            boolean popupMenuFound = false;
            for (GuiTablePopupListener<T> listener : listeners) {
                JPopupMenu popupMenu = listener.getPopupMenu(tuple.getRow(), tuple.getColumn());
                if (popupMenu != null) {
                    popupMenuFound = true;
                    popupMenu.show(e.getComponent(), e.getX(), e.getY());
                    break;
                }
            }
            if (false == popupMenuFound) {
                JPopupMenu managerPopupMenu = listenerManager.getPopupMenu(tuple);
                managerPopupMenu.show(e.getComponent(), e.getX(), e.getY());
            }
        }

        private void doubleClick(MouseEvent e, RowColumnTuple<T> tuple) {
            List<GuiTableDoubleClickListener<T>> listeners = listenerManager.getDoubleClickListeners(tuple.getRow(), tuple.getColumn());
            for (GuiTableDoubleClickListener<T> listener : listeners) {
                listener.doubleClick(tuple.getRow(), tuple.getColumn());
            }
        }
    }

    public class RowNumberColumn implements GuiTableColumn<T> {

        @Override
        public String getColumnHeader() {
            return "Row #";
        }

        @Override
        public Class getDataType() {
            return Integer.class;
        }

        @Override
        public Object getObject(T row) {
            return getRowNumber(row) + 1;
        }

        @Override
        public CellEditing<T> getCellEditing(T t) {
            return NO_OP;
        }
        private final CellEditing<T> NO_OP = new CellEditing.NoOp<>();
    }
}
