/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table.filter;

import edu.jhmi.cidr.lib.codegen.enumgen.EnumNameConverter;
import edu.jhmi.cidr.lib.gui.table.CellEditing;
import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;
import javax.swing.table.TableCellEditor;

/**
 *
 * @author David Newcomer Jun 12, 2012
 */
// package access
abstract class FilterEditingStrategy<T> implements CellEditing<FilterRow<T>> {

    private static class ACTIVE<T> extends FilterEditingStrategy<T> {

        @Override
        public void notifyEditingStopped(Object o, FilterRow<T> row) {
            row.getFilterEntry().setIsActive((Boolean) o);
            row.getTable().getTableToFilter().updateFilters();
        }
    }

    private static class NOT<T> extends FilterEditingStrategy<T> {

        @Override
        public void notifyEditingStopped(Object o, FilterRow<T> row) {
            row.getFilterEntry().setNot((Boolean) o);
            row.getTable().getTableToFilter().updateFilters();
        }
    }

    private static class NO_OP<T> extends FilterEditingStrategy<T> {

        @Override
        public void notifyEditingStopped(Object o, FilterRow<T> row) {
            // duh, see name of class
        }
    }

    
//    public static final CellEditing<FilterRow<?,?>> NO_OP = new CellEditing.NoOp<FilterRow<?,?>>();
    public static <T> ACTIVE<T> ACTIVE() {
        return new ACTIVE<T>();
    }

    public static <T> NO_OP<T> NO_OP() {
        return new NO_OP<T>();
    }

    public static <T> NOT<T> NOT() {
        return new NOT<T>();
    }

    @Override
    public boolean canEdit(FilterRow<T> row) {
        return true;
    }

    @Override
    public TableCellEditor getCellEditor(FilterRow<T> row) {
        return null;
    }

    @Override
    public void notifyEditingCancelled(Object o, FilterRow<T> row) {
        // no-op                                                         
    }

    @Override
    public String toString() {
        return EnumNameConverter.translateEnumNameIntoHumanReadableValue(super.toString());
    }
}
