/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib.gui.table.filter;

import edu.jhmi.cidr.lib.gui.GuiUtils;
import edu.jhmi.cidr.lib.gui.PopupMenuItem;
import edu.jhmi.cidr.lib.gui.table.GuiTable;
import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

/**
 *
 * @author David Newcomer
 */
public class FilterManagerPanel<R> extends javax.swing.JPanel {

    private final FilterTable<R> filterTable;
    private final GuiTable<R> tableToFilter;
    private final FilterManagerPopupMenu popUpMenu;
    private final JScrollPane paneUponWhichThisPanelIsSitting;

    /**
     * Creates new form FilterManagerPanel
     */
    public FilterManagerPanel(GuiTable<R> tableToFilter, JScrollPane paneUponWhichThisPanelIsSitting) {
        if (tableToFilter == null) {
            throw new NullPointerException("tableToFilter must not be null");
        }
        if (paneUponWhichThisPanelIsSitting == null) {
            throw new NullPointerException("paneUponWhichThisPanelIsSitting must not be null");
        }
        initComponents();
        List<FilterColumn<R>> filterColumns = FilterColumn.buildColumnList();
        this.tableToFilter = tableToFilter;
        this.filterTable = new FilterTable<R>(tableToFilter, filterColumns);
        this.popUpMenu = new FilterManagerPopupMenu();
        this.paneUponWhichThisPanelIsSitting = paneUponWhichThisPanelIsSitting;
        customInitComponents();
    }

    private void customInitComponents() {
        filterTable.addToScrollPane(filterManagerScrollPane);
        MyListener l = new MyListener();
        filterTable.addListenerToScrollPane(new RightClickListener());
        filterTable.addListener(l);
        synchRows();
        tableToFilter.addToScrollPane(dataScrollPane);
        jSplitPane1.setDividerLocation(paneUponWhichThisPanelIsSitting.getHeight() / 4);
        paneUponWhichThisPanelIsSitting.setViewportView(this);
    }

    public JScrollPane getPaneUponWhichThisPanelIsSitting() {
        return paneUponWhichThisPanelIsSitting;
    }

    private class MyListener implements GuiTable.GuiTablePopupListener<FilterRow<R>> {

        @Override
        public JPopupMenu getPopupMenu(FilterRow<R> row, GuiTableColumn<FilterRow<R>> column) {
            return popUpMenu;
        }
    }

    private class FilterManagerPopupMenu extends JPopupMenu {

        public FilterManagerPopupMenu() {
            super();
            add(new CreateNumberFilter());
            add(new CreateTextFilter());
            add(new CreateDateFilter());
            add(new DeleteFilter());
            add(new HideFilterManager());
        }
    }

    private class CreateNumberFilter extends PopupMenuItem {

        public CreateNumberFilter() {
            super("Filter Numbers");
        }

        @Override
        public void performAction(ActionEvent e) {
            GuiUtils.launchDialog(new NumberFilterDialog<R>(null, tableToFilter));
            synchRows();
        }
    }

    private class CreateDateFilter extends PopupMenuItem {

        public CreateDateFilter() {
            super("Filter Dates");
        }

        @Override
        public void performAction(ActionEvent e) {
            GuiUtils.launchDialog(new DateFilterDialog<R>(null, tableToFilter));
            synchRows();
        }
    }

    private class HideFilterManager extends PopupMenuItem {

        public HideFilterManager() {
            super("Hide Filter Manager");
        }

        @Override
        public void performAction(ActionEvent e) {
            tableToFilter.getFilterManager().setFilterManagerVisible(false);
        }
    }

    private class DeleteFilter extends PopupMenuItem {

        public DeleteFilter() {
            super("Delete Selected Filter(s)");
        }

        @Override
        public void performAction(ActionEvent e) {
            for (FilterRow<R> row : filterTable.getSelectedRows()) {
                tableToFilter.getFilterManager().removeEntry(row.getFilterEntry());
            }
            synchRows();
        }
    }

    private class CreateTextFilter extends PopupMenuItem {

        public CreateTextFilter() {
            super("Filter Text");
        }

        @Override
        public void performAction(ActionEvent e) {
            GuiUtils.launchDialog(new TextFilterDialog<R>(null, tableToFilter));
            synchRows();
        }
    }

    /**
     * This method makes the rows in the filterTable reflect the values found in
     * the tableToFilter's FilterManager.
     */
    /*package access*/ void synchRows() {

        List<FilterRow<R>> rowsInFilterTable = filterTable.getRows();
        List<FilterEntry<R>> filtersInFilterManager = tableToFilter.getFilterManager().getEntries();
        for (FilterEntry<R> filterEntry : filtersInFilterManager) {
            FilterRow<R> toTest = new FilterRow<R>(filterEntry, filterTable);
            if (false == rowsInFilterTable.contains(toTest)) {
                filterTable.addRow(toTest);
            }
        }
        for (FilterRow<?> filterRow : rowsInFilterTable) {
            @SuppressWarnings("unchecked") //We checked, and said it was okay.
            FilterRow<R> row = (FilterRow<R>) filterRow;
            if (false == filtersInFilterManager.contains(row.getFilterEntry())) {
                filterTable.removeRow(row);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        filterManagerScrollPane = new javax.swing.JScrollPane();
        dataScrollPane = new javax.swing.JScrollPane();

        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        filterManagerScrollPane.setBackground(new java.awt.Color(0, 153, 0));
        jSplitPane1.setTopComponent(filterManagerScrollPane);
        jSplitPane1.setRightComponent(dataScrollPane);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane dataScrollPane;
    private javax.swing.JScrollPane filterManagerScrollPane;
    private javax.swing.JSplitPane jSplitPane1;
    // End of variables declaration//GEN-END:variables

    private class RightClickListener extends MouseAdapter {

        @Override
        public void mouseReleased(MouseEvent e) {
            if (e.isPopupTrigger()) {
                popUpMenu.show(e.getComponent(), e.getX(), e.getY());
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            mouseReleased(e);
        }
    }
}
