/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table.filter;

import edu.jhmi.cidr.lib.DateFormatter;
import edu.jhmi.cidr.lib.codegen.enumgen.EnumNameConverter;
import edu.jhmi.cidr.lib.gui.table.GuiTable;
import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;
import edu.jhmi.cidr.lib.gui.table.GuiTableModel;
import java.util.Date;
import javax.swing.RowFilter;
import javax.swing.table.TableModel;

/**
 *
 * @author David Newcomer Jun 14, 2012
 */
public class DateMatchingFilter<T> extends RowFilter<TableModel, Integer> implements FilterManager.HasFilterEntryType {

    private final FilterEntry<T> filterEntry;
    private final ComparisonType comparisonType;
    private final long time;

    public DateMatchingFilter(FilterEntry<T> filterEntry, ComparisonType comparisonType, long time) {
        if (filterEntry == null) {
            throw new NullPointerException("filterEntry must not be null");
        }
        if (comparisonType == null) {
            throw new NullPointerException("comparisonType must not be null");
        }
     
        this.filterEntry = filterEntry;
        this.comparisonType = comparisonType;
        this.time = time;
    }
    
    @Override
    public boolean include(RowFilter.Entry<? extends TableModel, ? extends Integer> entry) {
        GuiTableModel<T> tableModel = GuiTable.getGuiTableModel(entry.getModel());
        int columnIndex = tableModel.getColumnIndex(filterEntry.getColumn());
        String columnValue = entry.getStringValue(columnIndex);
        Date dateValue = DateFormatter.parseDate(String.valueOf(columnValue));
        if (dateValue != null) {
            long value = dateValue.getTime();
               switch (comparisonType) {
                case AFTER:
                    return time < value;
                case BEFORE:
                    return time > value;
                case EQUAL:
                    return time == value;
                case NOT_EQUAL:
                    return time != value;
                default:
                    throw new IllegalStateException("Unrecognized comparison type:" + comparisonType);
            }
        } else {
            return false;
        }
    }

    @Override
    public FilterManager.FilterEntryType getType() {
        return FilterManager.FilterEntryType.DATE;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("values ");
        sb.append(EnumNameConverter.translateEnumNameIntoHumanReadableValue(comparisonType.toString()));
        sb.append(" ");
        sb.append(DateFormatter.getFormattedDate(time));
        return sb.toString();
    }
}
