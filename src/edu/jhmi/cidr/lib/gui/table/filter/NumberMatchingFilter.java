/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table.filter;

import edu.jhmi.cidr.lib.codegen.enumgen.EnumNameConverter;
import edu.jhmi.cidr.lib.gui.table.GuiTable;
import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;
import edu.jhmi.cidr.lib.gui.table.GuiTableModel;
import javax.swing.RowFilter;
import javax.swing.table.TableModel;

/**
 *
 * @author David Newcomer Jun 14, 2012
 */
public class NumberMatchingFilter<T> extends RowFilter<TableModel, Integer> implements FilterManager.HasFilterEntryType {

    private final FilterEntry<T> filterEntry;
    private final ComparisonType comparisonType;
    private final Number number;

    public NumberMatchingFilter(FilterEntry<T> filterEntry, ComparisonType comparisonType, Number number) {
        if (filterEntry == null) {
            throw new NullPointerException("filterEntry must not be null");
        }
        if (comparisonType == null) {
            throw new NullPointerException("comparisonType must not be null");
        }
        if (number == null) {
            throw new NullPointerException("number must not be null");
        }
        this.filterEntry = filterEntry;
        this.comparisonType = comparisonType;
        this.number = number;
    }

    @Override
    public boolean include(RowFilter.Entry<? extends TableModel, ? extends Integer> entry) {
        GuiTableModel<T> tableModel = GuiTable.getGuiTableModel(entry.getModel());
        int columnIndex = tableModel.getColumnIndex(filterEntry.getColumn());
        String columnValue = entry.getStringValue(columnIndex);
        try {
            double value = Double.parseDouble(String.valueOf(columnValue));
            switch (comparisonType) {
                case AFTER:
                    return number.doubleValue() < value;
                case BEFORE:
                    return number.doubleValue() > value;
                case EQUAL:
                    return number.doubleValue() == value;
                case NOT_EQUAL:
                    return number.doubleValue() != value;
                default:
                    throw new IllegalStateException("Unrecognized comparison type:" + comparisonType);
            }
        } catch (NumberFormatException ignore) {
            return false;
        }
    }

    @Override
    public FilterManager.FilterEntryType getType() {
        return FilterManager.FilterEntryType.NUMERICAL;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("values ");
        sb.append(EnumNameConverter.translateEnumNameIntoHumanReadableValue(comparisonType.toString()));
        sb.append(" ");
        sb.append(number);
        return sb.toString();
    }
}
