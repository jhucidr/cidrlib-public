/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table.filter;

import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.RowFilter;
import javax.swing.table.TableModel;

/**
 *
 * @author David Newcomer Jun 14, 2012
 */
public class FilterEntry<T> {

    private final List<String> values;
    private final GuiTableColumn<T> column;
    boolean isActive;
    private RowFilter<TableModel, Integer> myFilter;
    private boolean not;
    private FilterManager.FilterEntryType type;

    public FilterEntry(GuiTableColumn<T> column) {
        if (column == null) {
            throw new NullPointerException("columns must not be null");
        }
        this.column = column;
        this.values = new ArrayList<String>();
        this.isActive = true;
        this.not = false;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(Collection<String> toSet) {
        this.values.clear();
        this.values.addAll(toSet);
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public GuiTableColumn<T> getColumn() {
        return column;
    }

    public <F extends RowFilter<TableModel, Integer> & FilterManager.HasFilterEntryType> void setFilter(F myFilter) {
        this.myFilter = myFilter;
        this.type = myFilter.getType();
    }

    public RowFilter<TableModel, Integer> getFilter() {
        return myFilter;
    }

    public void setNot(boolean not) {
        this.not = not;
    }

    public boolean isNot() {
        return not;
    }

    public FilterManager.FilterEntryType getType() {
        return type;
    }
}
