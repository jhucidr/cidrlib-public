/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table.filter;

import edu.jhmi.cidr.lib.codegen.enumgen.EnumNameConverter;
import edu.jhmi.cidr.lib.gui.table.GuiTable;
import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;
import java.util.ArrayList;
import java.util.List;
import javax.swing.RowFilter;
import javax.swing.table.TableModel;

/**
 * T is the Row K is the Column M is the GuiTableModel
 *
 * @author David Newcomer Jun 11, 2012
 */
public class FilterManager<T> {

    private final List<FilterEntry<T>> entries;
    private final GuiTable<T> table;
    private FilterManagerPanel<T> filterManagerPanel;

    public FilterManager(GuiTable<T> table) {
        this.entries = new ArrayList<FilterEntry<T>>();
        this.table = table;
    }

    public void addEntry(FilterEntry<T> toAdd) {
        if (toAdd == null) {
            throw new NullPointerException("toAdd must not be null");
        }
        this.entries.add(toAdd);
        table.updateFilters();
        // To keep filter table in synch with programmatic filtering (DRL 2/5/13 for Sample QC Evaluation)
        if (isVisible()) {
            // TODO now that this has been added, their may be redundant calls within the filter dialogs.
            filterManagerPanel.synchRows();
        }
    }

    public RowFilter<TableModel, Integer> getFilter() {
        List<RowFilter<TableModel, Integer>> rowFilters = new ArrayList<RowFilter<TableModel, Integer>>();
        for (FilterEntry<T> filterEntry : entries) {
            if (filterEntry.isActive) {
                RowFilter<TableModel, Integer> filter = filterEntry.getFilter();
                if (filter != null) {
                    if (filterEntry.isNot()) {
                        filter = RowFilter.notFilter(filter);
                    }
                    rowFilters.add(filter);
                }
            }
        }
        return RowFilter.andFilter(rowFilters);
    }

    public void clearFilters() {
        for (FilterEntry<T> filterEntry : entries) {
            filterEntry.setIsActive(false);
        }
        table.updateFilters();
    }

    public List<FilterEntry<T>> getEntries() {
        return entries;
    }

    public void removeEntry(FilterEntry<T> filterEntry) {
        entries.remove(filterEntry);
        table.updateFilters();
    }

    public boolean isVisible() {
        return filterManagerPanel != null;
    }

    public void setFilterManagerVisible(boolean visible) {
        if (visible) {
            if (filterManagerPanel == null) {
                filterManagerPanel = new FilterManagerPanel<T>(table, table.getBackingScrollPane());
            }
        } else {
            table.addToScrollPane(filterManagerPanel.getPaneUponWhichThisPanelIsSitting());
            filterManagerPanel = null;
        }
    }

    public static enum FilterEntryType {

        TEXT, REGEX, NUMERICAL, DATE;

        @Override
        public String toString() {
            return EnumNameConverter.translateEnumNameIntoHumanReadableValue(super.toString());
        }
    }

    public static interface HasFilterEntryType {

        public FilterEntryType getType();
    }
}
