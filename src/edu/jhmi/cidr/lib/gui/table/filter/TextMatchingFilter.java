/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table.filter;

import edu.jhmi.cidr.lib.gui.table.GuiTable;
import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;
import edu.jhmi.cidr.lib.gui.table.GuiTableModel;
import javax.swing.RowFilter;
import javax.swing.table.TableModel;

/**
 *
 * @author David Newcomer Jun 14, 2012
 */
public class TextMatchingFilter<T> extends RowFilter<TableModel, Integer> implements FilterManager.HasFilterEntryType {

    private final FilterEntry<T> filterEntry;

    public TextMatchingFilter(FilterEntry<T> filterEntry) {
        if (filterEntry == null) {
            throw new NullPointerException("filterEntry must not be null");
        }
        this.filterEntry = filterEntry;
    }

    @Override
    public boolean include(RowFilter.Entry<? extends TableModel, ? extends Integer> entry) {
        //            M model = entry.getModel();
        //            K column = filterEntry.getColumn();
        //            T row = model.getRow(entry.getIdentifier());
        //            String value = String.valueOf(column.getObject(row));
        GuiTableModel<T> model = GuiTable.getGuiTableModel(entry.getModel());
        int columnIndex = model.getColumnIndex(filterEntry.getColumn());
        String value = entry.getStringValue(columnIndex);
        return filterEntry.getValues().contains(value);
    }

    @Override
    public FilterManager.FilterEntryType getType() {
        return FilterManager.FilterEntryType.TEXT;
    }
}
