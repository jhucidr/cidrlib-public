/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table.filter;

import edu.jhmi.cidr.lib.gui.table.CellEditing;
import edu.jhmi.cidr.lib.gui.table.CellRendering;
import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;
import edu.jhmi.cidr.lib.gui.table.HasPreferredColumnWidth;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.TableCellRenderer;

/**
 * @author David Newcomer Jun 12, 2012
 */
// package access
abstract class FilterColumn<T> implements GuiTableColumn<FilterRow<T>>, HasPreferredColumnWidth, CellRendering<FilterRow<T>> {

    public static <T> List<FilterColumn<T>> buildColumnList() {
        List<FilterColumn<T>> filterColumns = new ArrayList<FilterColumn<T>>();
        FilterColumn<T> noOp = new NO_OP<T>();
        filterColumns.add(noOp.new ACTIVE());
        filterColumns.add(noOp.new COLUMN());
        filterColumns.add(noOp.new FILTER_TYPE());
        filterColumns.add(noOp.new NOT());
        filterColumns.add(noOp.new VALUES());
        return filterColumns;
    }

    private class ACTIVE extends FilterColumn<T> {

        private ACTIVE() {
            super("Active", Boolean.class);
        }

        @Override
        public Object getObject(FilterRow<T> row) {
            return row.getFilterEntry().isActive();
        }

        @Override
        public int getPreferredColumnWidth() {
            return 60;
        }

        @Override
        public CellEditing<FilterRow<T>> getCellEditing(FilterRow<T> row) {
            return FilterEditingStrategy.ACTIVE();
        }
    }

    private class COLUMN extends FilterColumn<T> {

        private COLUMN() {
            super("Column", Object.class);
        }

        @Override
        public Object getObject(FilterRow<T> row) {
            return row.getFilterEntry().getColumn().getColumnHeader();
        }

        @Override
        public int getPreferredColumnWidth() {
            return 80;
        }
    }

    private class FILTER_TYPE extends FilterColumn<T> {

        private FILTER_TYPE() {
            super("Type", Object.class);
        }

        @Override
        public Object getObject(FilterRow<T> row) {

            return row.getFilterEntry().getType();
        }

        @Override
        public int getPreferredColumnWidth() {
            return 65;
        }
    }

    private class NOT extends FilterColumn<T> {

        private NOT() {
            super("Not", Boolean.class);
        }

        @Override
        public Object getObject(FilterRow<T> row) {
            return row.getFilterEntry().isNot();
        }

        @Override
        public int getPreferredColumnWidth() {
            return 35;
        }

        @Override
        public CellEditing<FilterRow<T>> getCellEditing(FilterRow<T> row) {
            return FilterEditingStrategy.NOT();
        }
    }

    private class VALUES extends FilterColumn<T> {

        private VALUES() {
            super("Filter Criteria", Object.class);
        }

        @Override
        public Object getObject(FilterRow<T> row) {
            switch (row.getFilterEntry().getType()) {
                case TEXT:
                    return "values matching " + row.getFilterEntry().getValues();
                case NUMERICAL:
                    
                case DATE:
                default:
                    return row.getFilterEntry().getFilter().toString();
            }
        }

        @Override
        public int getPreferredColumnWidth() {
            return 1000;
        }
    }
    private final String columnHeader;
    private final Class dataType;

    private FilterColumn(String columnHeader, Class dataType) {
        this.columnHeader = columnHeader;
        this.dataType = dataType;
    }

    @Override
    public String getColumnHeader() {
        return columnHeader;
    }

    @Override
    public Class getDataType() {
        return dataType;
    }

    @Override
    public CellEditing<FilterRow<T>> getCellEditing(FilterRow<T> row) {
        return FilterEditingStrategy.NO_OP();
    }

    @Override
    public String toString() {
        return columnHeader;
    }

    private static class NO_OP<T> extends FilterColumn<T> {

        public NO_OP() {
            super(null, null);
        }

        @Override
        public Object getObject(FilterRow<T> row) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public int getPreferredColumnWidth() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    @Override
    public TableCellRenderer getCellRenderer(FilterRow<T> dataSource) {
        return Rendering.getBackgroundColor(Rendering.DARK_GREEN);
    }
}
