/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table.filter;

import edu.jhmi.cidr.lib.gui.Changeable;
import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;
import edu.jhmi.cidr.lib.gui.table.HasAdjustableHeight;

/*
 * @author David Newcomer Jun 12, 2012
 */
// package access
class FilterRow<T> implements HasAdjustableHeight, Changeable<FilterColumn> {

    private final FilterTable<T> table;
    private final ChangeTracker<FilterColumn> changeTracker;
    private final FilterEntry<T> filterEntry;

    public FilterRow(FilterEntry<T> dataSource, FilterTable<T> table) {
        if (dataSource == null) {
            throw new NullPointerException("dataSource must not be null");
        }
        if (table == null) {
            throw new NullPointerException("table must not be null");
        }
        this.filterEntry = dataSource;
        this.table = table;
        this.changeTracker = new ChangeTracker<FilterColumn>();
    }

    public FilterTable<T> getTable() {
        return table;
    }

    public FilterEntry<T> getFilterEntry() {
        return filterEntry;
    }

    @Override
    public int getHeightMultiplier() {
        return 1;
    }

    @Override
    public ChangeTracker<FilterColumn> getChanges() {
        return changeTracker;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof FilterRow) {
            FilterRow<?> that = (FilterRow<?>) o;
            return this.getFilterEntry().equals(that.getFilterEntry());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getFilterEntry().hashCode();
    }
}
