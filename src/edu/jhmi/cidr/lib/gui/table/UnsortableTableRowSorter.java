/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table;

import java.util.Comparator;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 * Stoled in the night from the DFM codebase
 * @author sgriffit
 */
public class UnsortableTableRowSorter<M extends TableModel> extends TableRowSorter<M> {

    private boolean sortable = false;

    public UnsortableTableRowSorter(M model) {
        super(model);
        for (int col = 0; col < getModel().getColumnCount(); col++) {
            setComparator(col, new UnsortableComparator());
        }
    }

    @Override
    public boolean isSortable(int column) {
        return sortable ? super.isSortable(column) : false;
    }

    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }

    public static class UnsortableComparator implements Comparator<Object> {

        @Override
        public int compare(Object o1, Object o2) {
            if ((o1 instanceof UnsortableString) || o2 instanceof UnsortableString) {
                return 0;
            } else {
                return o1.toString().compareTo(o2.toString());
            }
        }
    }

    public static class UnsortableString implements Comparable<String> {

        private final String cargo;

        public UnsortableString(String cargo) {
            this.cargo = cargo;
        }

        public String getCargo() {
            return cargo;
        }

        @Override
        public String toString() {
            return cargo.toString();
        }

        @Override
        public int compareTo(String o) {
            // maintain position
            return 0;
        }
    }
}

