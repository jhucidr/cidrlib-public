/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table;

import edu.jhmi.cidr.lib.IndexedLookupSet;
import java.util.Collection;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

/**
 * Modified to use AbstractTableModel (DRL & JDN - July 2, 2013).
 *
 * @author David Newcomer Jan 25, 2012
 */
public class GuiTableModel<T> extends AbstractTableModel {
    
    private final GuiTable<T> table;
    private final IndexedLookupSet<T> cache;

    public GuiTableModel(GuiTable<T> table) {
        super();
        this.table = table;
        this.cache = new IndexedLookupSet<>();
    }

    @Override
    public String getColumnName(int columnIndex) {
        return getColumn(columnIndex).getColumnHeader();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return getColumn(columnIndex).getDataType();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        T row = getRow(rowIndex);
        GuiTableColumn<T> column = getColumn(columnIndex);
        return column.getCellEditing(row).canEdit(row);
    }
    
    @Override
    public synchronized int getRowCount() {
        return cache.size();
    }

    @Override
    public int getColumnCount() {
        return table.getColumns().size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return getColumn(columnIndex).getObject(getRow(rowIndex));
    }

    public int getColumnIndex(GuiTableColumn<T> column) {
        return table.getColumns().indexOf(column);
    }

    public GuiTableColumn<T> getColumn(int columnIndex) {
        return table.getColumns().get(columnIndex);
    }

    public synchronized int getRowIndex(T row) {
        return cache.indexOf(row);
    }

    public synchronized T getRow(int idx) {
        return cache.get(idx);
    }

    public synchronized List<T> getRowsAsList() {
        return cache.asList();
    }

    public synchronized boolean containsRow(T row) {
        return cache.contains(row);
    }

    public synchronized T addRow(T row) {
        if (containsRow(row)) {
            throw new IllegalArgumentException("Error: Row already exists and can't be added twice!");
        }
        T result = cache.add(row);
        int rowIndex = getRowIndex(row);
        fireTableRowsInserted(rowIndex, rowIndex);
        return result;
    }
    
    /**
     * @param rows the rows to add
     * @param callback See documentation of interface for usage
     */
    public synchronized void addRows(Collection<T> rows, AddRowsCallback<T> callback) {
        int rowNum = 1;
        for (T row : rows) {
            callback.onAddRow(row, rowNum);
            cache.add(row);
            rowNum++;
        }
        callback.onCompletion();
        fireTableDataChanged();
    }

    public synchronized T removeRow(T row) {
        T result = cache.remove(row);
        int rowIndex = getRowIndex(row);
        fireTableRowsDeleted(rowIndex, rowIndex);
        return result;
    }
    
    public synchronized void clear() {
        cache.clear();
        fireTableDataChanged();
    }
    
    
    public synchronized T updateRow(T row, Collection<? extends GuiTableColumn<T>> columns) {
        int rowIndex = getRowIndex(row);
        if (rowIndex == -1) {
            throw new IllegalArgumentException("Row not found in cache.  Update unsuccessful.");
        }
        T result = cache.replace(rowIndex, row);
        if (columns.size() == 1) {
            fireTableCellUpdated(rowIndex, table.getColumns().indexOf(columns.iterator().next()));
        } else {
            fireTableRowsUpdated(rowIndex, rowIndex);
        }
        return result;
    }
    
    /**
     *  Because this callback will be called from a thread-safe class,
     *  do not access shared mutable state from within the methods of classes
     *  of this type.
     */
    public interface AddRowsCallback<T> {

        void onAddRow(T row, int rowNum);

        void onCompletion();
    }
}

