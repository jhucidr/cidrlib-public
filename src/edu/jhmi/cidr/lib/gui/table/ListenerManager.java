/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table;

import edu.jhmi.cidr.lib.file.FileUtils;
import edu.jhmi.cidr.lib.gui.PopupMenuItem;
import edu.jhmi.cidr.lib.gui.table.filter.FilterManager;
import edu.jhmi.cidr.lib.math.Signum;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.AbstractButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

/**
 *
 * @author cidr
 */
//package-access
class ListenerManager<T> {

    private final GuiTable<T> table;
    private final List<MouseListener> mouseListeners;
    private final Map<GuiTableColumn<T>, List<GuiTable.GuiTableListener<T>>> listenerMap;
    private final DefaultPopupMenu popupMenu;

    public ListenerManager(GuiTable<T> table) {
        if (table == null) {
            throw new NullPointerException("table must not be null");
        }
        this.listenerMap = new HashMap<>();
        this.mouseListeners = new ArrayList<>();
        this.table = table;
        this.popupMenu = new DefaultPopupMenu();
        mouseListeners.add(new RightClickListener());
    }

    public List<MouseListener> getMouseListeners() {
        return mouseListeners;
    }

    public JPopupMenu getPopupMenu(RowColumnTuple<T> tuple) {
        return popupMenu.getPopupMenu(tuple);
    }

    public void addListener(GuiTableColumn<T> column, GuiTable.GuiTableListener<T> listener) {
        if (false == listenerMap.containsKey(column)) {
            listenerMap.put(column, new ArrayList<GuiTable.GuiTableListener<T>>());
        }
        listenerMap.get(column).add(listener);
    }

    public List<GuiTable.GuiTablePopupListener<T>> getPopupListeners(T row, GuiTableColumn<T> column) {
        List<GuiTable.GuiTablePopupListener<T>> result = new ArrayList<>();
        List<GuiTable.GuiTableListener<T>> colListeners = listenerMap.get(column);
        if (colListeners == null) {
            return result;
        }
        for (GuiTable.GuiTableListener<T> listener : colListeners) {
            if (listener instanceof GuiTable.GuiTablePopupListener) {
                // safe due to above check
                @SuppressWarnings(value = "unchecked")
                GuiTable.GuiTablePopupListener<T> popupListener = (GuiTable.GuiTablePopupListener<T>) listener;
                result.add(popupListener);
            }
        }
        return result;
    }

    List<GuiTable.GuiTableDoubleClickListener<T>> getDoubleClickListeners(T row, GuiTableColumn<T> column) {
        List<GuiTable.GuiTableDoubleClickListener<T>> result = new ArrayList<>();
        List<GuiTable.GuiTableListener<T>> colListeners = listenerMap.get(column);
        if (colListeners == null) {
            return result;
        }
        for (GuiTable.GuiTableListener<T> listener : colListeners) {
            if (listener instanceof GuiTable.GuiTableDoubleClickListener) {
                // safe due to above check
                @SuppressWarnings(value = "unchecked")
                GuiTable.GuiTableDoubleClickListener<T> doubleClickListener = (GuiTable.GuiTableDoubleClickListener<T>) listener;
                result.add(doubleClickListener);
            }
        }
        return result;
    }

    public void transferScrollPaneListeners(JScrollPane oldScrollPane, JScrollPane newScrollPane) {
        if (newScrollPane == null) {
            throw new NullPointerException("newScrollPane must not be null");
        }
        if (oldScrollPane != null) {
            for (MouseListener listener : mouseListeners) {
                oldScrollPane.removeMouseListener(listener);
            }
        }
        for (MouseListener listener : mouseListeners) {
            newScrollPane.addMouseListener(listener);
        }
    }

    public void removeScrollPaneListeners(JScrollPane scrollPane) {
        if (scrollPane != null) {
            for (MouseListener listener : mouseListeners) {
                scrollPane.removeMouseListener(listener);
            }
        }
    }

    public void addPopupMenuItem(JMenuItem item) {
        popupMenu.add(item);
    }

    public List<JMenuItem> getDefaultPopupMenuItems() {
        return new DefaultPopupMenu().getPopupMenuItems();
    }

    private static class ButtonSorter implements Comparator<Object> {

        @Override
        public int compare(Object o1, Object o2) {
            String s1 = String.valueOf(o1);
            String s2 = String.valueOf(o2);
            if (o1 instanceof AbstractButton) {
                s1 = ((AbstractButton) o1).getText();
            }
            if (o2 instanceof AbstractButton) {
                s2 = ((AbstractButton) o2).getText();
            }
            return Signum.getSign(s1, s2);
        }
    }

    private class DefaultPopupMenu {

        private final List<JMenuItem> popupMenuItems;
        private final List<JMenuItem> defaultMenuItems;

        {
            defaultMenuItems = new ArrayList<>();
            defaultMenuItems.add(new ExportToCsvItem());
            defaultMenuItems.add(new FilterManagerItem());
            defaultMenuItems.add(new SelectAllItem());
            defaultMenuItems.add(new PackAllItem());
            defaultMenuItems.add(new ColumnManagerItem());
            defaultMenuItems.add(new RowCountItem());
            //add(new CopyItem()); revisit after we handoff/deploy for testing
            Collections.sort(defaultMenuItems, new ButtonSorter());
        }

        public DefaultPopupMenu() {
            this.popupMenuItems = new ArrayList<>();
        }

        public final void add(JMenuItem item) {
            popupMenuItems.add(item);
            Collections.sort(popupMenuItems, new ButtonSorter());
        }

        public List<JMenuItem> getPopupMenuItems() {
            return new ArrayList<>(popupMenuItems);
        }

        public JPopupMenu getDefaultPopupMenu() {
            JPopupMenu popupMenu = new JPopupMenu();
            if (false == popupMenuItems.isEmpty()) {
                for (JMenuItem item : popupMenuItems) {
                    if (false == (item instanceof ConditionalPopupMenuItem)) {
                        popupMenu.add(item);
                    }
                }
                popupMenu.add(new JPopupMenu.Separator());
            }
            for (JMenuItem defaultItem : defaultMenuItems) {
                popupMenu.add(defaultItem);
            }
            return popupMenu;
        }

        public JPopupMenu getPopupMenu(RowColumnTuple<T> tuple) {
            JPopupMenu popupMenu = new JPopupMenu();
            if (false == popupMenuItems.isEmpty()) {
                for (JMenuItem item : popupMenuItems) {
                    if (item instanceof ConditionalPopupMenuItem) {
                        @SuppressWarnings("unchecked")
                        ConditionalPopupMenuItem<T> cpmi = (ConditionalPopupMenuItem<T>) item;
                        if (cpmi.showMenuItem(tuple, table)) {
                            popupMenu.add(item);
                        }
                    } else {
                        popupMenu.add(item);
                    }
                }
                popupMenu.add(new JPopupMenu.Separator());
            }
            for (JMenuItem defaultItem : defaultMenuItems) {
                popupMenu.add(defaultItem);
            }
            return popupMenu;
        }
    }

    class ColumnManagerItem extends PopupMenuItem {

        private static final String SHOW_COLUMN_MANAGER = "Show Column Manager";

        public ColumnManagerItem() {
            super(SHOW_COLUMN_MANAGER);
        }

        @Override
        public void performAction(ActionEvent e) {
            ColumnManagerDialog<T> d = new ColumnManagerDialog<>(null, table);
            d.setVisible(true);
        }
    }

    class RowCountItem extends PopupMenuItem {

        private static final String SHOW_COLUMN_MANAGER = "Row Count";

        public RowCountItem() {
            super(SHOW_COLUMN_MANAGER);
        }

        @Override
        public void performAction(ActionEvent e) {
            int visibleRows = table.getFilteredRows().size();
            int allRows = table.getRows().size();
            int hiddenRows = allRows - visibleRows;
            int selectedRows = table.getSelectedRows().size();
            StringBuilder sb = new StringBuilder();
            sb.append("Visible Rows = ").append(visibleRows).append("\n");
            sb.append("Hidden (Filtered) Rows = ").append(hiddenRows).append("\n");
            sb.append("Total Rows = ").append(allRows).append("\n");
            sb.append("Selected Rows = ").append(selectedRows).append("\n");
            JOptionPane.showMessageDialog(this, sb.toString(), "Row Count Info", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /*
     * package access
     */ class FilterManagerItem extends PopupMenuItem {

        private static final String SHOW_FILTER_MANAGER = "Show Filter Manager";
        private static final String HIDE_FILTER_MANAGER = "Hide Filter Manager";

        public FilterManagerItem() {
            super("See getText()");
        }

        @Override
        public void performAction(ActionEvent e) {
            FilterManager<T> manager = table.getFilterManager();
            manager.setFilterManagerVisible(!manager.isVisible());
        }

        @Override
        public String getText() {
            return table.getFilterManager().isVisible() ? HIDE_FILTER_MANAGER : SHOW_FILTER_MANAGER;
        }
    }

    class PackAllItem extends PopupMenuItem {

        public PackAllItem() {
            super("Auto-Resize Columns");
        }

        @Override
        public void performAction(ActionEvent e) {
            table.packAll();
        }
    }

    /*
     * package access
     */ class SelectAllItem extends PopupMenuItem {

        public SelectAllItem() {
            super("");
        }

        @Override
        public void performAction(ActionEvent e) {
            table.selectAll(table.getSelectAllButtonValue());
        }

        @Override
        public String getText() {
            return table.getSelectAllButtonValue().getText();
        }
    }

    /*
     * package access
     */ class CopyItem extends PopupMenuItem {

        public CopyItem() {
            super("Copy");
        }

        @Override
        public void performAction(ActionEvent e) {
            System.out.println("Hey y'all - lets copy some text!");
            //TODO: 
            //Copy the selected row, selected cell, or all selected values?
            //My inclination: Copy all selected information.  The only issue is that I don't think we (currently) have a way to
            //select individual cells vs. entire rows.
            //One possible alternative would be to display a JOptionPane giving them the options listed above, but
            //That could become annoying if one were doing a lot of copy paste.
            //I suppoose they could always export to .csv and open in the DFM to perform this kind of thing, but 
            //copy paste is the kind of functionality that is always useful and appreciated...
            // -JDN
        }
    }

    /*
     * package access
     */ class ExportToCsvItem extends PopupMenuItem {

        public ExportToCsvItem() {
            super("Export table contents to .xls");
        }

        @Override
        public void performAction(ActionEvent e) {
            FileUtils.exportTableToCsv(table);
        }
    }

    /**
     * This listener is used every time the user right clicks on the scrollPane
     * on which the GuiTable sits. The listener that fires when rows are clicked
     * upon is MouseAdapterOsAgnostic.popup().
     *
     * As written, ConditionalPopupMenuItem's will not be available for
     * selection when the user r-clicks on the ScrollPane. We may want to modify
     * this behaviour.
     */
    private class RightClickListener extends MouseAdapter {

        @Override
        public void mouseReleased(MouseEvent e) {
            if (e.isPopupTrigger()) {
                popupMenu.getDefaultPopupMenu().show(e.getComponent(), e.getX(), e.getY());
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {
            mouseReleased(e);
        }
    }
}
