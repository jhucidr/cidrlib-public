/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author David Newcomer Feb 27, 2013
 */
public class ColumnManager<T> {

    private final GuiTable<T> table;
    private final List<ColumnInfo<T>> hidden = new ArrayList<>();

    public ColumnManager(GuiTable<T> table) {
        this.table = table;
    }

    /**
     * Returns the columns in View order.
     */
    public List<GuiTableColumn<T>> getVisibleColumns() {
        List<GuiTableColumn<T>> allColumns = table.getColumns();
        JTable jTable = table.getJTable();
        List<GuiTableColumn<T>> result = new ArrayList<>();
        for (int i = 0; i < allColumns.size() - hidden.size(); i++) {
            result.add(allColumns.get(jTable.convertColumnIndexToModel(i)));
        }
        return Collections.unmodifiableList(result);
    }

    public List<GuiTableColumn<T>> getHiddenColumns() {
        List<GuiTableColumn<T>> result = new ArrayList<>();
        for (ColumnInfo<T> removedColumnInfo : hidden) {
            result.add(removedColumnInfo.getColumn());
        }
        return result;
    }

    /**
     * Returns the columns in Model order
     */
    public List<GuiTableColumn<T>> getAllColumns() {
        return table.getColumns();
    }

    public boolean isVisible(GuiTableColumn<T> column) {
        return getVisibleColumns().contains(column);
    }

    public void moveColumn(GuiTableColumn<T> columnToMove, int viewIndex) {
//        System.out.println("columnToMove = " + columnToMove + "; index = " + viewIndex + "; getColumnIndex() = " + getColumnViewIndex(columnToMove));
        if (isVisible(columnToMove)) {
            JTable myTable = table.getJTable();
            myTable.moveColumn(getColumnViewIndex(columnToMove), viewIndex);
        }
    }

    //Package-Access
    void importColumnInfo(ColumnInfo<T> ci, int newIndex) {
        GuiTableColumn<T> column = ci.getColumn();
        ColumnInfo<T> old = getColumnInfo(column);
        if (ci.isHidden() != old.isHidden()) {
            if (ci.isHidden()) {
                hideColumn(column);
            } else {
                showColumn(column);
            }
        }
        if (false == ci.isHidden()) {
            moveColumn(column, newIndex);
        }
    }

    public int getColumnViewIndex(GuiTableColumn<T> column) {
        JTable guiTable = table.getJTable();
        TableColumnModel tcm = guiTable.getColumnModel();
        return tcm.getColumnIndex(column.getColumnHeader());
    }

    public TableColumn getTableColumn(GuiTableColumn<T> column) {
        JTable guiTable = table.getJTable();
        TableColumnModel tcm = guiTable.getColumnModel();
        return tcm.getColumn(getColumnViewIndex(column));
    }

    public void hideColumn(GuiTableColumn<T> k) {
        if (isVisible(k)) {
            JTable guiTable = table.getJTable();
            TableColumnModel tcm = guiTable.getColumnModel();
            int index = tcm.getColumnIndex(k.getColumnHeader());
            TableColumn col = tcm.getColumn(index);
            tcm.removeColumn(col);
            hidden.add(new ColumnInfo<>(col, k, index, true));
        }
    }

    public void showColumn(GuiTableColumn<T> k, Comparator<GuiTableColumn<T>> cmp) {
        if (getHiddenColumns().contains(k)) {
            List<GuiTableColumn<T>> visibleColumnsBeforeAddition = getVisibleColumns();
            ColumnInfo<T> column = getColumnInfo(k);
            TableColumnModel tcm = table.getJTable().getColumnModel();
            tcm.addColumn(column.getTableColumn());
            int newIndex = tcm.getColumnCount() - 1;
            if (newIndex != visibleColumnsBeforeAddition.size()) {
                throw new IllegalStateException("index of newly added column does not match size "
                        + "of visible columns before addition: " + k.getColumnHeader());
            }
            boolean placed = false;
            while (false == placed) {
                GuiTableColumn<T> previousItem = visibleColumnsBeforeAddition.get(newIndex - 1);
                int cmpResult = cmp.compare(k, previousItem);
                if (cmpResult < 0) {
                    tcm.moveColumn(newIndex - 1, newIndex);
                    newIndex -= 1;
                } else {
                    placed = true;
                }
            }
            hidden.remove(column);
        } else {
            if (getVisibleColumns().contains(k)) {
                System.out.println("Unable to showColumn " + k + " because it is already visible.");
            } else {
                System.out.println("Unable to showColumn " + k + " because it does not yet exist in the table or has a poorly implemented equals().");
            }
        }
    }

    public void showColumn(GuiTableColumn<T> k) {
        if (getHiddenColumns().contains(k)) {
            ColumnInfo<T> column = getColumnInfo(k);
            TableColumnModel tcm = table.getJTable().getColumnModel();
            tcm.addColumn(column.getTableColumn());
            int lastColumn = tcm.getColumnCount() - 1;
            if (column.getIndex() < lastColumn) {
                tcm.moveColumn(lastColumn, column.getIndex());
            }
            hidden.remove(column);
        } else {
            if (getVisibleColumns().contains(k)) {
                System.out.println("Unable to showColumn " + k + " because it is already visible.");
            } else {
                System.out.println("Unable to showColumn " + k + " because it does not yet exist in the table or has a poorly implemented equals().");
            }
        }
    }

    public void showColumns() {
        for (GuiTableColumn<T> column : getHiddenColumns()) {
            showColumn(column);
        }
    }

    public void showColumns(Comparator<GuiTableColumn<T>> cmp) {
        for (GuiTableColumn<T> column : getHiddenColumns()) {
            showColumn(column, cmp);
        }
    }

    // Package-Access
    ColumnInfo<T> getColumnInfo(GuiTableColumn<T> column) {
        if (getVisibleColumns().contains(column)) {
            return new ColumnInfo<>(getTableColumn(column), column, getAllColumns().indexOf(column), false);
        } else {
            for (ColumnInfo<T> columnInfo : hidden) {
                if (columnInfo.getColumn().equals(column)) {
                    return columnInfo;
                }
            }
        }
        throw new IllegalStateException("Unable to find column " + column + " in table or hidden columns.");
    }

    public void hideColumns(Collection<? extends GuiTableColumn<T>> toHide) {
        for (GuiTableColumn<T> column : toHide) {
            hideColumn(column);
        }
    }

    public void showColumns(Collection<? extends GuiTableColumn<T>> columnsToShow) {
        for (GuiTableColumn<T> column : columnsToShow) {
            showColumn(column);
        }
    }

    public void showColumns(Collection<? extends GuiTableColumn<T>> columnsToShow,
            Comparator<GuiTableColumn<T>> cmp) {
        for (GuiTableColumn<T> column : columnsToShow) {
            showColumn(column, cmp);
        }
    }

    // Package-Access
    static class ColumnInfo<T> {

        private final TableColumn col;
        private final GuiTableColumn<T> guiCol;
        private final boolean hidden;
        private final int index;

        public ColumnInfo(TableColumn col, GuiTableColumn<T> guiCol, int index, boolean hidden) {
            this.col = col;
            this.guiCol = guiCol;
            this.hidden = hidden;
            this.index = index;
        }

        public boolean isHidden() {
            return hidden;
        }

        public ColumnInfo<T> changeHidden() {
            return new ColumnInfo<>(col, guiCol, index, !hidden);
        }

        public ColumnInfo<T> changeIndex(int newIndex) {
            return new ColumnInfo<>(col, guiCol, newIndex, hidden);
        }

        public TableColumn getTableColumn() {
            return col;
        }

        public int getIndex() {
            return index;
        }

        public GuiTableColumn<T> getColumn() {
            return guiCol;
        }

        @Override
        public String toString() {
            return getColumn().getColumnHeader();
        }
    }
}
