/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.table;

import edu.jhmi.cidr.lib.gui.PopupMenuItem;

/**
 * Use this PopupMenuItem for Actions that are only available under certain
 * circumstances.
 *
 * @author David Newcomer Sep 7, 2012
 */
public abstract class ConditionalPopupMenuItem<T> extends PopupMenuItem {

    public ConditionalPopupMenuItem(String itemText) {
        super(itemText);
    }

    /**
     * The RowColumnTuple is the Row & Column just clicked upon. This class is
     * intended to handle row/column specific popup menu items/actions. Please
     * note that the tuple will contain only the row that was clicked upon, even
     * if more than one row is selected. Conditional Popup Menu Items are NOT
     * available when the user clicks on the ScrollPane rather than the
     * GuiTable.
     */
    public abstract boolean showMenuItem(RowColumnTuple<T> tuple, GuiTable<T> table);
}
