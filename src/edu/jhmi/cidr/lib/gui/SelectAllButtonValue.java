/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author David Newcomer Aug 6, 2012
 */
public enum SelectAllButtonValue {

    SELECT_ALL("Select All", true),
    DESELECT_ALL("De-Select All", false);
    private final String text;
    private final boolean select;

    private SelectAllButtonValue(String text, boolean select) {
        this.text = text;
        this.select = select;
    }

    public String getText() {
        return text;
    }
    private static final Map<String, SelectAllButtonValue> map = new HashMap<String, SelectAllButtonValue>();

    static {
        for (SelectAllButtonValue val : values()) {
            map.put(val.toString().toUpperCase(), val);
        }
    }

    public SelectAllButtonValue toggle() {
        switch (this) {
            case SELECT_ALL:
                return DESELECT_ALL;
            case DESELECT_ALL:
                return SELECT_ALL;
            default:
                throw new IllegalStateException("Unrecognized SelectAllButtonValue: " + this.toString());
        }
    }
    
    public boolean makeSelected(){
        return select;
    }

    public static SelectAllButtonValue fromString(String s) {
        return s == null ? null : map.get(s.toUpperCase());
    }

    @Override
    public String toString() {
        return getText();
    }
}
