/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class FileBrowseListener implements ActionListener {

    private final String okButtonText;
    private /*final*/ String dialogTitle;
    private final int selectionMode;
    private final FileHandler fileHandler;
    private List<FileBrowseListener> linkedBrowseListeners;
    private File selectedFile;
    private String currentWorkingDirectoryPath = DEFAULT_DIRECTORY;
    private static final String DEFAULT_DIRECTORY = System.getProperty("user.home");

    public FileBrowseListener(String okButtonText, String dialogTitle, int selectionMode) {
        this(okButtonText, dialogTitle, selectionMode, new FileHandler() {

            @Override
            public void handleSelectedFile(File selectedFile) {
                // The Listener, it does nothing!
            }
        });
    }

    public FileBrowseListener(String okButtonText, String dialogTitle, int selectionMode, JTextField textField) {
        this(okButtonText, dialogTitle, selectionMode, new TextFieldFileHandler(textField));
    }

    public FileBrowseListener(String okButtonText, String dialogTitle, int selectionMode, FileHandler fileHandler) {
        this.okButtonText = okButtonText;
        this.dialogTitle = dialogTitle;
        this.fileHandler = fileHandler;
        this.selectionMode = selectionMode;
        linkedBrowseListeners = new ArrayList<FileBrowseListener>();
    }

    public File getSelectedFile() {
        return selectedFile;
    }

    public void setSelectedFile(File selectedFile) {
        if (selectedFile == null) {
            throw new NullPointerException("selectedFile must not be null.");
        }
        this.selectedFile = selectedFile;
    }
    

    public void setLinkedBrowseListeners(List<FileBrowseListener> browseListeners) {
        if (browseListeners == null) {
            throw new NullPointerException("browseListeners must not be null");
        }
        this.linkedBrowseListeners = browseListeners;
    }
    
    public void setDialogTitle(String dialogTitle) {
        if (dialogTitle == null) {
            throw new NullPointerException("dialogTitle must not be null.");
        }
        this.dialogTitle = dialogTitle;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser jfc = new JFileChooser(currentWorkingDirectoryPath);
        jfc.setDialogTitle(dialogTitle);
        jfc.setFileSelectionMode(selectionMode);
        if (selectedFile != null) {
            jfc.setSelectedFile(selectedFile);
        }
        JFrame jf = new JFrame();
        int x = jfc.showDialog(jf, okButtonText);
        if (x == JFileChooser.APPROVE_OPTION) {
            selectedFile = jfc.getSelectedFile();
            fileHandler.handleSelectedFile(selectedFile);
            currentWorkingDirectoryPath = selectedFile.isDirectory() ? selectedFile.getAbsolutePath() : selectedFile.getParent();
            for (FileBrowseListener browseListener : linkedBrowseListeners) {
                browseListener.setCurrentWorkingDirectoryPath(currentWorkingDirectoryPath);
            }
        }
    }

    public File showFileSelectionDialog() {
        actionPerformed(null);
        return selectedFile;
    }

    public String getCurrentWorkingDirectoryPath() {
        return currentWorkingDirectoryPath;
    }

    public void setCurrentWorkingDirectoryPath(String currentWorkingDirectoryPath) {
        this.currentWorkingDirectoryPath = currentWorkingDirectoryPath;
    }

    public static interface FileHandler {

        public void handleSelectedFile(File selectedFile);
    }

    public static class TextFieldFileHandler implements FileHandler {

        private final JTextField textField;

        public TextFieldFileHandler(JTextField textField) {
            if (textField == null) {
                throw new NullPointerException("textField must not be null");
            }
            this.textField = textField;
        }

        @Override
        public void handleSelectedFile(File selectedFile) {
            if (selectedFile == null) {
                JOptionPane.showMessageDialog(null, "Please make a selection.", "Input Required", JOptionPane.ERROR);
            } else {
                textField.setText(selectedFile.getAbsolutePath());
            }
        }
    }
}
