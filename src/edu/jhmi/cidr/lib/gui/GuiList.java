/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui;

import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.annotations.GuardedBy;
import edu.jhmi.cidr.lib.annotations.ThreadSafe;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

/**
 * Based on David's GuiListToo, but accepts parameterized JList that matches the
 * parameterization of 'this.'
 * 
 * @author dleary
 */
@ThreadSafe
public class GuiList<T> implements Iterable<T> {
    private final JList<T> jList;
    private final List<T> data;

    public GuiList(JList<T> jList) {
        this.jList = jList;
        this.data = new ArrayList<>();
    }
    
    public static <T> GuiList<T> getInstance(JList<T> jList) {
        return new GuiList<>(jList);
    }
    
    public void setCellRenderer(DefaultListCellRenderer renderer) {
        this.jList.setCellRenderer(renderer);
    }
    
    public synchronized void replaceAll(Collection<? extends T> data) {
        this.data.clear();
        this.data.addAll(data);
        synch();
    }

    public synchronized void clearData() {
        data.clear();
        synch();
    }

    public synchronized void sort(Comparator<T> c) {
        Collections.sort(data, c);
        synch();
    }

    public synchronized List<T> getData() {
        return new ArrayList<>(data);
    }

    public synchronized T set(int index, T t) {
        T result = data.set(index, t);
        synch();
        return result;
    }

    public synchronized void add(T toAdd) {
        if (toAdd == null) {
            throw new NullPointerException("toAdd must not be null");
        }
        data.add(toAdd);
        synch();
    }

    public synchronized void add(int index, T element) {
        data.add(index, element);
        synch();
    }

    public synchronized void remove(T toRemove) {
        if (toRemove == null) {
            throw new NullPointerException("toRemove must not be null");
        }
        data.remove(toRemove);
        synch();
    }

    public synchronized void removeAll(Collection<T> toRemove) {
        if (toRemove == null) {
            throw new NullPointerException("toRemove must not be null");
        }
        data.removeAll(toRemove);
        synch();
    }

    public synchronized void addAll(Collection<T> toAdd) {
        if (toAdd == null) {
            throw new NullPointerException("toAdd must not be null");
        }
        data.addAll(toAdd);
        synch();
    }

    public synchronized List<T> getSelected() {
        return jList.getSelectedValuesList();
    }

    public synchronized List<T> getNotSelectedItems() {
        List<T> retVal = getData();
        retVal.removeAll(getSelected());
        return retVal;
    }

    @GuardedBy("this") 
    private void synch() {
        List<T> selected = getSelected();
        @SuppressWarnings("unchecked") // Since it is zero-sized, it should be used merely for type information and not for returning a result
        T[] tRA = (T[])(new Object[] {});
        jList.setListData(data.toArray(tRA));
        setSelected(selected);
    }

    public JList getJList() {
        return jList;
    }

    @Override
    public Iterator<T> iterator() {
        return data.iterator();
    }

    public synchronized boolean isSelectionEmpty() {
        return getSelected().isEmpty();
    }

    public synchronized void clearSelection() {
        jList.clearSelection();
    }

    public synchronized void setSelected(T t) {
        jList.setSelectedIndex(data.indexOf(t));
    }

    public synchronized void setSelected(Collection<T> t) {
        int[] ints = new int[t.size()];
        int i = 0;
        for (T t1 : t) {
            ints[i] = data.indexOf(t1);
            i++;
        }
        jList.setSelectedIndices(ints);
    }

    public synchronized void setSelectedAll(boolean select) {
        if (select) {
            jList.setSelectedIndices(Utils.range(0, data.size() - 1));      
        } else {
            clearSelection();
        }
    }

    public synchronized T get(int index) {
        return data.get(index);
    }

    public synchronized T prev(T t) {
        return get(indexOf(t) - 1);
    }

    public synchronized T next(T t) {
        return get(indexOf(t) + 1);
    }

    public synchronized int size() {
        return data.size();
    }

    public synchronized int indexOf(T t) {
        return data.indexOf(t);
    }

    public synchronized boolean contains(T t) {
        return data.contains(t);
    }

    public synchronized boolean containsAll(Collection<?> c) {
        return data.containsAll(c);
    }

    public synchronized boolean isEmpty() {
        return data.isEmpty();
    }
}
