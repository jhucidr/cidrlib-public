/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui;

import java.rmi.RemoteException;
import javax.swing.JProgressBar;

/**
 * This is a convenience wrapper for the swing JProgressBar
 *
 * @author David Newcomer Dec 6, 2011
 */
public class GuiProgressBar {

    private final javax.swing.JProgressBar progressBar;

    public GuiProgressBar(JProgressBar progressBar) {
        if (progressBar == null) {
            throw new NullPointerException("progressBar must not be null");
        }
        this.progressBar = progressBar;
    }

    public void clear() {
        clear("");
    }

    public void clear(String message) {
       setProgress(0, 0, 0, message);
    }

    public void updateProgressBar(int percentCompletion) {
        updateProgressBar(percentCompletion, "");
    }

    public void updateProgressBar(int percentCompletion, String message) {
        if (percentCompletion < 0) {
            throw new IllegalArgumentException("percentCompletion must be >= zero.");
        }
        if (percentCompletion > 100) {
            percentCompletion = 100;
        }
        setProgress(0, 100, percentCompletion, message + " " + percentCompletion + "% complete.");
    }

    private void setProgress(int min, int max, int current, String msg) {
        if (current > max) {
            current = max;
        }
        progressBar.setIndeterminate(false);
        progressBar.setVisible(true);
        progressBar.setStringPainted(true);
        progressBar.setString(msg);
        progressBar.setMinimum(min);
        progressBar.setMaximum(max);
        progressBar.setValue(current);
    }

    public void setIndeterminate(String message) {
        progressBar.setIndeterminate(true);
        progressBar.setVisible(true);
        progressBar.setStringPainted(true);
        progressBar.setString(message);
    }

//    public void receiveProgressUpdate(ProgressUpdate<?> update) throws RemoteException {
//        if (update == null) {
//            throw new NullPointerException("Progress update object must not be null");
//        }
//        if (progressBar != null) {
//            String msg = update.getMsg();
//            if (msg != null) {
//                progressBar.setString(msg);
//            }
//            if (update.isIndeterminate()) {
//                progressBar.setIndeterminate(true);
//                return;
//            } else {
//                progressBar.setIndeterminate(false);
//            }
//            progressBar.setMinimum(update.getMin());
//            progressBar.setMaximum(update.getMax());
//            progressBar.setValue(update.getCurrent());
//        }
//    }
}
