/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui;

import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.gui.table.GuiTableColumn;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author cidr
 */
public interface Changeable<T> extends Serializable {

    public ChangeTracker<T> getChanges();

    public static class ChangeTracker<T> implements Serializable {

        private final Map<T, Boolean> layers;
        private static final long serialVersionUID = 42L;

        public ChangeTracker() {
            this.layers = new HashMap<T, Boolean>();
        }

        public void setHasUnsavedChanges(T changedItem, boolean hasUnsavedChanges) {
            layers.put(changedItem, hasUnsavedChanges);
        }

        public void setHasUnsavedChanges(boolean hasUnsavedChanges, T[] otherChangedItems) {
            setHasUnsavedChanges(hasUnsavedChanges, Utils.asList(otherChangedItems));
        }

        @SafeVarargs
        public final void setHasUnsavedChanges(boolean hasUnsavedChanges, T firstChangedItem, T... otherChangedItems) {
            setHasUnsavedChanges(hasUnsavedChanges, Utils.asList(firstChangedItem, otherChangedItems));
        }

        public void setHasUnsavedChanges(boolean hasUnsavedChanges, Collection<T> changedItems) {
            for (T t : changedItems) {
                layers.put(t, hasUnsavedChanges);
            }
        }

        public boolean hasUnsavedChanges() {
            for (Map.Entry<T, Boolean> en : layers.entrySet()) {
                boolean val = en.getValue();
                if (val) {
                    return true;
                }
            }
            return false;
        }

        public List<T> getItemsWithChanges() {
            List<T> changes = new ArrayList<T>();
            for (Map.Entry<T, Boolean> en : layers.entrySet()) {
                boolean val = en.getValue();
                changes.add(en.getKey());
            }
            return changes;
        }

        public boolean hasUnsavedChanges(T toCheck) {
            Boolean b = layers.get(toCheck);
            if (b == null) {
                return false;
            } else {
                return b;
            }
        }
    }

    public static class NullChangeTracker<T extends GuiTableColumn> extends ChangeTracker<T> {

        private static final long serialVersionUID = 42L;

        @Override
        public boolean hasUnsavedChanges() {
            return false;
        }

        @Override
        public boolean hasUnsavedChanges(T toCheck) {
            return false;
        }

        @Override
        public void setHasUnsavedChanges(T changedItem, boolean hasUnsavedChanges) {
            throw new UnsupportedOperationException("Cannot set state on NULL_CHANGE_TRACKER");
        }

        @Override
        public List<T> getItemsWithChanges() {
            throw new UnsupportedOperationException("Cannot get items with changes on NULL_CHANGE_TRACKER");
        }
    }
}
