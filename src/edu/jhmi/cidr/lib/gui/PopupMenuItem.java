/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui;

import edu.jhmi.cidr.lib.Application;
import edu.jhmi.cidr.lib.Utils;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

/**
 *
 * @author David Newcomer
 */
public abstract class PopupMenuItem extends JMenuItem implements ActionListener {

    public PopupMenuItem() {
        this("");
    }

    public PopupMenuItem(String itemText) {
        super(itemText);
        addActionListener(this);
    }

    @Override
    public String getText() {
        String superText = super.getText();
        if (superText == null || superText.trim().isEmpty()) {
            return generateItemText();
        } else {
            return superText;
        }
    }

    private String generateItemText() {
        String myName = Utils.translateClassName(getClass());
        Utils.replaceIgnoreCase(myName, "Popup", "");
        Utils.replaceIgnoreCase(myName, "JMenu", "");
        Utils.replaceIgnoreCase(myName, "Menu", "");
        Utils.replaceIgnoreCase(myName, "Item", "");
        return myName;
    }
    
    @Override
    //ATTNENTION!!!! Subclasses should override performAction instead of this method.
    public final void actionPerformed(final ActionEvent e) {
        //DO NOT OVERIDE ME IN SUBCLASSES - override performAction() instead.
        //TODO: Replace this with GuiThread after incorporating GuiThread into CidrLib.
        new Thread() {
            @Override
            public void run() {
                try {
                    performAction(e);
                } catch (Exception e) {
                    Application.reportError(PopupMenuItem.this, "Problem Encountered while performing " + getText() + ".", "Issue encountered", e);
                }
            }
        }.start();

    }

    public abstract void performAction(ActionEvent e);
}