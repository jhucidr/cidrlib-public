/*Copyright (c) 2015 Johns Hopkins Center for Inherited Disease Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

package edu.jhmi.cidr.lib.gui.combobox;

import edu.jhmi.cidr.lib.annotations.GuardedBy;
import edu.jhmi.cidr.lib.annotations.ThreadSafe;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;

/**
 * @author David Newcomer Apr 4, 2013
 */
@ThreadSafe
public class GuiComboBox<T> extends ArrayList<T> {

    private final JComboBox<T> comboBox;

    // Use the factory methods below
    private GuiComboBox(JComboBox<T> comboBox) {
        this.comboBox = comboBox;
        comboBox.removeAllItems();
    }

    public JComboBox<T> getJComboBox() {
        return comboBox;
    }
    
    public synchronized void setSelectedItem(T toSet){
        comboBox.setSelectedItem(toSet);
    }

    /**
     * Use if a combo-box is not already available (e.g., in a GuiTable) and
     * it is not appropriate to default on the selection.
     * Added by DRL 9/19/13
     */
    public static <T> GuiComboBox<T> getInstance() {
        return new GuiComboBox<>(new JComboBox<T>());
    }

    /**
     * Use if a combo-box is not already available (e.g., in a GuiTable).
     * Added by DRL 9/19/13
     */
    public static <T> GuiComboBox<T> getInstance(String defaultString) {
        return getInstance(new JComboBox<T>(), defaultString);
    }

    // Added by DRL 4/18/13
    public static <T> GuiComboBox<T> getInstance(JComboBox<T> comboBox, String defaultString) {
        comboBox.setRenderer(new NullValueRenderer(defaultString));
        GuiComboBox<T> result = new GuiComboBox<>(comboBox);
        result.add(null); // default value
        return result;
    }

    // Added by DRL 4/18/13
    public static <T extends Enum<T>> GuiComboBox<T> fromEnum(Class<T> type,
            JComboBox<T> comboBox, String defaultString) {
        comboBox.setRenderer(new NullValueRenderer(defaultString));
        GuiComboBox<T> result = new GuiComboBox<>(comboBox);
        result.add(null); // default value
        result.addAll(EnumSet.allOf(type));
        return result;
    }

    @GuardedBy("this")
    private void synch() {
        comboBox.removeAllItems();
        for (T t : this) {
            comboBox.addItem(t);
        }
    }

    @Override
    public synchronized boolean add(T e) {
        boolean result = super.add(e);
        comboBox.addItem(e);
        return result;
    }

    @Override
    public synchronized boolean remove(Object o) {
        boolean result = super.remove(o);
        comboBox.removeItem(o);
        return result;
    }

    @Override
    public synchronized boolean addAll(Collection<? extends T> c) {
        boolean result = super.addAll(c);
        for (T t : c) {
            this.add(t);
        }
        return result;
    }

    @Override
    public synchronized void clear() {
        super.clear();
        synch();
    }

    @Override
    public synchronized void add(int index, T element) {
        super.add(index, element);
        synch();
    }

    @Override
    public synchronized boolean addAll(int index, Collection<? extends T> c) {
        boolean result = super.addAll(index, c);
        synch();
        return result;
    }

    /**
     * Replaces the contents of the ComboBox with the provided collection, and
     * defaults the selected value to null.
     */
    public synchronized void replaceAll(Collection<? extends T> c) {
        super.clear();
        super.add(null);
        super.addAll(c);
        synch();
    }

    @Override
    public synchronized T remove(int index) {
        T result = super.remove(index);
        comboBox.removeItemAt(index);
        return result;
    }

    @Override
    public synchronized boolean removeAll(Collection<?> c) {
        boolean result = super.removeAll(c);
        synch();
        return result;
    }

    @Override
    protected synchronized void removeRange(int fromIndex, int toIndex) {
        super.removeRange(fromIndex, toIndex);
        synch();
    }

    @Override
    public synchronized boolean retainAll(Collection<?> c) {
        boolean result = super.retainAll(c);
        synch();
        return result;
    }

    @Override
    public synchronized T set(int index, T element) {
        T result = super.set(index, element);
        synch();
        return result;
    }

    @Override
    public synchronized List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Not Supported.");
    }

    public T getSelectedItem() {
        @SuppressWarnings("unchecked")
        T result = (T) comboBox.getSelectedItem();
        return result;
    }

    // Added by DRL 4/18/13
    private static class NullValueRenderer extends DefaultListCellRenderer {

        private final String message;

        public NullValueRenderer(String message) {
            this.message = message;
        }

        @Override
        public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            return (value == null)
                    ? new JLabel(message)
                    : super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        }
    }
}
