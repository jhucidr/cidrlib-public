/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 *
 * @author David Newcomer
 */
@RunWith(Suite.class)
@SuiteClasses({UtilsBetweenIgnoreCaseTest.class, UtilsBetweenTest.class})
public class UtilsTest {
    
    public UtilsTest() {
    }

}