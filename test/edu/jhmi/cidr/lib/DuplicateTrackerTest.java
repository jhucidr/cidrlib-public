/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import edu.jhmi.cidr.lib.HtmlUtils.HtmlBuilder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import javax.swing.text.html.HTML;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author asanch15
 */
public class DuplicateTrackerTest {

    private static Random rand;
    private static Map<Integer, List<Integer>> dupeMap;
    private static Map<Integer, List<Integer>> noDupeMap;
//    private static final int MAX_KEYS = 5;

    public DuplicateTrackerTest() {
//        dupeList    = new ArrayList<>();
//        noDupeList  = new ArrayList<>();
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("Setting up class");
        rand = new Random(System.nanoTime());
        dupeMap = new HashMap<>();
        noDupeMap = new HashMap<>();

        for (int i = 0; i < Math.abs(rand.nextInt(100)); i++) {
            List<Integer> intList = new ArrayList<>();
            List<Integer> oneList = new ArrayList<>();

            oneList.add(rand.nextInt(100));

            for (int j = 0; j < rand.nextInt(10) + 2; j++) {
                intList.add(rand.nextInt(100));
            }

            dupeMap.put((i % 2), intList);
            noDupeMap.put(i, oneList);
        }

        System.out.println("Dupe Map: ");
        System.out.println(dupeMap);

        System.out.println("No Dupe Map: ");
        System.out.println(noDupeMap);
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private static DuplicateTracker<Integer, Integer> setupTracker(Map<Integer, List<Integer>> insertMap) {
        DuplicateTracker<Integer, Integer> trackerInstance = new DuplicateTracker<>();

        for (Map.Entry<Integer, List<Integer>> en : insertMap.entrySet()) {
            for (Integer i : en.getValue()) {
                trackerInstance.addEntry(en.getKey(), i);
            }
        }

        return trackerInstance;
    }

    /**
     * Test of addEntry method, of class DuplicateTracker.
     */
    @Test
    public void testAddEntry() {
        DuplicateTracker<Integer, Integer> instance = setupTracker(noDupeMap);

        System.out.println("testAddEntry");

        for (Integer i : instance) {
            List<Integer> trackerList = instance.getDataSourcesForValue(i);
            List<Integer> compareList = noDupeMap.get(i);

            assertEquals(trackerList, compareList);
        }
    }

    /**
     * Test of hasDuplicates method, of class DuplicateTracker.
     */
    @Test
    public void testHasDuplicates_0args() {
        DuplicateTracker<Integer, Integer> dupeInstance =
                setupTracker(dupeMap);
        DuplicateTracker<Integer, Integer> noDupeInstance =
                setupTracker(noDupeMap);

        System.out.println("hasDuplicates_0args");

        //Test basic integer input
        assertTrue(dupeInstance.hasDuplicates());
        assertFalse(noDupeInstance.hasDuplicates());

        //See what happens when null comes into the mix
        dupeInstance.addEntry(null, 0);
        dupeInstance.addEntry(null, 0);

        noDupeInstance.addEntry(null, 0);

        assertTrue(dupeInstance.hasDuplicates());
        assertFalse(noDupeInstance.hasDuplicates());

        dupeInstance.addEntry(dupeMap.size(), null);
        dupeInstance.addEntry(dupeMap.size(), null);

        noDupeInstance.addEntry(noDupeMap.size(), null);

        assertTrue(dupeInstance.hasDuplicates());
        assertFalse(noDupeInstance.hasDuplicates());
    }

    /**
     * Test of hasDuplicates method, of class DuplicateTracker.
     */
    @Test
    public void testHasDuplicates_GenericType() {
        DuplicateTracker<Integer, Integer> dupeInstance =
                setupTracker(dupeMap);
        DuplicateTracker<Integer, Integer> noDupeInstance =
                setupTracker(noDupeMap);

        System.out.println("hasDuplicates_GenericType");

        //Test basic integer input
        for (Integer key : dupeInstance.getKeys()) {
            assertTrue(dupeInstance.hasDuplicates(key));
        }

        for (Integer key : noDupeInstance.getKeys()) {
            assertFalse(noDupeInstance.hasDuplicates(key));
        }
    }

    /**
     * Test of getDataSourcesForValue method, of class DuplicateTracker.
     */
    @Test
    public void testGetDataSourcesForValue() {
        DuplicateTracker<Integer, Integer> instance =
                setupTracker(noDupeMap);
        System.out.println("getDataSourcesForValue");

        for (Integer key : instance.getKeys()) {
            List<Integer> trackerList = instance.getDataSourcesForValue(key);

            assertEquals(trackerList, noDupeMap.get(key));
        }
    }

    /**
     * Test of getKeys method, of class DuplicateTracker.
     */
    @Test
    public void testGetKeys() {
        DuplicateTracker<Integer, Integer> instance =
                setupTracker(noDupeMap);

        System.out.println("getKeys");

        for (Integer key : instance.getKeys()) {
            assertTrue(noDupeMap.containsKey(key));
        }
    }

    /**
     * Test of getValues method, of class DuplicateTracker.
     */
    @Ignore
    @Test
    public void testGetValues() {
        DuplicateTracker<Integer, Integer> instance =
                setupTracker(noDupeMap);
        List<Integer> allValues = new ArrayList<>();
        List<Integer> instanceValues = new ArrayList<>(instance.getValues());

        System.out.println("getValues");

        for (Map.Entry<Integer, List<Integer>> en : noDupeMap.entrySet()) {
            allValues.addAll(en.getValue());
        }

        Collections.sort(allValues);
        Collections.sort(instanceValues);

        assertEquals(instanceValues, allValues);
    }

    /**
     * Test of getDuplicates method, of class DuplicateTracker.
     */
    @Test
    public void testGetDuplicates() {
        DuplicateTracker<Integer, Integer> instance =
                setupTracker(dupeMap);
        Map<Integer, List<Integer>> instanceDupeList = instance.getDuplicates();

        System.out.println("getDuplicates");

        for (Map.Entry<Integer, List<Integer>> en : dupeMap.entrySet()) {
            assertEquals(instanceDupeList.get(en.getKey()),
                    en.getValue());
        }
    }

    /**
     * Test of getDuplicateKeys method, of class DuplicateTracker.
     */
    @Test
    public void testGetDuplicateKeys() {
        DuplicateTracker<Integer, Integer> instance =
                setupTracker(dupeMap);
        List<Integer> dupeKeys = new ArrayList<>(instance.getDuplicateKeys());

        System.out.println("getDuplicateKeys");

        for (Integer key : dupeMap.keySet()) {
            assertTrue(dupeKeys.contains(key));
        }
    }

    /**
     * Test of getDuplicateValues method, of class DuplicateTracker.
     */
    @Test
    public void testGetDuplicateValues() {
        DuplicateTracker<Integer, Integer> instance =
                setupTracker(dupeMap);
        List<Integer> dupeInstanceValues = new ArrayList<>(instance.getDuplicateValues());
        List<Integer> dupeMapValues = new ArrayList<>();

        System.out.println("getDuplicateValues");

        for (Map.Entry<Integer, List<Integer>> en : dupeMap.entrySet()) {
            dupeMapValues.addAll(en.getValue());
        }

        Collections.sort(dupeMapValues);
        Collections.sort(dupeInstanceValues);

        assertEquals(dupeMapValues, dupeInstanceValues);
    }

    /**
     * Test of findDuplicates method, of class DuplicateTracker.
     */
    @Test
    public void testFindDuplicates_Collection() {
        int numDupes = Math.abs(rand.nextInt(10));
        ShuffledDupeList shufDupeList = new ShuffledDupeList(numDupes);
        Set<Integer> instanceDupeSet = new TreeSet<>(DuplicateTracker.findDuplicates(shufDupeList.getIntList()));
        System.out.println("shufDupeList.getIntList() = " + shufDupeList.getIntList());
        assertEquals(shufDupeList.getDupeSet(), instanceDupeSet);
    }

    /**
     * Test of findDuplicates method, of class DuplicateTracker.
     */
    @Test
    public void testFindDuplicates_Collection_Function() {

        class MyFunction implements Function<Integer, Integer> {

            @Override
            public Integer apply(Integer f) {
                return f * f;
            }
        }
        int numDupes = Math.abs(rand.nextInt(10));
        ShuffledDupeList shufDupeList = new ShuffledDupeList(numDupes);
        Set<Integer> instanceDupeSet = new TreeSet<>(DuplicateTracker.findDuplicates(shufDupeList.getIntList(), new MyFunction()));
        System.out.println("shufDupeList.getIntList() = " + shufDupeList.getIntList());
        assertEquals(shufDupeList.getDupeSet(), instanceDupeSet);
    }

    /**
     * Test of findDuplicatesMapIgnoreCase method, of class DuplicateTracker.
     */
    @Test
    public void testFindDuplicatesMapIgnoreCase() {
        int numDupes = Math.abs(rand.nextInt(10));
        ShuffledDupeList shufDupeList = new ShuffledDupeList(numDupes);
        Map<String, List<Integer>> instanceDupeMap = new HashMap<>(DuplicateTracker.findDuplicatesMapIgnoreCase(shufDupeList.getIntList()));
        Set<String> intStringSet = new TreeSet<>();
        Set<String> instanceDupMapKeysSet = new TreeSet<>(instanceDupeMap.keySet());

        System.out.println("findDuplicatesMapIgnoreCase");

        for (int i : shufDupeList.getDupeSet()) {
            intStringSet.add(String.valueOf(i));
        }

        assertEquals(instanceDupMapKeysSet, intStringSet);
    }

    /**
     * Test of findDuplicatesIgnoreCase method, of class DuplicateTracker.
     */
    @Test
    public void testFindDuplicatesIgnoreCase() {
        int numDupes = Math.abs(rand.nextInt(10));
        ShuffledDupeList shufDupeList = new ShuffledDupeList(numDupes);
        Set<String> instanceDupeSet =
                new TreeSet<>(DuplicateTracker.findDuplicatesIgnoreCase(
                shufDupeList.getStringIntList()));

        System.out.println("findDuplicatesIgnoreCase");
        System.out.println("shufDupeList.getDupeSet() = " + shufDupeList.getDupeSet());
        System.out.println("instanceDupeSet = " + instanceDupeSet);

        assertEquals(shufDupeList.getStringDupeSet(), instanceDupeSet);
    }

    private static Optional<String> mapToHTML(Map<Integer, List<Integer>> map,
            Function<Integer, String> f) {
        if (map.isEmpty()) {
            return Optional.absent();
        } else {
            HtmlBuilder htmlBuilder = new HtmlBuilder();

            for (Map.Entry<Integer, List<Integer>> en : map.entrySet()) {
                List<String> stringList = new ArrayList<>();

                for (int i : en.getValue()) {
                    stringList.add(f.apply(i));
                }

                htmlBuilder.append(en.getKey()).append(" was found duplicated in the following sources:").append(stringList);
            }

            return Optional.of(htmlBuilder.toString());
        }
    }

    /**
     * Test of duplicationErrors method, of class DuplicateTracker.
     */
    @Test
    public void testDuplicationErrors_0args() {
        Function<Integer, String> intToStringFunc = new Function<Integer, String>() {
            @Override
            public String apply(Integer f) {
                return String.valueOf(f);
            }
        };
        DuplicateTracker<Integer, Integer> instance = setupTracker(dupeMap);
        Optional<String> instanceOutput = instance.duplicationErrors();
        Optional<String> expOutput = mapToHTML(dupeMap, intToStringFunc);

        System.out.println("duplicationErrors");
        System.out.println("instanceOutput = " + instanceOutput);
        System.out.println("expOutput = " + expOutput);

        assertEquals(instanceOutput, expOutput);
    }

    /**
     * Test of duplicationErrors method, of class DuplicateTracker.
     */
    @Test
    public void testDuplicationErrors_Function() {
        Function<Integer, String> intToStringFunc = new Function<Integer, String>() {
            @Override
            public String apply(Integer f) {
                return String.valueOf(f);
            }
        };
        DuplicateTracker<Integer, Integer> instance = setupTracker(dupeMap);
        Optional<String> instanceOutput = instance.duplicationErrors(intToStringFunc);
        Optional<String> expOutput = mapToHTML(dupeMap, intToStringFunc);

        System.out.println("duplicationErrors_Function");
        
        System.out.println("instanceOutput = " + instanceOutput);
        System.out.println("expOutput = " + expOutput);

        assertEquals(instanceOutput, expOutput);
    }

    private static class ShuffledDupeList {

        private final List<Integer> intList;
        private final Set<Integer> dupeSet;

        public ShuffledDupeList(int numDupes) {
            Random rand = new Random(System.nanoTime());
            int listSize = (numDupes * 2) + Math.abs(rand.nextInt(10)) + 1;
            
            dupeSet = new TreeSet<>();
            intList = new ArrayList<>((listSize));
            while (dupeSet.size() < numDupes) {
                int dupeValue = (rand.nextInt(100));

                if (!dupeSet.contains(dupeValue)) {
                    dupeSet.add(dupeValue);
                }
            }
            intList.addAll(dupeSet);
            intList.addAll(dupeSet);

            while (intList.size() < listSize) {
                int randomValue = rand.nextInt(100);

                if (!intList.contains(randomValue)) {
                    intList.add(randomValue);
                }
            }
            Collections.shuffle(intList, new Random(System.nanoTime()));
        }

        public List<Integer> getIntList() {
            return Collections.unmodifiableList(intList);
        }

        public Set<Integer> getDupeSet() {
            return Collections.unmodifiableSet(dupeSet);
        }

        public List<String> getStringIntList() {
            List<String> intStringList = new ArrayList<>(intList.size());

            for (int i : this.intList) {
                intStringList.add(String.valueOf(i));
            }

            return Collections.unmodifiableList(intStringList);
        }

        public Set<String> getStringDupeSet() {
            Set<String> dupeStringSet = new TreeSet<>();

            for (int i : this.dupeSet) {
                dupeStringSet.add(String.valueOf(i));
            }

            return Collections.unmodifiableSet(dupeStringSet);
        }
    }
}