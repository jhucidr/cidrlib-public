/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author David Newcomer
 */
public class UtilsBetweenTest {

    public UtilsBetweenTest() {
    }

    @Test
    public void testBetweenNoStart() {
        assertEquals("", Utils.between("HelloWorld", "HELLO", "orld"));
    }

    @Test
    public void testBetweenTwoStarts() {
        assertEquals("Hello", Utils.between("HelloHelloWorld", "Hello", "World"));
    }

    @Test
    public void testBetweenTwoStops() {
        assertEquals("w", Utils.between("HellowWorldWorld", "Hello", "World"));
    }

    @Test
    public void testBetweenNoStop() {
        assertEquals("", Utils.between("HelloWorld", "Hello", "GoodBye"));

    }

    @Test
    public void testBetweenStopBeforeStart() {
        assertEquals("", Utils.between("HelloWorld", "World", "Hello"));
    }

    @Test
    public void testBetween() {
        assertEquals("loWo", Utils.between("HelloWorld", "Hel", "rld"));
    }
    @Test
    public void testBetweenEmptyStrings() {
        assertEquals("", Utils.between("HelloWorld", "", ""));
    }
        
}