package edu.jhmi.cidr.lib;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author David Newcomer
 * Sep 30, 2013
 */
public class UtilsBetweenIgnoreCaseTest {
  
    public UtilsBetweenIgnoreCaseTest() {
    }

    @Test
    public void testBetweenIgnoreCaseNoStart() {
        assertEquals("Wor", Utils.betweenIgnoreCase("HelloWorld", "HELLO", "LD"));
    }

    @Test
    public void testBetweenIgnoreCaseTwoStarts() {
        assertEquals("Hello", Utils.betweenIgnoreCase("HelloHelloWorld", "HELLO", "WORLD"));
    }

    @Test
    public void testBetweenIgnoreCaseTwoStops() {
        assertEquals("w", Utils.betweenIgnoreCase("HellowWorldWorld", "HeLLo", "World"));
    }

    @Test
    public void testBetweenIgnoreCaseNoStop() {
        assertEquals("", Utils.betweenIgnoreCase("HelloWorld", "HELLO", "GoodBye"));

    }

    @Test
    public void testBetweenIgnoreCaseStopBeforeStart() {
        assertEquals("", Utils.betweenIgnoreCase("HelloWorld", "WORLD", "Hello"));
    }

    @Test
    public void testBetweenIgnoreCase() {
        assertEquals("loWo", Utils.betweenIgnoreCase("HelloWorld", "HEL", "rld"));
    }
    @Test
    public void testBetweenIgnoreCaseEmptyStrings() {
        assertEquals("", Utils.betweenIgnoreCase("HelloWorld", "", ""));
    }
    
   
}
