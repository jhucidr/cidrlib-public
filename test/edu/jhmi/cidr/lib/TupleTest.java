/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib;

import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author asanch15
 */
public class TupleTest {
    
    private static Random rand;
    
    public TupleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        rand = new Random(System.nanoTime());
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of get1 method, of class Tuple.
     */
    @Test
    public void testGet1() {
        int tupleAVal = rand.nextInt();
        int tupleBVal = rand.nextInt();
        Tuple<Integer, Integer> instance = new Tuple<>(tupleAVal, tupleBVal);
        
        System.out.println("get1");
        
        assertEquals(tupleAVal, (int)instance.get1());
    }

    /**
     * Test of get2 method, of class Tuple.
     */
    @Test
    public void testGet2() {
        int tupleAVal = rand.nextInt();
        int tupleBVal = rand.nextInt();
        Tuple<Integer, Integer> instance = new Tuple<>(tupleAVal, tupleBVal);
        
        System.out.println("get2");
        
        assertEquals(tupleBVal, (int)instance.get2());
    }

    /**
     * Test of hashCode method, of class Tuple.
     */
    @Test
    public void testHashCode() {
        int tupleAVal   = rand.nextInt(10);
        int tupleBVal   = rand.nextInt(10) + 10;
        Tuple<Integer, Integer> instance1 = new Tuple<>(tupleAVal, tupleBVal);
        Tuple<Integer, Integer> instance2 = new Tuple<>(tupleAVal, tupleBVal);
        
        System.out.println("hashCode");
        
        assertEquals(instance1.hashCode(), instance2.hashCode());
    }

    /**
     * Test of equals method, of class Tuple.
     */
    @Test
    public void testEquals() {
        int tupleAVal   = rand.nextInt(10);
        int tupleBVal   = rand.nextInt(10) + 10;
        int tupleAVal2  = rand.nextInt(10) + 20;
        int tupleBVal2  = rand.nextInt(10) + 30;
        
        Tuple<Integer, Integer> instance1 = new Tuple<>(tupleAVal, tupleBVal);
        Tuple<Integer, Integer> instance2 = new Tuple<>(tupleAVal, tupleBVal);
        Tuple<Integer, Integer> swapInstance = new Tuple<>(tupleBVal, tupleAVal);
        Tuple<Integer, Integer> diffInstance = new Tuple<>(tupleAVal2, tupleBVal2);
        
        System.out.println("equals");
        
        assertTrue(instance1.equals(instance1));
        assertTrue(instance1.equals(instance2));
        
        assertFalse(instance1.equals(swapInstance));
        assertFalse(instance2.equals(swapInstance));
        
        assertFalse(instance1.equals(diffInstance));
        assertFalse(instance2.equals(diffInstance));
    }
}