package edu.jhmi.cidr.lib.file.content;

import edu.jhmi.cidr.lib.Displayable;
import edu.jhmi.cidr.lib.Utils;
import edu.jhmi.cidr.lib.file.FileUtils;
import edu.jhmi.cidr.lib.file.WritableFile;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import static edu.jhmi.cidr.lib.Utils.*;
import edu.jhmi.cidr.lib.file.content.AbstractFileLineParser.FileLineParseError;
import static edu.jhmi.cidr.lib.file.content.FileParserTest.Column.*;

/**
 * TODO test other delimiters TODO test starting header line on a different line
 *
 * @author dleary
 */
public class FileParserTest {

    private static final String DELIMITER = ",";
    private final String[][] FILE_DATA = new String[][]{
        {"00", "01", "02", "03"},
        {"10", "11", "12", "13"},
        {"20", "21", "22", "23"},
        {"30", "31", "32", "33"}};

    public static void main(String[] args) throws Exception {
//        new FileParserTest().testFileReadOneRowAllValidColumnsPresent();
    }

    public FileParserTest() {
    }

    @Test
    public void testFileRead_Empty() throws Exception {
        System.out.println("FileParserTest.testFileRead_Empty");
        File empty = writeToFile("");
        FileParser<Column> parser = parseFile(empty, new ArrayList<Column>());
        TypicalFileContent<Column> content = parser.getFileContent();
        assertTrue(parser.getParseErrors().isEmpty());
        assertTrue(content.getRecords().isEmpty());
    }

    @Test
    public void testFileRead_SingleLine_HeaderRowOnly() throws Exception {
        System.out.println("FileParserTest.testFileRead_SingleLine_HeaderRowOnly");
        File file = writeToFile("Column1,Column2,Column3 With Spaces,Column4");
        FileParser<Column> parser = parseFile(file, new ArrayList<Column>());
        TypicalFileContent<Column> content = parser.getFileContent();
        assertTrue(parser.getParseErrors().isEmpty());
        assertTrue(content.getRecords().isEmpty());
    }

    @Test
    public void testFileRead_AllValidColumnsPresent() throws Exception {
        System.out.println("FileParserTest.testFileRead_AllValidColumnsPresent");
        String fileString = join("\n",
                join(DELIMITER, ONE, TWO, THREE, FOUR),
                join(DELIMITER, FILE_DATA[0][0], FILE_DATA[0][1], FILE_DATA[0][2], FILE_DATA[0][3]),
                join(DELIMITER, FILE_DATA[1][0], FILE_DATA[1][1], FILE_DATA[1][2], FILE_DATA[1][3]),
                join(DELIMITER, FILE_DATA[2][0], FILE_DATA[2][1], FILE_DATA[2][2], FILE_DATA[2][3]),
                join(DELIMITER, FILE_DATA[3][0], FILE_DATA[3][1], FILE_DATA[3][2], FILE_DATA[3][3]));
        File file = writeToFile(fileString);
        FileParser<Column> parser = parseFile(file, new ArrayList<Column>());
        TypicalFileContent<Column> content = parser.getFileContent();
        assertTrue(parser.getParseErrors().isEmpty());
        assertEquals(4, content.getRecords().size());
        List<FileRecord<Column>> records = content.getRecords();
        for (int i = 0; i < records.size(); i++) {
            FileRecord<Column> record = records.get(i);
            for (int j = 0; j < ALL_COLUMNS.size(); j++) {
                Column col = ALL_COLUMNS.get(j);
                String parsedVal = record.getValue(col);
                String expectedVal = FILE_DATA[i][j];
                assertEquals(expectedVal, parsedVal);
            }

        }
    }

    @Test
    public void testFileRead_EmptyStringsOnDiagonal() throws Exception {
        System.out.println("FileParserTest.testFileRead_EmptyStringsOnDiagonal");
        String fileString = join("\n",
                join(DELIMITER, ONE, TWO, THREE, FOUR),
                join(DELIMITER, "", FILE_DATA[0][1], FILE_DATA[0][2], FILE_DATA[0][3]),
                join(DELIMITER, FILE_DATA[1][0], "", FILE_DATA[1][2], FILE_DATA[1][3]),
                join(DELIMITER, FILE_DATA[2][0], FILE_DATA[2][1], "", FILE_DATA[2][3]),
                join(DELIMITER, FILE_DATA[3][0], FILE_DATA[3][1], FILE_DATA[3][2], ""));
        File file = writeToFile(fileString);
        FileParser<Column> parser = parseFile(file, new ArrayList<Column>());
        TypicalFileContent<Column> content = parser.getFileContent();
        assertTrue(parser.getParseErrors().isEmpty());
        assertEquals(4, content.getRecords().size());
        List<FileRecord<Column>> records = content.getRecords();
        for (int i = 0; i < records.size(); i++) {
            FileRecord<Column> record = records.get(i);
            for (int j = 0; j < ALL_COLUMNS.size(); j++) {
                Column col = ALL_COLUMNS.get(j);
                String parsedVal = record.getValue(col);
                String expectedVal = i == j ? "" : FILE_DATA[i][j];
                assertEquals(expectedVal, parsedVal);
            }

        }
    }

    @Test
    public void testFileRead_EmptyLinesUnderHeader() throws Exception {
        System.out.println("FileParserTest.testFileRead_EmptyLinesUnderHeader");
        String fileString = join("\n",
                join(DELIMITER, ONE, TWO, THREE, FOUR),
                "",
                "",
                "",
                "");
        File file = writeToFile(fileString);
        FileParser<Column> parser = parseFile(file, new ArrayList<Column>());
        TypicalFileContent<Column> content = parser.getFileContent();
        assertTrue(parser.getParseErrors().isEmpty());
        assertTrue(content.getRecords().isEmpty());
    }

    @Test
    public void testFileRead_UnrecognizedColumn() throws Exception {
        System.out.println("FileParserTest.testFileRead_UnrecognizedColumn");
        String fileString = join("\n",
                join(DELIMITER, ONE, TWO, THREE, FOUR, "unknown"));
        File file = writeToFile(fileString);
        FileParser<Column> parser = parseFile(file, new ArrayList<Column>());
        assertFalse(parser.getParseErrors().isEmpty());
        for (FileLineParseError error : parser.getParseErrors()) {
            assertTrue(error instanceof FileLineParseError.UnrecognizedColumn);
        }
    }
    
    
    
    @Test
    public void testFileRead_UnrecognizedColumnWithContent()  throws Exception {
        System.out.println("FileParserTest.testFileRead_UnrecognizedColumnWithContent");
        String fileString = join("\n",
                join(DELIMITER, "won", TWO, THREE, FOUR),
                join(DELIMITER, "1","2","3","4"));
        File file = writeToFile(fileString);
        FileParser<Column> parser = parseFile(file, new ArrayList<Column>());
        assertFalse(parser.getParseErrors().isEmpty());
        for (FileLineParseError error : parser.getParseErrors()) {
            System.out.println(error.getClass());
            assertTrue(error instanceof FileLineParseError.UnrecognizedColumn);
        }
        TypicalFileContent<Column> content = parser.getFileContent();
        assertEquals(1, content.getRecords().size());
        for (FileRecord<Column> record : content.getRecords()) {
            assertArrayEquals(new Column[]{TWO, THREE, FOUR}, record.getColumns().toArray());
            assertNull(record.getValue(ONE));
            assertEquals("2", record.getValue(TWO));
            assertEquals("3", record.getValue(THREE));
            assertEquals("4", record.getValue(FOUR));
        }
    }
    
    @Test
    public void testFileRead_MissingRequiredColumn() throws Exception {
        System.out.println("FileParserTest.testFileRead_MissingRequiredColumn");
        String fileString = join("\n",
                join(DELIMITER, TWO, FOUR));
        File file = writeToFile(fileString);
        FileParser<Column> parser = parseFile(file, Utils.asList(ONE, THREE));
        assertFalse(parser.getParseErrors().isEmpty());
        for (FileLineParseError error : parser.getParseErrors()) {
            assertTrue(error instanceof FileLineParseError.MissingColumn);
        }
    }
    
    @Test
    public void testFileRead_UnexpectedValues() throws Exception {
        System.out.println("FileParserTest.testFileRead_UnexpectedValues");
        String fileString = join("\n",
                join(DELIMITER, ONE, TWO, THREE, FOUR),
                join(DELIMITER, "one", "two", "three", "four", "five"));
        File file = writeToFile(fileString);
        FileParser<Column> parser = parseFile(file, new ArrayList<Column>());
        assertFalse(parser.getParseErrors().isEmpty());
        for (FileLineParseError error : parser.getParseErrors()) {
            assertTrue(error instanceof FileLineParseError.UnexpectedValues);
        }
    }

    @Test
    public void testFileRead_DelimitersOnly() throws Exception {
        System.out.println("FileParserTest.testFileRead_EmptyLinesUnderHeader");
        String fileString = join("\n",
                join(DELIMITER, ONE, TWO, THREE, FOUR),
                ",,,,,,,,,,,,,,,,,,",
                ",,,,,,",
                ",,,,,,,,,,",
                ",,");
        File file = writeToFile(fileString);
        FileParser<Column> parser = parseFile(file, new ArrayList<Column>());
        TypicalFileContent<Column> content = parser.getFileContent();
        assertTrue(parser.getParseErrors().isEmpty());
        assertTrue(content.getRecords().isEmpty());
    }

//    @Test
//    public void testProblemHandlingFile() throws Exception {
//        System.out.println("FileParserTest.testSingleLineHeaderRowFileRead");
//        File file = writeToFile("a,Column2\n"
//                + "S103_100009-1-1020526940\n"
//                + "invalid-object,\n"
//                + "S103_100089-1-1020526998");
//        FileParser<Column> parser = parseFile(file, new ArrayList<Column>());
//        TypicalFileContent<Column> content = parser.getFileContent();
//        System.out.println(content.getDisplayString());
//        System.out.println(Utils.display(parser.getParseErrors()));
//    }
    /**
     * Not using an enum here so to demonstrate that this usage can work just as
     * well.
     */
    /*package access*/ static class Column implements Displayable, FileColumn {

        public static final Column1 ONE = new Column1();
        public static final Column2 TWO = new Column2();
        public static final Column3WithSpaces THREE = new Column3WithSpaces();
        public static final Column4 FOUR = new Column4();
        public static final List<Column> ALL_COLUMNS = Utils.asList(ONE, TWO, THREE, FOUR);

        @Override
        public String getColumnName() {
            return this.getClass().getSimpleName();
        }

        @Override
        public String getDisplayString() {
            return getColumnName();
        }
    }

    private static class Column1 extends Column {
    }

    private static class Column2 extends Column {
    }

    private static class Column3WithSpaces extends Column {

        @Override
        public String getColumnName() {
            return "Column3 With Spaces";
        }
    }

    private static class Column4 extends Column {
    }

    private static File writeToFile(String s) throws Exception {
        WritableFile file = new WritableFile(FileUtils.getNewTempFile());
        file.write(s);
        file.flushAndClose();
        return file.getFile();
    }

    private static FileParser<Column> parseFile(File f, List<Column> requiredColumns) throws Exception {
        FileParser<Column> parser = new FileParser<>(DELIMITER, Column.ALL_COLUMNS, requiredColumns);
        parser.parse(f);
        return parser;
    }
}