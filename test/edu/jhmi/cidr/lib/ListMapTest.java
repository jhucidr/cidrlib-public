/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.jhmi.cidr.lib;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author asanch15
 */
public class ListMapTest {

    private static Map<Integer, List<Integer>> testMap;
    private static Random rand;

    public ListMapTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        rand = new Random(System.nanoTime());
        testMap = createRandomMap();
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private static Map<Integer, List<Integer>> createRandomMap() {
        Map<Integer, List<Integer>> map = new HashMap<>();

        for (int i = 0; i < Math.abs(rand.nextInt(10)) + 1; i++) {
            ArrayList<Integer> intList = new ArrayList<>();

            for (int j = 0; j < Math.abs(rand.nextInt(10)) + 1; j++) {
                intList.add(rand.nextInt(100));
            }

            map.put(i, intList);
        }
        
        return Collections.unmodifiableMap(map);
    }

    private static ListMap<Integer, Integer> setupListMap(Map<Integer, List<Integer>> map) {
        ListMap<Integer, Integer> instance = new ListMap<>();

        for (Map.Entry<Integer, List<Integer>> en : map.entrySet()) {

            for (Integer i : en.getValue()) {
                instance.put(en.getKey(), i);
            }
        }

        return instance;
    }

    /**
     * Test of put method, of class ListMap.
     */
    @Test
    public void testPut() {
        ListMap<Integer, Integer> instance =
                setupListMap(testMap);

        System.out.println("put");

        System.out.println("testMap = " + testMap);
        System.out.println("instance.getMap() = " + instance.getMap());

        assertEquals(instance.getMap(), testMap);
    }

    /**
     * Test of putAll method, of class ListMap.
     */
    @Test
    public void testPutAll_GenericType_Iterable() {
        ListMap<Integer, Integer> instance =
                new ListMap<>();

        System.out.println("putAll");

        for (Map.Entry<Integer, List<Integer>> entry : testMap.entrySet()) {
            instance.putAll(entry.getKey(), entry.getValue());
        }

        System.out.println("testMap = " + testMap);
        System.out.println("instance.getMap() = " + instance.getMap());

        assertEquals(instance.getMap(), testMap);
    }

    /**
     * Test of putAll method, of class ListMap.
     */
    @Test
    public void testPutAll_ListMap() {
        ListMap<Integer, Integer> instance = new ListMap<>();

        System.out.println("putAll_ListMap");

        instance.putAll(setupListMap(testMap));

        System.out.println("testMap = " + testMap);
        System.out.println("instance.getMap() = " + instance.getMap());

        assertEquals(instance.getMap(), testMap);
    }

    /**
     * Test of union method, of class ListMap.
     */
    @Test
    public void testUnion() {
        Map<Integer, List<Integer>> randMap     = createRandomMap();
        Map<Integer, List<Integer>> unionMap    = new HashMap<>(testMap);
        ListMap<Integer, Integer> unionListMap  = 
                ListMap.union(setupListMap(testMap), setupListMap(randMap));
        
        System.out.println("testUnion");
        
        for (Map.Entry<Integer, List<Integer>> entry : randMap.entrySet()) {
            Integer key = entry.getKey();
            List<Integer> value = entry.getValue();
            
            if(!unionMap.containsKey(key)){
                unionMap.put(key, new ArrayList<Integer>());
            }
            
            for (Integer i : value) {
                unionMap.get(key).add(i);
            }
        }
        
        System.out.println("unionMap = " + unionMap);
        System.out.println("unionListMap.getMap() = " + unionListMap.getMap());
        
        assertEquals(unionListMap.getMap(), unionMap);
    }

    /**
     * Test of remove method, of class ListMap.
     */
    @Test
    public void testRemove() {
        ListMap<Integer, Integer> instance = setupListMap(testMap);
        int removeValueKey      = Math.abs(rand.nextInt(testMap.size()));
        int removeValueIndex    = 
                Math.abs(rand.nextInt(testMap.get(removeValueKey).size()));
        Integer removeValue         = testMap.get(removeValueKey).get(removeValueIndex);
        Map<Integer, List<Integer>> removeMap   = new HashMap<>(testMap);
        
        System.out.println("remove");
        
        System.out.println("removeValueKey = " + removeValueKey);
        System.out.println("removeValue = " + removeValue);
        removeMap.get(removeValueKey).remove(removeValue);
        instance.remove(removeValueKey, removeValue);
        
        System.out.println("removeMap = " + removeMap);
        System.out.println("instance.getMap() = " + instance.getMap());
        
        assertEquals(instance.getMap(), removeMap);
    }

    /**
     * Test of removeAll method, of class ListMap.
     */
    @Ignore
    @Test
    public void testRemoveAll() {
        System.out.println("removeAll");
        Object k = null;
        ListMap instance = new ListMap();
        List expResult = null;
        List result = instance.removeAll(k);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of get method, of class ListMap.
     */
    @Ignore
    @Test
    public void testGet() {
        System.out.println("get");
        Object k = null;
        ListMap instance = new ListMap();
        List expResult = null;
        List result = instance.get(k);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getKeys method, of class ListMap.
     */
    @Ignore
    @Test
    public void testGetKeys() {
        System.out.println("getKeys");
        Object v = null;
        ListMap instance = new ListMap();
        Set expResult = null;
        Set result = instance.getKeys(v);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of keySet method, of class ListMap.
     */
    @Ignore
    @Test
    public void testKeySet() {
        System.out.println("keySet");
        ListMap instance = new ListMap();
        Set expResult = null;
        Set result = instance.keySet();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of values method, of class ListMap.
     */
    @Ignore
    @Test
    public void testValues() {
        System.out.println("values");
        ListMap instance = new ListMap();
        List expResult = null;
        List result = instance.values();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMap method, of class ListMap.
     */
    @Ignore
    @Test
    public void testGetMap() {
        System.out.println("getMap");
        ListMap instance = new ListMap();
        Map expResult = null;
        Map result = instance.getMap();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of filter method, of class ListMap.
     */
    @Ignore
    @Test
    public void testFilter() {
        System.out.println("filter");
        ListMap instance = new ListMap();
        ListMap expResult = null;
        ListMap result = instance.filter(null);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of filterVal method, of class ListMap.
     */
    @Ignore
    @Test
    public void testFilterVal_Function() {
        System.out.println("filterVal");
        ListMap instance = new ListMap();
        ListMap expResult = null;
        ListMap result = instance.filterVal(null);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of filterVal method, of class ListMap.
     */
    @Ignore
    @Test
    public void testFilterVal_Function_GenericType() {
        System.out.println("filterVal");
        ListMap instance = new ListMap();
        ListMap expResult = null;
        ListMap result = instance.filterVal(null);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of map method, of class ListMap.
     */
    @Ignore
    @Test
    public void testMap() {
        System.out.println("map");
        ListMap instance = new ListMap();
        ListMap expResult = null;
        ListMap result = instance.map(null);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of reduce method, of class ListMap.
     */
    @Ignore
    @Test
    public void testReduce() {
        System.out.println("reduce");
        ListMap instance = new ListMap();
        List expResult = null;
        List result = instance.reduce(null);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of filterKeys method, of class ListMap.
     */
    @Ignore
    @Test
    public void testFilterKeys() {
        System.out.println("filterKeys");
        ListMap instance = new ListMap();
        ListMap expResult = null;
        ListMap result = instance.filterKeys(null);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of iterator method, of class ListMap.
     */
    @Ignore
    @Test
    public void testIterator() {
        System.out.println("iterator");
        ListMap instance = new ListMap();
        Iterator expResult = null;
        Iterator result = instance.iterator();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findDuplicates method, of class ListMap.
     */
    @Ignore
    @Test
    public void testFindDuplicates_0args() {
        System.out.println("findDuplicates");
        ListMap instance = new ListMap();
        Set expResult = null;
        Set result = instance.findDuplicates();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findDuplicates method, of class ListMap.
     */
    @Ignore
    @Test
    public void testFindDuplicates_GenericType() {
        System.out.println("findDuplicates");
        Object k = null;
        ListMap instance = new ListMap();
        Set expResult = null;
        Set result = instance.findDuplicates(k);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of toString method, of class ListMap.
     */
    @Ignore
    @Test
    public void testToString() {
        System.out.println("toString");
        ListMap instance = new ListMap();
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}