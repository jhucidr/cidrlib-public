package edu.jhmi.cidr.lib.math;

import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dleary
 */
public class SignumTest {

    public SignumTest() {
    }

    /**
     * Test of getSign method, of class Signum.
     */
    @Test
    public void testGetSign_long_long() {
        System.out.println("testGetSign_long_long");

        assertEquals(0, Signum.getSign(0, 0));
        assertEquals(1, Signum.getSign(1, 0));
        assertEquals(-1, Signum.getSign(0, 1));
        assertEquals(1, Signum.getSign(Long.MAX_VALUE, 0));
        assertEquals(-1, Signum.getSign(0, Long.MAX_VALUE));
        assertEquals(-1, Signum.getSign(Long.MIN_VALUE, 0));
        assertEquals(1, Signum.getSign(0, Long.MIN_VALUE));
    }

    /**
     * Test of getSign method, of class Signum.
     */
    @Test
    public void testGetSign_Date_Date() {
        System.out.println("testGetSign_Date_Date");

        assertEquals(0, Signum.getSign(new Date(), new Date()));
        assertEquals(1, Signum.getSign(new Date(1), new Date(0)));
        assertEquals(-1, Signum.getSign(new Date(0), new Date(1)));
        assertEquals(-1, Signum.getSign(new Date(-1), new Date(0)));
        assertEquals(1, Signum.getSign(new Date(0), new Date(-1)));
    }

    /**
     * Test of getSign method, of class Signum.
     */
    @Test
    public void testGetSign_double_double() {
        System.out.println("testGetSign_double_double");
        assertEquals(0, Signum.getSign(0d, 0d));
        assertEquals(1, Signum.getSign(1d, 0d));
        assertEquals(-1, Signum.getSign(0d, 1d));
        assertEquals(1, Signum.getSign(Double.MAX_VALUE, Double.MIN_VALUE));
        assertEquals(-1, Signum.getSign(Double.MIN_VALUE, Double.MAX_VALUE));
        assertEquals(1, Signum.getSign(Double.POSITIVE_INFINITY, Double.MIN_VALUE));
        assertEquals(1, Signum.getSign(Double.POSITIVE_INFINITY, 0d));
        assertEquals(1, Signum.getSign(Double.POSITIVE_INFINITY, Double.MAX_VALUE));
        assertEquals(-1, Signum.getSign(Double.NEGATIVE_INFINITY, Double.MAX_VALUE));
        assertEquals(-1, Signum.getSign(Double.NEGATIVE_INFINITY, 0d));
        assertEquals(-1, Signum.getSign(Double.NEGATIVE_INFINITY, Double.MIN_VALUE));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetSign_double_double_NaN_Left() {
        Signum.getSign(Double.NaN, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetSign_double_double_NaN_Right() {
        Signum.getSign(0, Double.NaN);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetSign_double_double_NaN_Both() {
        Signum.getSign(Double.NaN, Double.NaN);
    }

    /**
     * Test of getSign method, of class Signum.
     */
    @Test
    public void testGetSign_long() {
        System.out.println("testGetSign_long");
        assertEquals(0, Signum.getSign(0L));
        assertEquals(1, Signum.getSign(1L));
        assertEquals(1, Signum.getSign(Long.MAX_VALUE));
        assertEquals(-1, Signum.getSign(-1L));
        assertEquals(-1, Signum.getSign(Long.MIN_VALUE));
    }

    /**
     * Test of getSign method, of class Signum.
     */
    @Test
    public void testGetSign_double() {
        System.out.println("testGetSign_double");
        assertEquals(0, Signum.getSign(0.0d));
        assertEquals(1, Signum.getSign(1.0d));
        assertEquals(1, Signum.getSign(Double.MAX_VALUE));
        assertEquals(1, Signum.getSign(Double.POSITIVE_INFINITY));
        assertEquals(-1, Signum.getSign(-1.0d));
        assertEquals(1, Signum.getSign(Double.MIN_VALUE));
        assertEquals(-1, Signum.getSign(-1.0d * Double.MIN_VALUE));
        assertEquals(-1, Signum.getSign(Double.NEGATIVE_INFINITY));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGetSign_double_NaN() {
        Signum.getSign(Double.NaN);
    }

    /**
     * Test of getSign method, of class Signum.
     */
    @Test
    public void testGetSign_String_String() {
        System.out.println("testGetSign_String_String");
        assertEquals(0, Signum.getSign((String)null, (String)null));
        assertEquals(0, Signum.getSign("hi", "hi"));
        assertEquals(0, Signum.getSign("Hi", "hi"));
        assertEquals(0, Signum.getSign("hi", "Hi"));
        assertEquals(1, Signum.getSign("Ii", "hi"));
        assertEquals(-1, Signum.getSign("hi", "Ii"));
        assertEquals(1, Signum.getSign(null, "hi"));
        assertEquals(-1, Signum.getSign("hi", null));
    }
}